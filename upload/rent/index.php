<?php
require('path.inc.php');

$page->addCss('rent.css');

$page->name = 'index'; //页面名字,和文件名相同	
$page->title = $page->city.'租房 - '.$page->titlec;   //网站名称

//关键词和描述
$page->keyword = $page->rent_keyword;
$page->description = $page->rent_description;

//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);
$house_type_option = Dd::getArray('house_type');
$page->tpl->assign('house_type_option', $house_type_option);

$house_price_option = array(
	'0-600'=>'600元以下',
	'600-800'=>'600-800元',
	'800-1000'=>'800-1000元',
	'1000-1200'=>'1000-1200元',
	'1200-1500'=>'1200-1500元',
	'1500-2000'=>'1500-2000元',
	'2000-3000'=>'2000-3000元',
	'3000-4000'=>'3000-4000元',
	'4000-5000'=>'4000-5000元',
	'5000-0'=>'5000元以上'
);
$page->tpl->assign('house_price_option', $house_price_option);

$house_totalarea_option = array(
	'0-50'=>'50㎡以下',
	'50-70'=>'50-70㎡',
	'70-90'=>'70-90㎡',
	'90-110'=>'90-110㎡',
	'110-130'=>'110-130㎡',
	'130-150'=>'130-150㎡',
	'150-200'=>'150-200㎡',
	'200-250'=>'200-250㎡',
	'250-300'=>'250-300㎡',
	'300-500'=>'300-500㎡',
	'500-0'=>'500㎡以上'
);
$page->tpl->assign('house_totalarea_option', $house_totalarea_option);

$house_age_option = array(
	'0-1995'=>'1995年前',
	'1995-2000'=>'1995-2000年',
	'2000-2008'=>'2000-2008年',
	'2008-0'=>'2008年后'
);
$page->tpl->assign('house_age_option', $house_age_option);

$house_floor_option = array(
	'0-6'=>'6层以下',
	'6-12'=>'6-12层',
	'12-0'=>'12层以上'
);
$page->tpl->assign('house_floor_option', $house_floor_option);
//特色
$house_feature_option = Dd::getArray('rent_feature');
$page->tpl->assign('house_feature_option', $house_feature_option);

$where =' and status=1 ';

//cityarea
$cityarea = intval($_GET['cityarea']);
$page->tpl->assign('cityarea', $cityarea);
if($cityarea){
	$where .= ' and cityarea_id = '.$cityarea;
	$page->title = $cityarea_option[$cityarea].",".$page->title;
}

$dd = new Dd($query);
$borough_section=$dd->getSonList($cityarea);
$page->tpl->assign('borough_section',$borough_section);

//cityarea2
$cityarea2 = intval($_GET['cityarea2']);
$page->tpl->assign('cityarea2', $cityarea2);
if($cityarea2){
	$where .= ' and cityarea2_id = '.$cityarea2;
}

//选择标签
$switch = $_GET['switch'];
$page->tpl->assign('switch', $switch);
if($switch == "owner"){
	$where .= " and broker_id = 0 ";
	$page->title = "房东房源 - ".$page->title;
}
if($switch == "promote"){
	$where .=" and is_promote =1 ";
	$page->title = "店长推荐房源 - ".$page->title;
}
if($switch == "morePic"){
	$where .=" and is_more_pic =1";
	$page->title = "多图房源 -".$page->title;
}
//type
$type = intval($_GET['type']);
$page->tpl->assign('type', $type);
if($type){
	$where .= ' and house_type = '.$type;
	$page->title = $house_type_option[$type].",".$page->title;
}
//feature
$feature = intval($_GET['feature']);
$page->tpl->assign('feature', $feature);
if($feature){
	$where .= " and house_feature like '%,".$feature.",%'";
	$page->title = $house_feature_option[$feature].",".$page->title;
}
//price
$price = $_GET['price'];
$page->tpl->assign('price', $price);
if($price){
	$tmp = explode('-',$price);
	if($tmp[0]){
		$where .= ' and house_price >= '.intval($tmp[0]);
	}
	if($tmp[1]){
		$where .= ' and house_price <= '.intval($tmp[1]);
	}
	$page->title = $house_price_option[$price].",".$page->title;
}
	$room_option = array(1=>'一室',2=>'二室',3=>'三室',4=>'四室',5=>'五室',6=>'五室以上');
	$page->tpl->assign('room_option', $room_option);
//room
$room = intval($_GET['room']);
$page->tpl->assign('room', $room);
if($room){
	$where .= ' and house_room = '.$room;

	$page->title = $room_option[$room].",".$page->title;
}
//house_age

$house_age = $_GET['house_age'];
$page->tpl->assign('house_age', $house_age);
if($house_age){
	$tmp = explode('-',$house_age);
	if($tmp[0]){
		$where .= ' and house_age >= '.intval($tmp[0]);
	}
	if($tmp[1]){
		$where .= ' and house_age <= '.intval($tmp[1]);
	}
	
}
//house_floor
$house_floor = $_GET['house_floor'];
$page->tpl->assign('house_floor', $house_floor);
if($house_floor){
	$tmp = explode('-',$house_floor);
	if($tmp[0]){
		$where .= ' and house_floor >= '.intval($tmp[0]);
	}
	if($tmp[1]){
		$where .= ' and house_floor <= '.intval($tmp[1]);
	}
	
}
//q
$q = $_GET['q']=="可输入小区名、路名或划片学校" ? "":trim($_GET['q']);
if($q){
	// 小区名、路名或划片学校
	$where_borough ="borough_name like '%".$q."%' or borough_address like '%".$q."%' or elementary_school like '%".$q."%' or middle_school like '%".$q."%'" ;
	$borough = new Borough($query);
	$borough_ids = $borough->getAll('id',$where_borough);
	if($borough_ids){
		$borough_ids = implode(',',$borough_ids);
		$where .=" and ( borough_id in (".$borough_ids.") or borough_name like '%".$q."%' )";
	}else{
		//没有搜索到
		$where= ' and 0 ';
	}
	$page->title = $q.",".$page->title;
}
//totalarea
$totalarea = $_GET['totalarea'];
$page->tpl->assign('totalarea', $totalarea);
if($totalarea){
	$tmp = explode('-',$totalarea);
	if($tmp[0]){
		$where .= ' and house_totalarea >= '.intval($tmp[0]);
	}
	if($tmp[1]){
		$where .= ' and house_totalarea <= '.intval($tmp[1]);
	}
	$page->title = $house_totalarea_option[$totalarea].",".$page->title;
}
//list_order 排序转换
switch ($_GET['list_order']){
	case "created desc":
		$list_order = " order by is_top desc,updated desc";
		break;
	case "house_price asc":
		$list_order = " order by is_top desc,house_price asc";
		break;
	case "house_price desc":
		$list_order = " order by is_top desc,house_price desc";
		break;
	case "house_totalarea asc":
		$list_order = " order by is_top desc,house_totalarea asc";
		break;
	case "house_totalarea desc":
		$list_order = " order by is_top desc,house_totalarea desc";
		break;
	default:
		$list_order = " order by is_top desc,update_order desc";
		break;
}

/*print_rr($where);*/
//list_num
$list_num = intval($_GET['list_num']);
if(!$list_num){
	$list_num = 10;
}

$house = new HouseRent($query);

require($cfg['path']['lib'] . 'classes/Pages.class.php');
$row_count = $house->getCount(1,$where);
//$row_count = $house->getCount(3,$where);
$pages = new Pages($row_count,$list_num);

//page
$pageno = $_GET['pageno']?intval($_GET['pageno']):1;
$pre_page = $pageno>1?$pageno-1:1;
$next_page = $pageno<$pages->pageCount?$pageno+1:$page_count;
$page->tpl->assign('pageno', $pageno);
$page->tpl->assign('row_count', $row_count);
$page->tpl->assign('page_count', $pages->pageCount);
$page->tpl->assign('pre_page', $pages->fileName.'pageno='.$pre_page);
$page->tpl->assign('next_page', $pages->fileName.'pageno='.$next_page);

$pageLimit = $pages->getLimit();
$dataList = $house->getList($pageLimit,'*',1,$where,$list_order);
//$dataList = $house->getList($pageLimit,'*',3,$where,$list_order);
$member = new Member($query);
//积分配置文件
$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');

foreach ($dataList as $key=> $item){
	//图片数量
	$dataList[$key]['pic_num'] = $house->getImgNum($item['id']);
	/*	
	if($dataList[$key]['pic_num']>0 && trim($item['house_thumb'])==""){
		//插入第一条数据
		$house_img = $house->getImgList($item['id'],'*');
		$house->update($item['id'],'house_thumb',$house_img[0]['pic_url']);
	}
	*/
	$dataLis[$key]['updated'] = time2Units(time()-$item['updated']);
	//经纪人信息
	
		$dataList[$key]['broker_info'] = $member->getInfo($item['broker_id'],'*',true);
		$dataList[$key]['broker_info']['outlet'] = substr($dataList[$key]['broker_info']['outlet'],0,strpos($dataList[$key]['broker_info']['outlet'],'-'));
	
		$dataList[$key]['broker_info']['brokerRank'] = getNumByScore($dataList[$key]['broker_info']['scores'],$integral_array,'pic');
		$dataList[$key]['house_fitment'] =  Dd::getCaption('house_fitment',$item['house_fitment']);
}

$page->tpl->assign('dataList', $dataList);
$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条

//执行脚本，房源置顶过期
$topHouseId = $house->getToTime();
if($topHouseId){
$house->update($topHouseId,'is_top',0);
$house->deleteTop($topHouseId);
}

//右边浏览过房源
if($_COOKIE['RRecentlyGoods']){
	$browseHouse = $house->browseHouse($_COOKIE['RRecentlyGoods']);
    $page->tpl->assign('browseList', $browseHouse);
 }



$page->show();
?>