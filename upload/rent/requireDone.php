<?php
require('path.inc.php');

$page->addCss('sale.css');


$page->name = 'requireDone'; //页面名字,和文件名相同	

$id = intval($_GET['id']);
if(!$id){
	$page->urlto('requireList.php');
}
//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

$house_price_option = array(
	'0-600'=>'600元以下',
	'600-800'=>'600-800元',
	'800-1000'=>'800-1000元',
	'1000-1200'=>'1000-1200元',
	'1200-1500'=>'1200-1500元',
	'1500-2000'=>'1500-2000元',
	'2000-3000'=>'2000-3000元',
	'3000-4000'=>'3000-4000元',
	'4000-5000'=>'4000-5000元',
	'5000-0'=>'5000元以上'
);
$page->tpl->assign('house_price_option', $house_price_option);
//详细信息
$houseWanted = new HouseWanted($query);
$dataInfo = $houseWanted->getInfo($id,'*');

if(!$dataInfo){
	$page->urlto('requireList.php','信息不存在或已删除');
}
$page->tpl->assign('dataInfo', $dataInfo);

$page->show();
?>