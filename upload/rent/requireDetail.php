<?php
require('path.inc.php');


$page->addCss('sale.css');
//id
$id = intval($_GET['id']);
if(!$id){
	$page->urlto('requireList.php');
}
$houseWanted = new HouseWanted($query);

if($page->action =="reply"){
	//回复
	$_POST['wanted_id'] = $id;
	if (md5(strtolower($_POST['vaild']))!=$_COOKIE['validString']) {
		$page->back("验证码错误");
	}
	if(!$_POST['wanted_id']){
		$page->urlto("requireList.php");
	}
	if(!$_POST['linkman']){
		$page->back("请填写联系人");
	}
	if(!$_POST['content']){
		$page->back("请填写内容");
	}
	try{
		$houseWanted->saveReply($_POST);
		$page->urlto('requireDetail.php?id='.$id);
	}catch (Exception $e){
		$page->back('出错了');
	}
	exit;
}else{
	$page->name = 'requireDetail'; //页面名字,和文件名相同
	
	$page->addJs('FormValid.js');
	
	//区域字典
	$cityarea_option = Dd::getArray('cityarea');
	$page->tpl->assign('cityarea_option', $cityarea_option);
	
	$house_price_option = array(
		'0-600'=>'600元以下',
		'600-800'=>'600-800元',
		'800-1000'=>'800-1000元',
		'1000-1200'=>'1000-1200元',
		'1200-1500'=>'1200-1500元',
		'1500-2000'=>'1500-2000元',
		'2000-3000'=>'2000-3000元',
		'3000-4000'=>'3000-4000元',
		'4000-5000'=>'4000-5000元',
		'5000-0'=>'5000元以上'
	);
	$page->tpl->assign('house_price_option', $house_price_option);
	//详细信息
	$dataInfo = $houseWanted->getInfo($id,'*');
	
	if(!$dataInfo){
		$page->urlto('requireList.php','信息不存在或已删除');
	}
	$dataInfo['requirement_short'] = substrs($dataInfo['requirement'],40);
	$page->tpl->assign('dataInfo', $dataInfo);
	//回复列表
	$replyList = $houseWanted->getReplyList($id);
	$page->tpl->assign('replyList', $replyList);
	
	$page->tpl->assign('reply_num', count($replyList));
	
	$page->show();
}
?>