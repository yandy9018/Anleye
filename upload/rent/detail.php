<?php
require('path.inc.php');
$page->addCss('sale.css');

$page->name = 'detail'; //页面名字,和文件名相同

//举报使用的thickBox加载
$page->addJs('jquery-1.2.6.min.js');
$page->addcss("thickbox.css");
$page->addjs("thickbox.js");


$member = new Member($query);
if($_COOKIE['AUTH_MEMBER_NAME']){
	$user_type = $member->getAuthInfo('user_type');
	$page->tpl->assign('user_type', $user_type);
}
//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

$house_price_option = array(
	'0-600'=>'600元以下',
	'600-800'=>'600-800元',
	'800-1000'=>'800-1000元',
	'1000-1200'=>'1000-1200元',
	'1200-1500'=>'1200-1500元',
	'1500-2000'=>'1500-2000元',
	'2000-3000'=>'2000-3000元',
	'3000-4000'=>'3000-4000元',
	'4000-5000'=>'4000-5000元',
	'5000-0'=>'5000元以上'
);
$page->tpl->assign('house_price_option', $house_price_option);

//房源特色
$house_feature_option = Dd::getArray('house_feature');
$page->tpl->assign('house_feature_option', $house_feature_option);
//id
$id = intval($_GET['id']);
if(!$id){
	$page->urlto('index.php');
}

$house = new HouseRent($query);
//详细信息
$dataInfo = $house->getInfo($id,'*');

//将浏览过的房源写入cookies
$house->cookies($id);

//房屋类型
$houseTypeLists = Dd::getArray('house_type');

if(!$dataInfo){
	$page->urlto('index.php','该房源不存在或已删除');
}
$dataInfo['house_type'] = $houseTypeLists[$dataInfo['house_type']];
$dataInfo['updated'] = time2Units(time()-$dataInfo['updated']);
$dataInfo['cityarea_name'] = $cityarea_option[$dataInfo['cityarea_id']];
$dataInfo['house_toward'] = Dd::getCaption('house_toward',$dataInfo['house_toward']);
if($dataInfo['house_feature']){
	$dataInfo['house_feature'] =  Dd::getCaption('rent_feature',$dataInfo['house_feature']);
}
$dataInfo['house_fitment'] =  Dd::getCaption('house_fitment',$dataInfo['house_fitment']);
$dataInfo['house_support'] =  Dd::getCaption('house_installation',$dataInfo['house_support']);
$dataInfo['house_deposit'] =  Dd::getCaption('rent_deposittype',$dataInfo['house_deposit']);
$page->tpl->assign('dataInfo', $dataInfo);
//图片列表
$houseImageList = $house->getImgList($id,'*');
$page->tpl->assign('houseImageList', $houseImageList);

if(!$dataInfo['house_thumb'] && $houseImageList){
//没有缩略图，把房源图片抽出一张
	$rand_key = array_rand($houseImageList);
	$dataInfo['house_thumb'] = $houseImageList[$rand_key]['pic_thumb']?$houseImageList[$rand_key]['pic_thumb']:$houseImageList[$rand_key]['pic_url'];
	$house->update($id,'house_thumb',$dataInfo['house_thumb']);
}

//经纪人详细情况

	$brokerInfo = $member->getInfo($dataInfo['broker_id'],'*',true);
	if($brokerInfo['company_id']){
		$company = new Company($query);
		$brokerInfo['company_name'] = $company->getInfo($brokerInfo['company_id'],'company_name');
			$brokerInfo['company_phone'] = $company->getInfo($brokerInfo['company_id'],'company_phone');
		}
	//积分配置文件
	$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');
	$brokerInfo['brokerRank'] = getNumByScore($brokerInfo['scores'],$integral_array,'pic');
	$page->tpl->assign('brokerInfo', $brokerInfo);



//获取搬家公司信息
$company = new Company($query);
$moveCompanyList = $company->getList(array('rowFrom'=>0,'rowTo'=>9),'*',' and status=1 and type=1','');
$page->tpl->assign('moveCompanyList', $moveCompanyList);

//获取装修公司信息
$decorationCompanyList = $company->getList(array('rowFrom'=>0,'rowTo'=>9),'*',' and status=1 and type=2','');
$page->tpl->assign('decorationCompanyList', $decorationCompanyList);


//小区
$borough = new Borough($query);

if($dataInfo['borough_id']){
	//小区详细信息
	$boroughInfo = $borough->getInfo($dataInfo['borough_id'],'*',1,true);
	$boroughInfo['cityarea_name'] = $cityarea_option[$boroughInfo['cityarea_id']];
	$boroughInfo['borough_support'] = Dd::getCaption('borough_support',$boroughInfo['borough_support']);
	$page->tpl->assign('boroughInfo', $boroughInfo);
	$boroughImageList = $borough->getImgList($dataInfo['borough_id'],0,6);
	$page->tpl->assign('boroughImageList', $boroughImageList);
	
	if(!$dataInfo['house_thumb'] && $boroughImageList){
		//没有缩略图，把小区图片抽出一张
		$rand_key = array_rand($boroughImageList);
		$dataInfo['house_thumb'] = $boroughImageList[$rand_key]['pic_thumb']?$boroughImageList[$rand_key]['pic_thumb']:$boroughImageList[$rand_key]['pic_url'];
		$house->update($id,'house_thumb',$dataInfo['house_thumb']);
	}
	
	//该小区价格相近房源
	$where = " and status =1 and id <> ".$id ." and (borough_id = ".$dataInfo['borough_id']." or borough_name = '".$dataInfo['borough_name']."') and house_price > ".($dataInfo['house_price']-300)." and house_price <".($dataInfo['house_price']+300);
	$borougSamePrice = $house->getList(array('rowFrom'=>0,'rowTo'=>7),'*',1,$where,'order by order_weight desc');
	$page->tpl->assign('borougSamePrice', $borougSamePrice);
}

//同区域的价格相近房源
$where = " and status =1 and id <> ".$id ." and cityarea_id = ".$dataInfo['cityarea_id']." and house_price > ".($dataInfo['house_price']-300)." and house_price <".($dataInfo['house_price']+300);
$cityareaSamePrice = $house->getList(array('rowFrom'=>0,'rowTo'=>4),'*',1,$where,'order by order_weight desc');
$page->tpl->assign('cityareaSamePrice', $cityareaSamePrice);

//该经纪人的其他房源
$where = " and status =1 and id <> ".$id ." and broker_id = ".$dataInfo['broker_id'];
$brokerOthersList = $house->getList(array('rowFrom'=>0,'rowTo'=>4),'*',1,$where,'order by order_weight desc');
$page->tpl->assign('brokerOthersList', $brokerOthersList);

//标题
$page->title = $dataInfo['borough_name'].'出租，'.$dataInfo['house_room'].'室'.$dataInfo['house_hall'].'厅'.$dataInfo['house_toilet'].'卫'.$dataInfo['house_veranda'].'阳，'.$dataInfo['house_title'].' - '.$page->city.$page->titlec;

//关键词
$page->keyword = $dataInfo['borough_name'].'出租,'.$dataInfo['borough_name'].'房屋出租,'.$dataInfo['borough_name'].'租房,'.$dataInfo['borough_name'];

//描述
$page->description='';

//点击增加统计
$house->addClick($id);

$_SERVER['REQUEST_URI'] = $_SERVER['REQUEST_URI']."&open=1";
$page->tpl->assign('back_to', urlencode($_SERVER['REQUEST_URI']));

$page->show();
?>