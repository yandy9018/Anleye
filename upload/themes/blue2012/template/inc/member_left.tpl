<div class="memberLeftNav">
			<div class="title"></div>
			<ul class="navList">
				<li class="item">
				 <p>房源管理</p>
					<ul>
					<li>
<a href="houseSale.php">发布出售</a><br />
<a href="houseRent.php">发布出租</a><br />
<a href="manageSale.php">出售管理</a><br />
<a href="manageRent.php">出租管理</a></li>
				  </ul>
                  	
				</li><br />

                <li class="item">
					<p>店铺管理</p>
					<ul>
						<li>
                        <a href="shopProfile.php">店铺资料</a><br />
                         <a href="shopDiy.php">店铺DIY</a><br />
                         <a href="shopSaleRec.php">店铺推荐</a><br />
                          <a target="_blank" href="<!--{$cfg.url}-->shop/evaluate.php?id=<!--{$member_id}-->">服务评价</a><br />
						</li>
				  </ul>
                  	
				</li>
                <br />

                  <li class="item">
					<p>人脉管理</p>
					<ul>
						<li>
                        <a href="snsFriends.php">我的好友</a><br />
                         <a href="snsLinkIn.php">谁来看过我</a><br />
                          <a href="snsInviteIn.php">好友邀请</a><br />

						</li>
				  </ul>
                  	
				</li>
                <br />

                  <li class="item">
					<p>站内信</p>
					<ul>
						<li>
                        <a href="msgWrite.php">撰写信件</a><br />
                         <a href="msgInbox.php">收件箱</a><br />
                          <a href="msgSend.php">已发信件</a><br />
                           <a href="msgGarbage.php">垃圾箱</a><br />

						</li>
				  </ul>
                  	
				</li>
                <br />
				

                       <li class="item">
					<p>资料管理</p>
					<ul>
						<li>
                        <a href="brokerProfile.php">修改资料</a><br />
                         <a href="brokerIdentity.php">实名认证</a><br />
                          <a href="brokerPhoto.php">修改头像</a><br />
                           <a href="pwdEdit.php">修改密码</a><br />
                             <a href="brokerAptitude.php">执业认证</a><br />

						</li>
				  </ul>
                  	
				</li>
			</ul>
			<div class="endLeft"></div>
		</div>