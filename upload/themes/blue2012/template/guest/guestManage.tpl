<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN" xml:lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk2312" />
<title><!--{$cfg.page.title}--></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="stylesheet" type="text/css" href="<!--{$cfg.path.css}-->guestManage.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<!--{$cfg.path.css}-->css.css" media="screen" />
</head>

<body><!--{include file="inc/indextop.tpl"}-->
<div id="wrap">

<div class="cbox">
	<h2 class="hd"><!--{$cfg.page.titlec}--> 个人房源管理</h2>
	<div class="bd">
      <!--{if $dataList or $dataList1}-->
      <p>第一部：请输入您的手机号，查找房源</p>
      
       <!--{else}-->
		<p class="stepon">第一部：请输入您的手机号，查找房源</p>
          <!--{/if}-->
		<form method="get" action="guestManage.php" class="form_s_phone">
        <input type="hidden" name="action" value="search">
		<fieldset>
		<span class="ipt_text"><input type="text" name="mobile" value="<!--{$smarty.get.mobile}-->" /></span>
		<input type="submit" value="查找" class="btn_submit" />
		<p>本功能只对个人用户开放</p>
		</fieldset>
		</form>
          <!--{if $dataList or $dataList1}-->
		<p class="stepon">第二步：请在下方的列表中，对房源进行修改和删除操作</p>
         <!--{else}-->
         <p>第二步：请在下方的列表中，对房源进行修改和删除操作</p>
         <!--{/if}-->
         
          <!--{if $dataList or $dataList1}-->
		<div class="s_box">
			<div class="tit">以下是查找<em><!--{$smarty.get.mobile}--></em>得到的结果：</div>
			<ul class="s_box_list">
            <!--{foreach item=item from=$dataList}-->
				<li>
					<span class="date"><!--{$item.created|date_format:"%Y-%m-%d"}--></span> <span class="tt"> | <a target="_blank" href="<!--{$cfg.url}-->sale/d-<!--{$item.id}-->.html"><!--{$item.house_title}--></a> <font color="#FF0000"><!--{if $item.is_checked ==0}-->(未审核)<!--{/if}--></font></span>
					<span class="state">出售</span><span class="act"><a target="_blank" href="<!--{$cfg.url}-->sale/d-<!--{$item.id}-->.html">对该房源进行修改或删除</a></span>
				</li>
			  <!--{/foreach}-->
               <!--{foreach item=item from=$dataList1}-->
				<li>
					<span class="date"><!--{$item.created|date_format:"%Y-%m-%d"}--></span><span class="tt"> | <a target="_blank" href="<!--{$cfg.url}-->rent/d-<!--{$item.id}-->.html"><!--{$item.house_title}--></a> <font color="#FF0000"><!--{if $item.is_checked ==0}-->(未审核)<!--{/if}--></font></span>
					<span class="state">出租</span><span class="act"><a target="_blank" href="<!--{$cfg.url}-->rent/d-<!--{$item.id}-->.html">对该房源进行修改或删除</a></span>
				</li>
			  <!--{/foreach}-->
			</ul>
		</div>
         <!--{/if}-->
		<div class="more">
        <!--{if $dataList or $dataList1}-->
        还有更多房源？<a target="_blank" href="<!--{$cfg.url}-->guest/houseSale.php">现在就去发布&gt;&gt;</a>
         <!--{elseif $smarty.get.action==search}-->
        <font color="#FF0000">没有找到 <!--{$smarty.get.mobile}--> 的任何房源</font> <a target="_blank" href="<!--{$cfg.url}-->guest/houseSale.php">现在发布出售&gt;&gt;</a> <a href="<!--{$cfg.url}-->guest/houseRent.php">现在发布出租&gt;&gt;</a>
        <!--{else}-->
         <a target="_blank" href="<!--{$cfg.url}-->guest/houseSale.php">发布出售&gt;&gt;</a> <a target="_blank" href="<!--{$cfg.url}-->guest/houseRent.php">发布出租&gt;&gt;</a>
          <!--{/if}-->
        </div>
		<p class="suggest">对发布者管理有想法？请告诉我们</p>
	</div>
    
	<div class="ft"></div>
</div>
</div>
<div class="blank"></div>
<!--{include file="inc/foot.tpl"}-->
</body>
</html>
