<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--></title>
<meta name="keywords" content="<!--{$cfg.page.keyword}-->" />
<meta name="description" content="<!--{$cfg.page.housedescription}-->" />
 
<!--{$cssFiles}-->
<!--{$jsFiles}-->
<script language="javascript">
function changeSearch(formDom){
	location.href=formDom.value;
}
</script>
</head>

<body>
<div id="box">
	<!-- 头部 -->

	<!-- 头部 结束 -->
	<!-- 主内容 -->
	<div id="photoMain">
		<!-- 总览导航 -->
		<div class="switchContBox">
			<div id="communityGeneralTitle" class="switchContBoxTitle">
				<ul>
					<li><a href="<!--{$cfg.url_newHouse}-->d-<!--{$dataInfo.id}-->.html"><span><!--{$dataInfo.borough_name}-->楼盘总览</span></a></li>
					<li><a href="<!--{$cfg.url_newHouse}-->p-<!--{$dataInfo.id}-->.html"><span>项目图片</span></a></li>
					<li class="linkOn"><a href="<!--{$cfg.url_newHouse}-->s-<!--{$dataInfo.id}-->.html"><span>户型鉴赏</span></a></li>
				</ul>
				<span class="photoUploadLink"><a class="color690" href="edit.php?id=<!--{$dataInfo.id}-->#uploadStructure">上传户型图<span class="familyArial">&gt;&gt;</span></a></span>
			</div>
		</div>
		<!-- 总览导航 结束 -->
		<!-- 图片区 -->
		<div class="photoBox">
			<p class="communityNameTip color690"><!--{$dataInfo.borough_name}--> <span class="color999">户型鉴赏</span></p>
			<table class="photo" cellpadding="0" cellspacing="15" border="0">
				<tr>
					<!--{foreach  name=borough_img from=$boroughImageList key=key item=item}-->
					<td ><span><a href="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->" target="_blank"><img src="<!--{$cfg.url}-->upfile/<!--{$item.pic_thumb}-->"  width="240" height="180" /></a><p><a class="colorF90" href="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->" target="_blank"><!--{$item.pic_desc}--></a></p></span></td>
				<!--{if $smarty.foreach.borough_img.iteration % 3 ==0}-->	
				</tr>
				<tr>
				<!--{/if}-->
				<!--{/foreach}-->
				<!--{section name=e loop=$borough_img_num }-->
					<td ></td>
				<!--{/section}-->
				</tr>
			</table>
		</div>
		<!-- 图片区 结束 -->
	</div>
	<!-- 主内容 结束 -->

</div>
</body>
</html>
