<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--{include file="inc/head.tpl"}-->
<script src="<!--{$cfg.path.js}-->select.js" type="text/javascript"></script>
<script src="<!--{$cfg.path.js}-->js.js" type="text/javascript"></script>
<link href="<!--{$cfg.path.css}-->add.css" rel="stylesheet" type="text/css" />
<link href="<!--{$cfg.path.css}-->menuSearch.css" rel="stylesheet" type="text/css" />
<script type="text/ecmascript">
$(function(){
		$(".listing>ol").hover(
			function(){
				$(this).addClass("hover")
				$(this).find(".hr").css({"border-bottom":"#1d78c0 1px solid"})
			},
			function(){
				$(this).removeClass("hover")	
				$(this).find(".hr").css({"border-bottom":"#ccc 1px dashed"})
			}
		
		)	
		
	})
</script>
</head>
 
<body>
 
<!--{include file="inc/top.tpl"}-->
 
<div>
<!--{include file="inc/city_selecter2.tpl"}-->
</div>
 <div class="blank"></div>
<div class="content">
 <!--中部左侧开始-->
<div class="sale_xx_left">
<div id="c">
    <ul>
    <input type="hidden" id="switch" name="switch" value="<!--{$smarty.get.switch}-->">
    <li <!--{if $smarty.get.switch ==''}-->class="li2"<!--{else}-->class="li1"<!--{/if}-->><span><a href="index.php?q=<!--{$smarty.get.q}-->&letter=<!--{$smarty.get.letter}-->&cityarea=<!--{$smarty.get.cityarea}-->&borough=<!--{$smarty.get.borough}-->&price=<!--{$smarty.get.price}-->&totalarea=<!--{$smarty.get.totalarea}-->&room=<!--{$smarty.get.room}-->&type=<!--{$smarty.get.type}-->&feature=<!--{$smarty.get.feature}-->&switch=&showstyle=<!--{$smarty.get.showstyle}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" >全部</a></span></li>
    <!--{if $cfg.page.guest==1}-->
    <li <!--{if $smarty.get.switch =='owner'}-->class="li2"<!--{else}-->class="li1"<!--{/if}-->><span><a href="index.php?switch=owner">个人</a></span></li>
    <!--{/if}-->
     <li <!--{if $smarty.get.switch =='vexation'}-->class="li2"<!--{else}-->class="li1"<!--{/if}-->><span><a href="index.php?switch=vexation">急售</a></span></li>
     <li <!--{if $smarty.get.switch =='morePic'}-->class="li2"<!--{else}-->class="li1"<!--{/if}-->><span><a href="index.php?switch=morePic">多图</a></span></li>
    
<li <!--{if $smarty.get.switch =='promote'}-->class="li2"<!--{else}-->class="li1"<!--{/if}-->><span><a href="index.php?switch=promote">店长</a></span></li>
<li class="li1"><span><a target="_blank" href="<!--{$cfg.url}-->sale/requireList.php">求购信息</a></span></li>
<li class="li1"><span><a target="_blank" href="<!--{$cfg.url}-->m/map">地图找房</a></span></li>
    </ul>
<div class="filter_right">
			<table>
				<tr>
				 
<td>                       
<!--{if $pageno > 1}-->
<a href="index.php?q=<!--{$smarty.get.q}-->&letter=<!--{$smarty.get.letter}-->&cityarea=<!--{$smarty.get.cityarea}-->&borough=<!--{$smarty.get.borough}-->&price=<!--{$smarty.get.price}-->&totalarea=<!--{$smarty.get.totalarea}-->&room=<!--{$smarty.get.room}-->&type=<!--{$smarty.get.type}-->&feature=<!--{$smarty.get.feature}-->&switch=<!--{$smarty.get.switch}-->&showstyle=<!--{$smarty.get.showstyle}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->&pageno=<!--{$pageno-1}-->" onfocus=blur()>
<img src="<!--{$cfg.path.images}-->sale01a.jpg" border="0" class="RL-Prev"/>
</a>
<!--{else}-->
<a href="javascript:void(0);">
<img src="<!--{$cfg.path.images}-->sale01.jpg" border="0" class="RL-Prev"/>
</a>
<!--{/if}-->
</td>
					<td id="RL_NoPage"><!--{$pageno}-->/<!--{$page_count}--></td>
					<td>
<!--{if $pageno < $page_count and $pageno < 100}-->
<a href="index.php?q=<!--{$smarty.get.q}-->&letter=<!--{$smarty.get.letter}-->&cityarea=<!--{$smarty.get.cityarea}-->&borough=<!--{$smarty.get.borough}-->&price=<!--{$smarty.get.price}-->&totalarea=<!--{$smarty.get.totalarea}-->&room=<!--{$smarty.get.room}-->&type=<!--{$smarty.get.type}-->&feature=<!--{$smarty.get.feature}-->&switch=<!--{$smarty.get.switch}-->&showstyle=<!--{$smarty.get.showstyle}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->&pageno=<!--{$pageno+1}-->" onfocus=blur()>
<img src="<!--{$cfg.path.images}-->sale02a.jpg" border="0" class="RL-Next"/>
</a>
<!--{else}-->
<a href="javascript:void(0);">
<img src="<!--{$cfg.path.images}-->sale02.jpg" border="0" class="RL-Prev"/>
</a>
<!--{/if}-->
</td>
				</tr>
			</table>
  </div></div>

<div class="all_fy_bg">
    <div class="all_fy_bgl">
    <a onClick="quickSearch('default','list_order');return false;" href="javascript:" class="<!--{if $smarty.get.list_order=='default'}-->input_mrpxx<!--{else}-->input_mrpx<!--{/if}-->" onfocus=blur()>默认排序</a>
    <a onClick="quickSearch('house_price asc','list_order');return false;" href="javascript:" class="<!--{if $smarty.get.list_order=='house_price asc'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" onfocus=blur()>总价<!--{if $qqq.house_price_class=='down'}-->&uarr;<!--{else}-->&darr;<!--{/if}--></a>
    
    <a onClick="quickSearch('house_totalarea asc','list_order');return false;" href="javascript:" class="<!--{if $smarty.get.list_order=='house_totalarea asc'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" onfocus=blur()>面积<!--{if $qqq.house_totalarea_class=='down'}-->&uarr;<!--{else}-->&darr;<!--{/if}--></a>
    
    <a onClick="quickSearch('avg_price asc','list_order');return false;" href="javascript:" class="<!--{if $smarty.get.list_order=='avg_price asc'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" onfocus=blur()>单价<!--{if $qqq.avg_price_class=='down'}-->&uarr;<!--{else}-->&darr;<!--{/if}--></a>
    
    <a onClick="quickSearch('created desc','list_order');return false;" href="javascript:"  class="<!--{if $smarty.get.list_order=='created desc'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" onfocus=blur()>时间</a>
    </div>
    <div class="all_fy_bgr">
    <a  onClick="quickSearch('listshow','showstyle');return false;" href="javascript:"  class="<!--{if $smarty.get.showstyle == ''  || $smarty.get.showstyle == 'listshow'}-->input_lb_selected<!--{else}-->input_lb<!--{/if}-->"></a>
    <a  onClick="quickSearch('drawingshow','showstyle');return false;" href="javascript:" class="<!--{if $smarty.get.showstyle == 'drawingshow'}-->input_fxt_selected<!--{else}-->input_fxt<!--{/if}-->"></a>
     <a  onClick="quickSearch('roompicshow','showstyle');return false;" href="javascript:" class="<!--{if $smarty.get.showstyle == 'roompicshow'}-->input_snt_selected<!--{else}-->input_snt<!--{/if}-->"></a>
    </div>
</div>

<div class="content_b1">
<!--{if ($smarty.get.showstyle=='' || $smarty.get.showstyle=='listshow')}-->
<div class="listing">
  <!--  列表循环体开始  -->
  <!--{foreach from=$dataList item=item key=key}-->
  
<ol id="list_body" >
<div class="h15"></div>
<div class="property2">
	    <div class="photo">
	       <a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}--><!--{$item.house_room}-->居" target="_blank"><img src="<!--{if $item.house_thumb}--><!--{$cfg.url_upfile}--><!--{$item.house_thumb}--><!--{else}--><!--{$cfg.path.images}-->housePhotoDefault.gif<!--{/if}-->" width="100" height="80" /></a>    </div>
	    <div class="details" style="margin-left:10px;">
	        <h4>
	            <a class="color000" href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank"><!--{$item.house_title}--></a><!--{include file="inc/four_tag.tpl"}-->
	        </h4>
	        <address>小区：<!--{$item.borough_name}--></address>
	        <div class="all_fyyy"><p>单价：<!--{$item.avg_price}-->元/平米</p><p>面积：<!--{$item.house_totalarea}-->平米</p><p>楼层：<!--{$item.house_floor}-->/<!--{$item.house_topfloor}--></p><br /><p>户型：<!--{$item.house_room}-->室<!--{$item.house_hall}-->厅</p>
</div>
 <div class="broker_name"><p><font color="#999999"><!--{if $item.broker_info.realname}-->经纪人：<a target="_blank" href="<!--{$cfg.url}-->shop/<!--{$item.broker_id}-->"><!--{$item.broker_info.realname}--></a><!--{else}-->房东：<!--{$item.owner_name}--><!--{/if}-->  <!--{$item.updated|date_format:"%m月%d日"}-->发布</font></p></div>
   	      </div>
	    <div class="price"><span><!--{if $item.house_price == 0}-->面议<!--{else}--><!--{$item.house_price}--><!--{/if}--></span>万元</div>
	    <div class="tags"><!--{if $item.is_vexation==1}--><img src="<!--{$cfg.path.images}-->sale_js.gif" title="急售" alt="急售" /><!--{/if}-->&nbsp;<!--{if $item.is_top==1}--><img src="<!--{$cfg.path.images}-->newpush.gif" title="新推" alt="新推" /><!--{/if}--><!--{if $item.broker_id==0}--><img src="<!--{$cfg.path.images}-->owner.gif" title="无中介费" alt="无中介费" /><!--{/if}-->&nbsp;</div>
    </div>
    <div class="h23"></div>
    <div class="hr"></div>

    </ol>
    <!--{/foreach}-->
    <!--  列表循环体结束  -->
</div>
   <!--{/if}-->
 
<!--{if $smarty.get.showstyle=='roompicshow'}-->
<!--  列表循环体开始  -->
<!--{foreach from=$dataList item=item key=key}-->
    <ol id="list_body" class="big_img">
     <li class="big_img" <!--{if $item.is_rowlast neq 1}--> style="margin-right:21px; *margin-right:10px;_margin-right:12px; "<!--{/if}-->>
	<div class="property_snt big_link_all" style="border-bottom:none;">
	  <div class="er_snt_aa" style=" padding:0;">
        <table width="220" border="0" align="center" cellpadding="0" cellspacing="0"ststyle="margin-left:0px; margin-top:2px;" >
          <tr>
            <td height="165" align="center" valign="top"><a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank"><img src="<!--{if $item.house_thumb}--><!--{$cfg.url_upfile}--><!--{$item.house_thumb}--><!--{else}--><!--{$cfg.path.images}-->housePhotoDefault.gif<!--{/if}-->" width="216" height="165" border="0" /></a></td>
          </tr>
          <tr>
            <td height="24" align="left" valign="bottom" class="font_00359d" style="padding:0 5px;"><a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank"><!--{$item.borough_name}--></a></td>
          </tr>
          <tr>
            <td height="27" align="left" valign="middle" style="border-bottom:1px #71a6db solid; color:#7e7e7e; padding:0 5px;"><!--{$item.house_room}-->室<!--{$item.house_hall}-->厅 | <!--{$item.house_totalarea}-->平米 |</td>
          </tr>
          <tr>
            <td align="left" bgcolor="#f6f6f6"><table width="210" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="33" align="left" valign="middle" bgcolor="#f6f6f6" class="price_zz">售价：<span><!--{$item.house_price}--></span><span class="aabb">万元</span></td>
                  <td align="right" valign="middle" bgcolor="#f6f6f6"><!--{include file="inc/four_tag.tpl"}--></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td height="45" align="left" bgcolor="#f6f6f6" class="font_2f5aaf" style="line-height:21px; padding:0 5px;"><!--{$item.house_title}--><br /></td>
          </tr>
          <tr>
            <td height="5" align="left" bgcolor="#f6f6f6"></td>
          </tr>
        </table>
	    <span class="xj_c xj_c01"></span> <span class="xj_c xj_c06"></span> <span class="xj_c xj_c03"></span> </div>
	  <div class="bottom_shadow"></div>
    </div>
</li>                              
      </ol>
<!--{/foreach}-->
   
<!--{/if}-->
                                                     
                            
                            
<!--{if $smarty.get.showstyle=='drawingshow'}-->

<!--  列表循环体开始  -->
<!--{foreach from=$dataList item=item key=key}-->
<ol id="list_body" class="big_img">
                                    <li class="big_img"  <!--{if $item.is_rowlast neq 1}--> style="margin-right:21px;*margin-right:10px; _margin-right:12px;"<!--{/if}-->>
	<div class="property_snt big_link_all" style="border-bottom:none;">
	  <div class="er_snt_aa" style=" padding:0;">
        <table width="220" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:0px; margin-top:2px;">
          <tr>
            <td height="165" align="center" valign="top"><a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank"><img src="<!--{if $item.house_drawing}--><!--{$cfg.url_upfile}--><!--{$item.house_drawing}--><!--{else}--><!--{$cfg.path.images}-->housePhotoDefault.gif<!--{/if}-->" width="216" height="165" border="0" /></a></td>
          </tr>
          <tr>
            <td height="24" align="left" valign="bottom" class="font_00359d" style="padding:0 5px;"><a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank"><!--{$item.borough_name}--></a></td>
          </tr>
          <tr>
            <td height="27" align="left" valign="middle" style="border-bottom:1px #71a6db solid; color:#7e7e7e; padding:0 5px;"><!--{$item.house_room}-->室<!--{$item.house_hall}-->厅 | <!--{$item.house_totalarea}-->平米 |</td>
          </tr>
          <tr>
            <td align="left" bgcolor="#f6f6f6"><table width="210" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="33" align="left" valign="middle" bgcolor="#f6f6f6" class="price_zz">售价：<span><!--{$item.house_price}--></span><span class="aabb">万元</span></td>
                  <td align="right" valign="middle" bgcolor="#f6f6f6"><!--{include file="inc/four_tag.tpl"}--></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td height="45" align="left" bgcolor="#f6f6f6" class="font_2f5aaf" style="line-height:21px; padding:0 5px;"><!--{$item.house_title}--></td>
          </tr>
          <tr>
            <td height="5" align="left" bgcolor="#f6f6f6"></td>
          </tr>
        </table>
	    <span class="xj_c xj_c01"></span> <span class="xj_c xj_c06"></span> <span class="xj_c xj_c03"></span> </div>
	  <div class="bottom_shadow"></div>
    </div>
</li>  
      </ol>
<!--{/foreach}-->     

<!--{/if}-->
<!--{include file="inc/page_fenye.tpl"}-->
</div>
 
 
    
</div>
 <div class="sale_xx_right">
  <div class="hz_list_pub">
 <div class="gpf_content">
    	<h2 class="title_top">您有房屋出售吗？</h2>
        <p class="text">您通过此处发布购房者无需缴纳<span>中介费</span></p>
        <div class="pub_btn_box">
        	<a target="_blank" class="pub_but" hidefocus="true" title="个人房东发布" href="<!--{$cfg.url}-->guest/houseSale.php"></a>
        </div>
    </div>
    </div>
<div class="region">
  <h3>浏览过的房源</h3>
  <!--{if $browseList}-->
  <!--{foreach from=$browseList item=item key=key}-->
        <div class="region_main">
          <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
       
           <tr>
            <td width="58%" rowspan="3" align="left"><a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank">
            
            <img style="padding:1px; border:1px #d7d7d7 solid;" src="<!--{if $item.house_thumb}--><!--{$cfg.url_upfile}--><!--{$item.house_thumb}--><!--{else}--><!--{$cfg.path.images}-->housePhotoDefault.gif<!--{/if}-->" width="75px" height="50px" alt="<!--{$item.borough_name}-->" title="<!--{$item.borough_name}-->" />
            
            </a></td>
            
            <td width="42%" align="left" class="font_2f5aaf"><strong><a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank"><!--{$item.borough_name}--></a></strong></td>
            
          </tr>
          <tr>
            <td align="left"><!--{$item.house_room}-->室<!--{$item.house_hall}-->厅</td>
          </tr>
          <tr>
            <td align="left" class="Community_z"><!--{$item.house_price}-->万</td>
          </tr>
     
        </table>
      </div>
<!--{/foreach}-->   
  <!--{else}-->
  <img src="<!--{$cfg.path.images}-->browse.gif">
    <!--{/if}-->
      

    </div> 
      <!--{include file="inc/saleRent_AD.tpl"}-->
    
    </div>
</div>
<!--中部右侧结束-->
<!-- 大框架结尾 -->
</div>

<!--{include file="inc/foot.tpl"}-->
</body>
</html>
