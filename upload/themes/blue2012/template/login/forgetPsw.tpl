<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--></title>
<!--{$jsFiles}-->
<script type="text/javascript">
FormValid.showError = function(errMsg,errName,formName) {
	if (formName=='loginform') {
		for (key in FormValid.allName) {
			document.getElementById('errMsg_'+FormValid.allName[key]).innerHTML = '';
		}
		for (key in errMsg) {
			document.getElementById('errMsg_'+errName[key]).innerHTML = errMsg[key];
		}
	}
}
</script>
<!--{$cssFiles}-->
</head>
<body>
<div id="box">
<!-- 头部 --><!--{include file="aboutHeader.tpl"}--><!-- 头部 -->
<div class="pages">
	<div class="login_left">
    	<form id="loginform" name="loginform" method="post" action="" onsubmit="return validator(this)">
    	<input name="action" type="hidden" value="getPass" />
		<ul>
        	<li class="title">找回密码</li>
            <li class="login_inp">帐&nbsp;&nbsp;号<input name="username" type="text" maxlength="16" valid="required" errmsg="帐号不能为空!" /><span id="errMsg_username"></span></li>
            <li class="login_inp">邮&nbsp;&nbsp;箱<input name="email" type="text" maxlength="50" valid="required|isEmail" errmsg="邮箱地址不能为空!|邮件地址不符合规则" /><span id="errMsg_email"></span><p>输入你注册时留的Email地址</p></li>
            <li class="login_velidate">验证码<input name="vaild" type="text" size="8" valid="required" errmsg="验证码不能为空!" />&nbsp;<img src="<!--{$cfg.url}-->valid.php" /><span id="errMsg_vaild"></span></li>
            <li class="login_btn"><input name="sendPassword" type="button" value="发送密码" onclick="javascript:document.loginform.submit();" /></li>
        	<!--{if $errorMsg}--><li class="login_check"><span><!--{$errorMsg}--></span></li><!--{/if}-->
        </ul>
        </form>
    </div>
	<div class="login_right">
    	 <p class="title"><a href="registerFormOwner.php">免费注册会员</a></p>
        <div class="box">
        	<ul>
            	<li>足不出户，在线淘房</li>
				<li>免费房产评估，管理您的财富</li>
            	<li>委托买卖需求，上千经纪人任你挑</li>
            	<li>买卖价格参考，行情尽在掌握</li>
            </ul>
        </div>
        <p class="title"><a href="registerFormBroker.php">免费注册网络经纪人</a></p>
        <div class="box">
        	<ul>
            	<li>免费开网店，轻松创业绩</li>
            	<li>多渠道营销支持，让客户找到你</li>
            	<li>接受在线委托，业务源源不断</li>
            	<li>结交同行好友，拓展商务人脉</li>
            </ul>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
initValid(document.loginform);
</script>
</body>
</html>