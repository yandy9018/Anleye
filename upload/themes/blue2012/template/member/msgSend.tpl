<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 查看发件箱</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
			<div class="msgOperationBox">
				<ul class="msgOperation">
					<li><a href="msgWrite.php"><span>撰写信件</span></a></li>
					<li><a href="msgInbox.php"><span>收件箱(<!--{$msgCount}-->)</span></a></li>
					<li class="linkOn"><a href="msgSend.php"><span>已发信件</span></a></li>
					<li><a href="msgGarbage.php"><span>垃圾箱</span></a></li>
				</ul>
			</div>
			<div class="manageBox">
				<div class="houseList">
					<table border="0" cellpadding="0" cellspacing="1">
						<thead class="tableTitle">
							<tr>
								<td width="5%">&nbsp;</td>
								<td width="45%">信件主题</td>
								<td width="30%">收件人</td>
								<td>发件时间</td>
							</tr>
						</thead>	
						<form name="myform" action="" method="POST">
						<tbody>
							<!--{foreach name=dataList from=$dataList item=item key=key}-->
							<tr>
								<td><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" /></td>
								<td class="msgTitleLeft"><a href="msgDetail.php?id=<!--{$item.id}-->" title="打开信件"><!--{$item.msg_title}--></a></td>
								<td><!--{$item.msg_to}--></td>
								<td><!--{$item.add_time|date_format:"%Y-%m-%d %H:%M"}--></td>
							</tr>
							<!--{/foreach}-->
							<tr>
								<td colspan="4" class="listOperation">
									<label for="checkBoxAll"><input type="checkbox" id="checkBoxAll" name="checkBoxAll" value="1" onclick="checkAll(this);" />全选</label>
									<input class="listOperationBtn" type="button" value="删除选中的信件" onclick="if(confirm('你确认要删除选中的信件么？')){myform.action='?action=fromDel';myform.submit();}" />
								</td>
							</tr>
						</tbody>
						</form>
					</table>
					<!--{$pagePanel}-->
				</div>
			</div>
		</div>
	</div>
	
</div>
<script language="javascript">
function checkAll(formObj){
	if(formObj.checked){
		$('input[type=checkbox]').attr('checked', true);	
	}else{
		$('input[type=checkbox]').attr('checked', false);
	}
}

</script>
</body>
</html>