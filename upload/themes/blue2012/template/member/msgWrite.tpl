<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 发送信件</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
			<div class="msgOperationBox">
				<ul class="msgOperation">
					<li class="linkOn"><a href="msgWrite.php"><span>撰写信件</span></a></li>
					<li><a href="msgInbox.php"><span>收件箱(<!--{$msgCount}-->)</span></a></li>
					<li><a href="msgSend.php"><span>已发信件</span></a></li>
					<li><a href="msgGarbage.php"><span>垃圾箱</span></a></li>
				</ul>
			</div>
			<div class="manageBox">
			<form id="dataInfo" name="dataInfo" method="post" action="?action=save">
			<table class="memberBoxTable msgTable" cellpadding="0" cellspacing="5" border="0">
			<input type="hidden" name="reply_to" value="<!--{$dataInfo.reply_to}-->">
				<tbody>
					<tr>
						<td class="row1">收 件 人：</td>
						<td colspan="3"><input id="msg_to" class="input" name="msg_to" type="text" size="40" value="<!--{$dataInfo.msg_to}-->" />&nbsp;<span class="tip">输入收件人用户名</span></td>
					</tr>
					<tr>
						<td class="row1">信件主题：</td>
						<td colspan="3"><input id="msg_title" class="input" name="msg_title" type="text" size="40" value="<!--{$dataInfo.msg_title}-->" />&nbsp;<span class="tip">主题最多25个汉字</span></td>
					</tr>
					<tr>
						<td class="row1">信件内容：</td>
						<td colspan="3"><textarea class="textarea" name="msg_content" cols="70" rows="10"></textarea></td>
					</tr>
					<tr><td colspan="4" class="br"></td></tr>
				</tbody>
			</table>
			<div class="submitBtn"><input type="button" value="发送信件" onclick="javascript:if(validator(document.dataInfo)){document.dataInfo.submit();}" /></div>
			</form>
			</div>
		</div>
	</div>
	
</div>
</body>
</html>