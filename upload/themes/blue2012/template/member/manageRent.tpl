<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 租房源管理</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
			<div class="memberBoxTab">
				<ul>
				  <li><a href="houseSale.php"><span>发布出售</span></a></li>
				  <li><a href="houseRent.php"><span>发布出租</span></a></li>
				  <li><a href="manageSale.php"><span>出售管理</span></a></li>
				  <li><a href="#" class="linkOn"><span>出租管理</span></a></li>
				</ul>
			</div>
			<div class="manageSub">
				<ul class="manageSubNav">
					<li class="linkOn"><a href="#"><span>出租中</span></a></li>
                    <li><a href="manageRentTop.php"><span>置顶中</span></a></li>
					<li><a href="manageRentRecycle.php"><span>回收站</span></a></li>
					<li><a href="manageRentDone.php"><span>成交榜</span></a></li>
				</ul>
			</div>
			<div class="manageBox">
				<div class="houseSearch">
					<form name="searchForm" action="" method="GET">
						<input type="text" class="input" name="q" size="30" value="<!--{if $q}--><!--{$q}--><!--{else}-->输入房源编号或小区名称<!--{/if}-->" onblur="if(this.value ==''||this.value == '输入房源编号或小区名称'){this.value = '输入房源编号或小区名称';}" onfocus="if(this.value == '输入房源编号或小区名称'){this.value = '';}"  />&nbsp;<input type="button" value="搜 索" onclick="javascript:document.searchForm.submit();" />&nbsp;<a href="#notice">租房源管理必读</a><span class="tip">您已发布<span class="f90"><!--{$houseNum}--></span>条出租房源，<!--{if $houseLeft>=0}-->还可发布<span class="f90"><!--{$houseLeft}--></span>条出租房源<!--{else}-->已超出<span class="f90"><!--{$houseLeft|abs}--></span>条<!--{/if}--> <!--{if $memberVip==0}-->| <a href="#" onclick="showBoxnum();return false;">购买条数</a><!--{/if}--></span>
					</form>
				</div>
				<div class="houseList">
					<table border="0" cellpadding="0" cellspacing="1">
						<thead class="tableTitle">
							<tr>
								<td colspan="5">房源基本信息</td>
																<td colspan="5">操作</td>
							</tr>
						</thead>
						<thead class="tableSubTitle">
						<tr>
								<td width="12%">房源编号</td>
								<td width="15%">小区名称</td>
								<td width="10%">户型</td>
								<td width="8%">面积</td>
								<td width="8%">租金</td>	                                                                							                        <td width="8%">可刷新</td>
                                                               <td width="18%">预约刷新</td>							
	                 				       <td width="28%">常用操作</td>
                                
							</tr>
						</thead>
						<tbody>
							<form name="myform" enctype="text/html" action="" method="POST">
							<input type="hidden" name="to_url" value="<!--{$to_url}-->">
							<!--{foreach name=dataList from=$dataList item=item key=key}-->
							<tr>
								<td><input type="checkbox" name="ids[]" value="<!--{$item.id}-->"  />&nbsp;<a href="<!--{$cfg.url_rent}-->d-<!--{$item.id}-->.html" title="查看房源详细信息" target="_blank"><!--{$item.house_no}--></a></td>
								<td><!--{$item.borough_name}--></td>
								<td><!--{$item.house_room}-->-<!--{$item.house_hall}-->-<!--{$item.house_toilet}-->-<!--{$item.house_veranda}--></td>
								<td><!--{$item.house_totalarea}-->㎡</td>
								<td class="f90"><!--{if $item.house_price == 0}-->面议<!--{else}--><!--{$item.house_price}-->元	<!--{/if}-->							  </td>						
                                <td><!--{$item.refresh}-->次</td>
                                <td> <!--增加预约刷新按钮 hyjfc.com-->
                                                    <!--{if $item.is_appo}-->
                                                    <img src="../images/time.gif" width="23" height="16" align="absmiddle"  />
                                                     <a href="../appointment/index.php?action=appoShowHouse&site=rent&house_id=<!--{$item.id}-->" title="查看">查看</a>|<a href="../appointment/index.php?action=appoDel&site=rent&house_id=<!--{$item.id}-->" title="取消">取消</a>

                                                    <!--{else}-->
                                                        <a href="../appointment/index.php?action=appoRefresh&site=rent&ids=<!--{$item.id}-->" title="立即预约">立即预约</a>
                                                    <!--{/if}--></td>
								<td><a href="houseRent.php?action=edit&id=<!--{$item.id}-->" title="编辑房源信息">编辑</a>&nbsp;<a href="?house_id=<!--{$item.id}-->" onclick="showBox(<!--{$item.id}-->);return false;" title="该房源已成交">成交</a>&nbsp;<a href="?house_id=<!--{$item.id}-->" onclick="showBoxtop(<!--{$item.id}-->);return false;" title="将此房源置顶">置顶</a>&nbsp;<a href="?house_id=<!--{$item.id}-->" onclick="showBoxOwner(<!--{$item.id}-->);return false;" title="查看房东信息">房东</a>&nbsp;  
                                                      </td>
                                                      </tr>
							<!--{/foreach}-->
							<tr>
								<td colspan="10" class="listOperation">
								<label for="checkBoxAll"><input type="checkbox" id="checkBoxAll" name="checkBoxAll" value="1" onclick="checkAll(this);" />全选</label> 
								<input class="listOperationBtn" type="button" value="刷新选中的房源" onclick="if(confirm('你确认刷新选中的房源么？')){myform.action='?action=refresh';myform.submit();}" />&nbsp;
                                                                <input class="listOperationBtn" type="button" value="批量预约刷新房源" onclick="if(confirm('你确认预约刷新选中的房源么？')){myform.action='../appointment/index.php?action=appoRefresh&site=rent';myform.submit();}" />&nbsp;
								<input class="listOperationBtn" type="button" value="下架选中的房源" onclick="if(confirm('你确认下架选中的房源么？')){myform.action='?action=notSell';myform.submit();}" />
								</td>
							</tr>
						</tbody>
					</table>
					<!--{$pagePanel}-->
				</div>
			</div>
			<div class="note">
				<p><a name="notice" id="notice"></a>租房源管理必读：</p>
				<ul>
					
					
					<li>每日刷新房源一次，还可有效提高房源在租房栏目中的排名位置，提升房源点击率；</li>
					<li>点击房源编号链接浏览房源详细信息；</li>
					<li>通过“编辑”可对已发布的房源信息进行修改；</li>
					<li>删除的房源将首先下架到回收站，可以在回收站中彻底删除；</li>
					<li>若房源已成交，点击“成交”操作并输入相关的成交信息，可方便经纪人日后在“成交榜”中进行交易业绩和客户资料管理。已成交的房源将不再对外展示；</li>
					<li>星级经纪人可发布30条租房源，钻石级经纪人可发布60条租房源；超过数量限制将不能再发布，请及时将无效房源下架或删除；</li>
				</ul>
			</div>
		</div>
	</div>
	
</div>
<script language="javascript">
function checkAll(formObj){
	if(formObj.checked){
		$('input[type=checkbox]').attr('checked', true);	
	}else{
		$('input[type=checkbox]').attr('checked', false);
	}
}
function showBox(item_id){
	TB_show('房源成交','rentBargain.php?house_id='+item_id+'&height=370&width=400&modal=true&rnd='+Math.random(),false);
}
function showBoxOwner(item_id){
	TB_show('业主信息','landlordRentInfo.php?house_id='+item_id+'&height=150&width=400&modal=true&rnd='+Math.random(),false);
}
function showBoxtop(item_id){
	TB_show('房源置顶','rentTop.php?house_id='+item_id+'&height=200&width=400&modal=true&rnd='+Math.random(),false);
}
function showBoxnum(){
	TB_show('购买条数','rentNum.php?height=200&width=400&modal=true&rnd='+Math.random(),false);
}
</script>
<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
</body>
</html>