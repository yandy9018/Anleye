<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}-->  - 查看信件</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
			<div class="msgOperationBox">
				<ul class="msgOperation">
					<li><a href="msgWrite.php"><span>撰写信件</span></a></li>
					<li><a href="msgInbox.php"><span>收件箱(<!--{$msgCount}-->)</span></a></li>
					<li><a href="msgSend.php"><span>已发信件</span></a></li>
					<li><a href="msgGarbage.php"><span>垃圾箱</span></a></li>
				</ul>
			</div>
			<div class="manageBox">
				<form>
			<table class="memberBoxTable msgTable" cellpadding="0" cellspacing="5" border="0">
				<tbody class="msgDetail">
					<tr>
						<td class="row1">信件主题：</td>
						<td colspan="3"><!--{$dataInfo.msg_title}--></td>
					</tr>
					<tr>
						<td class="row1">信件内容：</td>
						<td colspan="3">
							<div class="msgContent"><!--{$dataInfo.msg_content}-->
								<span class="msgSender">发件人：<!--{$dataInfo.msg_from}--> | 发送时间：<!--{$dataInfo.add_time|date_format:"%Y-%m-%d %T"}--></span>
							</div>
						</td>
					</tr>
					<tr><td colspan="4" class="br"></td></tr>
				</tbody>
			</table>
			<div class="submitBtn"><input type="button" value="回复" onclick="document.location='msgWrite.php?reply_id=<!--{$dataInfo.id}-->'" /> <input type="button" value="删除" onclick="document.location='msgInbox.php?action=toDel&id=<!--{$dataInfo.id}-->'" /></div>
			</form>
			</div>
		</div>
	</div>
	
</div>
</body>
</html>