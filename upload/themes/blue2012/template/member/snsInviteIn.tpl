<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 好友邀请</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/memberHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/member_left.tpl"}-->
		<div class="memberBox">
			<div class="msgOperationBox">
				<ul class="msgOperation">
					<li><a href="snsFriends.php"><span>我的好友</span></a></li>
					<li><a href="snsLinkIn.php"><span>谁来看过我</span></a></li>
					<li class="linkOn"><a href="snsInviteIn.php"><span>好友邀请</span></a></li>
				</ul>
			</div>
			<div class="manageBox">
				<div class="friendsList">
					<!--{foreach name=list from=$dataList key=key item=item}-->
					<table border="0" cellpadding="0" cellspacing="0">	
						<tbody>
							<tr>
								<td class="rowPhoto"><a href="<!--{$cfg.url_shop}--><!--{$item.broker_id}-->" title="<!--{$item.realname}-->" target="_blank"><!--{if $item.avatar}--><img class="demoImgBorder" src="<!--{$cfg.url}-->upfile/<!--{$item.avatar}-->" width="42" height="50" /><!--{else}--><img class="demoImgBorder" src="<!--{$cfg.path.images}-->demoPhoto.jpg" width="42" height="50" /><!--{/if}--></a></td>
								<td class="rowName" colspan="2">
									<p class="p1"><a class="font14px fontBold" href="<!--{$cfg.url_shop}--><!--{$item.broker_id}-->" target="_blank"><!--{$item.realname}--></a>&nbsp;<span class="snsInviteInBg">想加你为好友！</span></p>
									<p class="p2"><span class="alphaFontFamily">邀请时间：<!--{$item.add_time|date_format:'%Y-%m-%d %T'}--></span></p>
								</td>
								<td><a href="snsInviteIn.php?action=status&status=1&id=<!--{$item.id}-->">接受邀请</a>&nbsp;&nbsp;<a href="snsInviteIn.php?action=status&status=2&id=<!--{$item.id}-->">拒绝邀请</a></td>
							</tr>
						</tbody>
					</table>
					<!--{/foreach}-->
					<!--{$pagePanel}-->
				</div>
			</div>
		</div>
	</div>
	
</div>
</body>
</html>