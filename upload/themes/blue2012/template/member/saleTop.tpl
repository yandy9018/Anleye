<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 置顶</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->

</head>
<body>
<div id="modalWindow">
    您好，您账户的余额为<font color="#FF0000"> <!--{$money}--> </font>元
      <p> 
      每天置顶房源的价格为<!--{$cfg.page.sellPrice}-->元
      </p>
        <p> 
      置顶金额 = <!--{$cfg.page.sellPrice}--> * 置顶天数
      </p>
	<form name="dataForm" method="POST" action="">
	<input type="hidden"  name="house_id" value="<!--{$house_id}-->">
    <p>
        <label>您需要置顶的天数:</label>
       <input name="days"  maxlength=10    onkeypress="var   k=event.keyCode;   return   k>=49&&k<=57" onpaste="return   !clipboardData.getData('text').match(/\D/)"  ondragenter="return   false" style="ime-mode:Disabled" >
    </p>
     <p> 
     <font color="#FF0000">（天数必须为整数，如果有小数点将取小数点前面的值为天数）</font>
      </p>
     
     <p>
     	<input type="button" value=" 确认置顶 " onclick="if(confirm('确认将会从您的账户中扣除相应的费用，确认么？')){dataForm.action='saleTop.php?action=save';dataForm.submit();}">
    </p>
    </form>
</div>

</body>
</html>