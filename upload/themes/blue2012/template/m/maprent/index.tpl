﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML><HEAD><TITLE>地图找租房 - <!--{$cfg.page.title}--></TITLE>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<LINK rev=stylesheet href="Map_MapFindding.css" type=text/css rel=stylesheet></LINK>
<LINK rev=stylesheet href="top.css" type=text/css rel=stylesheet></LINK>
<META content="MSHTML 6.00.2900.5848" name=GENERATOR></HEAD>
<BODY>

<DIV id=container>
<DIV id=header>
<DIV class=banner>
<DIV class=logo><A href="<!--{$cfg.url}-->"><IMG src="<!--{$cfg.path.images}-->logo.png"></A></A></DIV>

<DIV class=tabbar>
<DIV class=tabs>
        
		 <a href="<!--{$cfg.url}-->m/map" title="<!--{$cfg.page.city}-->二手房">二手房</a>
         <a class="actived" href="<!--{$cfg.url}-->m/maprent" title="<!--{$cfg.page.city}-->租房">租房</a>
          <a href="<!--{$cfg.url}-->m/mapnewhouse" title="<!--{$cfg.page.city}-->租房">新房</a>
 
  </DIV>
<DIV class=bg>

<DIV class=nw></DIV>
<DIV class=cen></DIV>
<DIV class=ne></DIV></DIV>
<DIV style="CLEAR: both"></DIV></DIV>
<DIV style="CLEAR: both"></DIV></DIV>
<DIV class=bar></DIV></DIV>
<DIV style="CLEAR: both"></DIV>
<DIV id=content>
<DIV class=mapFinddingFilterBlock id=apf_id_1_filter>
<DIV class=mapFinddingFilter id=apf_id_10_filter>
<INPUT id=html_html_id type=hidden value=apf_id_10 name=html_html_id> 
<DIV class=mapFinddingFilter_Nav><SPAN><IMG src="map.dim_14x14.gif"> 
出租房地图 </SPAN><SPAN style="PADDING-LEFT: 5px; BORDER-LEFT: #ddd 1px solid"><IMG 
src="list_14x14.gif"> <INPUT id=apf_id_10_hidden_colum type=hidden 
value=sale name=apf_id_10_hidden_colum> <A href="<!--{$cfg.url_rent}-->" id=apf_id_10_select_colum 
style="CURSOR: pointer; COLOR: #0041d9">出租房列表</A> </SPAN></DIV>
<DIV class=mapFinddingFilter_Caption>位置导航</DIV>
<DIV class=mapFinddingFilter_BoxTop></DIV>
<DIV class=mapFinddingFilter_BoxMiddle><IMG src="area_2_16x16.gif" 
align=absMiddle>&nbsp;
<SELECT class=mapFinddingFilter_Slt2 id=apf_id_10_select_area name=apf_id_10_select_area>
 <OPTION value=0  selected>选择区域</OPTION> 
 <!--{foreach from=$cmap_option item=item key=key }-->
 <OPTION id=apf_id_10_select_area_1<!--{$key}--> value=1<!--{$key}--> lng="<!--{$item.lnt}-->" lat="<!--{$item.lat}-->"><!--{$item.cityarea_name}--></OPTION>
<!--{/foreach}-->
</SELECT>  
<DIV class=mapFinddingFilter_SpaceBar></DIV><IMG src="range_16x16.gif" 
align=absMiddle>&nbsp; <SELECT style="display:none;" class=mapFinddingFilter_Slt2 
id=apf_id_10_select_block disabled> <OPTION selected>选择板块</OPTION></SELECT> 
</DIV>
<DIV class=mapFinddingFilter_BoxBottom></DIV>
<DIV class=mapFinddingFilter_Caption>选择筛选条件</DIV>
<DIV class=mapFinddingFilter_BoxTop></DIV>
<DIV class=mapFinddingFilter_BoxMiddle><IMG src="price_16x16.gif" 
align=absMiddle>&nbsp; 
<SELECT class=mapFinddingFilter_Slt2 id=apf_id_10_select_price> 
 <OPTION value="" selected>租金不限</OPTION>
    <!--{html_options options=$house_price_option}-->
 </SELECT> 
<DIV class=mapFinddingFilter_SpaceBar></DIV><IMG 
src="space_2_16x16.gif" align=absMiddle>&nbsp; 
<SELECT class=mapFinddingFilter_Slt2 id=apf_id_10_select_range> 
<OPTION value=""  selected>面积不限</OPTION>
  <!--{html_options options=$house_totalarea_option}-->
    </SELECT> 
<DIV class=mapFinddingFilter_SpaceBar></DIV><IMG src="model_16x16.gif" 
align=absMiddle>&nbsp; <SELECT class=mapFinddingFilter_Slt2 
id=apf_id_10_select_model> <OPTION value="" selected>房型不限</OPTION> <OPTION 
  value=1>一室</OPTION> <OPTION value=2>二室</OPTION> <OPTION value=3>三室</OPTION> 
  <OPTION value=4>四室</OPTION> <OPTION value=5>五室</OPTION> <OPTION 
  value=6>五室以上</OPTION></SELECT> 
<DIV class=mapFinddingFilter_SpaceBar></DIV><IMG 
src="uestype_16x16.gif" align=absMiddle>&nbsp; 
<SELECT class=mapFinddingFilter_Slt2 id=apf_id_10_select_uestype> 
<OPTION value=""  selected>类型不限</OPTION> 
<!--{html_options options=$house_type_option}-->
</SELECT>
</DIV>
<DIV class=mapFinddingFilter_BoxBottom></DIV>
<DIV class=mapFinddingFilter_Caption>要在哪里找房子</DIV>
<DIV class=mapFinddingFilter_BoxTop></DIV>
<DIV class=mapFinddingFilter_BoxMiddle><IMG src="glass_16x16.gif" 
align=absMiddle>&nbsp; <INPUT id=apf_id_10_map_keywords size=8><BUTTON 
id=apf_id_10_map_search>搜索</BUTTON> </DIV>
<DIV class=mapFinddingFilter_BoxBottom></DIV>
<DIV class=mapFinddingFilter_Err id=mapFinddingFilter_Err>没找到你要的地址，换一个试试吧。 
</DIV>
<DIV style="MARGIN-TOP: 10px; FONT-WEIGHT: bold; FONT-SIZE: 13px; COLOR: #eb6100; TEXT-ALIGN: center">
<A id=apf_id_10_other_tt href="<!--{$cfg.url}-->m/map/">切换到二手房地图&nbsp;&gt;&gt;</A> <br>
<A id=apf_id_10_other_tt href="<!--{$cfg.url}-->m/mapnewhouse/">切换到新房地图&nbsp;&gt;&gt;</A>
</DIV>
<DIV class=mapFinddingFilter_Tips><A href="#" 
target=_blank>感谢使用地图找房测试版，<BR>
  有意见、建议、好玩的想法？<BR>立刻告诉我们吧！</A> </DIV></DIV></DIV>
<DIV class=mapFinddingCanvasBlock id=apf_id_1_canvas>
<DIV id=apf_id_1_splitter title=缩放地图 
style="Z-INDEX: 10; LEFT: 0px; BACKGROUND-IMAGE: url(images/scale_16x16.png); WIDTH: 16px; CURSOR: pointer; POSITION: absolute; HEIGHT: 16px; BACKGROUND-COLOR: #ececec"></DIV>
<DIV id=apf_id_11 style="POSITION: relative">
<DIV class=mapFinddingCanvas id=mapfindding_canvas></DIV>
<DIV class="mapfindding_canvas_toptip mapfindding_canvas_toptip_over" 
id=mapfindding_toptip>正在为您读取数据，请稍后...</DIV></DIV></DIV></DIV>
<DIV style="CLEAR: both"></DIV>
<DIV id=footer><!-- 增加seo块--><BR clear=all>
<DIV></DIV>
<DIV style="CLEAR: left"></DIV>
<DIV id=topic style="PADDING-TOP: 8px; TEXT-ALIGN: center">
</DIV>
<DIV style="CLEAR: left">
<DIV style="HEIGHT: 50px"></DIV></DIV><!-- m=app02-002--></DIV></DIV>
<SCRIPT language=JavaScript src="direction.js" type=text/javascript></SCRIPT>

<SCRIPT src="prototype_packed.js" type=text/javascript></SCRIPT>

<SCRIPT src="effects_packed.js" type=text/javascript></SCRIPT>

<SCRIPT src="controls_packed.js" type=text/javascript></SCRIPT>

 <script type="text/javascript" src="http://maps.google.cn/maps?file=api&amp;v=3&amp;key&amp;sensor=false&amp;hl=zh-CN"></script>




<SCRIPT src="dragdrop_packed.js" type=text/javascript></SCRIPT>

<SCRIPT src="markermanager_packed.js" type=text/javascript></SCRIPT>

<SCRIPT src="labeledmarker_packed.js" type=text/javascript></SCRIPT>

<SCRIPT src="extinfowindow_packed.js" type=text/javascript></SCRIPT>

<SCRIPT src="used.js" type=text/javascript></SCRIPT>

<SCRIPT type=text/javascript>
new function(){

var other_tt=$('apf_id_10_other_tt');

other_tt.observe('click',function(event){

other_tt.href=other_tt.href+document.location.hash;


} );


} 

document.observe("dom:loaded",function(){

function setFilter(s,value){

if(s==undefined){

return;


} 

for(i=0;

i<s.options.length;

i++){

if(s.options[i].value==value){

s.options[i].selected=true;

break;


} 

} 

} 

var params=document.location.hash.substring(1).toQueryParams();


} );

Event.observe("apf_id_10_select_area","change",function(){

var value=$("apf_id_10_select_area").value;

if(value!=0){

var lat=$("apf_id_10_select_area_"+value).readAttribute("lat");

var lng=$("apf_id_10_select_area_"+value).readAttribute("lng");

$("mapfindding_canvas").fire("map:pan",{

"lat":lat,"lng":lng

} );

var url="block.php?p1="+value+"&p2=apf_id_10";

new Ajax.Updater("apf_id_10_select_block",url);

$("apf_id_10_select_block").disabled=false;


} 

} );

Event.observe("apf_id_10_select_block","change",function(){

var value=$("apf_id_10_select_block").value;

if(value!=0){

var lat=$("apf_id_10_select_block_"+value).readAttribute("lat");

var lng=$("apf_id_10_select_block_"+value).readAttribute("lng");

$("mapfindding_canvas").fire("map:pan",{

"lat":lat,"lng":lng

} );


} 

} );

Event.observe("apf_id_10_select_price","change",function(){

var value=$("apf_id_10_select_price").value;

$("mapfindding_canvas").fire("set:price",{

"value":value

} );

$("mapfindding_canvas").fire("map:searchBounds");


} );

Event.observe("apf_id_10_select_range","change",function(){

var value=$("apf_id_10_select_range").value;

$("mapfindding_canvas").fire("set:range",{

"value":value

} );

$("mapfindding_canvas").fire("map:searchBounds");


} );

Event.observe("apf_id_10_select_model","change",function(){

var value=$("apf_id_10_select_model").value;

$("mapfindding_canvas").fire("set:model",{

"value":value

} );

$("mapfindding_canvas").fire("map:searchBounds");


} );

if($("apf_id_10_select_uestype")!=undefined){

Event.observe("apf_id_10_select_uestype","change",function(){

var value=$("apf_id_10_select_uestype").value;

$("mapfindding_canvas").fire("set:usetype",{

"value":value

} );

$("mapfindding_canvas").fire("map:searchBounds");


} );


} 

Event.observe("apf_id_10_map_search","click",function(){

var value="<!--{$cfg.page.city}--> "+$("apf_id_10_map_keywords").value;

$("mapfindding_canvas").fire("set:keywords",{

"value":value

} );


} );

Event.observe("apf_id_10_map_keywords","keyup",function(event){

var keyCode=event.keyCode;

if(keyCode==13){

var value=$("apf_id_10_map_keywords").value;

$("mapfindding_canvas").fire("set:keywords",{

"value":value

} );


} 

} );

Event.observe("apf_id_10_select_colum","click",function(){

var urltype=$("apf_id_10_hidden_colum").value;

var params=document.location.hash.substring(1).toQueryParams();

var pp="";

if(params.f1&&params.f1!="0"){

pp+="QQp3Z"+params.f1;


} 

if(params.f2&&params.f2!="0"){

pp+="QQp4Z"+params.f2;


} 

if(params.f3&&params.f3!="0"){

pp+="QQp5Z"+params.f3;


} 

if(params.f4&&params.f4!="0"){

pp+="QQp6Z"+params.f4;


} 

window.location="/rent/" 

return false;


} ) 

Event.observe(document,"dom:loaded",function(){

var tradeType=1;

var cityInfo={

lat:<!--{$cfg.page.lat}-->,lng:<!--{$cfg.page.lnt}-->,zoom:15,name:"<!--{$cfg.page.city}-->"

} ;

var finddingCanvas=new Anjuke.MapFindding.FinddingCanvas("mapfindding",tradeType,cityInfo);

finddingCanvas.drawCanvas();

finddingCanvas.drawMap();

finddingCanvas.queryBounds();

Event.observe(window,"resize",function(){

finddingCanvas.drawCanvas();

finddingCanvas.setBounds();

finddingCanvas.queryBounds();


} );


} );

Event.observe("apf_id_1_splitter","click",function(){

$("apf_id_1_filter").toggle();

$("apf_id_1_canvas").toggleClassName("mapFinddingCanvasBlock_wide");

$("header").toggle();

Element.fire("mapfindding_canvas","map:drawCanvas");


} );
</SCRIPT>
</BODY></HTML>
