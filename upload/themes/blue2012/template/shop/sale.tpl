<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--{include file="inc/head.tpl"}-->
<link href="<!--{$cfg.path.css}-->add.css" rel="stylesheet" type="text/css" />
<script language="javascript"> 
function changeSearch(formDom){
	location.href=formDom.value;
}
function quickSearch(domValue,domType){
	var inputDom = document.getElementById(domType);
	if(inputDom){
		inputDom.value=domValue;
	}
	var priceValue = document.getElementById('price').value;
	var roomValue = document.getElementById('room').value;
	var boroughidValue = document.getElementById('borough_id').value;
	var totalareaValue = document.getElementById('totalarea').value;
	var list_numValue = document.getElementById('list_num').value;
	var list_orderValue = document.getElementById('list_order').value;
	location.href='sale.php?id=<!--{$dataInfo.id}-->&borough_id='+boroughidValue+'&price='+priceValue+'&room='+roomValue+'&totalarea='+totalareaValue+'&list_num='+list_numValue+'&list_order='+list_orderValue;
	return false;
}
</script>
</head>
 
<body>
<!--{include file="inc/top.tpl"}-->
 
 <!--{include file="inc/shop_head.tpl"}-->
 
 <input type="hidden" id="price" name="price" value="<!--{$smarty.get.price}-->">
    <input type="hidden" id="search_type" name="search_type" value="<!--{$smarty.get.search_type}-->">
    <input type="hidden" id="cityarea" name="cityarea" value="<!--{$smarty.get.cityarea}-->">
    <input type="hidden" id="totalarea" name="totalarea" value="<!--{$smarty.get.totalarea}-->">
    <input type="hidden" id="room" name="room" value="<!--{$smarty.get.room}-->">
    <input type="hidden" id="list_num" name="list_num" value="10">
    <input type="hidden" id="list_order" name="list_order" value=""> 
    
    
    <input type="hidden" id="borough_id" name="borough_id" value="<!--{$smarty.get.borough_id}-->">
    
    
<div class="content_erji">
<div id="brok_c">
<li id=r1 class="liq"><a onclick="xz(1)" href="<!--{$cfg.url_shop}--><!--{$smarty.get.id}-->" onfocus=blur()>精选房源</a></li>
<li id=r2 class="liv"><a onclick="xz(2)" href="sale.php?id=<!--{$smarty.get.id}-->" onfocus=blur()>二手房源</a></li>
<li id=r3 class="liq"><a onclick="xz(3)" href="rent.php?id=<!--{$smarty.get.id}-->" onfocus=blur()>出租房源</a></li>
<li id=r3 class="liq"><a onclick="xz(4)" href="evaluate.php?id=<!--{$smarty.get.id}-->" onfocus=blur()>服务评价</a></li>
<li id=r5 class="liq"><a onclick="xz(5)" href="<!--{$cfg.url_shop}-->p-<!--{$dataInfo.id}-->.html" onfocus=blur()>关于我</a></li></ul>
</div>
<div id=h1 class="txt">
<div class="content">
<div class="Broker_left">
<div class="left_b">
<h3>房源分类</h3>
<div class="Broker_intr">
<ul>
 
<li><strong>按小区:</strong></li>
<li <!--{if $smarty.get.borough_id == ''}-->class="br_normal"<!--{/if}-->><span><a href="sale.php?id=<!--{$smarty.get.id}-->&price=<!--{$smarty.get.price}-->&room=<!--{$smarty.get.room}-->" >不限</a></span></li>
<!--{foreach from=$saleCountBorough key=key item=item}-->
                    	<li <!--{if $smarty.get.borough_id == $item.borough_id}-->class="br_normal"<!--{/if}-->><span><a href="sale.php?id=<!--{$dataInfo.id}-->&borough_id=<!--{$item.borough_id}-->&price=<!--{$smarty.get.price}-->&room=<!--{$smarty.get.room}-->"><!--{$item.borough_name}-->(<!--{$item.house_num}-->)</a></span></li>
                    	<!--{/foreach}-->
<div style="height:12px; clear:both; overflow:hidden"></div>
</ul>
</div>
<div style="height:12px; clear:both; overflow:hidden"></div>
<div class="Broker_intr">
<ul>
<li><strong>按售价:</strong></li>
<li <!--{if $smarty.get.price == ''}-->class="br_normal"<!--{/if}-->><span><a href="sale.php?id=<!--{$smarty.get.id}-->&borough_id=<!--{$smarty.get.borough_id}-->&room=<!--{$smarty.get.room}-->">不限</a></span></li>
<!--{foreach from=$saleCountPrice key=key item=item}-->
                    	<li <!--{if $smarty.get.price == $item.price}-->class="br_normal"<!--{/if}-->><span><a href="sale.php?id=<!--{$dataInfo.id}-->&borough_id=<!--{$smarty.get.borough_id}-->&price=<!--{$item.price}-->&room=<!--{$smarty.get.room}-->"><!--{$item.house_price}-->(<!--{$item.house_num}-->)</a></span></li>
                    	<!--{/foreach}-->
<div style="height:12px; clear:both; overflow:hidden"></div>
</ul>
</div>
<div style="height:12px; clear:both; overflow:hidden"></div>
<div class="Broker_intr" style="border-bottom:none;">
<ul>
<li><strong>按房型:</strong></li>
<li <!--{if $smarty.get.room == ''}-->class="br_normal"<!--{/if}-->><span>
<a href="sale.php?id=<!--{$smarty.get.id}-->&borough_id=<!--{$smarty.get.borough_id}-->&price=<!--{$smarty.get.price}-->" >不限</a></span></li>
<!--{foreach from=$saleCountRoom key=key item=item}-->
                    	<li <!--{if $smarty.get.room == $item.room}-->class="br_normal"<!--{/if}-->><span><a href="sale.php?id=<!--{$dataInfo.id}-->&borough_id=<!--{$smarty.get.borough_id}-->&price=<!--{$smarty.get.price}-->&room=<!--{$item.room}-->"><!--{$item.house_room}-->(<!--{$item.house_num}-->)</a></span></li>
                    	<!--{/foreach}-->
</ul>
</div>
<div style="height:12px; clear:both; overflow:hidden"></div>
</div>
</div>
<div class="jjr_right">
<div class="jjr_content">
<div id="jjr_xx_top">
<div class="bg_bg">全部房源</div>
<div class="jjr_er_next">
  <table border="0" cellpadding="0" cellspacing="0">
				<tr>
				  <td width="190" height="30">共找到 <span style="color:#61a00d; font-weight:bold;"><!--{$row_count}--></span> 套符合要求的房子</td>
					<td width="23" align="left">
                   <!--{if $pageno > 1}-->
                    	<a href="sale.php?id=<!--{$dataInfo.id}-->&letter=<!--{$smarty.get.letter}-->&room=<!--{$smarty.get.room}-->&totalarea=<!--{$smarty.get.totalarea}-->&price=<!--{$smarty.get.price}-->&type=<!--{$smarty.get.type}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->&pageno=<!--{$pageno-1}-->" onfocus=blur()><img src="<!--{$cfg.path.images}-->rl_page_Prev_bg.png" border="0" class="RL-Prev"/></a>
                        <!--{else}-->
                        <img src="<!--{$cfg.path.images}-->rl_page_Prev_null_bg.png" border="0" class="RL-Prev"/>
                        <!--{/if}-->
                    </td>
					<td id="RL_NoPage"><font><!--{$pageno}--></font>/<!--{$page_count}--></td>
				  <td width="23" align="right">
                  <!--{if $pageno < $page_count}--><a href="sale.php?id=<!--{$dataInfo.id}-->&letter=<!--{$smarty.get.letter}-->&room=<!--{$smarty.get.room}-->&totalarea=<!--{$smarty.get.totalarea}-->&price=<!--{$smarty.get.price}-->&type=<!--{$smarty.get.type}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->&pageno=<!--{$pageno+1}-->" onfocus=blur()><img src="<!--{$cfg.path.images}-->rl_page_Next_bg.png" border="0" class="RL-Next"/></a><!--{else}--><img src="<!--{$cfg.path.images}-->rl_page_Next_Null_bg.png" border="0" class="RL-Next"/><!--{/if}-->
                  </td>
				</tr>
	  </table></div>
</div>
</div>
<div class="jjr_content">
<div class="view_all" style="padding-top:3px; border-left:1px #b9d6f4 solid; border-right:1px #b9d6f4 solid;">
  <div class="all_fy_bgl"> 
  <a onclick="quickSearch('default','list_order');" onfocus="blur()" class="<!--{if $list_order_class=='default'}-->input_mrpxx<!--{else}-->input_mrpx<!--{/if}-->" href="javascript:void(0);">
    	默认排序
    </a> 
    <a onclick="quickSearch('<!--{$list_order_house_price}-->','list_order');return false;" onfocus="blur()" class="<!--{if $list_order_class=='house_price'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" href="javascript:void(0);">
    	总价<!--{if $list_order_class=='house_price'}--><!--{if $list_order_house_price=='house_price asc'}-->↓<!--{else}-->↑<!--{/if}--><!--{else}-->↑<!--{/if}-->
    </a> 
    <a onclick="quickSearch('<!--{$list_order_house_totalarea}-->','list_order');return false;" onfocus="blur()" class="<!--{if $list_order_class=='house_totalarea'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" href="javascript:void(0);">
    	面积<!--{if $list_order_class=='house_totalarea'}--><!--{if $list_order_house_totalarea=='house_totalarea asc'}-->↓<!--{else}-->↑<!--{/if}--><!--{else}-->↑<!--{/if}-->
    </a> 
    <a onclick="quickSearch('<!--{$list_order_avg_price}-->','list_order');return false;" onfocus="blur()" class="<!--{if $list_order_class=='avg_price'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" href="javascript:void(0);">
    	单价<!--{if $list_order_class=='avg_price'}--><!--{if $list_order_avg_price=='avg_price asc'}-->↓<!--{else}-->↑<!--{/if}--><!--{else}-->↑<!--{/if}-->
    </a> 
        <a onclick="quickSearch('<!--{$list_order_created_order}-->','list_order');return false;" onfocus="blur()" class="<!--{if $list_order_class=='created'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" href="javascript:void(0);">
    	时间
    </a> 
  </div>
</div>
<div class="jjr_yuanjiao">
<div class="h15"></div>
 
  	<!--{foreach from=$dataList item=item key=key}-->
	<!-- 循环体 -->
    <div style="height:6px; clear:both; overflow:hidden;"></div>
    <ol id="list_body">
<div class="property_jjr">
	    <div class="photo" style="margin-left:10px; margin-top:2px;">
	        <a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" alt="" target="_blank"><img class="thumbnail" id="apf_id_69_a" src="<!--{if $item.house_thumb}--><!--{$cfg.url_upfile}--><!--{$item.house_thumb}--><!--{else}--><!--{$cfg.path.images}-->housePhotoDefault.gif<!--{/if}-->" alt="" /></a>	    </div>
	    <div class="jjr_details">
	        <h4>
	            <a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="<!--{$item.borough_name}-->" target="_blank"><!--{$item.house_title}--></a><!--{if $item.is_hot==1}--><span><img src="<!--{$cfg.path.images}-->sale_rd.gif" title="热点" align="absmiddle" class="er_duotu"></span ><!--{/if}-->
				<!--{if $item.is_more_pic==1}--><span><img src="<!--{$cfg.path.images}-->duotu_sale.gif" titile="多图" align="absmiddle" class="er_duotu"></span ><!--{/if}--></h4>
	        <address>小区：<!--{$item.borough_name}--> </address><!--<a href="#" target="_blank" class="input_jjr"></a> -->
	        <div class="jjr_ppp"><p>售价：<span><!--{$item.house_price}-->万</span> </p><p>房型：<!--{$item.house_room}-->室<!--{$item.house_hall}-->厅</p><p>面积：<!--{$item.house_totalarea}-->平米</p><p>楼层：<!--{$item.house_floor}-->F/<!--{$item.house_topfloor}-->F</p><p>单价：<!--{$item.avg_price}-->元/平米</p><p>房龄：<!--{$item.house_age}-->年</p></div>
   	      </div>
	    <div class="jjr_tags">
        <!--{if $item.is_vexation==1}--><img src="<!--{$cfg.path.images}-->sale_js.gif" title="急售" alt="急售" /><!--{/if}-->&nbsp;<!--{if $item.is_new==1}--><img src="<!--{$cfg.path.images}-->newpush.gif" title="新推" alt="新推" /><!--{/if}-->
        </div>
    </div>
        <div style="height:6px; clear:both; overflow:hidden;"></div>
    <div class="hr"></div>
    </ol>
					<!-- 循环体 结束 -->
					<!--{/foreach}-->
 
		<div style="height:11px; clear:both; overflow:hidden;"></div>
    <div class="jjr-div">
		        <div class="contain">
		        	<div class="multipage">
                    <!--{$pagePanel}-->
                    </div>		        
                    </div>
		        <div class="result">共有  <span style="color:#61a00d; font-weight:bold;"><!--{$row_count}--></span> 套符合要求的房子</div>
   	  </div>
      <div style="height:13px; clear:both; overflow:hidden;"></div>
      <p align="right"></p>
      <span class="xj_c xj_c03"></span>
<span class="xj_c xj_c04"></span>     </div>
</div>
</div>
</div>
</div>
 
</div>
<!--{include file="inc/foot.tpl"}-->
</body>
</html>
