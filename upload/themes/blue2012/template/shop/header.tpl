<div class="topInfo">
	<div class="topInfoMain">
    	<div class="logo"><a href="../"><img src="<!--{$cfg.path.images}-->shop/topInfoLogo.gif" /></a></div>
		<div class="shopName"><!--{$dataInfo.realname}-->的网上店铺</div>
        <div class="link">
			<ul>
				 <li><a href="<!--{$cfg.url}-->" title=" <!--{$cfg.page.city}-->二手房首页">首页</a></li>	
					<li><a href="<!--{$cfg.url_sale}-->" title=" <!--{$cfg.page.city}-->二手房">二手房</a></li>
					<li><a href="<!--{$cfg.url_rent}-->" title=" <!--{$cfg.page.city}-->租房">租房</a></li>
					<li><a href="<!--{$cfg.url_newHouse}-->" title=" <!--{$cfg.page.city}-->新楼盘">新楼盘</a></li>
					<li><a href="<!--{$cfg.url_community}-->" title=" <!--{$cfg.page.city}-->小区房价">小区房价</a></li>
					<li><a href="<!--{$cfg.url_broker}-->" title=" <!--{$cfg.page.city}-->经纪人">经纪人</a></li>
                     <li><a href="<!--{$cfg.url_pinggu}-->" title=" <!--{$cfg.page.city}-->房屋估价">估价</a></li>
					<li><a href="<!--{$cfg.url_bbs}-->/" target="_blank" title=" <!--{$cfg.page.city}-->房屋论坛">论坛</a></li>
                
                
               
			</ul>
		</div>
    </div>
</div>