<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--{include file="inc/head.tpl"}-->
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.path.js}-->FormValid.js"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.path.js}-->FV_onBlur.js"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.path.js}-->jquery-1.2.6.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){$("#province").change(function(){$("#province option").each(function(i,o){if($(this).attr("selected")){$(".city").hide();$(".city").eq(i).show();}});});$("#province").change();});</script>
<link href="<!--{$cfg.path.css}-->region.css" rel="stylesheet" type="text/css" />

</head>
<body>
<!--{include file="inc/indextop.tpl"}-->
<div class="body">
  <div class="reg">
    <div class="reg_a">
      <div style="height:26px; clear:both; overflow:hidden; width:100%"></div>
      <div class="reg_b1">
        <div class="reg_b11"><font class="t20 b">公司平台登录</font></div>
      </div>
 
    </div>
    <div class="reg_b">

      <form onsubmit="return validator(this)" action="<!--{$cfg.url}-->company/login/login.php" method="post" name="loginform" id="loginform">
        <input type="hidden" value="login" name="action">
        <input type="hidden" value="" name="back_to">
        <div class="reg_b3">
          <div style="height:3px; clear:both; overflow:hidden"></div>
          <div class="reg_b32">
            <div class="reg_b321">用户名：</div>
            <div class="reg_b322">
              <input type="text" name="username" class="input_region">
            </div>
          </div>
          <div style="height:11px; clear:both; overflow:hidden;"></div>
          <div class="reg_b32">
            <div class="reg_b321">密&nbsp;&nbsp;码：</div>
            <div class="reg_b322">
              <input type="password" name="passwd" class="input_region">
            </div>
          </div>
          <div style="height:10px; clear:both; overflow:hidden;"></div>
          <div class="reg_b32">
            <div class="reg_b321">&nbsp;</div>
            <div class="reg_b324">
              <input name="notForget" value="1" type="checkbox" id="remberme" checked="checked" />
              &nbsp;下次直接登录（慎用） <span>
              <!--<a href="#">忘记密码？</a></span>-->
            </div>
          </div>
          <div style="height:4px; clear:both; overflow:hidden"></div>
          <div class="reg_b123"><font color="#FF0000">
            <!--{$errorMsg1}-->
            </font></div>
          <div class="reg_b32">
            <div class="reg_b321"></div>
            <div class="reg_b323">
              <input type="image" src="<!--{$cfg.path.images}-->login.jpg" />
            </div>
          </div>
          <div style="height:33px; clear:both; overflow:hidden"></div>
          <div class="reg_b33">
            <div style="height:10px; clear:both; overflow:hidden;"></div>
            <ul>
              <li><font class="b" style="font-size:13px; _font-size:12px;">登陆注册有问题请联系：<!--{$cfg.page.rexian}--></font></li>
              
             
            </ul>
            <div style="height:10px; clear:both; overflow:hidden"></div>
          </div>
        </div>
      </form>
    </div>
    <div class="reg_c"></div>
  </div>
</div>
<script type="text/javascript">

var  msgInfo  =  [];
msgInfo["username"] = '4-16个字符(包括小写字母、数字、下划线)<span class="cRed">(必填)</span>' ;
msgInfo["passwd"] = '6-20个字符组成(包括英文字母、数字、符号)，区分大小写。<span class="cRed">(必填)</span>' ;
msgInfo["passwd2"] = '请再输入一遍您上面输入的密码。<span class="cRed">(必填)</span>'; 
msgInfo["email"] = '重要！这是您找回密码的唯一方式！请留常用的Email。<span class="cRed">(必填)</span>'; 
msgInfo["valid"] = '请输入左侧显示的字符。<span class="cRed">(必填)</span>';

FormValid.showError = function(errMsg,errName,formName) {
	if (formName=='registerform') {
		for (key in FormValid.allName) {//alert('errMsg_'+FormValid.allName[key]);
			document.getElementById('errMsg_'+FormValid.allName[key]).innerHTML = '';
		}
		for (key in errMsg) {
			document.getElementById('errMsg_'+errName[key]).innerHTML = errMsg[key];
		}
	}
}

var r = null;
function ckname (inp) {
	$.ajax({type:"GET", url:"ajax.php?r="+Math.random()+'&action=unique&username='+inp.value, dataType:"text",async:false,success:function (msg){
		r = msg;
	}}); 
	if (r==0) {
		return false;
	} else {
		return true;
	}
}
</script>
<script type="text/javascript">
initValid(document.registerform);
</script>
<div class="blank"></div>
<!--{include file="inc/foot.tpl"}-->
</body>
</html>
