<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 房源管理</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<!--{include file="inc/companyHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/company_left.tpl"}-->
		<div class="memberBox">
			
			<div class="manageSub">
				<ul class="manageSubNav">
					<li><a href="manageSale.php"><span>出售中</span></a></li>
					<li class="linkOn"><a href="#"><span>回收站</span></a></li>
				</ul>
			</div>
			<div class="manageBox">
				<div class="houseSearch">
					<form name="searchForm" action="" method="GET">
						<input class="input" name="q" type="text" size="30" value="<!--{if $q}--><!--{$q}--><!--{else}-->输入房源编号或小区名称<!--{/if}-->" onblur="if(this.value ==''||this.value == '输入房源编号或小区名称'){this.value = '输入房源编号或小区名称';}" onfocus="if(this.value == '输入房源编号或小区名称'){this.value = '';}" />&nbsp;<input type="button" value="搜 索"  onclick="javascript:document.searchForm.submit();"  />&nbsp;<a href="#notice">回收站管理必读</a>
					</form>
				</div>
				<div class="houseList">
					<table border="0" cellpadding="0" cellspacing="1">
						<thead class="tableTitle">
							<tr>
								<td colspan="6">房源基本信息</td>
								<td>经纪人</td>
								<td>下架时间</td>
								<td>操作</td>
							</tr>
						</thead>
						<form name="myform" action="" method="POST">
						<input type="hidden" name="to_url" value="<!--{$to_url}-->">
						<thead class="tableSubTitle">
							<tr>
								<td width="13%">房源编号</td>
								<td width="20%">小区名称</td>
								<td width="10%">户型</td>
								<td width="8%">面积</td>
								<td width="8%">售价</td>
								<td width="10%">发布时间</td>
								<td width="10%">姓名</td>
								<td width="10%">&nbsp;</td>
								<td width="9%">&nbsp;</td>
							</tr>
						</thead>
						<tbody>
							<!--{foreach name=dataList from=$dataList item=item key=key}-->
							<tr>
								<td><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" />&nbsp;<a href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html" title="查看房源详细信息" target="_blank"><!--{$item.house_no}--></a></td>
								<td><!--{$item.borough_name}--></td>
								<td><!--{$item.house_room}-->-<!--{$item.house_hall}-->-<!--{$item.house_toilet}-->-<!--{$item.house_veranda}--></td>
								<td><!--{$item.house_totalarea}-->㎡</td>
								<td class="f90"><!--{if $item.house_price == 0}-->面议<!--{else}--><!--{$item.house_price}-->万<!--{/if}--></td>
								<td><!--{$item.created|date_format:'%Y-%m-%d'}--></td>
								<td><a target="_blank" href="<!--{$cfg.url}-->shop/<!--{$item.brokerInfo.id}-->"><!--{$item.brokerInfo.realname}--></a></td>
								<td><!--{if $item.house_downtime}--><!--{$item.house_downtime|date_format:'%Y-%m-%d'}--><!--{else}-->&nbsp;<!--{/if}--></td>
								<td><!--{if $item.status==2}--><a href="?action=onsell&id=<!--{$item.id}-->" title="重新上架房源">上架</a><!--{else}-->已过期<!--{/if}--></td>
							</tr>
							<!--{/foreach}-->
							<tr>
								<td colspan="9" class="listOperation">
								<label for="checkBoxAll"><input type="checkbox" id="checkBoxAll" name="checkBoxAll" value="1" onclick="checkAll(this);" />全选</label> 
								<input class="listOperationBtn" type="button" value="彻底删除选中的房源" onclick="if(confirm('你确定彻底删除选中的房源么？')){myform.action='?action=delete';document.myform.submit();}" />
								</td>
							</tr>
						</tbody>
						</form>
					</table>
					<!--{$pagePanel}-->
				</div>
			</div>
			<div class="note">
				<p><a name="notice" id="notice"></a>回收站管理必读：</p>
				<ul>
					<li>回收站内的房源均不对外展示；</li>
					<li>超过有效期(90天)的房源，不可再上架，请删除；</li>
					<li>有效期内的房源，点击“上架”后可重新对外展示；</li>
					<li>回收站中不提供批量上架功能，请经纪人逐一核实房源的有效性，逐条上架。无效房源请删除；</li>
					<li>回收站中进行“删除”操作，将彻底删除房源不可回收；</li>
				</ul>
			</div>
		</div>
	</div>
	
</div>
<script language="javascript">
function checkAll(formObj){
	if(formObj.checked){
		$('input[type=checkbox]').attr('checked', true);	
	}else{
		$('input[type=checkbox]').attr('checked', false);
	}
}
</script>
</body>
</html>