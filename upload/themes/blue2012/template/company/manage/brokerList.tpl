<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 经纪人管理</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->

</head>
<body>
<!--{include file="inc/companyHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/company_left.tpl"}-->
		<div class="memberBox">
		
		
<div class="manageBox">
				<div class="houseSearch">
					<form name="searchForm" action="" method="GET">
						<input type="text" class="input" name="q" size="30" value="<!--{if $q}--><!--{$q}--><!--{else}-->请输入经纪人姓名<!--{/if}-->" onblur="if(this.value ==''||this.value == '请输入经纪人姓名'){this.value = '请输入经纪人姓名';}" onfocus="if(this.value == '请输入经纪人姓名'){this.value = '';}" />&nbsp;<input type="button" value="搜 索" onclick="javascript:document.searchForm.submit();" />&nbsp;<a href="#notice">经纪人管理必读</a>
					</form>
				</div>
				<div class="houseList">
					<table border="0" cellpadding="0" cellspacing="1">
						<thead class="tableTitle">
							<tr>
								<td colspan="4">经纪人基本信息</td>
								<td colspan="3">统计信息</td>
								<td colspan="3">操作</td>
							</tr>
						</thead>
						<thead class="tableSubTitle">
							<tr>
								<td width="9%">用户名</td>
								<td width="11%">经纪人姓名</td>
								<td width="8%">电话</td>
								<td width="13%">门店</td>
								<td width="9%">积分</td>
								<td width="14%">出售</td>
								<td width="10%">出租</td>					
								<td width="26%">常用操作</td>
                                
							</tr>
						</thead>
						<tbody>
					
							<!--{foreach name=dataList from=$dataList item=item key=key}-->
							<tr>
								<td><a href="<!--{$cfg.url}-->shop/<!--{$item.id}-->" title="查看经纪人店铺" target="_blank"><!--{$item.username}--></a></td>
								<td><!--{$item.realname}--></td>
								<td><!--{$item.mobile}--></td>
								<td><!--{$item.outlet_last}--></td>
								<td class="f90"><!--{$item.scores}-->分</td>
								<td class="f90"><!--{$item.sell_num}-->套</td>
								<td class="f90"><!--{$item.rent_num}-->套</td>		
        <td><a href="?action=delete&id=<!--{$item.id}-->" title="点击后会将该经纪人踢出该公司">踢出公司</a></td>
						    
                                            </tr>
                                            <!--{/foreach}-->
                                            <tr>
                                                <td colspan="10" class="listOperation"> 
                                                </td>
                                            </tr>
                          
                                </tbody>
                            </table>
                            <!--{$pagePanel}-->
</div>
                    </div>
                    <div class="note">
                       <p><a name="notice" id="notice"></a>经纪人管理必读：</p>
				<ul>
					
					
					<li>所有经纪人均为经纪人自己加入公司；</li>
					<li>如果该经纪人不属于该公司可以踢出该公司；</li>
					<li>点击踢出该公司没有任何提示，直接踢出请慎重选择；</li>
					
				</ul>
			</div>
		</div>
	</div>
	
</div>
<script language="javascript">
function checkAll(formObj){
	if(formObj.checked){
		$('input[type=checkbox]').attr('checked', true);	
	}else{
		$('input[type=checkbox]').attr('checked', false);
	}
}
function showBox(item_id){
	TB_show('房源成交','saleBargain.php?house_id='+item_id+'&height=370&width=400&modal=true&rnd='+Math.random(),false);
}
function showBoxOwner(item_id){
	TB_show('业主信息','landlordSaleInfo.php?house_id='+item_id+'&height=150&width=400&modal=true&rnd='+Math.random(),false);
}
function showBoxtop(item_id){
	TB_show('房源置顶','saleTop.php?house_id='+item_id+'&height=200&width=400&modal=true&rnd='+Math.random(),false);
}
function showBoxnum(){
	TB_show('购买条数','saleNum.php?height=200&width=400&modal=true&rnd='+Math.random(),false);
}
</script>
<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
</body>
</html>
