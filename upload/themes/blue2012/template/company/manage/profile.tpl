<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 资料修改</title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
<script type="text/javascript">
function addToBoroughItem(bid,bname,b_addr){
	$("#borough_id").val(bid);
	$("#borough_name").val(bname);
	$("#borough_addr").val(b_addr);
	$("#borough_addr_tr").css("display","");
}
$().ready(function() {
	$("#borough_name").autocomplete("ajax.php?action=getBoroughList", {
		minChars: 2,
		width: 260,
		delay:0,
		mustMatch:true,
		matchContains: false,
		scrollHeight: 220,
		selectFirst:true,
		scroll: true,
		formatItem: function(data, i, total) {
			if(data[1]=="addBorough"){
				return '<strong>'+data[0]+'</strong>';
			}
			return data[0];
		}
	});
	
	$("#borough_name").result(function(event, data, formatted) {
		if(data[1]=="addBorough"){
			//TB_show('增加小区','#TB_inline?height=155&width=400&inlineId=modalWindow',false);
			TB_show('增加小区','addBorough.php?height=170&width=400&modal=true&rnd='+Math.random(),false);
			$(this).val('');
		}else{
			$("#borough_id").val(data[1]);
			$("#borough_addr").val(data[2]);
			$("#borough_addr_tr").css("display",""); 
		}
		
		/*if (data)
			$(this).parent().next().find("input").val(data[1]);*/
	});
});
</script>

</head>
<body>
<!--{include file="inc/companyHeader.tpl"}-->
<div class="main">
	<div class="page">
<!--{include file="inc/company_left.tpl"}-->
		<div class="memberBox">
		
		
			<form name="dataInfo" method="POST" action="?action=save">
			<input type="hidden" name="id" value="<!--{$dataInfo.id}-->">
			<table class="memberBoxTable" cellpadding="0" cellspacing="5" border="0">
				<thead>
					<tr>
						<td colspan="4"><span class="concentTitle">公司信息修改</span></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="row1">用 户 名：</td>
						<td colspan="3"><input id="username" class="input" name="company[username]" type="text" size="25" value="<!--{$dataInfo.username}-->" disabled="disabled" /></td>
					</tr>
       
                    <tr>
						<td class="row1"><span class="must">*</span>公司名称：</td>
						<td colspan="3">
                     
 <input id="realname" class="input" name="company[company_name]" type="text" size="25" value="<!--{$dataInfo.company_name}-->" disabled="disabled" />
 
                        
                        </td>
					</tr>
                    
					<tr>
						<td class="row1"><span class="must">*</span>公司地址：</td>
						<td colspan="3"><input id="company_address" class="input" name="company[company_address]" type="text" size="25" valid="required" errmsg="请输入公司地址!" value="<!--{$dataInfo.company_address}-->" />&nbsp;<span class="tip">公司的详细联系地址</span></td>
					</tr>
                         <tr>
						<td class="row1"><span class="must">*</span>联系人：</td>
						<td colspan="3"><input id="company_clerk" class="input" name="company[company_clerk]" type="text" size="25" valid="required" errmsg="请输入公司联系人!" value="<!--{$dataInfo.company_clerk}-->" />&nbsp;<span class="tip">公司负责业务的联系人</span></td>
					</tr>
                    
                    
                    <tr>
						<td class="row1"><span class="must">*</span>联系电话：</td>
						<td colspan="3"><input id="company_phone" class="input" name="company[company_phone]" type="text" size="25" valid="required" errmsg="请输入公司联系电话!" value="<!--{$dataInfo.company_phone}-->" />&nbsp;<span class="tip">公司的联系电话</span></td>
					</tr>
                        <tr>
						<td class="row1"><span class="must">*</span>在线QQ：</td>
						<td colspan="3"><input id="qq" class="input" name="company[qq]" type="text" size="25" valid="required" errmsg="请输入QQ!" value="<!--{$dataInfo.qq}-->" />&nbsp;<span class="tip">公司的联系电话</span></td>
					</tr>

                    <tr>
						<td class="row1"><span class="must">*</span>Email：</td>
						<td colspan="3"><input id="email" class="input" name="company[email]" type="text" size="25" valid="required|isEmail" errmsg="请输入Email地址!|Email地址格式不对!" value="<!--{$dataInfo.email}-->" />&nbsp;<span class="tip">请输入有效且常用的Email邮箱地址</span></td>
					</tr>
                    
                    
                   
                    
                    <tr>
						<td class="row1">LOGO上传：</td>
						<td colspan="3"><span class="tip">&nbsp;宽度240px 高度70px</span><br>
                        
                         <input type="hidden" name="company[company_logo]" id="image_url" value="<!--{$dataInfo.company_logo}-->" >
<input type="hidden" name="image_thumb" id="image_thumb" value="<!--{$dataInfo.image_thumb}-->">
		        	<div id="image_dis"><!--{if $dataInfo.company_logo}--><img src="<!--{$cfg.url}-->upfile/<!--{ $dataInfo.company_logo}-->" width="240" height="70" /><!--{/if}--></div>
		        
		        	<iframe name="uploadImage" width="100%" height="35" scrolling="No" frameborder="no"  src="<!--{$cfg.url}-->upload.php?to=uploadImage|company|logo" align="left"></iframe>
 <script language="javascript">
function uploadImage( furl,fname,thumbUrl ){
	
	document.getElementById('image_url').value = furl;
	document.getElementById('image_thumb').value = thumbUrl;
	document.getElementById('image_dis').innerHTML = '<img src="<!--{$cfg.url}-->upfile/'+furl+'" width="240" height="70">';
}
</script>
                        
                        </td>
					</tr>
                    
                    
						<tr>
						<td class="row1">业务描述：</td>
						<td colspan="3"><span class="tip">&nbsp;不超过200个汉字</span><br><textarea class="textarea" name="company[company_description]" cols="70" rows="8"><!--{$dataInfo.company_description}--></textarea></td>
					</tr>
					<tr><td colspan="4" class="br"></td></tr>
                    
                    <tr>
						<td class="row1">关于我们：</td>
						<td colspan="3"><span class="tip">&nbsp;不超过200个汉字</span><br><textarea class="textarea" name="company[company_about]" cols="70" rows="8"><!--{$dataInfo.company_about}--></textarea></td>
					</tr>
					<tr><td colspan="4" class="br"></td></tr>
					
                    
				</tbody>
			
			</table>
			</form>
			<div class="submitBtn"><input type="button" value="确认修改" onclick="javascript:if(validator(document.dataInfo)){document.dataInfo.submit();}" /></div>
		</div>
	</div>
	
</div>
</body>
</html>