<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--></title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->

<script language="javascript">
$().ready(function() {
	$("#borough_name").autocomplete("ajax.php?action=getBoroughList", {
		minChars: 2,
		width: 260,
		delay:0,
		mustMatch:true,
		matchContains: false,
		scrollHeight: 220,
		selectFirst:true,
		scroll: true,
		formatItem: function(data, i, total) {
					
			if(data[1]=="addBorough"){
				return '<strong>'+data[0]+'</strong>';
			}
			
			return data[0];
		}
	});
	
	$("#borough_name").result(function(event, data, formatted) {
		
		if(data[1]=="addBorough"){
			//TB_show('增加小区','#TB_inline?height=155&width=400&inlineId=modalWindow',false);
			//TB_show('增加小区','addBorough.php?height=170&width=400&modal=true&rnd='+Math.random(),false);
			window.open('index.php?action=add','addBorough_win');
			$(this).val('');
		}else{
			$("#borough_id").val(data[1]);
			/*
			$("#borough_addr").val(data[2]);
			$("#borough_addr_tr").css("display","");
			*/
		}
		
		/*if (data)
			$(this).parent().next().find("input").val(data[1]);*/
	});
});

</script>
</head>
<body>

<!--列表-->
<table cellpadding="2" cellspacing="1" class="table_info">
  <caption>小区管理</caption>
  <tr>
    <td><a href="check.php"><font class="red">待审核小区列表</font></a></td>
  </tr>
</table>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
<thead>
	<caption>小区信息管理</caption>
	<tr>
		<th width="30">选中</th>
		<th>小区名称</th>
		<th width="60">所在区域</th>
		<th>小区地址</th>
		<th>添加人</th>
		<th width="100">管理操作</th>
	</tr>
</thead>
<tbody>
<!--{foreach item=item from=$boroughList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->"></td>
	<td><a href="<!--{$cfg.url_community}-->g-<!--{$item.id}-->.html" target="_blank"><!--{$item.borough_name}--> </a> &nbsp;</td>
	<td class="align_c"><!--{$item.cityarea_id}--> </td>
	<td class="align_c"><!--{$item.borough_address}--></td>
	<td class="align_c"><!--{$item.creater}--></td>
	<td class="align_c">
		<a href="index.php?action=edit&id=<!--{$item.id}-->">补充资料</a>
	</td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<div style="width:100%">
		<div style="float:left; width:450px;">
			<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
			<input type="button" value=" 审核通过 " onclick="if(confirm('你确认审核通过选中的条目么？')){myform.action='check.php?action=check';myform.submit();}">
			<input type="button" value=" 审核不通过 " onclick="if(confirm('审核不通过为直接删除小区，如果其中有小区请删除，你确认删除审核不通过选中的条目么？')){myform.action='check.php?action=delete';myform.submit();}">
			
		</div>
		<div id="combine_div" style="float:left;display:none;">
			<input type="text" id="borough_name" name="borough_name" >
			<input type="hidden" id="borough_id" name="borough_id" >
			
		</div>
	</div>
</div>
<!--{$pagePanel}-->
</form>
<script language="javascript">
function showCombine(){
	$('#combine_div').toggle();
}
function combine_to_borough(){
	if(confirm('合并小区会把该小区的房源移动到目标小区中，你确认合并选中的条目么？')){
		myform.action='check.php?action=combine';
		myform.submit();
	}
	return true;
}
</script>
<!--{include file="admin/footer.tpl"}-->
