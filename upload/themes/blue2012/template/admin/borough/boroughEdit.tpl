<!--{include file="admin/header.tpl"}-->
<form id="editForm" name="editForm" method="post" action="index.php?action=save" onsubmit="return validator(this)">
<input type="hidden" name="id" value="<!--{$borough.id}-->">
<input type="hidden" name="to_url" value="<!--{$to_url}-->">
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>小区概要</caption>
	<tr>
		<th width="13%"><strong>小区名称</strong></th>
		<td width="37%">
   
<input name="borough[borough_name]" valid="required" class="inputtitle" type="text" id="name"  size="35" maxlength="30" value="<!--{$borough.borough_name}-->" errmsg="请填写小区名!" /></td>
        
		<th width="13%"><strong>小区别名</strong></th>
		<td width="37%"><input name="borough[borough_alias]" type="text" id="borough_alias"  size="35" maxlength="30" value="<!--{$borough.borough_alias}-->" /></td>
	</tr>

	<tr>
		<th width="13%"><strong>小区地址</strong></th>
		<td colspan="3">
  <input name="borough[borough_address]" type="text" id="address"  size="70" value="<!--{$borough.borough_address}-->" /></td>
	</tr>
   <!--{if $smarty.get.action==edit}-->
     <tr>
						<th><strong>地图坐标</strong></th>
						<td colspan="3">
							<link href="<!--{$cfg.path.css}-->add_map.css" type="text/css" rel="stylesheet" />
          <script type="text/javascript" src="<!--{$cfg.path.js}-->jquery-1.4.2.min.js"></script>
  
            <input name="borough[layout_map]" type="text" id="layout_map" value="<!--{$borough.layout_map}-->" size="50" /> <a href="http://www.yanwee.com/forum.php?mod=viewthread&tid=30794" target="_blank">如何手动获取坐标？</a>
            <div id="mapdiv">
            <div class='add'><span class="maptitle">添加位置</span><span style="float:right"></span></div>
            <div id='mark_mymap'></div>
            <div class='detail'>
           <span>拖动红色图标到小区所在位置</span> <input type="button" value="添加" onClick="changeImg()" />
            </div>
            </div>

		 <script type="text/javascript" src="http://maps.google.cn/maps?file=api&amp;v=3&amp;key&amp;sensor=false&amp;hl=zh-CN"></script>
			         
		   <script type="text/javascript" language="javascript">
		   function atmark() { //标注接口开始
		   $("#addmap").html('<img src="<!--{$cfg.path.images}-->button-f.gif">');
				var map = null;
				if (GBrowserIsCompatible()) { //判断是否生成
					var map = new GMap2(document.getElementById('mark_mymap'));
					map.setCenter(new GLatLng(<!--{$cfg.page.lat}-->,<!--{$cfg.page.lnt}-->), 11);
					map.addControl(new GSmallMapControl()); //是否显示缩放
					map.addControl(new GMapTypeControl()); //是否显示卫星地图
				}
				//map.clearOverlays(marker);   //清除地图上的标记点，否则会显示多个                        
				var Center = map.getCenter();
				var lat = new String(Center.lat());
				var lng = new String(Center.lng());
				setLatLng(lat, lng);
				var marker = new GMarker(new GLatLng(lat, lng), {draggable: true});
				GEvent.addListener(marker, "dragend", function() {
				var latlng = marker.getLatLng();
				lat = String(latlng.lat());
				lng = String(latlng.lng());
				setLatLng(lat, lng);
			});
			map.addOverlay(marker); // 写入标记到地图上
			}
			function setLatLng(lat,lng) {
				document.getElementById("layout_map").value='('+lat+','+lng+')';
			}
			function changeImg(){
				$('#mapdiv').hide();
				$("#addmap").html('<img src="<!--{$cfg.path.images}-->button-f.gif"><span style="color:red; margin-left:20px">添加成功!</span');
			}
			</script>
			<div id="addmap" onclick="$('#mapdiv').show();atmark();"><img src="<!--{$cfg.path.images}-->button-f.gif"></div>
							
							<p id="errMsg_house_mapid" class="errorMessage" display="none"></p>
						</td>
					</tr>  <!--{/if}-->
	<tr>
		<th><strong>区域</strong></th>
		<td><select name="borough[cityarea_id]" id="cityarea_id">	
			<option  value="">请选择区域</option>
			<!--{html_options options=$cityarea_option selected=$borough.cityarea_id }-->
			</select>
         <span id="quote">   
<select name="borough[cityarea2_id]">
<!--{if $borough.cityarea2_id=='' }-->   
<option value="">请选择</option>  
 <!--{else}-->
<option  value="<!--{$borough.cityarea2_id }-->"><!--{$borough.cityarea2_name}--></option>
 <!--{/if}-->
</select>   
</span>   
<script language="javascript">   
$("#cityarea_id").change(function(){    
$("#quote").load("../areas.php?id="+$("#cityarea_id").val());    
});   
</script> 
		</td>
		<th><strong>版块</strong></td>
		<td><select name="borough[borough_section]" id="borough_section">
			<option  value="">请选择版块</option>
			<!--{html_options options=$borough_section_option selected=$borough.borough_section }-->
			</select>
            
		</td>
	</tr>
	<tr>
		<th><strong>物业类型</strong></th>
		<td><select name="borough[borough_type]" id="borough_type">
			<option value="">请选择物业类型</option>
			<!--{html_options options=$borough_type_option selected=$borough.borough_type }-->
			</select>
		</td> 
		<th><strong>物业公司</strong></th>
		<td><input name="boroughInfo[borough_company]" type="text" id="borough_company" value="<!--{$boroughInfo.borough_company}-->" size="35"/></td>
	</tr>
	<tr>
		<th><strong>物&nbsp;业&nbsp;费</strong></th>
		<td><input name="boroughInfo[borough_costs]" type="text" id="borough_costs"  value="<!--{$boroughInfo.borough_costs}-->" size="15" maxlength="8" />元(/㎡/月)</td>
		<th><strong>开发商</strong></th>
		<td><input name="boroughInfo[borough_developer]" type="text" value="<!--{$boroughInfo.borough_developer}-->"  id="borough_developer" size="35"/></td> 
		  
	</tr>
	<tr>
		<th><strong>小区配套</strong></th>
		<td colspan="3">
			<!--{html_checkboxes name=boroughInfo[borough_support] options=$borough_support_option selected=$boroughInfo.borough_support separator="&nbsp;" }-->
		</td>
		<!--<td class="align_l">&nbsp;&nbsp;<span id="borough_thumb_dis"><!--{if $borough.borough_thumb}--><img src="<!--{$cfg.url}-->upfile/<!--{$borough.borough_thumb}-->" width="68" height="50"><!--{/if}--></span><input type="hidden" name="borough[borough_thumb]" value="<!--{$borough.borough_thumb}-->" id="borough_thumb" ><input type="button" name="add_thum" onclick="addThumbImg('uploadBoroughThumb|borough|thumb');return false;" value="上传小区缩略图" />--></td>
	</tr>
</table>			
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>周边配套</caption>
	<tr>
		<th width="13%"><strong>公交线路</strong></th>
		<td><input name="boroughInfo[borough_bus]" type="text" id="borough_bus" size="50" value="<!--{$boroughInfo.borough_bus}-->" /></td>
		<th width="13%"><strong>银行邮政</strong></th>
		<td><input name="boroughInfo[borough_bank]" type="text" id="borough_bank" size="50" value="<!--{$boroughInfo.borough_bank}-->" /></td>
	</tr>
	<tr>
		<th><strong>划片小学</strong></th>
		<td><input name="borough[elementary_school]" type="text" id="elementary_school" size="50" value="<!--{$borough.elementary_school}-->" /></td>
		<th ><strong>划片中学</strong></th>
		<td><input name="borough[middle_school]" type="text" id="middle_school" size="50" value="<!--{$borough.middle_school}-->" /></td>
	</tr>
	<tr>
		<th><strong>医院药房</strong></th>
		<td><input name="boroughInfo[borough_hospital]" type="text" id="borough_hospital" size="50" value="<!--{$boroughInfo.borough_hospital}-->" /></td>
		<th><strong>超市商场</strong></th>
		<td><input name="boroughInfo[borough_shop]" type="text" id="borough_shop" size="50" value="<!--{$boroughInfo.borough_shop}-->" /></td>
	</tr>
	<tr>
		<th><strong>餐馆酒店</strong></th>
		<td colspan="3"><input name="boroughInfo[borough_dining]" type="text" id="borough_dining" size="50"  value="<!--{$boroughInfo.borough_dining}-->" /></td>
		
	</tr>
	<tr>
		<th><strong>公园景观</strong></th>
		<td colspan="3">
		<!--{html_checkboxes name=boroughInfo[borough_sight] options=$borough_sight_option selected=$boroughInfo.borough_sight separator="&nbsp;" }-->
		</td>
	</tr>
</table>

<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>建筑数据</caption>
	<tr>
		<th width="13%"><strong>总建筑面积</strong></th>
		<td width="37%"><input name="boroughInfo[borough_totalarea]"  value="<!--{$boroughInfo.borough_totalarea}-->" type="text"  id="borough_totalarea" size="15" maxlength="15" /> 平米</td>
		<th width="13%"><strong>占地面积</strong></th>
		<td width="37%"><input name="boroughInfo[borough_area]"  value="<!--{$boroughInfo.borough_area}-->" type="text" id="borough_area" size="15" maxlength="15" /> 平米</td>
	</tr>
	<tr>
		<th><strong>总&nbsp;户&nbsp;数</strong></th>
		<td><input name="boroughInfo[borough_number]"  value="<!--{$boroughInfo.borough_number}-->" type="text" id="borough_number" size="15" maxlength="10" valid="isInt" errmsg="总户数必须为数字!" /> 户</td>
		<th><strong>绿&nbsp;化&nbsp;率</strong></th>
		<td><input name="boroughInfo[borough_green]"  value="<!--{$boroughInfo.borough_green}-->" type="text" id="borough_green" size="15" maxlength="10" /> %</td>
	</tr>
	<tr>
		<th><strong>容&nbsp;积&nbsp;率</strong></th>
		<td><input name="boroughInfo[borough_volume]"  value="<!--{$boroughInfo.borough_volume}-->" type="text" id="borough_volume" size="15" maxlength="10" /></td>
		<th><strong>停&nbsp;车&nbsp;位</strong></th>
		<td><input name="boroughInfo[borough_parking]"  value="<!--{$boroughInfo.borough_parking}-->" type="text" id="borough_parking" size="15" maxlength="10" /> 个</td>
	</tr>
	<tr>
		<th><strong>楼盘均价</strong></th>
		<td><input name="borough[borough_avgprice]"  value="<!--{$borough.borough_avgprice}-->" type="text" id="borough_avgprice" size="15" maxlength="15" /> 元/㎡</td>
		<th><strong>开盘时间</strong></th>
		<td><input name="borough[sell_time]"  value="<!--{$borough.sell_time}-->" type="text" id="sell_time" size="15" maxlength="10" onClick="WdatePicker({skin:'whyGreen'})" /></td>
	</tr>
	<tr>
		<th><strong>竣工时间</strong></th>
		<td colspan="3"><input name="boroughInfo[borough_completion]"  value="<!--{$boroughInfo.borough_completion}-->" type="text" id="borough_completion" size="15" maxlength="10" onClick="WdatePicker({skin:'whyGreen'})" /></td>
	</tr>
</table>

<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>小区介绍</caption>
	<tr>
		<td class="align_c"><textarea name="boroughInfo[borough_content]" id="borough_content" rows="12" style="padding:6px; width:98%; line-height:17px;"><!--{$boroughInfo.borough_content}--></textarea></td>
	</tr>
</table>

<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>小区图片</caption>
	<tr>
		<td colspan="2"><div id="borough_picture_dis">
		<!--{foreach name=borough_pic from=$borough.botough_picture item=item key=key}-->
			<div class="upload_shower" id="container_picture_<!--{$smarty.foreach.borough_pic.index}-->">
				<a href="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->"><img src="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->" width="160" height="120"></a>
				<br/><input type="text" name="borough_picture_desc[]" value="<!--{$item.pic_desc}-->" size="12">
				<input type="hidden" name="borough_picture_url[]" value="<!--{$item.pic_url}-->">
				<input type="hidden" name="borough_picture_thumb[]" value="<!--{$item.pic_thumb}-->">
				<input type="button" name="deletePicture_<!--{$smarty.foreach.borough_pic.index}-->" onclick="dropContainer('container_picture_<!--{$smarty.foreach.borough_pic.index}-->')" value="【※】">
			</div>
		<!--{/foreach}-->
		</div></td>
	</tr>
	<tr>
		<th width="30%"><strong>小区照片：</strong></th>
		<td><input type="button" name="add_picture" onclick="addThumbImg('uploadBoroughPicture|borough|picture');return false;" value="点击上传小区照片" /></td>
	</tr>
</table>

<table cellpadding="2" cellspacing="1" class="table_form">
    <caption>户型图</caption>
    <tr>
		<td colspan="2"><div id="borough_drawing_dis">
		<!--{foreach name=borough_draw from=$borough.botough_drawing item=item key=key}-->
			<div class="upload_shower" id="container_drawing_<!--{$smarty.foreach.borough_draw.index}-->">
				<a href="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->"><img src="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->" width="160" height="120"></a>
				<br/><input type="text" name="borough_drawing_desc[]" value="<!--{$item.pic_desc}-->" size="12">
				<input type="hidden" name="borough_drawing_url[]" value="<!--{$item.pic_url}-->">
				<input type="hidden" name="borough_drawing_thumb[]" value="<!--{$item.pic_thumb}-->">
				<input type="button" name="deleteDrawing_<!--{$smarty.foreach.borough_draw.index}-->" onclick="dropContainer('container_drawing_<!--{$smarty.foreach.borough_draw.index}-->')" value="【※】">
			</div>
		<!--{/foreach}-->
		</div></td>
	</tr>
    <tr>
		<th width="30%"><strong>户型图：</strong></th>
		<td><input type="button" name="add_drawing" onclick="addThumbImg('uploadBoroughDrawing|borough|drawing');return false;" value="点击上传小区户型图" /></td>
	</tr>
</table>
<br/>
<table cellpadding="2" cellspacing="1" class="table_form">
	<tr>
		<td height="50" class="align_c"><input type="button" name="Submit" value="保存小区信息" onclick="editForm.submit();" >&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="Return" value="返回小区列表" onclick="javascript:history.go(-1)"> </td>
	</tr>
</table>
</form>

<br/>
<table cellpadding="2" cellspacing="1" class="table_form">
    <caption>帮助</caption>
    <tr>
		<td ><div class="helper">
				<ul>
					<li>上传完小区缩略图后，会自动把这张图片增加到小区的照片中，所以无需重新上传这张照片。</li>
				</ul>
			</div>
		</td>
	</tr>
</table>
 
<script language="javascript">
function addThumbImg(targetFunc){
	var uploadWin = window.open('<!--{$cfg.url}-->upload.php?to='+targetFunc, 'uploadWindow','width=400,height=100,left=300,top=300,resizable=no,scrollbars=no');
	
}
function uploadBoroughThumb( furl,fname,thumbUrl ){
	document.getElementById('borough_thumb').value = furl;
	//document.getElementById('borough_thumb_desc').value = fname;
	document.getElementById('borough_thumb_dis').innerHTML = '<a href="<!--{$cfg.url}-->upfile/'+furl+'">\<img src="<!--{$cfg.url}-->upfile/'+thumbUrl+'" width="68" height="50"></a>';
}
var pictureIndex=<!--{$picture_num}-->;
function uploadBoroughPicture( furl,fname,thumbUrl ){
	document.getElementById('borough_picture_dis').innerHTML += '<div class="upload_shower" id="container_picture_'+pictureIndex+'">\
	<a href="<!--{$cfg.url}-->upfile/'+furl+'"><img src="<!--{$cfg.url}-->upfile/'+thumbUrl+'" width="160" height="120"></a>\
	<br/><input type="text" name="borough_picture_desc[]" value="<!--{$borough.borough_name}-->" size="15">\
	<input type="hidden" name="borough_picture_url[]" value="'+furl+'">\
	<input type="hidden" name="borough_picture_thumb[]" value="'+thumbUrl+'">\
	<input type="button" name="deletePicture_'+pictureIndex+'" onclick="dropContainer(\'container_picture_'+pictureIndex+'\')" value="【※】">\
	</div>';
	pictureIndex++;
}

var drawingIndex=<!--{$drawing_num}-->;
function uploadBoroughDrawing( furl,fname,thumbUrl ){
	document.getElementById('borough_drawing_dis').innerHTML += '<div class="upload_shower" id="container_drawing_'+drawingIndex+'">\
	<a href="<!--{$cfg.url}-->upfile/'+furl+'"><img src="<!--{$cfg.url}-->upfile/'+thumbUrl+'" width="180" height="120"></a>\
	<br/><input type="text" name="borough_drawing_desc[]" value="<!--{$borough.borough_name}-->" size="15">\
	<input type="hidden" name="borough_drawing_url[]" value="'+furl+'">\
	<input type="hidden" name="borough_drawing_thumb[]" value="'+thumbUrl+'">\
	<input type="button" name="deleteDrawing_'+drawingIndex+'" onclick="dropContainer(\'container_drawing_'+drawingIndex+'\')" value="【※】">\
	</div>';
	drawingIndex++;
}
function dropContainer(containerId){
	var containerEle = document.getElementById(containerId);
	var parentEle = containerEle.parentElement;
	parentEle.removeChild(containerEle);
}
</script> 

<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
<!--{include file="admin/footer.tpl"}-->
