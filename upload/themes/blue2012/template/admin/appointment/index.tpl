<!--{include file="admin/header.tpl"}-->
<!--列表-->
<form name="myform" method="post" action="">
    <table cellpadding="2" cellspacing="1" class="table_list">
        <caption>预约管理设置</caption>
        <tr>
            <th width="200">名称</th>
            <th width="400">值</th>
            <th>注释</th>
        </tr>
        <tbody>
            <tr>
                <td class="align_c">预约刷新间隔时间</td>
                <td class="align_c"><input type="text" name="appTime" value="<!--{$appo.appTime}-->"/></td>
                <td class="align_l">每隔多少分钟为一个预约刷新段(分钟/单位)</td>
            </tr>
            <tr>
                <td class="align_c">预约方案执行天数</td>
                <td class="align_c"><input type="text" name="appNum" value="<!--{$appo.appNum}-->" /></td>
                <td class="align_l">可以提前预约多少天</td>
            </tr>
            <tr>
                <td class="align_c">预约刷新权限分配</td>
                <td class="align_c">
					<input type="checkbox" value="0" name="appInterest[]" <!--{foreach from=$appo.appInterest item=item}-->
                                            <!--{if $item eq 0}-->checked='true'<!--{/if}--><!--{/foreach}--> />普通会员&nbsp;
					<input type="checkbox" value="1" name="appInterest[]" <!--{foreach from=$appo.appInterest item=item}-->
                                            <!--{if $item eq 1}-->checked='true'<!--{/if}--><!--{/foreach}--> />体验版VIP&nbsp;
					<input type="checkbox" value="2" name="appInterest[]" <!--{foreach from=$appo.appInterest item=item}-->
                                            <!--{if $item eq 2}-->checked='true'<!--{/if}--><!--{/foreach}--> />标准版VIP
                </td>
                <td class="align_l">选择可以使用此功能的会员</td>
            </tr>
            <tr>
                <td class="align_c">时间段预约刷新总次数</td>
                <td class="align_c"><input type="text" name="appCountNum" value="<!--{$appo.appCountNum}-->"/></td>
                <td class="align_l">每个时间段最多可以预约刷新多少个房源</td>
            </tr>
            <tr>
                <td colspan="3" class="align_c"><input type="submit" value="提交"/></td>
            </tr>
        </tbody>
    </table>
</form>
<!--{include file="admin/footer.tpl"}-->