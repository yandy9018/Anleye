<!--{include file="admin/header.tpl"}-->
<form name="editForm" method="post" action="price.php?action=save">


<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>增值套餐介绍</caption>
	<tr>
		<th width="13%"><strong>套餐1售价</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip1_price]" type="text"  size="30" value="<!--{$webConfig.vip_vip1_price}-->" /> <font color="#999999">套餐1（体验套餐）价格</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>套餐1出售条数</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip1_sale_num]" type="text"  size="30" value="<!--{$webConfig.vip_vip1_sale_num}-->" /> <font color="#999999">套餐1（体验套餐）的经纪人可发布出售的条数</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>套餐1出租条数</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip1_rent_num]" type="text"  size="30" value="<!--{$webConfig.vip_vip1_rent_num}-->" /> <font color="#999999">套餐1（体验套餐）的经纪人可发布出租的条数</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>套餐1急售标签条数</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip1_vexation]" type="text"  size="30" value="<!--{$webConfig.vip_vip1_vexation}-->" /> <font color="#999999">套餐1（体验套餐）的经纪人可发布急售标签的条数</font></td>
	</tr>
			<tr>
		<th width="13%"><strong>套餐1刷新次数</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip1_refresh]" type="text"  size="30" value="<!--{$webConfig.vip_vip1_refresh}-->" /> <font color="#999999">套餐1（体验套餐）的经纪人房源可刷新次数</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>套餐2售价</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip2_price]" type="text"  size="30" value="<!--{$webConfig.vip_vip2_price}-->" /> <font color="#999999">套餐2（标准套餐）价格</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>套餐2出售条数</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip2_sale_num]" type="text"  size="30" value="<!--{$webConfig.vip_vip2_sale_num}-->" /> <font color="#999999">套餐2（标准套餐）的经纪人可发布出售的条数</font></td>
	</tr>
    <tr>
		<th width="13%"><strong>套餐2出租条数</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip2_rent_num]" type="text"  size="30" value="<!--{$webConfig.vip_vip2_rent_num}-->" /> <font color="#999999">套餐2（标准套餐）的经纪人可发布出租的条数</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>套餐2急售标签条数</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip2_vexation]" type="text"  size="30" value="<!--{$webConfig.vip_vip2_vexation}-->" /> <font color="#999999">套餐2（标准套餐）的经纪人可发布急售标签的条数</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>套餐2刷新次数</strong></th>
		<td colspan="3">
  <input name="webConfig[vip_vip2_refresh]" type="text"  size="30" value="<!--{$webConfig.vip_vip2_refresh}-->" /> <font color="#999999">套餐2（标准套餐）的经纪人房源可刷新次数</font></td>
	</tr>
	
</table>
<table cellpadding="2" cellspacing="1" class="table_form">
	<caption>房源条数价格</caption>
	<tr>
		<th width="13%"><strong>出售房源置顶单价</strong></th>
		<td colspan="3">
  <input name="webConfig[alipay_sell_price]" type="text"  size="30" value="<!--{$webConfig.alipay_sell_price}-->" /> <font color="#999999">经纪人购买出售房源置顶业务每天的价格</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>出租房源置顶单价</strong></th>
		<td colspan="3">
  <input name="webConfig[alipay_rent_price]" type="text"  size="30" value="<!--{$webConfig.alipay_rent_price}-->" /> <font color="#999999">经纪人购买出租房源置顶业务每天的价格</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>购买出售条数单价</strong></th>
		<td colspan="3">
  <input name="webConfig[alipay_sell_num]" type="text"  size="30" value="<!--{$webConfig.alipay_sell_num}-->" /> <font color="#999999">经纪人购买出售房源容量每条的价格</font></td>
	</tr>
	<tr>
		<th width="13%"><strong>购买出租条数单价</strong></th>
		<td colspan="3">
  <input name="webConfig[alipay_rent_num]" type="text"  size="30" value="<!--{$webConfig.alipay_rent_num}-->" /> <font color="#999999">经纪人购买出租房源容量每条的价格</font></td>
	</tr>
	
</table>
<br/>
<table cellpadding="2" cellspacing="1" class="table_form">
	<tr>
		<td height="50" class="align_c"><input type="Submit" value="保存配置"></td>
	</tr>
</table>
</form>


<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
<!--{include file="admin/footer.tpl"}-->
