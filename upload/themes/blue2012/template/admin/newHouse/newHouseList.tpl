<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>新盘管理</caption>
  <tr>
    <td><a href="index.php?check=1"><!--{if $smarty.get.check==1}--><font class="red">全部新盘</font><!--{else}-->全部新盘<!--{/if}--></a> | <a href="index.php?check=2"><!--{if $smarty.get.check==2}--><font class="red">促销楼盘</font><!--{else}-->促销楼盘<!--{/if}--></a> | <a href="index.php?check=3"><!--{if $smarty.get.check==3}--><font class="red">推广楼盘</font><!--{else}-->推广楼盘<!--{/if}--></a> | <a href="index.php?check=4"><!--{if $smarty.get.check==4}--><font class="red">即将开盘</font><!--{else}-->即将开盘<!--{/if}--></a> | <a href="index.php?action=add">添加新盘</a></td>
  </tr>
</table>

<form name="search" method="get" action="">
<table cellpadding="0" cellspacing="1" class="table_form">
	<caption>信息查询</caption>
	<tr>
		<td class="align_c">
			<select name='cityarea'>
				<option value="">所在区域</option>
				<!--{foreach from=$areaLists key=key item=item}-->
				<!--{if $cityarea==$key}-->
				<option value="<!--{$key}-->" selected><!--{$item}--></option>
				<!--{else}-->
				<option value="<!--{$key}-->"><!--{$item}--></option>
				<!--{/if}-->
				<!--{/foreach}-->
			</select>
			<input type="text" name="q" onblur="if(this.value ==''||this.value == '请输入新盘名称,新盘地址'){this.value = '请输入新盘名称,新盘地址';this.style.color = '#999999';}" onfocus="if(this.value == '请输入新盘名称,新盘地址'){this.value = '';this.style.color = '#333333';}" value="<!--{if $q}--><!--{$q}--><!--{else}-->请输入新盘名称,新盘地址<!--{/if}-->" size="35" />&nbsp;
			<input type="submit" name="dosubmit" value=" 查询 " />
		</td>
	</tr>
</table>
</form>

<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>新盘信息管理</caption>
	<tr>
		<th width="30">选中</th>
		<th>新盘名称</th>
		<th width="60">所在区域</th>
		<th>所在板块</th>
        <th>推广</th>
         <th>促销</th>
		<th>新盘地址</th>
		<th>添加人</th>
		<th width="100">管理操作</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.borough_name}-->"></td>
	<td><a href="../../newHouse/detail.php?id=<!--{$item.id}-->" target="_blank"><!--{$item.borough_name}--> </a> &nbsp;</td>
	<td class="align_c"><!--{$item.cityarea_id}--> </td>
	<td class="align_c"><!--{$item.borough_section}--></td>
    <td class="align_c"><!--{if $item.is_promote == 1}--><font color="#FF0000">是</font><!--{else}-->否<!--{/if}--></td>
      <td class="align_c"><!--{if $item.is_priceoff == 1}--><font color="#FF0000">是</font><!--{else}-->否<!--{/if}--></td>
    
	<td class="align_c"><!--{$item.borough_address}--></td>
	<td class="align_c"><!--{$item.creater}--></td>
	<td class="align_c">
		 <a href="?action=edit&id=<!--{$item.id}-->">修改</a> | <a href="?action=intention&id=<!--{$item.id}-->">意向</a> | <a href="?action=news&id=<!--{$item.id}-->">动态</a>
	</td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=delete';myform.submit();}">
    <input type="button" name="delete" value=" 推广 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=promote&value=1';myform.submit();}">
      <input type="button" name="delete" value=" 取消推广 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=promote&value=0';myform.submit();}">
        <input type="button" name="delete" value=" 促销 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=priceoff&value=1';myform.submit();}">
            <input type="button" name="delete" value=" 取消促销 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=priceoff&value=0';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
