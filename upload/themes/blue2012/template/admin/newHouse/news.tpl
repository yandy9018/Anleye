<!--{include file="admin/header.tpl"}-->

<form name="addItem" method="post" action="index.php?action=newsSave" onsubmit="return validator(this)">
<input type="hidden" name="borough_id" value="<!--{$smarty.get.id}-->" />
<table cellpadding="1" class="table_form">
	<caption>增加动态</caption>
	<tr>
		<td>

动态类型：<input name="type" value="<!--{$dataInfo.type}-->" valid="required" noValue="0" errmsg="类型不能为空!" />
动态标题：<input name="title" value="<!--{$dataInfo.title}-->" valid="required" noValue="0" errmsg="标题不能为空!" />
<input type="submit" value="保存" />  <font color="#FF0000">注意：添加之后不可修改只能删除</font>
		</td>
	</tr>
</table>
</form>

<form name="myform" method="post" action="index.php?action=deleteNews">
<table cellpadding="2" cellspacing="1" class="table_list">
 <caption>新闻动态列表</caption>
  <tr>
    <th width="30">选中</th>
    <th width="50">类型</th>
    <th width="188">标题</th>
     <th width="52">添加时间</th>
  </tr>
<!--{foreach item=item from=$boroughNewsList}-->
  <tr>
    <td class="align_c"><input type="checkbox" name="ids[]" value="<!--{$item.id}-->"></td>
    <td class="align_c"><!--{$item.type}--></td>
    <td class="align_c"><!--{$item.title}--></td>
     <td class="align_c"><!--{$item.time|date_format:"%Y-%m-%d"}--></td>
  </tr>
<!--{/foreach}-->
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.submit();}">
</div>
</form>
<!--{$pagePanel}-->
<!--{include file="admin/footer.tpl"}-->
