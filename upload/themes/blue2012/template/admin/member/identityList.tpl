<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>身份认证审核管理</caption>
  <tr>
    <td>
     <a href="identity.php"><!--{if !isset($smarty.get.status) }--><font class="red">全部</font><!--{else}-->全部<!--{/if}--></a> | 
     <a href="identity.php?status=0"><!--{if isset($smarty.get.status) && $smarty.get.status==0}--><font class="red">未审核</font><!--{else}-->未审核<!--{/if}--></a> | 
     <a href="identity.php?status=1"><!--{if $smarty.get.status==1}--><font class="red">已审核</font><!--{else}-->已审核<!--{/if}--></a> 
     
    </td>
  </tr>
</table>
<!--
<form name="search" method="get" action="identity.php">
<input type="hidden" name="action" value="search">
<table cellpadding="0" cellspacing="1" class="table_form">
	<caption>信息查询</caption>
	<tr>
		<td class="align_c">
			<input type="text" name="q" onblur="if(this.value ==''||this.value == '请输入申请人姓名'){this.value = '请输入申请人姓名';this.style.color = '#999999';}" onfocus="if(this.value == '请输入申请人姓名'){this.value = '';this.style.color = '#333333';}" value="<!--{if $q}--><!--{$q}--><!--{else}-->请输入申请人姓名<!--{/if}-->" size="35" />&nbsp;
			<input type="submit" name="dosubmit" value=" 查询 " />
		</td>
	</tr>
</table>
</form>
-->
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>信息列表</caption>
	<tr>
		<th width="30">选中</th>
		
		<th>身份证号码</th>
		<th>身份证图片</th>
		<th>申请人用户名</th>
		<th>状态</th>
		<th>申请时间</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.username}-->"></td>
	<td class="align_c"><!--{$item.idcard}--><!--{if $item.idcard_exist}--><font color="red"><a href="<!--{$cfg.url_shop}--><!--{$item.idcard_exist}-->">[已存在]</a></font><!--{/if}--></td>
	<td class="align_c">
		<!--{if $item.idcard_pic}-->
			<a href="#" onmouseover="showPic('<!--{$item.idcard_pic}-->');return false;">
				<img src="<!--{$cfg.url}-->upfile/<!--{$item.idcard_pic}-->" width="80" height="64">
			</a>
		<!--{/if}-->
	</td>
	<td class="align_c"><a href="<!--{$cfg.url_shop}--><!--{$item.user.id}-->"><!--{$item.user.username}--><!--{if $item.user.realname}-->[<!--{$item.user.realname}-->]<!--{/if}--></td>
	<td class="align_c"><!--{if $item.status==0}-->正常<!--{/if}--><!--{if $item.status==1}--><font color="red">已通过</font><!--{/if}--><!--{if $item.status==2}--><font color="red">已删除</font><!--{/if}--></td>
	<td class="align_c"><!--{$item.add_time|date_format:"%Y-%m-%d %T"}--></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="lock" value=" 审核通过 " onclick="if(confirm('你确认审核通过选中的条目么？')){myform.action='identity.php?action=status&dostatus=1';myform.submit();}">
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='identity.php?action=delete';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<div id="pic_shower" style="position:absolute;color:white; width:640px; right:-30px; top:-10px; z-index:10000;background-color:#6680aa;display:none;"></div>
<script language="javascript">
function showPic(picUrl){
	//alert('aaaa');
	if (!e) var e = window.event;

	if(!document.all){
        mouse_x=e.pageX;
        mouse_y=e.pageY;
    }else{
        mouse_x=document.body.scrollLeft+event.clientX;
        mouse_y=document.body.scrollTop+event.clientY;
    }
    //便于跟踪调试
    $('#pic_shower').html('<img src="<!--{$cfg.url}-->upfile/'+picUrl+'">');
    $('#pic_shower').css('left',mouse_x+10).css('top',mouse_y+10).css('display','block');

}
function hidePic(){
	//alert('bbb');
	$('#pic_shower').css('display','none');
}
$(document).click(function(){hidePic();});
</script>
<!--{include file="admin/footer.tpl"}-->
