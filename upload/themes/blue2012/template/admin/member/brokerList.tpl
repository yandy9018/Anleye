<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>经纪人管理</caption>
  <tr>
    <td>
    <a href="index.php"><!--{if $smarty.get.status==0 && $smarty.get.isindex==0 && $smarty.get.avatar==0 && $smarty.get.identity==0}--><font class="red">全部用户</font><!--{else}-->全部用户<!--{/if}--></a> | 
     <a href="index.php?avatar=1"><!--{if $smarty.get.avatar==1}--><font class="red">有头像用户</font><!--{else}-->有头像用户<!--{/if}--></a> |
     <a href="index.php?identity=1"><!--{if $smarty.get.identity==1}--><font class="red">已身份认证</font><!--{else}-->已身份认证<!--{/if}--></a>
       <hr>
    <a href="index.php?vip=1"><!--{if $smarty.get.vip==1}--><font class="red">体验套餐</font><!--{else}-->体验套餐<!--{/if}--></a> |
      <a href="index.php?vip=2"><!--{if $smarty.get.vip==2}--><font class="red">标准套餐</font><!--{else}-->标准套餐<!--{/if}--></a>
    </td>
  </tr>
</table>

<form name="search" method="get" action="index.php">
<input type="hidden" name="action" value="search">
<table cellpadding="0" cellspacing="1" class="table_form">
	<caption>信息查询</caption>
	<tr>
		<td class="align_c">
			用户名：<input type="text" name="username" value="<!--{$smarty.get.username}-->" size="10" />&nbsp;
			姓名：<input type="text" name="realname" value="<!--{$smarty.get.realname}-->" size="10" />&nbsp;
			电话号码：<input type="text" name="tel" value="<!--{$smarty.get.tel}-->" size="10" />&nbsp;
			email：<input type="text" name="email" value="<!--{$smarty.get.email}-->" size="10" />&nbsp;
			身份证：<input type="text" name="idcard" value="<!--{$smarty.get.idcard}-->" size="10" />&nbsp;
			公司/门店：<input type="text" name="com" value="<!--{$smarty.get.com}-->" size="10" />&nbsp;
		</td>
		<td>
			<input type="submit" name="dosubmit" value=" 查询 " />
		</td>
	</tr>
</table>
</form>

<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>用户信息管理</caption>
	<tr>
		<th width="30">选中</th>
		<th>用户名</th>
		<th>姓名</th>
		<th>头像</th>
		<th>电话</th>
		<th>Email</th>
		<th>用户套餐</th>
		<th>积分</th>
		<th>状态</th>
		<th>帐号状态</th>
		<th width="100">管理操作</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.username}-->"></td>
	<td><a href="<!--{$cfg.url_shop}--><!--{$item.id}-->" target="_blank"><!--{$item.username}--> </a></td>
	<td class="align_c">
		<a href="changeName.php?id=<!--{$item.id}-->" onclick="showBox('<!--{$item.id}-->');return false;">

		<!--{$item.member_info.realname}-->

		</a>
	</td>
	<td class="align_c"><!--{if $item.member_info.avatar}--><img src="<!--{$cfg.url}-->upfile/<!--{$item.member_info.avatar}-->" width="50" height="62"><!--{/if}--> </td>
	<td class="align_l">手机：<!--{$item.member_info.mobile}--><!--{if $item.member_info.com_tell <> ''}--><br/>公司电话：<!--{$item.member_info.com_tell}--><!--{/if}--> </td>
	<td class="align_c"><!--{$item.email}--> </td>
	<td class="align_c">
   
    <!--{if $item.vip==1}-->
   <font color="red">体验套餐</font>
    <!--{elseif $item.vip==2}-->
   <font color="red">标准套餐</font>
    <!--{else}-->
    普通用户
     <!--{/if}-->

    </td>
	<td class="align_c"><!--{$item.scores}--></td>
	<td class="align_c"><!--{if $item.status==0}-->正常<!--{else}--><a href="index.php?status=1"><font color="red">未开通</font></a><!--{/if}--></td>
	<td class="align_c">最后登陆:<!--{$item.last_login|date_format:"%Y-%m-%d %H：%M"}--><br/>注册时间:<!--{$item.add_time|date_format:"%Y-%m-%d %H：%M"}--></td>
	<td class="align_c">
		<a href="?action=view&id=<!--{$item.id}-->">详细</a> | <a href="changePw.php?id=<!--{$item.id}-->" onclick="showBox1('<!--{$item.id}-->');return false;">密码</a> | <a href="changeSc.php?id=<!--{$item.id}-->" onclick="showBox2('<!--{$item.id}-->');return false;">积分</a> | <a href="changeVip.php?id=<!--{$item.id}-->" onclick="showBox4('<!--{$item.id}-->');return false;">套餐</a> | <a href="changeMoney.php?id=<!--{$item.id}-->" onclick="showBox3('<!--{$item.id}-->');return false;">充值</a>
	</td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
    
     <input type="button" name="open" value=" 开通 " onclick="if(confirm('确定开通该账户的使用权限？')){myform.action='index.php?action=status&dostatus=0';myform.submit();}">
     
    <input type="button" name="delete" value=" 删除 " onclick="if(confirm('注意：尽量不要删除会员，该会员所有的相关记录以及发布的房源都将呗删除')){myform.action='index.php?action=delete';myform.submit();}">
	<input type="button" name="avatar" value=" 删除用户头像 " onclick="if(confirm('你确认删除选中的用户的头像么？')){myform.action='index.php?action=avatar';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<script language="javascript">
function showBox(item_id){
	TB_show('修改用户姓名','changeName.php?id='+item_id+'&height=200&width=400&modal=true&rnd='+Math.random(),false);
}
function showBox2(item_id){
	TB_show('修改用户积分','changeSc.php?id='+item_id+'&height=200&width=400&modal=true&rnd='+Math.random(),false);
}
function showBox4(item_id){
	TB_show('修改用户套餐','changeVip.php?id='+item_id+'&height=200&width=400&modal=true&rnd='+Math.random(),false);
}
function showBox3(item_id){
	TB_show('为用户充值','changeMoney.php?id='+item_id+'&height=200&width=400&modal=true&rnd='+Math.random(),false);
}
function showBox1(item_id){
	TB_show('修改用户密码','changePw.php?id='+item_id+'&height=150&width=400&modal=true&rnd='+Math.random(),false);
}
</script>
<!--{include file="admin/footer.tpl"}-->
