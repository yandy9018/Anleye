<!--{include file="admin/header.tpl"}-->
<!--列表-->
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>评价信息管理</caption>
	<tr>
		<th width="30">选中</th>
		<th>经纪人姓名</th>
		<th>房屋信息</th>
		<th>服务态度</th>
		<th>业务水平</th>
		<th>评价内容</th>
		<th>经纪人回复</th>
		<th>评价时间</th>
		
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.username}-->"></td>
	
    <td><a href="<!--{$cfg.url_shop}--><!--{$item.id}-->" target="_blank"><!--{$item.broker_info.realname}--> </a></td>
	<td class="align_c">
		<!--{$item.accurate}--> 分
	</td>
	<td class="align_c"><!--{$item.satisfaction}--> 分</td>
	<td class="align_l"><!--{$item.specialty}--> 分</td>
	<td class="align_c"><!--{if $item.content}--><!--{$item.content}--><!--{else}-->无内容<!--{/if}--> </td>
	<td class="align_c"><!--{if $item.reply}--><!--{$item.reply}--><!--{else}-->无回复<!--{/if}--> </td>
	<td class="align_c"><!--{$item.time|date_format:"%m月%d日"}--></td>

  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>

 <input type="button" name="delete" value=" 删除 " onclick="if(confirm('确认删除该条信息么')){myform.action='evaluate.php?action=delete';myform.submit();}">

</div>
<!--{$pagePanel}-->

</form>

<!--{include file="admin/footer.tpl"}-->
