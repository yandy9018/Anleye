<!--{include file="admin/header.tpl"}-->
<form action="" method="post" onsubmit="return beforesubmit();">
<table cellpadding="1" cellspacing="1"  class="table_form">
 <caption>修改密码</caption>
  <tr>
    <th><strong>旧密码：</strong></th>
    <td><label>
      <input type="password" name="oldpwd" id="oldpwd">
    </label></td>
  </tr>
  <tr>
    <th><strong>新密码：</strong></th>
    <td><label>
      <input name="newpwd" type="password" id="newpwd" maxlength="32">
    </label></td>
  </tr>
  <tr>
    <th><strong>确认密码：</strong></th>
    <td align="left"><label>
      <input name="checkpwd" type="password" id="checkpwd" maxlength="32">
    </label><span id="spanerror" style="color:red;"></span>&nbsp;</td>
  </tr>
  <tr>
    <th>&nbsp;</th>
    <td><label>
      <input type="submit" name="button" id="button"  class="button_b" value="提交">
    </label></td>
  </tr>
</table>
</form>
<!--{include file="admin/footer.tpl"}-->
<script type="text/javascript">
function beforesubmit(){
	if($.trim($('#oldpwd').val()) == ''){
		alert('请输入旧密码！');
		return false;
	}
	if($.trim($('#newpwd').val()) == ''){
		alert('请输入新密码！');
		return false;
	}
	if($.trim($('#checkpwd').val()) == ''){
		alert('请对新密码进行确认！');
		return false;
	}

	if($.trim($('#newpwd').val()).toLowerCase() == $.trim($('#checkpwd').val()).toLowerCase()){
		$('#newpwd').val($('#newpwd').val().toLowerCase());
		$('#checkpwd').val($('#checkpwd').val().toLowerCase());
		return true;
	}else{
		//alert('新密码不相符，请重新输入');
		$('#spanerror').text('新密码确认不相符，请重新输入');
		$('#checkpwd').val('');
		$('#newpwd')[0].focus();
		return false;
	}
	return false;
}
<!--{$alertJs}-->
</script>