<!--{include file="admin/header.tpl"}-->
<!--{if $cfg.page.action=='add'}-->
<form id="form1" name="form1" method="post" action="user.php?action=save" onsubmit="return validator(this)">
<input id="user_id" name="user_id" type="hidden" value="<!--{$userInfo.user_id}-->" />
<table cellpadding="2" cellspacing="1"  class="table_form">
 <caption>增加后台用户</caption>
  <tr>
    <th><strong>用户名：</strong></th>
    <td><input value="<!--{$userInfo.username}-->" name="username" type="text" id="username" maxlength="16" valid="required" errmsg="用户名不能为空!" /></td>
  </tr>
  <tr>
    <th><strong>密码：</strong></th>
    <td><input name="passwd" type="password" id="passwd" maxlength="16" <!--{if $user_id==''}--> valid="required" errmsg="密码不能为空!"<!--{/if}-->  /></td>
  </tr>
  <tr>
    <th><strong>确认密码：</strong></td>
    <td><input name="passwd2" type="password" id="passwd2" maxlength="16" valid="eqaul" eqaulName="passwd" errmsg="两次密码不同!"  /></td>
  </tr>
  <tr>
    <th><strong>邮箱地址：</strong></th>
    <td><input value="<!--{$userInfo.email}-->" name="email" type="text" id="email" maxlength="50" valid="required|isEmail" errmsg="Email不能为空|Email格式不对!" /></td>
  </tr>
  <tr>
    <th><strong>用户名：</strong></th>
    <td><!--{$select}--></td>
  </tr>
  <tr>
    <th>&nbsp;</th>
    <td><input type="submit" name="submit" value="保存" /></td>
  </tr>
</table>
</form>
<!--{elseif $cfg.page.action=='right'}-->
<form method="post" action="user.php?action=right">
<input type="hidden" name="user_id" value="<!--{$user_id}-->">
<input type="hidden" name="groupId" value="<!--{$group_id}-->">
<table cellspacing="1" cellpadding="0" class="table_form" >
<!--{foreach item=item from=$menus}-->
  <tr>
    <td><b><!--{$item.caption}--></b></td>
  </tr>
  <!--{foreach item=item2 from=$item.sub key=key}-->
  <tr>
    <td height="25">&nbsp;&nbsp;<input <!--{if $userRight.$key}--> checked="checked"<!--{/if}--> type="checkbox" name="rights[<!--{$key}-->]" value="1"><!--{$item2.caption}--></td>
  </tr>
  <!--{/foreach}-->
<!--{/foreach}-->
	<tr>
		<td><input type="submit" class="button" value="保存"></td>
	</tr>
</table>
</form>
<!--{else}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>后台用户管理</caption>
  <tr>
    <td><a href="user.php"><font class="red">用户列表</font></a> | <a href="user.php?action=add">添加用户</a></td>
  </tr>
</table>
<form name="myform" method="post" action="user.php?action=delete">
<table cellpadding="2" cellspacing="1" class="table_list">
<caption>后台用户列表</caption>
  <tr>
    <th width="30">选中</th>
    <th width="200">用户名</th>
    <th>Email</th>
    <th width="150">操作</th>
  </tr>
<tbody>
<!--{foreach item=item from=$userList}-->
  <tr id="tr_<!--{$item.user_id}-->">
    <td class="align_c" ><input type="checkbox" name="users[]" value="<!--{$item.user_id}-->"></td>
    <td class="align_c"><!--{$item.username}--></td>
    <td> <!--{$item.email}--></td>
    <td class="align_c"><a href="?action=edit&user_id=<!--{$item.user_id}-->">编辑</a> <a onclick="return confirm('真的要删除吗?')" href="javascript:delUser(<!--{$item.user_id}-->)">删除</a></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 删除 " onclick="delTr(document.myform)">
</div>
<!--{$pagePanel}-->
</form>
<script type="text/javascript">
function delTr(form) {
	var a = new Array();
	if (confirm('真的要删除吗?')) {
		for (var i=0;i<form.elements.length;i++) {
			var e = form.elements[i];
			if (e.checked==true&&e.name=="users[]") {
				a.push(e.value);
			}
		}
		if (a.length==0) {
			alert('没有选择！');
			//return false; 
		}
		for (i in a) {
			delUser(a[i]);
		}
		//return false;
	} else {
		//return false;
	}
}
function delUser(user_id) {
	$.get('user.php?action=delete&user_id='+user_id+'&t='+Math.random(),null,function (msg) {
		if(msg) alert(msg)
		else {//$('#tr_'+user_id).remove();
			window.location.reload();
		}
	});
}
function resumeUser(user_id) //恢复被删除的用户
{
	$.ajax({type:'post',url:'user.php',data:'action=resume&user_id='+user_id,success:function(msg){
		if(msg)alert(msg);else{window.location.reload();}
	}});
}
</script>
<!--{/if}-->
</div>
<!--{include file="admin/footer.tpl"}-->
