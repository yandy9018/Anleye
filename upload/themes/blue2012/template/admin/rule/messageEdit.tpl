<!--{include file="admin/header.tpl"}-->
<form id="editForm" name="editForm" method="post" action="?action=save" onsubmit="return validator(this)">
	<input name="id" type="hidden" value="<!--{$dataInfo.id}-->">
    <table class="table_form" cellpadding="2" cellspacing="1">
    	<caption>添加规则</caption>
    	<tr>
        	<th width="17%">规则名称：</th>
        	<td><input name="rule_name" type="text" id="rule_name" size="55" valid="required" errmsg="名称必填" value="<!--{$dataInfo.rule_name}-->"  /></td>
        </tr>
        <tr>
        	<th width="17%">分类：</th>
        	<td><select name="rule_class">
        		<!--{html_options options=$rule_class selected=$dataInfo.rule_class }-->	
        	</select>
        	</td>
        </tr>
    	<tr>
        	<th width="17%">发送内容：</th>
        	<td><textarea name="rule_remark" rows="5" style="padding:6px; width:98%; line-height:17px;"/><!--{$dataInfo.rule_remark}--></textarea></td>
        </tr>
        <tr>
        	<td></td>
        	<td><input name="Submit" type="button" value=" 提交 " onclick="javascript:if (validator(document.editForm)) document.editForm.submit();" ></td>
        </tr>
    </table>
</form>
<!--{include file="admin/footer.tpl"}-->