<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>积分规则管理</caption>
  <tr>
    <td><a href="index.php">积分规则列表</a> | <a href="index.php?action=add">积分规则添加</a></td>
  </tr>
</table>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>积分规则列表</caption>
	<tr>
		<th width="30">选中</th>
		<th width="25%">操作事项</th>
		<th width="15%">分类</th>
		<th width="5%">积分</th>
		<th>说明</th>
		<th width="5%">状态</th>
		<th width="20%">管理操作</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.id}-->"></td>
	<td class="align_c"><a href="index.php?action=edit&id=<!--{$item.id}-->"><!--{$item.rule_name}--></a></td>
	<td class="align_c"><!--{$item.rule_class}--></td>
	<td class="align_c"><!--{$item.rule_score}--></td>
	<td class="align_c"><!--{$item.rule_remark}--></td>
	<td class="align_c"><!--{if $item.rule_status}--><font color="Red">禁用</font><!--{else}-->启用<!--{/if}--></td>
  	<td class="align_c">
		<a href="?action=edit&id=<!--{$item.id}-->">修改</a>
	</td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="statusoff" value=" 禁用 " onclick="if(confirm('你确认禁用选中的条目么？')){myform.action='index.php?action=status&status=1';myform.submit();}">
	<input type="button" name="statuson" value=" 启用 " onclick="if(confirm('你确认启用选中的条目么？')){myform.action='index.php?action=status&status=0';myform.submit();}">
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=delete';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
