<!--{include file="admin/header.tpl"}-->

<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>快速导航</caption>
  <tr>
    <td>
     <a href="?"><!--{if !isset($smarty.get.class) }--><font class="red">全部</font><!--{else}-->全部<!--{/if}--></a> | 
     <!--{foreach from=$link_class key=key item=item}-->
		<!--{if $smarty.get.class == $key}-->
			<a href="?class=<!--{$key}-->"><font color="Red"><!--{$item}--></font></a> | 
		<!--{else}-->
			<a href="?class=<!--{$key}-->"><!--{$item}--></a> | 
		<!--{/if}-->
     <!--{/foreach}-->
	
    </td>
    <td width="10%" class="align_c"><a href="?action=add">添加外部链接</a></td>
  </tr>
</table>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>友情链接管理</caption>
	<tr>
		<th width="30">选中</th>
		<th>链接名称</th>
		<th>链接分类</th>
		<th>链接类型</th>
		<th>链接文字或LOGO</th>
		<th>链接地址</th>
		<th>排序</th>
		<th>添加时间</th>
		<th>状态</th>
		<th width="100">管理操作</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.link_name }-->"></td>
	<td class="align_c"><!--{$item.link_name}--></td>
  	<td class="align_c"><!--{$item.link_class_str}--></td>
	<td class="align_c"><!--{if $item.link_type==2}-->图片<!--{else}-->文字<!--{/if}--></td>
	<td class="align_c"><!--{if $item.link_logo}--><img src="<!--{$cfg.url}-->upfile/<!--{$item.link_logo}-->" width="88" height="31"><!--{else}--><!--{$item.link_text}--><!--{/if}--></td>
  	<td class="align_c"><!--{$item.link_url}--></td>
	<td class="align_c"><input type="text" name="list_order[<!--{$item.id}-->]" value="<!--{$item.list_order}-->" size="3"></td>
	<td class="align_c"><!--{$item.add_time|date_format:"%Y-%m-%d %T"}--></td>
	<td class="align_c"><!--{if $item.status == 0 }-->有效<!--{else}--><font color="Red">无效</font><!--{/if}--></td>
  	<td class="align_c">
		 <a href="?action=edit&id=<!--{$item.id}-->">修改</a>
	</td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 排序 " onclick="myform.action='?action=order';myform.submit();">
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='?action=delete';myform.submit();}">
	<input type="button" name="delete" value=" 启用 " onclick="if(confirm('你确认启用选中的条目么？')){myform.action='?action=status&status=0';myform.submit();}">
	<input type="button" name="delete" value=" 禁用 " onclick="if(confirm('你确认禁用选中的条目么？')){myform.action='?action=status&status=1';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
