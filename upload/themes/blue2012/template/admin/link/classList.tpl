<!--{include file="admin/header.tpl"}-->
<form name="addItem" method="post" action="?action=save" onsubmit="return validator(this)">
<table cellpadding="2" cellspacing="1" class="table_list">
  <input type="hidden" name="id" value="<!--{$info.id}-->" />
  <caption>添加/编辑分类</caption>
  <tr>
    <td align="left">
      名称：<input name="class_name" value="<!--{$info.class_name}-->" valid="required" noValue="0" errmsg="类型名称不能为空!" />
      类型：<select name="link_type" >
      	<option value="0" <!--{if $info.link_type == 0}-->selected<!--{/if}-->>文字</option>
      	<option value="1" <!--{if $info.link_type == 1}-->selected<!--{/if}-->>图片</option>
      </select>
        <input type="submit" value="保存"/>
     </td>
  </tr>
</table>
</form>
<!--列表-->

<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>友情链接分类管理</caption>
	<tr>
		<th width="30">选中</th>
		<th>分类名称</th>
		<th>分类类型</th>
		<th>排序</th>
		<th width="100">管理操作</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.class_name}-->"></td>
	<td class="align_c"><!--{$item.class_name}--></td>
	<td class="align_c"><!--{if $item.link_type}-->图片<!--{else}-->文字<!--{/if}--></td>
  	<td class="align_c"><input type="text" name="list_order[<!--{$item.id}-->]" value="<!--{$item.list_order}-->" size="3"></td>
	<td class="align_c">
		 <a href="?action=edit&id=<!--{$item.id}-->">修改</a>
	</td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 排序 " onclick="myform.action='?action=order';myform.submit();">
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='?action=delete';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
