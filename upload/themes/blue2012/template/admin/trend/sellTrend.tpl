<!--{include file="admin/header.tpl"}-->

<form name="addItem" method="post" action="sell.php?action=edit" onsubmit="return validator(this)">
<input type="hidden" name="id" value="<!--{$dataInfo.id}-->" />
<table cellpadding="1" class="table_form">
	<caption>增加/修改每月参数值</caption>
	<tr>
		<td>
年月份：<input name="timestr" value="<!--{$dataInfo.time}-->" valid="required|isNumber|isNo" noValue="0" errmsg="值不能为空!|值必须为数字|值不允许为0" />
成交套数：<input name="number" value="<!--{$dataInfo.number}-->" valid="required|isNumber|isNo" noValue="0" errmsg="值不能为空!|值必须为数字|值不允许为0" />
成交均价：<input name="price" value="<!--{$dataInfo.price}-->" valid="required|isNumber|isNo" noValue="0" errmsg="值不能为空!|值必须为数字|值不允许为0" />
成交面积:<input name="area" value="<!--{$dataInfo.area}-->" valid="required" errmsg="名称不能为空!" /> <input type="submit" value="保存" />
		</td>
	</tr>
</table>
</form>

<form name="myform" method="post" action="sell.php?action=delete">
<table cellpadding="2" cellspacing="1" class="table_list">
 <caption>出售房源走势图</caption>
  <tr>
    <th width="30">选中</th>
    <th width="60">时间</th>
    <th width="200">成交套数</th>
    <th>成交均价</th>
    <th width="60">成交面积</th>
    <th width="150">操作</th>
  </tr>
<!--{foreach item=item from=$list}-->
  <tr>
    <td class="align_c"><input type="checkbox" name="ids[]" value="<!--{$item.id}-->"></td>
    <td class="align_c"><!--{$item.time}--></td>
    <td class="align_c"><!--{$item.number}-->套</td>
    <td><!--{$item.price}-->元/平米</td>
    <td><!--{$item.area}-->平米</td>
    <td class="align_c"><a href="?action=edit&id=<!--{$item.id}-->">编辑</a></td>
  </tr>
<!--{/foreach}-->
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.submit();}">
</div>
</form>
<!--{$pagePanel}-->
<!--{include file="admin/footer.tpl"}-->
