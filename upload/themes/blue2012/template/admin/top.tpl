<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--></title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
<script language="javascript">
function fnOnTabClick(tab)
{
	//修改Tab样式
    $("#header_tabs > ul > li").removeClass("current");
    $(tab).parent().parent().addClass("current");
}
</script>
</head>
<body>
<!-- header -->
<div id="header">
  <div id="logo"> <!--{$cfg.page.title}-->管理后台</div>
  <div id="panel">

    <div id="header_tabs">
      <ul>
	<!--{foreach name=toptab from=$menus key=menu_tab item=this_menu}-->
        <li <!--{if $smarty.foreach.toptab.index == 0 }-->class = "current"<!--{/if}-->>
          <span>
            <!--{if $this_menu.url }-->
            <a href="<!--{$this_menu.url}-->" target="_blank" class="more"><!--{$this_menu.caption}--></a>
            <!--{else}-->
            <a href="left.php?tab=<!--{$menu_tab}-->" onclick="fnOnTabClick(this);" target="leftFrame"><!--{$this_menu.caption}--></a>
            <!--{/if}-->
          </span>
        </li>
	<!--{/foreach}-->
      </ul>

      <div id="header_links">
        <p>您好, <strong><!--{$username}--></strong> &nbsp;&nbsp;[&nbsp;<a href="login.php?action=logout">退出</a>&nbsp;]</p>
        <p><a href="../index.php" class="btnlink" target="_blank">网站首页</a> </p>
		   <p><a href="http://www.yanwee.com/forum.php?mod=forumdisplay&fid=47" class="btnlink" target="_blank">帮助？</a> </p>
      </div>
    </div>

    <div id="breadcrumbs"></div>

  </div>
</div>
<!-- /header -->
</body>
</html>
