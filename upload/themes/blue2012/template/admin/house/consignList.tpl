<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>委托出售房源管理</caption>

  <td>
  <a href="consign.php?check=0"><!--{if $smarty.get.check==0}--><font class="red">全部房源</font><!--{else}-->全部房源<!--{/if}--></a> |
  <a href="consign.php?check=1"><!--{if $smarty.get.check==1}--><font class="red">未受理</font><!--{else}-->未受理<!--{/if}--></a> |
   <a href="consign.php?check=2"><!--{if $smarty.get.check==2}--><font class="red">已回访</font><!--{else}-->已回访<!--{/if}--></a>
   | <a href="consign.php?check=3"><!--{if $smarty.get.check==3}--><font class="red">已受理</font><!--{else}-->已受理<!--{/if}--></a>
  </td>

</table>

<form name="search" method="get" action="consign.php?action=search">
<input type="hidden" name="action" value="search">
<table cellpadding="0" cellspacing="1" class="table_form">
	<caption>信息查询</caption>
	<tr>
		<td class="align_c">

			<input type="text" name="q" onblur="if(this.value ==''||this.value == '请输入电话号码'){this.value = '请输入电话号码';this.style.color = '#999999';}" onfocus="if(this.value == '请输入电话号码'){this.value = '';this.style.color = '#333333';}" value="<!--{if $q}--><!--{$q}--><!--{else}-->请输入电话号码<!--{/if}-->" size="35" />&nbsp;
            <input type="submit" name="dosubmit" value=" 查询 " />

		</td>
	</tr>
</table>
</form>

<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="2" class="table_list">
	<caption>委托出售信息管理</caption>
	<tr>
		<th width="30">选中</th>
		<th width="120">小区名称</th>
		<th width="80">户型</th>
		<th width="80">楼层</th>
        	<th width="80">面积</th>
		<th width="90">期望售价</th>
		<th width="60">简单描述</th>
        <th width="60">发布时间</th>
        <th width="60">房东</th>
		<th width="60">房东电话</th>
        <th width="60">状态</th>

	</tr>
<tbody>
<!--{foreach name=data item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->"></td>
	<td><!--{$item.borough_name}-->&nbsp;</td>
    
	<td class="align_c">
    <!--{$item.house_room}-->室 <!--{$item.house_hall}-->厅
    </td>
	<td class="align_c">第<!--{$item.house_floor}-->层 / 共<!--{$item.house_topfloor}-->层</td>
    <td class="align_c"><!--{$item.house_area}-->平米</td>
    
	<td class="align_c"><!--{$item.house_price}-->万</td>
	<td class="align_c">
 <!--{$item.house_desc}-->
    </td>
       <td class="align_c"><!--{$item.time|date_format:"%Y-%m-%d"}--></td>
          <td class="align_c"> <!--{$item.owner_name}--></td>
	<td class="align_c">
    <!--{$item.owner_phone}-->
    </td>

    <td class="align_c">
    <!--{if $item.status==0}--><font color="#FF0000">未受理</font><!--{elseif $item.status==1}-->已回访 <!--{else $item.status==2}-->已受理 <!--{/if}-->
    </td>
   
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>


    <input type="button" value=" 删除 " onclick="if(confirm('你确认删除选中的委托房源么？')){myform.action='consign.php?action=delete';myform.submit();}">
    <input type="button" value=" 回访 " onclick="if(confirm('你确定已经回访该房源了么？')){myform.action='consign.php?action=dostatus&status=1';myform.submit();}">
  <input type="button" value=" 受理 " onclick="if(confirm('你确定受理该房源么？')){myform.action='consign.php?action=dostatus&status=2';myform.submit();}">



</div>
<!--{$pagePanel}-->
<div id="pic_shower" style="position:absolute;color:white; width:320px; right:-30px; top:-10px; z-index:10000;background-color:#6680aa;display:none;"></div>
</form>

<!--{include file="admin/footer.tpl"}-->
