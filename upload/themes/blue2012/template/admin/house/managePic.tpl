<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--></title>
<!--{$cssFiles}-->
<!--{$jsFiles}-->
</head>
<body>
<div id="modalWindow">
	<form name="modalWindowForm" method="post" action="">
	<table cellpadding="2" cellspacing="2" class="table_list">
		<caption>房源图片管理<!--{if $smarty.get.tp==pic}-->--实景图<!--{else}-->--户型图<!--{/if}--></caption>
		<tr>
			<td width="80">房源图片:</td>
			<td>
				<!--{foreach name=list from=$houseImgList item=item key=key}-->
				<div style="float:left;width:80px;">
					<img src="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->" width="80" height="60" onclick="imgCheck(this)"><input type="checkbox" name="imgList[]" class="imglistCheckbox" id="img_<!--{$smarty.foreach.list.index}-->" value="<!--{$item.id}-->">
				</div>
				<!--{/foreach}-->
			</td>
		</tr>
		<tr>
			<td colspan="2" >
			<div class="button_box">
				<input type="button" name="delete" class="button_style" value="【X】删除图片" onclick="deleteImg();">&nbsp;&nbsp;
				<input type="button" name="toBoroguh" class="button_style" value="【↓】<!--{if $smarty.get.tp==pic}-->移动<!--{else}-->拷贝<!--{/if}-->到小区" onclick="moveToBorough();"> 
			</div>
			</td>
		</tr>
		<tr>
			<td width="80">小区已有图片:</td>
			<td id="boroughImg">
				<!--{foreach from=$boroughImgList item=item key=key}-->
				<div style="float:left;width:80px;">
					<img src="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->" width="80" height="60">
				</div>
				<!--{/foreach}-->
			</td>
		</tr>
	</table>
    </form>
</div>
<script language="javascript">
function imgCheck(ele){
	var checkEle = ele.nextSibling;
	if(checkEle.checked==true){
		checkEle.checked=false;
	}else{
		checkEle.checked=true;
	}
}
function deleteImg(){
	var selectValue='';
	labelsForChecked = $('#modalWindow').find('input[@type="checkbox"][@checked]');

	jQuery.each(labelsForChecked,function(i,val){selectValue+=','+jQuery(val).val();});
	
	if(confirm("你确定要删除房源中的图片么？")){
		$.post('managePic.php?action=delImg&cls=<!--{$smarty.get.cls}-->&tp=<!--{$smarty.get.tp}-->&house_id=<!--{$smarty.get.house_id}-->',{picid:selectValue},function(data){
			//成功就删除对象
			if(data == 1 ){
				jQuery.each(labelsForChecked,function(i,val){
					jQuery(val).parent().remove();
				});
			}
		});
		return true;
	}else{
		return false;
	}
}
function moveToBorough(){
	var selectValue='';
	labelsForChecked = $('#modalWindow').find('input[@type="checkbox"][@checked]');

	jQuery.each(labelsForChecked,function(i,val){selectValue+=','+jQuery(val).val();});
	
	if(confirm("你确定要把图片移动到小区图片库中么？")){
		$.post('managePic.php?action=moveToBorough&cls=<!--{$smarty.get.cls}-->&tp=<!--{$smarty.get.tp}-->&house_id=<!--{$smarty.get.house_id}-->',{picid:selectValue},function(data){
			//成功就删除对象
			if(data == 1 ){
				//户型图不删除
				<!--{if $smarty.get.tp==pic}-->
				jQuery.each(labelsForChecked,function(i,val){
					jQuery(val).parent().remove();
				});
				<!--{/if}-->
				reloadBoroughImg();
			}
		});
		return true;
	}else{
		return false;
	}
}
function reloadBoroughImg(){
	$.get('managePic.php',{action:"getBoroughImg",cls:"<!--{$smarty.get.cls}-->",tp:"<!--{$smarty.get.tp}-->",house_id:"<!--{$smarty.get.house_id}-->"},function(data){
		//成功就删除对象
		$('#boroughImg').html(data);
	});
}
</script>
</body>
</html>