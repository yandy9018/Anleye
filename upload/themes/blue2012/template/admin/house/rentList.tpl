<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>出售房源管理</caption>

  <td>
  <a href="rent.php?check=0"><!--{if $smarty.get.check==0}--><font class="red">全部房源</font><!--{else}-->全部房源<!--{/if}--></a> |
  <a href="rent.php?check=10"><!--{if $smarty.get.check==10}--><font class="red">置顶</font><!--{else}-->置顶<!--{/if}--></a> |
   <a href="rent.php?check=5"><!--{if $smarty.get.check==5}--><font class="red">首页推荐</font><!--{else}-->首页推荐<!--{/if}--></a> | 
   <a href="rent.php?check=11"><!--{if $smarty.get.check==11}--><font class="red">猜你喜欢</font><!--{else}-->猜你喜欢<!--{/if}--></a> | 
   <a href="rent.php?check=12"><!--{if $smarty.get.check==12}--><font class="red">主题推荐</font><!--{else}-->主题推荐<!--{/if}--></a>
   
    <!--{if $cfg.page.guest==1}-->
   | <a href="rent.php?check=2"><!--{if $smarty.get.check==2}--><font class="red">游客审核</font><!--{else}-->游客审核<!--{/if}--></a>
     <!--{/if}--> | 
   <a href="rent.php?check=13"><!--{if $smarty.get.check==13}--><font class="red">昨日房源</font><!--{else}-->昨日房源<!--{/if}--></a>
  <hr>
  <a href="rent.php?check=6"><!--{if $smarty.get.check==6}--><font class="red">出租</font><!--{else}-->出租<!--{/if}--></a> |
   <a href="rent.php?check=7"><!--{if $smarty.get.check==7}--><font class="red">下架</font><!--{else}-->下架<!--{/if}--></a> |
     <a href="rent.php?check=9"><!--{if $smarty.get.check==9}--><font class="red">已成交</font><!--{else}-->已成交<!--{/if}--></a>

  </td>

</table>

<form name="search" method="get" action="rent.php?action=search">
<input type="hidden" name="action" value="search">
<table cellpadding="0" cellspacing="1" class="table_form">
	<caption>信息查询</caption>
	<tr>
		<td class="align_c">
			<select name='cityarea'>
				<option value="">所在区域</option>
				<!--{foreach from=$areaLists key=key item=item}-->
				<!--{if $cityarea==$key}-->
				<option value="<!--{$key}-->" selected><!--{$item}--></option>
				<!--{else}-->
				<option value="<!--{$key}-->"><!--{$item}--></option>
				<!--{/if}-->
				<!--{/foreach}-->
			</select>
			<input type="text" name="q" onblur="if(this.value ==''||this.value == '请输入房源编号'){this.value = '请输入房源编号';this.style.color = '#999999';}" onfocus="if(this.value == '请输入房源编号'){this.value = '';this.style.color = '#333333';}" value="<!--{if $q}--><!--{$q}--><!--{else}-->请输入房源编号<!--{/if}-->" size="35" />&nbsp;
			<input type="submit" name="dosubmit" value=" 查询 " />
		</td>
	</tr>
</table>
</form>
<form method="POST" action="">
<input type="hidden" name="action" value="dateDel" />
<table cellpadding="0" cellspacing="1" class="table_form">
	<caption>删除房源</caption>
	<tr>
		<td class="align_c">
			时间：<input style="color:#999999" type="text" name="dateDel" onblur="if(this.value ==''||this.value == '请输入时间。例：2012-02-12'){this.value = '请输入时间。例：2012-02-12';this.style.color = '#999999';}" onfocus="if(this.value == '请输入时间。例：2012-02-12'){this.value = '';this.style.color = '#333333';}" value="请输入时间。例：2012-02-12" size="35" />&nbsp;
			<input type="submit" name="dosubmit" value=" 删除 " />*删除小于这个时间的房源
		</td>
	</tr>
</table>
</form>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="2" class="table_list">
	<caption>出租信息管理</caption>
	<tr>
		<th width="30">选中</th>
		<th width="120">小区/房源标题</th>
		<th width="80">经纪人/房东</th>
		<th width="80">区域/类型</th>
		<th width="90">房源信息</th>
		<th width="60">状态</th>
        <th width="60">首页</th>
        <th width="60">置顶</th>
		<th width="60">房源属性</th>
		<th width="60">楼层</th>
		<th width="100">户型图</th>
		<th width="500">实景图</th>
		<th width="60">点击数</th>
		<th width="60">发布时间</th>
        <th width="60">操作</th>
	</tr>
<tbody>
<!--{foreach name=data item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.house_title}-->"></td>
	<td><a href="<!--{$cfg.url_rent}-->d-<!--{$item.id}-->.html" target="_blank"><!--{$item.borough_name}--><br/><!--{$item.house_title}--> </a> &nbsp;</td>
	<td class="align_c">
     <!--{if $item.broker_id==0 && $item.consigner_id!=0}-->
    <!--{$item.consigner_name}-->
    <!--{elseif $item.broker_id!=0 && $item.consigner_id==0}-->
    <a href="<!--{$cfg.url_shop}--><!--{$item.broker_id}-->" target="_blank"><!--{$item.real_name}--></a>
     <!--{else}-->
     <font color="#FF0000">游客</font>
    <!--{/if}-->
    </td>
	<td class="align_c"><!--{$item.cityarea_id}--><br/><!--{$item.house_type}--> </td>
	<td class="align_c"><!--{$item.house_room}-->室<!--{$item.house_hall}-->厅<!--{$item.house_toilet}-->卫<!--{$item.house_veranda}-->阳 <br/><!--{$item.house_totalarea}-->平<!--{$item.house_price}-->万</td>
	<td class="align_c">
    <!--{if $item.is_checked==0}--><font color="red">未审核</font><!--{/if}-->
     <!--{if $item.is_checked==1}-->已审核<br />

<!--{/if}-->
     <!--{if $item.is_checked==2}-->审核不通过<!--{/if}-->
      </td>
      <td class="align_c"><!--{if $item.is_index==1}--><font color="red">推荐</font><!--{else}-->正常<!--{/if}--></td>
          <td class="align_c">  <!--{if $item.is_top==1}--><font color="red">置顶</font><!--{else}-->正常<!--{/if}--></td>
	<td class="align_c">
    <!--{if $item.status==1}-->
    出租
    <!--{elseif $item.status==2}-->
    <font color="red">下架</font>
    <!--{elseif $item.status==4}-->
    <font color="red"> 已成交</font>
  <!--{/if}-->
    </td>
	<td class="align_c"><!--{$item.house_floor}-->F/<!--{$item.house_topfloor}-->F</td>
	<td class="align_c">
	<!--{if $item.house_drawing}-->
		<div id="draw_div_<!--{$item.id}-->" class="upload_shower" >
			<a href="managePic.php?house_id=<!--{$item.id}-->&class=rent&type=pic" onclick="showBox('<!--{$item.id}-->','rent','draw');return false;" onmouseover="showDraw('draw_div_<!--{$item.id}-->','<!--{$item.id}-->','<!--{$item.house_drawing}-->');return false;">
				<img src="<!--{$cfg.url}-->upfile/<!--{$item.house_drawing}-->" width="64" height="48">
			</a>
		</div>
	<!--{/if}-->
	</td>
	<td>
		<!--{foreach name=pic item=pic_item from=$item.house_pic}-->
		<div id="pic_div_<!--{$item.id}-->_<!--{$pic_item.id}-->" class="upload_shower" >
			<a href="managePic.php?house_id=<!--{$item.id}-->&class=rent&type=pic" onclick="showBox('<!--{$item.id}-->','rent','pic');return false;" onmouseover="showPic('pic_div_<!--{$item.id}-->_<!--{$pic_item.id}-->','<!--{$pic_item.id}-->','<!--{if $pic_item.pic_thumb}--><!--{$pic_item.pic_thumb}--><!--{else}--><!--{$pic_item.pic_url}--><!--{/if}-->');return false;">
				<img src="<!--{$cfg.url}-->upfile/<!--{if $pic_item.pic_thumb}--><!--{$pic_item.pic_thumb}--><!--{else}--><!--{$pic_item.pic_url}--><!--{/if}-->" width="64" height="48">
			</a>
		</div>
		<!--{/foreach}-->
	</td>
	<td>今日：<font color="#FF0000"><!--{$item.today_click}--></font>次<br />
昨日：<font color="#FF0000"><!--{$item.yestoday_click}--></font>次<br />

    总：<font color="#FF0000"><!--{$item.click_num}--></font>次</td>
	<td><!--{$item.created|date_format:'%Y-%m-%d'}--></td>
   <td><a target="_blank" href="<!--{$cfg.url_rent}-->d-<!--{$item.id}-->.html">查看</a> | <a href="rentEdit.php?id=<!--{$item.id}-->">编辑</a></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	  <!--{if $smarty.get.check == 2}-->
	<input type="button" value=" 审核通过 " onclick="if(confirm('你确认审核通过选中的条目么？')){myform.action='rent.php?action=docheck&status=1';myform.submit();}">
    <input type="button" value=" 删除 " onclick="if(confirm('删除会给用户发送"删除"的站内信，你确认删除选中的条目么？')){myform.action='rent.php?action=delete';myform.submit();}">
	<!--{else}-->
    <input type="button" value=" 删除 " onclick="if(confirm('删除会给用户发送"删除"的站内信，你确认删除选中的条目么？')){myform.action='rent.php?action=delete';myform.submit();}">
    <input type="button" value=" 首页推荐 " onclick="if(confirm('首页推荐之后将会在首页的精选租房处显示，你确定么？')){myform.action='rent.php?action=isindex&status=1';myform.submit();}">
     <input type="button" value=" 取消推荐 " onclick="if(confirm('取消推荐之后首页的精选租房将不显示，你确定么？')){myform.action='rent.php?action=isindex&status=0';myform.submit();}">
     
      <input type="button" value=" 猜你喜欢 " onclick="if(confirm('推荐之后将会在猜你喜欢栏目处显示，你确定么？')){myform.action='rent.php?action=islike&status=1';myform.submit();}">
     <input type="button" value=" 取消喜欢 " onclick="if(confirm('取消喜欢之后喜欢栏目将不显示，你确定么？')){myform.action='rent.php?action=islike&status=0';myform.submit();}">
     
      <input type="button" value=" 主题推荐 " onclick="if(confirm('推荐之后将会主题房源处显示，你确定么？')){myform.action='rent.php?action=isthemes&status=1';myform.submit();}">
     <input type="button" value=" 取消主题 " onclick="if(confirm('取消主题之后主题栏目将不显示，你确定么？')){myform.action='rent.php?action=isthemes&status=0';myform.submit();}">
    <!--{/if}-->
</div>
<!--{$pagePanel}-->
<div id="pic_shower" style="position:absolute;color:white; width:320px; right:-30px; top:-10px; z-index:10000;background-color:#6680aa;display:none;"></div>
</form>
<script language="javascript">
function showPic(picDiv,picId,picUrl){
	//alert('aaaa');
	if (!e) var e = window.event;

	if(!document.all){
        mouse_x=e.pageX;
        mouse_y=e.pageY;
    }else{
        mouse_x=document.body.scrollLeft+event.clientX;
        mouse_y=document.body.scrollTop+event.clientY;
    }
    clientW = document.documentElement.clientWidth;
    clientH = document.documentElement.clientHeight;
    if (mouse_x+330>clientW){
    	mouse_x = mouse_x-330;
    }else{
    	mouse_x = mouse_x+30;
    }
    if (mouse_y+250>clientH){
    	mouse_y = mouse_y-120;
    }else{
    	mouse_y = mouse_y+30;
    }
    //便于跟踪调试
    //mouse_x = mouse_x - 510;
    $('#pic_shower').html('<img src="<!--{$cfg.url}-->upfile/'+picUrl+'" width="320" height="240">');
    $('#pic_shower').css('left',mouse_x).css('top',mouse_y).css('display','block');

}
function showDraw(drawDiv,dataId,drawUrl){
	if (!e) var e = window.event;

	if(!document.all){
        mouse_x=e.pageX;
        mouse_y=e.pageY;
    }else{
        mouse_x=document.body.scrollLeft+event.clientX;
        mouse_y=document.body.scrollTop+event.clientY;
    }
    //便于跟踪调试
/*    clientW = document.body.clientWidth;
    clientH = document.body.clientHeight;*/
    clientW = document.documentElement.clientWidth;
    clientH = document.documentElement.clientHeight;
    if (mouse_x+330>clientW){
    	mouse_x = mouse_x-330;
    }else{
    	mouse_x = mouse_x+30;
    }
    if (mouse_y+250>clientH){
    	mouse_y = mouse_y-120;
    }else{
    	mouse_y = mouse_y+30;
    }
    //mouse_x = mouse_x - 510;
    $('#pic_shower').html('<img src="<!--{$cfg.url}-->upfile/'+drawUrl+'" width="320" height="240" >');

    $('#pic_shower').css('left',mouse_x).css('top',mouse_y).css('display','block');
}
function hidePic(){
	//alert('bbb');
	$('#pic_shower').css('display','none');
}
function delPic(picId,picDiv){
	$.get("rentajax.php", { action: "delpic", id: picId ,rnd:Math.random()}, function(data){
		//alert(data);
		if(data == 1){
			$('#'+picDiv).remove();
			hidePic();
		}
	});
}
function delDraw(dataId,drawDiv){
	$.get("rentajax.php", { action: "deldraw", id: dataId,rnd:Math.random()}, function(data){
		if(data == 1){
			$('#'+drawDiv).remove();
			hidePic();
		}
	});
}
$(document).click(function(){hidePic();});
function showBox(item_id,picClass,picType){
	TB_show('图片管理','managePic.php?cls='+picClass+'&tp='+picType+'&house_id='+item_id+'&height=400&width=700&modal=true&rnd='+Math.random(),false);
}
</script>
<!--{include file="admin/footer.tpl"}-->
