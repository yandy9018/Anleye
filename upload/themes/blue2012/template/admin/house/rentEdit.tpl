<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title><!--{$cfg.page.title}--> - 添加/编辑房源</title>
<LINK href="<!--{$cfg.site.themes}-->css/member.css" rel=stylesheet>
<LINK href="<!--{$cfg.site.themes}-->css/thickbox.css" rel=stylesheet>
<LINK href="<!--{$cfg.site.themes}-->js/Autocompleter/jquery.autocomplete.css" rel=stylesheet>

<script language="JavaScript" type="text/javascript" src="<!--{$cfg.site.themes}-->js/jquery-1.2.6.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.site.themes}-->js/map.js"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.site.themes}-->js/FormValid.js"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.site.themes}-->js/FV_onBlur.js"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.site.themes}-->js/thickbox.js"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.site.themes}-->js/Autocompleter/lib/jquery.bgiframe.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.site.themes}-->js/Autocompleter/lib/ajaxQueue.js"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.site.themes}-->js/Autocompleter/jquery.autocomplete.js"></script>
<link href="<!--{$cfg.path.url_lib}-->SWFUpload/css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<!--{$cfg.path.url_lib}-->SWFUpload/swfupload.js"></script>
<script type="text/javascript" src="<!--{$cfg.path.url_lib}-->SWFUpload/js/handlers.js"></script>
<script type="text/javascript">
		var swfu;
		window.onload = function () {
			swfu = new SWFUpload({
				// Backend Settings
				upload_url: "<!--{$cfg.path.url_lib}-->SWFUpload/upload.php?to=uploadHousePicture|house|rent",
				post_params: {"PHPSESSID": "<!--{$session_id}-->"},

				// File Upload Settings
				file_size_limit : "2 MB",	// 2MB
				file_types : "*.jpeg;*.jpg;*.gif;*.bmp;*.png",
				file_types_description : "Images",
				file_upload_limit : 0,

				// Event Handler Settings - these functions as defined in Handlers.js
				//  The handlers are not part of SWFUpload but are part of my website and control how
				//  my website reacts to the SWFUpload events.
				swfupload_preload_handler : preLoad,
				swfupload_load_failed_handler : loadFailed,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,

				// Button Settings
				button_image_url : "<!--{$cfg.path.url_lib}-->SWFUpload/images/SmallSpyGlassWithTransperancy_17x18.png",
				button_placeholder_id : "spanButtonPlaceholder",
				button_width: 180,
				button_height: 18,
				button_text : '<span class="button">选择图片 <span class="buttonSmall">(最大支持2 MB )</span></span>',
				button_text_style : '.button { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; } .buttonSmall { font-size: 10pt; }',
				button_text_top_padding: 0,
				button_text_left_padding: 18,
				button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
				button_cursor: SWFUpload.CURSOR.HAND,
				
				// Flash Settings
				flash_url : "<!--{$cfg.path.url_lib}-->SWFUpload/swfupload.swf",
				flash9_url : "<!--{$cfg.path.url_lib}-->SWFUpload/swfupload_FP9.swf",

				custom_settings : {
					upload_target : "divFileProgressContainer"
				},
				
				// Debug Settings
				debug: false
			});
		};
	</script>
<script type="text/javascript">
FormValid.showError = function(errMsg,errName,formName) {
	if (formName=='dataForm') {
		for (key in FormValid.allName) {
			//document.getElementById('errMsg_'+FormValid.allName[key]).innerHTML = '';
			//document.getElementById('errMsg_'+FormValid.allName[key]).style.display = 'none';
			
		}
		for (key in errMsg) {
			document.getElementById('errMsg_'+errName[key]).innerHTML = errMsg[key];
			document.getElementById('errMsg_'+errName[key]).style.display = '';
			
		}
	}
}
function addToBoroughItem(bid,bname,b_addr){
	$("#borough_id").val(bid);
	$("#borough_name").val(bname);
	$("#borough_addr").val(b_addr);
	$("#borough_addr_tr").css("display",""); 
}
$().ready(function() {
	$("#borough_name").autocomplete("ajax.php?action=getBoroughList", {
		minChars: 2,
		width: 260,
		delay:0,
		mustMatch:true,
		matchContains: false,
		scrollHeight: 220,
		selectFirst:true,
		scroll: true,
		formatItem: function(data, i, total) {
			if(data[1]=="addBorough"){
				return '<strong>'+data[0]+'</strong>';
			}
			return data[0];
		}
	});
	
	$("#borough_name").result(function(event, data, formatted) {
		if(data[1]=="addBorough"){
			//TB_show('增加小区','#TB_inline?height=155&width=400&inlineId=modalWindow',false);
			TB_show('增加小区','addBorough.php?height=165&width=400&modal=true&rnd='+Math.random(),false);
			$(this).val('');
		}else{
			var pic_thumb=data[3];
			var pic_url=data[4];
			var pic_desc=data[5];
			var str="<div style='width:647px;line-height:30px;color:#999999'>以下为小区上传户型图，如无发布房源户型，请自行上传！</div>";
				 str+="<ul style='width:647px'>";
			pic_thumb=pic_thumb.split(',');
			pic_url=pic_url.split(',');
			pic_desc=pic_desc.split(',');
			for(i=0;i<pic_thumb.length-1;i++){
				str+='<li style="width:160px; height:150px;margin-right:15px;float:left; text-align:center;overflow:hidden; margin-top:5px;">\
				<a href="<!--{$cfg.url_upfile}-->'+pic_url[i]+'" target="_blank"><img src="<!--{$cfg.url_upfile}-->'+pic_thumb[i]+'" width=160 height=120 style="padding-bottom:3px"/></a>\
				<input name="house_drawing" id="house_drawing"type="radio" value="'+pic_url[i]+'" />\
				<span>'+pic_desc[i]+'</span>\
				</li>\
				';
			}
			str+='</ul>';
			$("#borough_id").val(data[1]);
			$("#borough_addr").val(data[2]);
			$("#house_drawing_dis").html(str);
			$("#borough_addr_tr").css("display","");
		}
		
		/*if (data)
			$(this).parent().next().find("input").val(data[1]);*/
	});
});
function checkBoxNum(){
	var form = document.forms['dataForm'];
	var i,j=0;
	for (i=0; i<form.length; i++){
		var e=form[i];
		if (e.checked && e.type=='checkbox' && e.name=='house_feature[]') {
			j++;
			if(j==5){
				alert("房源特色最多只能选择4项！");
				return false;
				break;
			}
		}
	}
}
    function mianyi(){
		$("#h_price").val("0");
		$("#h_priceInfo").html("");
	}
</script>
</head>
<body>

<div class="main">
	<div class="page">
		<div class="memberBox">
			
		
			<form id="dataForm" name="dataForm" action="?action=save" method="POST" onsubmit="return checkForm(this)">
			<input type="hidden" name="id" value="<!--{$dataInfo.id}-->">
			<input type="hidden" name="cid" value="<!--{$smarty.get.cid}-->">
			<input type="hidden" name="to_url" value="<!--{$to_url}-->">	
			<table class="memberBoxTable" cellpadding="0" cellspacing="5" border="0">
				<thead>
					<tr>
						<td colspan="4"><span class="concentTitle">第一步：房源基本信息（必填项）</span></td>
					</tr>
				</thead>
				<tbody>
                <tr>
						<td class="row1">房源标题：</td>
						<td colspan="3">
							<input class="input" name="house_title" type="text" size="30" value="<!--{$dataInfo.house_title}-->"  valid="required" errmsg="请输入房源标题!"  />&nbsp;
							<span class="tip">好的标题能有效提升房源点击率，最多20个汉字</span>
							<p id="errMsg_house_title" class="errorMessage" display="none"></p>
						</td>
					</tr>
					<tr>
						<td class="row1">房屋类型：</td>
						<td colspan="3">
						<!--{foreach name=house_type from=$house_type_option item=item key=key}-->
							<!--{if $key == $dataInfo.house_type}-->
								<label for="house_type_<!--{$key}-->"><input  type="radio" name="house_type" id="house_type_<!--{$key}-->" value="<!--{$key}-->" valid="requireChecked" errmsg="请选择房源类型!" checked /><!--{$item}--> </label>	
							<!--{else}-->
								<label for="house_type_<!--{$key}-->"><input  type="radio" name="house_type" id="house_type_<!--{$key}-->" value="<!--{$key}-->" valid="requireChecked" errmsg="请选择房源类型!" /><!--{$item}--> </label>
							<!--{/if}-->
						<!--{/foreach}-->
						<p id="errMsg_house_type" class="errorMessage"></p>
						</td>
					</tr>
					<tr>
						<td class="row1">小区名称：</td>
						<td colspan="3">
							<input type="hidden" id="borough_id" name="borough_id" value="<!--{$dataInfo.borough_id}-->" >
							<input id="borough_name" class="input" name="borough_name" type="text" size="30" value="<!--{$dataInfo.borough_name}-->"  errmsg="请输入小区名称!" /> 
							<span class="tip">例：输入“大名城”或拼音首字母“dmc”，从下拉列表中选择</span>
							<p id="errMsg_borough_name" class="errorMessage" display="none"></p>
						</td>
					</tr>
					<tr id="borough_addr_tr" style="display:none;">
						<td class="row1">小区地址：</td>
						<td colspan="3"><input id="borough_addr" type="text" class="input" name="borough_addr"  size="30" value="<!--{$dataInfo.borough_addrest}-->" disabled /></td>
					</tr>
   
                      <tr>
						<td class="row1">楼&nbsp;&nbsp;&nbsp;&nbsp;层：</td>
						<td colspan="3">
							第<input class="input" name="house_floor" type="text" size="1" value="<!--{$dataInfo.house_floor}-->"  valid="required|isInt" errmsg="请输入所在楼层!|请输入整数！"  /> 层 /
							 共<input class="input" name="house_topfloor" type="text" size="1" value="<!--{$dataInfo.house_topfloor}-->" valid="required|isInt" errmsg="请输入楼层总数!|请输入整数！" /> 层
							<p id="errMsg_house_floor" class="errorMessage" display="none"></p><p id="errMsg_house_topfloor" class="errorMessage" display="none"></p>
						</td>
					</tr>
                    
          <tr>
						<td class="row1">户&nbsp;&nbsp;&nbsp;&nbsp;型：</td>
						<td colspan="3">
                 
                 <select class="select" name="house_room">
						<!--{assign var="loop" value="5"}-->
						<!--{section name="loop_room" loop=$loop}-->
						 <!--{if $smarty.section.loop_room.iteration==$dataInfo.house_room }-->
						 <option value="<!--{$smarty.section.loop_room.iteration}-->" selected><!--{$smarty.section.loop_room.iteration}--></option>
						 <!--{else}-->
						 <option value="<!--{$smarty.section.loop_room.iteration}-->"><!--{$smarty.section.loop_room.iteration}--></option>
						 <!--{/if}-->
						<!--{/section}-->
						<!--{if $dataInfo.house_room>5 }-->
						 <option value="6" selected>5室以上</option>
						 <!--{else}-->
						  <option value="6">5室以上</option>
						 <!--{/if}-->
						</select> 室<select class="select" name="house_hall">
						<!--{assign var="loop" value="6"}-->
						<!--{section name="loop_hall" loop=$loop}-->
						 <!--{if $smarty.section.loop_hall.index==$dataInfo.house_hall }-->
						 <option value="<!--{$smarty.section.loop_hall.index}-->" selected><!--{$smarty.section.loop_hall.index}--></option>
						 <!--{else}-->
						 <option value="<!--{$smarty.section.loop_hall.index}-->"><!--{$smarty.section.loop_hall.index}--></option>
						 <!--{/if}-->
						<!--{/section}-->
						</select> 厅<select class="select" name="house_toilet">
						 <!--{assign var="loop" value="6"}-->
						<!--{section name="loop_toilet" loop=$loop}-->
						 <!--{if $smarty.section.loop_toilet.index==$dataInfo.house_toilet }-->
						 <option value="<!--{$smarty.section.loop_toilet.index}-->" selected><!--{$smarty.section.loop_toilet.index}--></option>
						 <!--{else}-->
						 <option value="<!--{$smarty.section.loop_toilet.index}-->"><!--{$smarty.section.loop_toilet.index}--></option>
						 <!--{/if}-->
						<!--{/section}-->
						 </select> 卫<select class="select" name="house_veranda">
						 <!--{assign var="loop" value="6"}-->
						<!--{section name="loop_veranda" loop=$loop}-->
						 <!--{if $smarty.section.loop_veranda.index==$dataInfo.house_veranda }-->
						 <option value="<!--{$smarty.section.loop_veranda.index}-->" selected><!--{$smarty.section.loop_veranda.index}--></option>
						 <!--{else}-->
						 <option value="<!--{$smarty.section.loop_veranda.index}-->"><!--{$smarty.section.loop_veranda.index}--></option>
						 <!--{/if}-->
						<!--{/section}-->
						 </select> 阳&nbsp;<span class="tip">如果户型超过五室请在“房源描述”里进行说明</span>
                 			</td>
					</tr>
                 
					<tr>
						<td class="row1">租&nbsp;&nbsp;&nbsp;&nbsp;金：</td>
						<td colspan="3">
							<input id="h_price" class="input" name="house_price" type="text" size="10" value="<!--{$dataInfo.house_price}-->"  valid="required|isNumber" errmsg="请输入租金!|请输入数字！"  />&nbsp;元/月
							<p id="errMsg_house_price" class="errorMessage" display="none"></p>
						</td>
					</tr>
					 <tr>
						<td class="row1">付款方式：</td>
						<td colspan="3">
                        
                        
						<select class="select" name="house_deposit" valid="required" errmsg="请选择付款方式！">
								<option>请选择</option>
								 <!--{html_options options=$rent_deposittype_option selected=$dataInfo.house_deposit}-->
							</select>
						 
                            <p id="errMsg_rent_deposittype_option" class="errorMessage" display="none"></p>
						 </td>
					</tr>
					<tr>
						<td class="row1">建筑面积：</td>
						<td colspan="3">
							<input id="house_totalarea" class="input" name="house_totalarea" type="text" size="10" value="<!--{$dataInfo.house_totalarea}-->" valid="required|isNumber" errmsg="请输入建筑面积!|请输入数字！" />&nbsp;平米
							<p id="errMsg_house_totalarea" class="errorMessage" display="none"></td>
					</tr>

					 <tr>
						<td class="row1">房&nbsp;&nbsp;&nbsp;&nbsp;龄：</td>
						<td colspan="3">
							<select class="select" name="house_age" valid="required" errmsg="请选择房龄！">
								<option value="">请选择</option>
								<!--{html_options values=$house_age_option selected=$dataInfo.house_age output=$house_age_option}-->
							</select>
						 	年&nbsp;
                            <p id="errMsg_house_age" class="errorMessage" display="none"></p>
						 </td>
					</tr>
                    
					<tr>
						<td class="row1">装修情况：</td>
						<td colspan="3">
						<!--{foreach name=house_fitment from=$house_fitment_option item=item key=key}-->
							<!--{if $key == $dataInfo.house_fitment}-->
								<label for="house_fitment_<!--{$key}-->"><input  type="radio" name="house_fitment" id="house_fitment_<!--{$key}-->" value="<!--{$key}-->" valid="requireChecked" errmsg="请选择装修情况!" checked /><!--{$item}--> </label>
							<!--{else}-->
								<label for="house_fitment_<!--{$key}-->"><input  type="radio" name="house_fitment" id="house_fitment_<!--{$key}-->" value="<!--{$key}-->" valid="requireChecked" errmsg="请选择装修情况!" /><!--{$item}--> </label>
							<!--{/if}-->
						<!--{/foreach}-->
						<p id="errMsg_house_fitment" class="errorMessage" display="none"></p>
						</td>
					</tr>
					
					<tr>
						<td class="row1">房源描述：</td>
						<td colspan="3"><span class="tip">&nbsp;详细、生动地描述房源全面信息，赢得客户，避免无效带看</span><br>
						<div style="width:100%;"><input type="hidden" id="house_desc" name="house_desc" value='<!--{$dataInfo.house_desc}-->' /><input type="hidden" id="house_desc___Config" value="FullPage=false"/><iframe id="house_desc___Frame" src="<!--{$cfg.path.url_lib}-->FCKeditor/editor/fckeditor.html?InstanceName=house_desc&amp;Toolbar=Basic" width="100%" height="220" frameborder="no" scrolling="no"></iframe></div>
					  <p><br>&nbsp;<span class="tip">请勿在房源描述内添加其它房产网站链接</span><br>&nbsp;<span class="tip">请勿在房源描述内留联系电话</span></p></td>
					</tr>
					<tr><td colspan="4" class="br"></td></tr>
				</tbody>
			</table>
             <table class="memberBoxTable" cellpadding="0" cellspacing="5" border="0">
				<thead>
					<tr>
						<td colspan="4"><span class="concentTitle">第二步：补充信息（选填项）</span></td>
					</tr>
				</thead>
				<tbody>
             
                    <tr>
						<td class="row1">朝&nbsp;&nbsp;&nbsp;&nbsp;向：</td>
						<td colspan="3">
							<!--{foreach name=house_toward from=$house_toward_option item=item key=key}-->
							<!--{if $key == $dataInfo.house_toward}-->
								<label for="house_toward_<!--{$key}-->"><input  type="radio" name="house_toward" id="house_toward_<!--{$key}-->" value="<!--{$key}-->"  errmsg="请选择房屋朝向!" checked /><!--{$item}--> </label>
							<!--{else}-->
								<label for="house_toward_<!--{$key}-->"><input  type="radio" name="house_toward" id="house_toward_<!--{$key}-->" value="<!--{$key}-->"  errmsg="朝向必择!" /><!--{$item}--> </label>
							<!--{/if}-->
						<!--{/foreach}-->
						<p id="errMsg_house_toward" class="errorMessage" display="none"></p>
						</td>
					</tr>
                    <tr>
						<td class="row1">房源特色：</td>
						<td colspan="3">
						<!--{foreach name=house_feature_group from=$house_feature_option item=group key=key}-->
						<p><!--{$house_feature_group.$key}-->：&nbsp;
							<!--{foreach name=house_feature from=$group item=house_feature key=feature_id}-->
							<!--{if in_array($feature_id,$dataInfo.house_feature)}-->
								<label for="house_feature_<!--{$feature_id}-->"><input  type="checkbox" name="house_feature[]" id="house_feature_<!--{$feature_id}-->" value="<!--{$feature_id}-->" checked onclick="return checkBoxNum()" /><!--{$house_feature}--> </label>
							<!--{else}-->
								<label for="house_feature_<!--{$feature_id}-->"><input  type="checkbox" name="house_feature[]" id="house_feature_<!--{$feature_id}-->" value="<!--{$feature_id}-->" onclick="return checkBoxNum()"  /><!--{$house_feature}--> </label>
							<!--{/if}-->
							<!--{/foreach}-->
						</p>
						<!--{/foreach}-->
						<span class="tip">最多选择4项</span>
						<p id="errMsg_house_feature" class="errorMessage" display="none"></p>
					</tr>
                    <tr>
						<td class="row1">房源配套：</td>
						<td colspan="3">
						<!--{foreach name=house_installation from=$house_installation_option item=item key=key}-->
							<!--{if in_array($key,$dataInfo.house_support)}-->
								<label for="house_support_<!--{$key}-->"><input  type="checkbox" name="house_support[]" id="house_support_<!--{$key}-->" value="<!--{$key}-->"  checked /><!--{$item}--> </label>
							<!--{else}-->
								<label for="house_support_<!--{$key}-->"><input  type="checkbox" name="house_support[]" id="house_support_<!--{$key}-->" value="<!--{$key}-->" /><!--{$item}--> </label>
							<!--{/if}-->
							<!--{if $smarty.foreach.house_installation.iteration % 7 == 0}-->
							<br>
							<!--{/if}-->
						<!--{/foreach}-->
					</tr>
					<tr>
						<td class="row1">户型图：</td>
						<td colspan="3">
				<span class="tip" style="float:left; padding-top:10px;">上传户型图</span><span><iframe name="uploadHouseDrawing" width="100%" height="25" scrolling="No" frameborder="no"  src="<!--{$cfg.url}-->upload.php?to=uploadHouseDrawing|borough|drawing" align="left"></iframe></span><input type="hidden" id="house_drawing" name="house_drawing" value="<!--{$dataInfo.house_drawing}-->"></p><p id="house_drawing_dis" style="clear:both;"><!--{if $dataInfo.house_drawing}--><img src="<!--{$cfg.url}-->upfile/<!--{$dataInfo.house_drawing}-->" width="120" height="90"><!--{/if}-->
						 </td>
					</tr>
                     <tr>
						<td class="row1">房屋视频：</td>
						<td><span class="tip">&nbsp;可粘贴优酷土豆的站外引用代码</span><br />
							<input id="video" class="input" name="video" type="text" size="60" value='<!--{$dataInfo.video}-->' />
						</td>
					</tr>
					<tr>
						<td class="row1">室内照片：</td>
						<td>		<div style="width: 180px; height: 18px; border: solid 1px #E7F1A2; background-color:#F1F8C5; padding: 2px;">
			<span id="spanButtonPlaceholder"></span>
		</div><span class="tip" style="margin-left:15px; line-height:30px">&nbsp;可选择多个图片同时上传</span>
		<div id="divFileProgressContainer"></div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="house_picture_dis">
							<!--{foreach name=house_pic from=$dataInfo.house_pic item=item key=key}-->
								<div class="upload_shower" id="container_picture_<!--{$smarty.foreach.house_pic.index}-->">
									<a href="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->"><img src="<!--{$cfg.url}-->upfile/<!--{$item.pic_url}-->" width="160" height="120"></a>
									<br/><input type="text" name="house_picture_desc[]" value="<!--{$item.pic_desc}-->" size="12">
									<input type="hidden" name="house_picture_url[]" value="<!--{$item.pic_url}-->">
									<input type="hidden" name="house_picture_thumb[]" value="<!--{$item.pic_thumb}-->">
									<input type="button" name="deletePicture_<!--{$smarty.foreach.house_pic.index}-->" onclick="dropContainer('container_picture_<!--{$smarty.foreach.house_pic.index}-->')" value="删">
								</div>
							<!--{/foreach}-->
							</div>
						</td>
					</tr>
					<!--{if !$dataInfo.id}-->
					<tr>
						<td class="row1">小区照片：</td>
						<td><span class="tip">&nbsp;小于500K</span><br />
							<iframe name="uploadBoroughPicture" width="100%" height="35" scrolling="No" frameborder="no"  src="<!--{$cfg.url}-->upload.php?to=uploadBoroughPicture|borough|picture" align="left"></iframe>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="borough_picture_dis"></div>
						</td>
					</tr>
					<!--{/if}-->
				</tbody>
			</table>
              <table class="memberBoxTable" cellpadding="0" cellspacing="5" border="0">
				<thead>
					<tr>
						<td colspan="4"><span class="concentTitle">第三步：业主信息（选填）</span></td>
					</tr>
				</thead>
				<tbody>
                     <tr>
						<td class="row1">业主姓名：</td>
						<td><span class="tip">&nbsp;请输入此房源的业主姓名</span><br />
							<input id="owner_name" class="input" name="owner_name"  type="text" size="40" value='<!--{$dataInfo.owner_name}-->' />
						</td>
					</tr>
                    <tr>
						<td class="row1">联系电话：</td>
						<td><span class="tip">&nbsp;请输入此房源的业主联系电话</span><br />
							<input id="owner_phone" class="input" name="owner_phone" type="text" size="40" value='<!--{$dataInfo.owner_phone}-->' />
						</td>
					</tr>
                      <tr>
						<td class="row1">备注：</td>
						<td><span class="tip">&nbsp;业主备注信息</span><br />
                          <textarea name="owner_notes" cols="40" rows="5" class="textarea" id="owner_notes"><!--{$dataInfo.owner_notes}--></textarea>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="submitPromsie"><input  id="check_agree" name="check_agree" type="checkbox" checked />我承诺我所发布的房源信息全部属实，我接受 <!--{$cfg.page.titlec}-->的用户协议</div>
		<!--{if $dataInfo.id}-->
  <div class="submitBtn"><input type="submit" value="确认编辑" /></div>
   <!--{else}-->
			<div class="submitBtn"><input type="submit" value="立即发布" /></div>
            <!--{/if}-->
			</form>
		</div>
	</div>
	
</div>

<script type="text/javascript">
initValid(document.dataForm);

function checkForm(formDom){   
	var Content =FCKeditorAPI.GetInstance("house_desc").GetXHTML();   
	if(Content==null||Content==""){   
		alert('内容不能为空');   
		return false;    
	}else{
		return validator(formDom);
	} 
}   

function uploadHouseDrawing( furl,fname,thumbUrl ){
	document.getElementById('house_drawing').value = furl;
	//document.getElementById('borough_thumb_desc').value = fname;
	document.getElementById('house_drawing_dis').innerHTML = '<a href="<!--{$cfg.url}-->upfile/'+furl+'"><img src="<!--{$cfg.url}-->upfile/'+thumbUrl+'" width="120" height="90"></a>';
}
var pictureIndex=<!--{$picture_num}-->;
var borough_name_val = $('#borough_name').val();
function uploadHousePicture( furl,fname,thumbUrl ){
	document.getElementById('house_picture_dis').innerHTML += '<div class="upload_shower" id="container_picture_'+pictureIndex+'">\
	<a href="<!--{$cfg.url}-->upfile/'+furl+'"><img src="<!--{$cfg.url}-->upfile/'+thumbUrl+'" width="160" height="120"></a>\
	<br/><input type="text" name="house_picture_desc[]" value="'+borough_name_val+'" size="15">\
	<input type="hidden" name="house_picture_url[]" value="'+furl+'">\
	<input type="hidden" name="house_picture_thumb[]" value="'+thumbUrl+'">\
	<input type="button" name="deletePicture_'+pictureIndex+'" onclick="dropContainer(\'container_picture_'+pictureIndex+'\')" value="删">\
	</div>';
	pictureIndex++;
}

var boroughIndex=0;
function uploadBoroughPicture( furl,fname,thumbUrl ){
	document.getElementById('borough_picture_dis').innerHTML += '<div class="upload_shower" id="container_borough_picture_'+boroughIndex+'">\
	<a href="<!--{$cfg.url}-->upfile/'+furl+'"><img src="<!--{$cfg.url}-->upfile/'+thumbUrl+'" width="160" height="120"></a>\
	<br/><input type="text" name="borough_picture_desc[]" value="'+borough_name_val+'" size="15">\
	<input type="hidden" name="borough_picture_url[]" value="'+furl+'">\
	<input type="hidden" name="borough_picture_thumb[]" value="'+thumbUrl+'">\
	<input type="button" name="delete_borough_picture_'+boroughIndex+'" onclick="dropContainer(\'container_borough_picture_'+boroughIndex+'\')" value="删">\
	</div>';
	boroughIndex++;
}
function dropContainer(containerId){
	var containerEle = document.getElementById(containerId);
	var parentEle = containerEle.parentElement;
	parentEle.removeChild(containerEle);
}
</script>
</body>
</html>