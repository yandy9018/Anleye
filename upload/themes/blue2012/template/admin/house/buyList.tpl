<!--{include file="admin/header.tpl"}-->
<!--列表-->

<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>求购管理</caption>
  <tr>
    <td>
     <a href="buy.php"><!--{if !isset($smarty.get.status) }--><font class="red">全部</font><!--{else}-->全部<!--{/if}--></a> | 
     <a href="buy.php?status=0"><!--{if isset($smarty.get.status) && $smarty.get.status==0}--><font class="red">未审核</font><!--{else}-->未审核<!--{/if}--></a> | 
     <a href="buy.php?status=1"><!--{if $smarty.get.status==1}--><font class="red">已审核</font><!--{else}-->已审核<!--{/if}--></a>
    </td>
  </tr>
</table>

<form name="search" method="get" action="buy.php">
<input type="hidden" name="action" value="search">
<table cellpadding="0" cellspacing="1" class="table_form">
	<caption>信息查询</caption>
	<tr>
		<td class="align_c">
			<input type="text" name="q" onblur="if(this.value ==''||this.value == '请输入联系人或联系电话'){this.value = '请输入联系人或联系电话';this.style.color = '#999999';}" onfocus="if(this.value == '请输入联系人或联系电话'){this.value = '';this.style.color = '#333333';}" value="<!--{if $q}--><!--{$q}--><!--{else}-->请输入联系人或联系电话<!--{/if}-->" size="35" />&nbsp;
			<input type="submit" name="dosubmit" value=" 查询 " />
		</td>
	</tr>
</table>
</form>

<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<caption>信息列表</caption>
	<tr>
		<th width="30">选中</th>
		<th width="60">联系人</th>
		<th width="80">联系电话</th><br>
		<th>房源要求</th>
		<th width="80">发布时间</th>
		<th width="45">状态</th>
		<th width="45">解决</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.username}-->"></td>
	<td class="align_c"><!--{$item.linkman}--></td>
	<td class="align_c"><!--{$item.link_tell}--></td>
	<td><!--{$item.requirement}--></td>
	
	<td class="align_c"><!--{$item.add_time|date_format:"%Y-%m-%d %T"}--></td>
	<td class="align_c"><!--{if $item.status==0}-->未审核<!--{/if}--><!--{if $item.status==1}--><font color="red">已通过</font><!--{/if}--><!--{if $item.status==2}--><font color="red">已删除</font><!--{/if}--></td>

	<td class="align_c"><!--{if $item.is_solve==0}-->未解决<!--{/if}--><!--{if $item.is_solve==1}--><font color="red">已解决</font><!--{/if}--></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="lock" value=" 审核通过 " onclick="if(confirm('你确认审核通过选中的条目么？')){myform.action='buy.php?action=status&dostatus=1';myform.submit();}">
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='buy.php?action=status&dostatus=2';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
