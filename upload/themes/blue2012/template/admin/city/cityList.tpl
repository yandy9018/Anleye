<!--{include file="admin/header.tpl"}-->
<!--列表-->

<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>多城市管理</caption>
  <tr>
    <td>
     	<a href="index.php?action=add">城市添加</a>
    </td>
  </tr>
</table>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
	<input type="hidden" name="place_id" value="<!--{$place_id}-->">
	<caption>城市列表</caption>
	<tr>
		<th width="30">选中</th>
		<th>城市LOGO</th>
		<th>城市名称</th>
		<th>城市连接地址</th>
		<th>是否加盟</th>
        <th>是否热门</th>
		<th>状态</th>
        <th>排序</th>
		<th>管理操作</th>
	</tr>
<tbody>
<!--{foreach item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->" title="<!--{$item.name}-->"></td>
	<td class="align_c"><img src="<!--{$cfg.url}-->upfile/<!--{$item.logo}-->" width="85" height="85" ></td>
	<td class="align_c"><!--{$item.name}--></td>
	<td class="align_c"><a href="<!--{$item.url}-->"  target="_blank"><!--{$item.url}--></a></td>
	<td class="align_c"><!--{if $item.is_proxy==0}-->直营<!--{elseif $item.is_proxy==1}-->加盟<!--{/if}--></td>
    <td class="align_c"><!--{if $item.is_hot==0}-->否<!--{elseif $item.is_hot==1}--><font color="Red">是</font><!--{/if}--></td>
    <td class="align_c"><!--{if $item.status == 0}-->开通<!--{elseif  $item.status == 1}--><font color="Red">未开通</font><!--{/if}--></td>
    <td class="align_c"><!--{$item.pid}--></td>
	<td class="align_c"><a href="index.php?action=edit&id=<!--{$item.id}-->">修改</a></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
	<input type="button" name="delete" value=" 删除 " onclick="if(confirm('你确认删除选中的条目么？')){myform.action='index.php?action=delete';myform.submit();}">
	<input type="button" name="lock" value=" 未开通 " onclick="if(confirm('你确认锁定选中的条目么？')){myform.action='index.php?action=lock';myform.submit();}">
	<input type="button" name="unlock" value=" 开通 " onclick="if(confirm('你确认解锁选中的条目么？')){myform.action='index.php?action=unlock';myform.submit();}">
</div>
<!--{$pagePanel}-->

</form>
<!--{include file="admin/footer.tpl"}-->
