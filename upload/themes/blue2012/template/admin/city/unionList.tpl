<!--{include file="admin/header.tpl"}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>加盟信息管理</caption>

  <td>
  <a href="union.php?check=0"><!--{if $smarty.get.check==0}--><font class="red">全部信息</font><!--{else}-->全部信息<!--{/if}--></a> |
  <a href="union.php?check=1"><!--{if $smarty.get.check==1}--><font class="red">未受理</font><!--{else}-->未受理<!--{/if}--></a> |
   <a href="union.php?check=2"><!--{if $smarty.get.check==2}--><font class="red">已回访</font><!--{else}-->已回访<!--{/if}--></a>
   | <a href="union.php?check=3"><!--{if $smarty.get.check==3}--><font class="red">已受理</font><!--{else}-->已受理<!--{/if}--></a>
  </td>

</table>


<form name="myform" method="post" action="">
<table width="921" cellpadding="2" cellspacing="2" class="table_list">
	<caption>加盟信息管理</caption>
	<tr>
		<th width="28">选中</th>
		<th width="118">公司名称</th>
		<th width="78">城市名称</th>
		<th width="78">公司地址</th>
        	<th width="78">联系人</th>
		<th width="88">手机</th>
		<th width="52">固定电话</th>
        <th width="58">申请时间</th>
        <th width="129">状态</th>

	</tr>
<tbody>
<!--{foreach name=data item=item from=$dataList }-->
  <tr>
  	<td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->"></td>
	<td><!--{$item.cname}-->&nbsp;</td>
    
	<td class="align_c">
    <!--{$item.cityid}-->
    </td>
	<td class="align_c"><!--{$item.address}--></td>
    <td class="align_c"><!--{$item.con}--></td>
    
	<td class="align_c"><!--{$item.mo}--></td>
	<td class="align_c">
	  <!--{$item.tel}-->
	  </td>
       <td class="align_c">
          <!--{$item.time|date_format:"%Y-%m-%d"}-->
        </td>

    <td class="align_c">
    <!--{if $item.status==0}--><font color="#FF0000">未受理</font><!--{elseif $item.status==1}-->已回访 <!--{else $item.status==2}-->已受理 <!--{/if}-->
    </td>
   
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>


    <input type="button" value=" 删除 " onclick="if(confirm('你确认删除选中的加盟信息么？')){myform.action='union.php?action=delete';myform.submit();}">
    <input type="button" value=" 回访 " onclick="if(confirm('你确定已经回访该加盟信息了么？')){myform.action='union.php?action=dostatus&status=1';myform.submit();}">
  <input type="button" value=" 受理 " onclick="if(confirm('你确定受理该加盟信息么？')){myform.action='union.php?action=dostatus&status=2';myform.submit();}">



</div>
<!--{$pagePanel}-->
<div id="pic_shower" style="position:absolute;color:white; width:320px; right:-30px; top:-10px; z-index:10000;background-color:#6680aa;display:none;"></div>
</form>

<!--{include file="admin/footer.tpl"}-->
