<!--{include file="admin/header.tpl"}-->
<!--{if $cfg.page.action=='add'}-->
<form id="form1" name="form1" method="post" action="index.php?action=save" onsubmit="return validator(this)">
<input name="id" type="hidden" value="<!--{$dataInfo.id}-->" />
<input name="company[type]" type="hidden" value="<!--{$smarty.get.type}-->" />
<table cellpadding="2" cellspacing="1"  class="table_form">
 <caption>增加公司</caption>
  <!--{if $smarty.get.type==0}-->
  <tr>
    <th><strong>登录帐号：</strong></th>
    <td><input name="company[username]" value="<!--{$dataInfo.username}-->" type="text" maxlength="16" /></td>
  </tr>
  
   <tr>
    <th><strong>登录密码：</strong></th>
    <td><input name="company[passwd]" value="" type="text" maxlength="16" /> 修改时留空即不修改密码</td>
  </tr>
 <!--{/if}-->
 
  <tr>
    <th><strong>公司名称：</strong></th>
    <td><input name="company[company_name]" value="<!--{$dataInfo.company_name}-->" type="text" maxlength="16" /></td>
  </tr>
  <tr>
    <th><strong>公司地址：</strong></th>
    <td><input name="company[company_address]" value="<!--{$dataInfo.company_address}-->" type="text" maxlength="16"  /></td>
  </tr>
   <tr>
    <th><strong>联系人：</strong></td>
    <td><input name="company[company_clerk]" value="<!--{$dataInfo.company_clerk}-->" type="text" maxlength="16" /></td>
  </tr>
  <tr>
    <th><strong>联系电话：</strong></td>
    <td><input name="company[company_phone]" value="<!--{$dataInfo.company_phone}-->" type="text" maxlength="16" /></td>
  </tr>
   <tr>
    <th><strong>E-MAIL：</strong></td>
    <td><input name="company[email]" value="<!--{$dataInfo.email}-->" type="text" maxlength="16" /></td>
  </tr>
   <tr>
    <th><strong>在线QQ：</strong></td>
    <td><input name="company[qq]" value="<!--{$dataInfo.qq}-->" type="text" maxlength="16" /></td>
  </tr>
     <tr>
    <th><strong>业务描述：</strong></td>
    <td><input name="company[company_description]" type="text" value="<!--{$dataInfo.company_description}-->"  maxlength="16"  /></td>
  </tr>   
  
  <tr>
    <th><strong>网站样式：</strong></td>
    <td><select name="company[company_style]">
      <option value="shopStyleDefault.css" selected="selected">默认风格</option>
      <option value="shopStylePink.css">粉红风格</option>
       <option value="shopStyleBlue.css">蓝色风格</option>
        <option value="shopStyleGreen.css">绿色风格</option>
        <option value="shopStyleSilver.css">银色风格</option>
    </select>
   </td>
  </tr>   
    
  <tr>
    <th><strong>关于我们：</strong></th>
    <td><input type="hidden" id="company_about" name="company[company_about]" value="<!--{$dataInfo.company_about}-->" /><input type="hidden" id="company_about___Config" value="FullPage=false"/><iframe id="company_about___Frame" src="<!--{$cfg.path.url_lib}-->FCKeditor/editor/fckeditor.html?InstanceName=company_about&amp;Toolbar=Basic" width="100%" height="220" frameborder="no" scrolling="no"></iframe></td>
  </tr>
    <tr>
    <th><strong>LOGO上传：</strong></th>
 <td>
 
 <input type="hidden" name="company[company_logo]" id="image_url" value="<!--{$dataInfo.company_logo}-->" >
<input type="hidden" name="image_thumb" id="image_thumb" value="<!--{$dataInfo.image_thumb}-->">
		        	<div id="image_dis"><!--{if $dataInfo.company_logo}--><img src="<!--{$cfg.url}-->upfile/<!--{ $dataInfo.company_logo}-->" width="240" height="70" /><!--{/if}--></div>
		        
		        	<iframe name="uploadImage" width="100%" height="35" scrolling="No" frameborder="no"  src="<!--{$cfg.url}-->upload.php?to=uploadImage|company|logo" align="left"></iframe>
                   </td> 
         </tr>             
                    
  
  <tr>
    <th>&nbsp;</th>
    <td><input type="submit" name="submit" value="保存" /></td>
  </tr>

</table>
<script language="javascript">
function checkForm(formDom){   
	var Content =FCKeditorAPI.GetInstance("company_about").GetXHTML();   
	if(Content==null||Content==""){   
		alert('内容不能为空');   
		return false;    
	}else{
		return validator(formDom);
	} 
}   

function uploadImage( furl,fname,thumbUrl ){
	
	document.getElementById('image_url').value = furl;
	document.getElementById('image_thumb').value = thumbUrl;
	document.getElementById('image_dis').innerHTML = '<img src="<!--{$cfg.url}-->upfile/'+furl+'" width="240" height="70">';
}
</script>
<script src="<!--{$cfg.path.js}-->My97DatePicker/WdatePicker.js" language="javascript"></script>
</form>
<!--{elseif $cfg.page.action=='right'}-->
<form method="post" action="user.php?action=right">

<table cellspacing="1" cellpadding="0" class="table_form" >
<!--{foreach item=item from=$menus}-->
  <tr>
    <td><b><!--{$item.caption}--></b></td>
  </tr>
  <!--{foreach item=item2 from=$item.sub key=key}-->
  <tr>
    <td height="25">&nbsp;&nbsp;<input <!--{if $userRight.$key}--> checked="checked"<!--{/if}--> type="checkbox" name="rights[<!--{$key}-->]" value="1"><!--{$item2.caption}--></td>
  </tr>
  <!--{/foreach}-->
<!--{/foreach}-->
	<tr>
		<td><input type="submit" class="button" value="保存"></td>
	</tr>
</table>
</form>

<!--{else}-->
<!--列表-->
<table cellpadding="0" cellspacing="1" class="table_info">
  <caption>公司列表</caption>
  <tr>
    <td><a href="index.php?type=<!--{$smarty.get.type}-->"><font class="red">公司列表</font></a> | <a href="index.php?action=add&type=<!--{$smarty.get.type}-->">添加公司</a></td>
  </tr>
</table>
<form name="myform" method="post" action="">
<table cellpadding="2" cellspacing="1" class="table_list">
<caption>公司列表</caption>
  <tr>
    <th width="30">选中</th>
    <th width="200">公司名称</th>
    <th>公司地址</th>
    <th width="40">状态</th>
    <th width="150">操作</th>
  </tr>
<tbody>
<!--{foreach item=item from=$companyList}-->
  <tr id="tr_<!--{$item.user_id}-->">
    <td class="align_c" ><input type="checkbox" name="ids[]" value="<!--{$item.id}-->"></td>
    <td class="align_c"><!--{$item.company_name}--></td>
    <td> <!--{$item.company_address}--></td>
     <td><!--{if $item.status==0}-->正常<!--{elseif $item.status==1}--><font color="#FF0000">推荐</font><!--{/if}--></td>
    <td class="align_c"><a href="?action=edit&id=<!--{$item.id}-->&type=<!--{$item.type}-->">编辑</a></td>
  </tr>
<!--{/foreach}-->
</tbody>
</table>
<div class="button_box">
	<span style="width:60px"><a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', true);return false;">全选</a>/<a href="#" onclick="javascript:$('input[type=checkbox]').attr('checked', false)">取消</a></span>
<input type="button" value=" 删除 " onclick="if(confirm('确定删除该中介公司么？')){myform.action='index.php?action=delete';myform.submit();}">
<input type="button" value=" 推荐 " onclick="if(confirm('确定推荐该中介公司么？')){myform.action='index.php?action=status&dostatus=1';myform.submit();}">
<input type="button" value=" 取消推荐 " onclick="if(confirm('确定取消推荐该中介公司么？')){myform.action='index.php?action=status&dostatus=0';myform.submit();}">
</div>
<!--{$pagePanel}-->
</form>
<!--{/if}-->
</div>
<!--{include file="admin/footer.tpl"}-->
