<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--{include file="inc/head.tpl"}-->
<script src="<!--{$cfg.path.js}-->jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="<!--{$cfg.path.js}-->select.js" type="text/javascript"></script>
<script src="<!--{$cfg.path.js}-->js.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="<!--{$cfg.path.js}-->FormValid.js"></script>
<link href="<!--{$cfg.path.css}-->add.css" rel="stylesheet" type="text/css" />
<link href="<!--{$cfg.path.css}-->menuSearch.css" rel="stylesheet" type="text/css" />

</head>
 
<body>
 
<!--{include file="inc/top.tpl"}-->
 
<div id="content">
<div id="requireMain">
		<div class="requireTitle">
			 <!--{$cfg.page.city}-->二手房 - 求租信息
		</div>
		<div class="requireListCtrl">
			<div class="search">
				<form name="searchFormThis" method="GET" action="">
					<input class="text" name="q" type="text" onblur="if(this.value ==''||this.value == '输入求购联系人或联系电话'){this.value = '输入求购联系人或联系电话';}" onfocus="if(this.value == '输入求购联系人或联系电话'){this.value = '';}" value="<!--{if $smarty.get.q==""}-->输入求购联系人或联系电话<!--{else}--><!--{$smarty.get.q}--><!--{/if}-->"  />
					<input class="button" name="Submit" type="button" value="搜索" onclick="document.searchFormThis.submit();" />
				</form>
			</div>
		</div>
		<div class="requireDataListBox">
			<table class="tHead" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<td width="10%">&nbsp;</td>
						<td width="75%" class="tdAlignLeft" >求租要求</td>
						
						<td width="15%">发布时间</td>
					</tr>
				</thead>
			</table>
			<table class="tBody" cellpadding="0" cellspacing="0" border="0">
				<tbody>
				<!--{foreach from=$dataList key=key item=item}-->
					<tr onmouseover="this.className='mouseOver'" onmouseout="this.className=''">
						<td width="10%">[求租]</td>
						<td width="75%" class="tdAlignLeft" ><a class="color690 aUnderline" href="requireDetail.php?id=<!--{$item.id}-->" target="_blank"><!--{$item.requirement_short}--></a></td>
						
						<td width="15%"><!--{$item.add_time|date_format:"%Y-%m-%d"}--></td>
					</tr>
				<!--{/foreach}-->
				</tbody>
			</table>
		</div>
		<!-- 分页 -->
		<!--{include file="inc/page_fenye.tpl"}-->
		<!-- 分页 结束 -->
	</div>
</div>
<!--{include file="inc/foot.tpl"}-->
</body>
</html>
