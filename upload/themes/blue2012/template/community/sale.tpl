<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--{include file="inc/head.tpl"}-->
<SCRIPT src="<!--{$cfg.path.js}-->jquery-1.4.2.min.js" type=text/javascript></SCRIPT>
<script language="javascript">
function changeSearch(formDom){
	location.href=formDom.value;
}
function quickSearch(domValue,domType){
	var inputDom = document.getElementById(domType);
	if(inputDom){
		inputDom.value=domValue;
	}
	var priceValue = document.getElementById('price').value;
	var roomValue = document.getElementById('room').value;
	var totalareaValue = document.getElementById('totalarea').value;
	var list_numValue = document.getElementById('list_num').value;
	var list_orderValue = document.getElementById('list_order').value;
	location.href='sale.php?id=<!--{$dataInfo.id}-->&price='+priceValue+'&room='+roomValue+'&totalarea='+totalareaValue+'&list_num='+list_numValue+'&list_order='+list_orderValue;
	return false;
}
</script>

</head>

<body>
<!--{include file="inc/top.tpl"}-->
<div class="content">
<!--{include file="inc/community_view_nav.tpl"}-->
<div class="view">
<!--{include file="inc/community_view_left.tpl"}-->
<div class="view_right">
<div class="view_content">
<!--{include file="inc/community_right_menu.tpl"}-->
<div class="view_right_cc">
  <div id="b1"> 
    <div style="padding: 16px 0pt 0pt 20px;" class="view_regionc">
    <input type="hidden" id="price" name="price" value="<!--{$smarty.get.price}-->">
    <input type="hidden" id="search_type" name="search_type" value="<!--{$smarty.get.search_type}-->">
    <input type="hidden" id="cityarea" name="cityarea" value="<!--{$smarty.get.cityarea}-->">
    <input type="hidden" id="totalarea" name="totalarea" value="<!--{$smarty.get.totalarea}-->">
    <input type="hidden" id="room" name="room" value="<!--{$smarty.get.room}-->">
    <input type="hidden" id="list_num" name="list_num" value="10">
    <input type="hidden" id="list_order" name="list_order" value="">
            <dl>
            <dt>售价：</dt>
            <dd>
            <a href="sale.php?id=<!--{$dataInfo.id}-->&room=<!--{$smarty.get.room}-->&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('','price');"><!--{if $smarty.get.price==''}--><span>不限</span><!--{else}-->不限<!--{/if}--></a>
            <!--{foreach from=$house_price_option item=item key=key }-->
				<!--{if $key==$smarty.get.price}-->
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$key}-->&room=<!--{$smarty.get.room}-->&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('<!--{$key}-->','price');"><span><!--{$item}--></span></a>
                <!--{else}-->
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$key}-->&room=<!--{$smarty.get.room}-->&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('<!--{$key}-->','price');"><!--{$item}--></a>
                <!--{/if}-->
			<!--{/foreach}-->
            </dd>
            </dl>
            <dl>
            <dt>面积：</dt>
            <dd>
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&room=<!--{$smarty.get.room}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('','totalarea');"><!--{if $smarty.get.totalarea==''}--><span>不限</span><!--{else}-->不限<!--{/if}--></a>
            <!--{foreach from=$house_totalarea_option item=item key=key }-->
            <!--{if $key==$smarty.get.totalarea}-->
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&room=<!--{$smarty.get.room}-->&totalarea=<!--{$key}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('<!--{$key}-->','totalarea');"><span><!--{$item}--></span></a>
            <!--{else}-->
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&room=<!--{$smarty.get.room}-->&totalarea=<!--{$key}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('<!--{$key}-->','totalarea');"><!--{$item}--></a>
            <!--{/if}-->
            <!--{/foreach}-->
            </dd>
            </dl>
            <dl>
            <dt>户型：</dt>
            <dd>
             <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('','room');"><!--{if $smarty.get.room==''}--><span>不限</span><!--{else}-->不限<!--{/if}--></a>
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&room=1&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('1','room');"><!--{if $smarty.get.room==1}--><span>一室</span><!--{else}-->一室<!--{/if}--></a>
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&room=2&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('2','room');"><!--{if $smarty.get.room==2}--><span>二室</span><!--{else}-->二室<!--{/if}--></a>
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&room=3&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('3','room');"><!--{if $smarty.get.room==3}--><span>三室</span><!--{else}-->三室<!--{/if}--></a>
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&room=4&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('4','room');"><!--{if $smarty.get.room==4}--><span>四室</span><!--{else}-->四室<!--{/if}--></a>
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&room=5&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('5','room');"><!--{if $smarty.get.room==5}--><span>五室</span><!--{else}-->五室<!--{/if}--></a>
            <a href="sale.php?id=<!--{$dataInfo.id}-->&price=<!--{$smarty.get.price}-->&room=6&totalarea=<!--{$smarty.get.totalarea}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->" onclick="return quickSearch('6','room');"><!--{if $smarty.get.room==6}--><span>五室以上</span><!--{else}-->五室以上<!--{/if}--></a>
            </dd>
            </dl>
        </div>
	</div>
</div>
</div>
<div class="view_content">

<div id="view_xx_top">
<ul>
<li class="li_bg">全部房源</li>
</ul>
<div class="view_xx_top_next"><table cellspacing="0" cellpadding="0" border="0">
				<tbody><tr>
				  <td width="190" height="30">共找到 <span style="color: rgb(97, 160, 13); font-weight: bold;"><!--{$row_count}--></span> 套符合要求的房子</td>
					<td width="23" align="left">
                    	<!--{if $pageno > 1}-->
                    	<a href="sale.php?id=<!--{$dataInfo.id}-->&letter=<!--{$smarty.get.letter}-->&room=<!--{$smarty.get.room}-->&totalarea=<!--{$smarty.get.totalarea}-->&price=<!--{$smarty.get.price}-->&type=<!--{$smarty.get.type}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->&pageno=<!--{$pageno-1}-->" onfocus=blur()><img src="<!--{$cfg.path.images}-->rl_page_Prev_bg.png" border="0" class="RL-Prev"/></a>
                        <!--{else}-->
                        <img src="<!--{$cfg.path.images}-->rl_page_Prev_null_bg.png" border="0" class="RL-Prev"/>
                        <!--{/if}-->
                        
                    </td>
					<td id="RL_NoPage"><font><!--{$pageno}--></font>/<!--{$page_count}--></td>
				  <td width="23" align="right">
                  <!--{if $pageno < $page_count}--><a href="sale.php?id=<!--{$dataInfo.id}-->&letter=<!--{$smarty.get.letter}-->&room=<!--{$smarty.get.room}-->&totalarea=<!--{$smarty.get.totalarea}-->&price=<!--{$smarty.get.price}-->&type=<!--{$smarty.get.type}-->&list_num=<!--{$smarty.get.list_num}-->&list_order=<!--{$smarty.get.list_order}-->&pageno=<!--{$pageno+1}-->" onfocus=blur()><img src="<!--{$cfg.path.images}-->rl_page_Next_bg.png" border="0" class="RL-Next"/></a><!--{else}--><img src="<!--{$cfg.path.images}-->rl_page_Next_Null_bg.png" border="0" class="RL-Next"/><!--{/if}-->
                  </td>
				</tr>
	  </tbody></table></div>
</div>
<div class="view_all">
  <div class="all_fy_bgl"> 
  	<a onclick="quickSearch('update_order desc','list_order');" onfocus="blur()" class="<!--{if $smarty.get.list_order=='update_order desc'}-->input_mrpxx<!--{else}-->input_mrpx<!--{/if}-->" href="javascript:void(0);">
    	默认排序
    </a> 
    <a onclick="quickSearch('house_price asc','list_order');return false;" onfocus="blur()" class="<!--{if $smarty.get.list_order=='house_price asc'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" href="javascript:void(0);">
    	总价↓
    </a> 
    <a onclick="quickSearch('house_totalarea asc','list_order');return false;" onfocus="blur()" class="<!--{if $smarty.get.list_order=='house_totalarea asc'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" href="javascript:void(0);">
    	面积↓
    </a> 
    <a onclick="quickSearch('avg_price asc','list_order');return false;" onfocus="blur()" class="<!--{if $smarty.get.list_order=='avg_price asc'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" href="javascript:void(0);">
    	单价↓
    </a>
   <a onclick="quickSearch('created desc','list_order');return false;" onfocus="blur()" class="<!--{if $smarty.get.list_order=='created desc'}-->input_zj<!--{else}-->input_pxbg56<!--{/if}-->" href="javascript:void(0);">
        时间
    </a>
     
  </div>
</div>
</div>
<div class="view_content">
<!--{foreach from=$dataList item=item key=key}-->
	<ol id="list_body">
	<div class="h15"></div>
	<div class="view_property">
	    <div style="margin-left: 10px; margin-top: 3px;" class="photo">
	        <a target="_blank" alt="" title="" href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html"><img alt="<!--{$item.borough_name}-->" src="<!--{if $item.house_thumb}--><!--{$cfg.url_upfile}--><!--{$item.house_thumb}--><!--{else}--><!--{$cfg.path.images}-->housePhotoDefault.gif<!--{/if}-->" id="apf_id_69_a" class="thumbnail" title="<!--{$item.borough_name}-->"></a>	    </div>
	    <div class="view_details">
	        <h4>
	            <a target="_blank" title="<!--{$item.borough_name}-->" href="<!--{$cfg.url_sale}-->d-<!--{$item.id}-->.html"><!--{$item.house_title}--></a>
        <!--{if $item.is_hot==1}--><span><img src="<!--{$cfg.path.images}-->sale_rd.gif" title="热门"></span ><!--{/if}--><!--{if $item.is_more_pic==1}--><span><img src="<!--{$cfg.path.images}-->duotu_sale.gif" titile="多图"></span ><!--{/if}--></h4>
	        <address>小区：<!--{$item.borough_name}--> </address>
	        <div class="view_ppp">
               
                <p>售价：<span><!--{$item.house_price}-->万 </span> </p>
                <p>房型：<!--{$item.house_room}-->室<!--{$item.house_hall}-->厅<!--{$item.house_toilet}-->卫</p>
                <p>面积：<!--{$item.house_totalarea}-->平米 </p> <br />
                <p>楼层：<!--{$item.house_floor}-->/<!--{$item.house_topfloor}--> </p> 
                 <p>单价：<!--{$item.avg_price}-->元 </p> 
                 <p>房龄：<!--{$item.house_age}-->年</p>
                 
              </div>
   	      </div>
	    <div class="view_tags">
        <!--{if $item.is_vexation==1}--><img src="<!--{$cfg.path.images}-->sale_js.gif" title="急售" alt="急售" /><!--{else}-->&nbsp;<!--{if $item.is_new==1}--><img src="<!--{$cfg.path.images}-->newpush.gif" title="新推" alt="新推" /><!--{/if}--><!--{/if}-->
        </div>
    </div></ol>
    <div class="h23"></div>
    <div class="hr"></div>
    <div style="height: 12px; clear: both; overflow: hidden;"></div>
<!--{/foreach}-->
    <div class="view-div">
		   <div class="contain">
            <!-- 小区列表分页 -->
            <!--{$pagePanel}-->
            <!-- 小区列表分页 结束 -->
		   </div>
		   <div class="result">共有  <span style="color: rgb(97, 160, 13); font-weight: bold;"><!--{$row_count}--></span> 套符合要求的房子</div>
   	  </div>
</div>
</div>
  <p align="right">&nbsp;</p>
<span class="xj_c xj_c01"></span>
<span class="xj_c xj_c06"></span>
<span class="xj_c xj_c03"></span>
<span class="xj_c xj_c04"></span></div>
</div>
<!--{include file="inc/foot.tpl"}-->
</body>
</html>