<?php
/**
 * 根目录包含文件，一般是全站都可以使用的功能包含这个文件 ， 例如 上传 ， 缩略图 ， 登陆等
 *
 * @author 阿一 yandy@yanwee.com
 * @package 1.0
 * @version $Id$
 */

require('../common.inc.php');
$page->dir  = 's';//目录名
$realname = $_COOKIE['AUTH_MEMBER_REALNAME'] ? $_COOKIE['AUTH_MEMBER_REALNAME'] :$_COOKIE['AUTH_MEMBER_NAME'];
$page->tpl->assign('username',$realname);
if($_COOKIE['AUTH_MEMBER_STRING']){
	$member = new Member($query);
	$user_type = $member->getAuthInfo('user_type');
	$page->tpl->assign('user_type',$user_type);
}

$newMsgCount = 0;
if($_COOKIE['AUTH_MEMBER_NAME']){
	$innernote = new Innernote($query);
	$newMsgCount = $innernote->getCount(' is_new = 1 and msg_to = \''.$_COOKIE['AUTH_MEMBER_NAME'].'\'');
}
$page->tpl->assign('msgCount',$newMsgCount);

?>