<?php
/**
 * 登陆用户
 *
 * @author 阿一 yandy@yanwee.com
 * @package package
 * @version $Id$
 */

require('../common.inc.php');
$page->dir  = 'login';//目录名
$page->addCss('member.css');
$page->addCss('login.css');

$realname = $_COOKIE['AUTH_MEMBER_REALNAME'] ? $_COOKIE['AUTH_MEMBER_REALNAME'] :$_COOKIE['AUTH_MEMBER_NAME'];
$page->tpl->assign('username',$realname);

$newMsgCount = 0;
if($_COOKIE['AUTH_MEMBER_NAME']){
	$innernote = new Innernote($query);
	$newMsgCount = $innernote->getCount(' to_del=0 and is_new = 1 and msg_to = \''.$_COOKIE['AUTH_MEMBER_NAME'].'\'');
	
}
$page->tpl->assign('msgCount',$newMsgCount);

?>