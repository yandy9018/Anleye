<?php
 /**
  * 前台登陆以及注册页面
  *
  * @copyright Copyright (c) 2007 - 2011 Yanwee.com (www.anleye.com)
  * @author 阿一 yandy@yanwee.com
  * @package package
  * @version $Id$
  */
 
require('path.inc.php');

$page->name = 'login';
$page->title =  $page->title.' - 会员登陆';
$page->addJs('jquery-1.2.6.min.js');
$page->addJs('FormValid.js');
$page->addJs('FV_onBlur.js');
$page->tpl->assign('back_to', $_GET['back_to']);
$member = new Member($query);

if ($_POST['action']=='login') {
	
	if (!$errorMsg1) {
		$back_to = $_POST['back_to'];
		specConvert($_POST, array('username'));
		if ($_POST['username'] && $_POST['passwd']) {
			$result = $member->login($_POST['username'], $_POST['passwd']);
			if ($result===true) {
				if($page->uc==1){
				//同步登陆
				require($cfg['path']['root'].'uc_client/client.php');
				$username = $member->getAuthInfo('username');
				$uid = uc_get_user($username,0);
				$sysloginJs  =  uc_user_synlogin($uid[0]);
				echo $sysloginJs;
				}
				$user_type = $member->getAuthInfo('user_type');
				if($back_to){
				$back_to=str_replace('owner','member',$back_to);
				$page->urlto($back_to);
				}else{
				$page->urlto('../member/index.php');
				}
			}else{
				 $errorMsg1 = $result;
				}
			   
		} else {
			$errorMsg1 = '请输入用户名或密码！';
		}
	}
} elseif ($_GET['action']=='logout') {
	if($_COOKIE['AUTH_MEMBER_STRING']){
		$uid = $member->getAuthInfo('id');
	}
	$member->logout();
	if($uid){
		if($page->uc==1){
		require($cfg['path']['root'].'uc_client/client.php');
		echo uc_user_synlogout($uid);
		}
	}
	$page->urlto($cfg['url'] . 'login/login.php');
}elseif ($_POST['action']=='register') {
	if (md5(strtolower($_POST['vaild']))!=$_COOKIE['validString']) {
		$errorMsg = '验证码错误！';
	}
	if (!$errorMsg) {
		specConvert($_POST, array('username'));
		if (!$_POST['username'] || !$_POST['passwd']) {
			$errorMsg = '请输入用户名或密码！';
		}
		
		//判断是否开通经纪人免费注册
		if($page->memberOpen==2){
			$_POST['status'] = 1;
			}else{
				$_POST['status'] = 0;
				}
		
		$result = $member->register($_POST);
		if ($result > 0) {
			if($page->uc==1){
			require($cfg['path']['root'].'uc_client/client.php');
			$uid = uc_user_register($_POST['username'],$_POST['passwd'],$_POST['email']);
		
			//同步登陆
			$sysloginJs  =  uc_user_synlogin($uid);
			echo $sysloginJs;
			}
			$page->urlto('registerBrokerDone.php');
		} elseif($result == -1) {
			$errorMsg = "注册失败，用户名 [".$_POST['username']."]已经存在";
		} else{
			$errorMsg = "注册失败";
		}
	}
}
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);
$page->tpl->assign('errorMsg', $errorMsg);
$page->tpl->assign('errorMsg1', $errorMsg1);
$page->show();
?>