<?php
require('path.inc.php');

$page->name = 'photo'; //页面名字,和文件名相同

//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

//id
$id = intval($_GET['id']);
if(!$id){
	$page->urlto('index.php');
}

//小区
$borough = new Borough($query);

//小区详细信息
$boroughInfo = $borough->getInfo($id,'id,borough_name',0);
$page->tpl->assign('dataInfo', $boroughInfo);

//小区图片
$boroughImageList = $borough->getImgList($id,0);
$page->tpl->assign('boroughImageList', $boroughImageList);
$borough_img_num = count($boroughImageList);
if($borough_img_num%3){
	$borough_img_num = 3-$borough_img_num%3;
	$page->tpl->assign('borough_img_num', $borough_img_num);
}

//页面标题
$page->title = $boroughInfo['borough_name'].'的项目图片 - '.$page->title;

$page->show();
?>