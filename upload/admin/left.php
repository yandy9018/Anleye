<?php
require('path.inc.php');
$page->title .= '左边导航';
$page->name = 'left'; //页面名字,和文件名相同

$user = new User($query);
$user->auth();

$page->addCss('adminframe.css');
$page->addJs('jquery-1.2.6.min.js');

$menus = require($cfg['path']['conf'] . 'adminMenu.cfg.php');

if(isset($_GET['tab'])){
	$menus = $menus[$_GET['tab']]['sub'];
}else{
	//第一个menu
	$menus = array_shift($menus);
	$menus = $menus['sub'];
}
$rights =  $user->getRight($user->getAuthInfo('user_id'));
$rights['editpwd'] = 1; //给每个人修改自己密码的权限

//处理如果没有权限的导航,移除该导航 admin 不限制
if($user->getAuthInfo('user_id')!=SYSTEM_ADMIN_ID){
	foreach($menus as $subkey=>$menuitem){
		if(!key_exists($subkey,$rights) && $rights[$subkey] !=1 ){
			unset($menus[$subkey]);
		}
	}
}
$page->tpl->assign('menus', $menus);
$page->show();
?>