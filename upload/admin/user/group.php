<?php
/**
 * 群组操作
 *
 * @copyright Copyright (c) 2005 - 2009 Yanwee.net (www.anleye.com)
 * @author 王岩 yandy@yanwee.com
 * @package Core
 * @version $Id$
 */

require('path.inc.php');
$page->title .= ' 用户组管理';
$page->name = 'group'; //页面名字,和文件名相同
$page->addJs('FormValid.js');
$group = new Group($query);

if ($page->action=='save') {
	$user->allow('group');
	$group->save($_POST);
	$page->urlto('group.php','保存成功！');
} else if ($page->action=='delete') {
	$user->allow('group');
	if ($_POST['groups']) {
		$group->delete($_POST['groups']);
		$page->urlto('group.php','删除成功！');
	}
} else {
	$user->allow('group');
	$groupList = $group->getList();
	$page->tpl->assign('groupList', $groupList);
	if ($_GET['group_id']) {
		$page->tpl->assign('info', $group->getInfo($_GET['group_id']));
	}
}
$page->show();
?>