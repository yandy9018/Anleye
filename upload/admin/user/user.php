<?php
/**
 * 后台用户
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');
$page->title .= ' 用户管理';
$page->name = 'user'; //页面名字,和文件名相同

$user = new User($query);
$_GET['user_id'] = intval($_GET['user_id']);

if ($page->action=='edit') {
	$page->action = 'add';
}
if ($page->action=='add') {
	$user->allow('userAdd');
	$page->addJs('FormValid.js');
	if ($_GET['user_id']) {
		$userInfo = $user->getInfo($_GET['user_id']);
		$page->tpl->assign('userInfo', $userInfo);
		$page->tpl->assign('user_id', $_GET['user_id']);
	}
	$group = new Group($query);
	$select = $group->getSelect($userInfo['group_id']);
	$page->tpl->assign('select', $select);
} elseif ($page->action=='save') {
	if( $_POST['user_id'] =="" && $user->getAllUsers('*',"username ='".$_POST['username']."'")){
		$page->back('用户已存在');
	}
	if($user->save($_POST)){
		$page->urlto('user.php','保存成功');	
	}else{
		$page->back('增加用户失败');
	}
	
	exit;
} else if ($page->action=='delete') {
	$user->allow('user');
	$users = $_GET['user_id'] ? $_GET['user_id'] : $_POST['users'];
	if ($users) {
		try {
			$item = array('isdel' => 1);
			
			$flag = false;
			if (is_array($users)) {
				if (in_array(SYSTEM_ADMIN_ID, $users)) {
					$flag = true;
				}
				$users = implode(',',$users);
				$where = 'user_id in (' . $users . ')';
			} else {
				if (SYSTEM_ADMIN_ID==$users) {
					$flag = true;
				}
				$where = 'user_id=' . intval($users);
			}
			if ($flag) {
				throw new Exception('系统管理员不能够被删除！');
			}	
			$user->delete($users);
		} catch (Exception $e) {
			echo $e->getMessage();
			
		}
		
	}
	exit;
} else if ($page->action=='resume') {  //恢复删除的用户
	$user->allow('user');
	$users = $_POST['user_id'] ? $_POST['user_id'] : $_POST['users'];
	if ($users) {
		try {
			$item = array('isdel' => 0);
			
			$flag = false;
			if (is_array($users)) {
				$users = implode(',',$users);
				$where = 'isdel = 1 and user_id in (' . $users . ')';
			} else {
				$where = 'isdel=1 and user_id=' . intval($users);
			}
			$rtn = $user->db->update('fke_users',$item,$where); 
			//$user->delete($users);
		} catch (Exception $e) {
			echo $e->getMessage();
			//$page->urlto('user.php',$e->getMessage());
		}
		//$page->urlto('user.php','删除成功！');
	}
	exit;
} else if ($page->action=='right') {
	$user->allow('user');
	$group = intval($_POST['groupId']);
	if ($_POST['rights']) {
		if ($group) {
			$user->saveGroupRight($_POST['rights'], $group);
			$page->urlto('group.php', '保存成功！');
		} else {
			$user->saveRight($_POST['rights'], $_POST['user_id']);
			$page->urlto('user.php?action=right&user_id=' .  $_POST['user_id'], '保存成功！');
		}
	}
	
	$userId = $_GET['user_id'] ? intval($_GET['user_id']) : intval($_POST['user_id']);
	if ($userId) { $til = "用户权限管理";  $arrRight=$user->getRight($userId); }
	
	$groupId = $_GET['group_id'] ? intval($_GET['group_id']) : intval($_POST['group_id']);
	if ($groupId){ $til = "用户组权限管理"; $arrRight=$user->getGroupRight($groupId); }
	$menus = require($cfg['path']['conf'] . 'adminMenu.cfg.php');
	
	$page->tpl->assign('menus', $menus);
	$page->tpl->assign('user_id', $userId );
	$page->tpl->assign('group_id',$groupId);
	$page->tpl->assign('userRight',$arrRight);
	
} else {
	$user->allow('user');
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	//分页类
	$pages = new Pages($user->getCount());
	$pageLimit = $pages->getLimit();
	$userList = $user->getList($pageLimit);
	$page->tpl->assign('userList', $userList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>