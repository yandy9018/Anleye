<?php
/*
 * Copyright (c) 2006-2008 walkwatch.com
 * All rights reserved.
 * Support : lsw(singlecanoe@126.com)
 *
 * Version :  1.0
 */
 
require('path.inc.php');
$user = new User($query);
$page->title .= ' 修改密码';
$page->name = 'editpwd'; //页面名字,和文件名相同

if($_POST['newpwd']){
	//进入修改密码
	$_POST['newpwd'] = strtolower(trim($_POST['newpwd']));
	$_POST['checkpwd'] = strtolower(trim($_POST['checkpwd']));
	$_POST['oldpwd'] = strtolower(trim($_POST['oldpwd']));
	
	if($_POST['newpwd'] != $_POST['checkpwd']){
		$alertJs = 'alert("新密码与确认密码不符");';
	}elseif(empty($_POST['oldpwd'])){
		$alertJs = 'alert("新密码与确认密码不符");';
	}else{
		$pass = false;//标记是否旧密码检验通过
		$oldpwd = md5($_POST['oldpwd']);		
		$dbpwd = $user->getAuthInfo('passwd');
		if($oldpwd == $dbpwd){
			$pass = true;
		}

		if($pass){
			//检验旧密码通过，进行更新密码操作
			$rtn = $user->modifyPwd($_POST['newpwd']);
			if($rtn){
				setcookie('AUTH_STRING',authcode($user->getAuthInfo('user_id') . "\t" . md5($_POST['newpwd']), 'ENCODE', $cfg['auth_key']),0,'/admin/');
				$alertJs = 'alert("修改成功.");';
			}elseif($rtn === false){
				$alertJs = 'alert("对不起，修改失败");';
			}else{
				$alertJs = 'alert("参数不足");';
			}
		}else{
			$alertJs = 'alert("旧密码错误，修改失败");';
		}
	}
}
$page->tpl->assign('alertJs',$alertJs);
$page->show();
?>