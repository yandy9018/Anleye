<?php
require('path.inc.php');
$config = new Config($query);
if ($page->action=='save') {
	$_POST['webConfig']['borough_avg_time'] = $_POST['webConfig']['borough_avg_time']*24*60*60;
	$_POST['webConfig']['member_num_time'] = $_POST['webConfig']['member_num_time']*24*60*60;
	$_POST['webConfig']['broker_integral_time'] = $_POST['webConfig']['broker_integral_time']*24*60*60;
	$_POST['webConfig']['broker_active_Rate_time'] = $_POST['webConfig']['broker_active_Rate_time']*24*60*60;
	$_POST['webConfig']['statistics_time'] = $_POST['webConfig']['statistics_time']*24*60*60;
	$_POST['webConfig']['borough_pic_num_time'] = $_POST['webConfig']['borough_pic_num_time']*24*60*60;
	$_POST['webConfig']['house_invalid_time'] = $_POST['webConfig']['house_invalid_time']*24*60*60;
	
	$config->save($_POST);
	$page->urlto('crontab.php','保存成功');	
	exit;
}else{
	$page->name = 'crontab'; //页面名字,和文件名相同
	$webConfig = $config->getInfo(1,'*');
	$webConfig['borough_avg_time'] = $webConfig['borough_avg_time']/60/60/24;
	$webConfig['member_num_time'] = $webConfig['member_num_time']/60/60/24;
	$webConfig['broker_integral_time'] = $webConfig['broker_integral_time']/60/60/24;
	$webConfig['broker_active_Rate_time'] = $webConfig['broker_active_Rate_time']/60/60/24;
	$webConfig['statistics_time'] = $webConfig['statistics_time']/60/60/24;
	$webConfig['borough_pic_num_time'] = $webConfig['borough_pic_num_time']/60/60/24;
	$webConfig['house_invalid_time'] = $webConfig['house_invalid_time']/60/60/24;
	$page->tpl->assign('webConfig', $webConfig);
	}

$page->show();
?>