<?php

require('path.inc.php');

$user->allow('class');
$linkClass = new linkClass($query);

if ($page->action=='edit') {
	$page->action = 'add';
}
if ($page->action=='add') {
	$page->addJs('FormValid.js');
	$class_id = intval($_GET['id']);
	if($class_id){
		$classInfo = $linkClass->getInfo($class_id,'*');
	}
	$page->tpl->assign('info',$classInfo);
	
}elseif ($page->action=='save') {
	if($linkClass->save($_POST)){
		$page->urlto('class.php','保存成功');	
	}else{
		$page->back('保存失败');
	}
	exit;
}elseif ($page->action=='order') {
	array_walk($_POST['list_order'],'intval');
	if($linkClass->order($_POST['list_order'])){
		$page->urlto('class.php','保存成功');
	}else{
		$page->back('保存失败');
	}
	exit;
}elseif ($page->action=='delete') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	if($linkClass->delete($ids)){
		$page->urlto('class.php','删除成功');
	}else{
		$page->back('删除失败');
	}
	exit;
}

$page->name = 'classList'; //页面名字,和文件名相同

$dataList = $linkClass->getAll($where,'*',' order by list_order asc ');
$page->tpl->assign('dataList', $dataList);

$page->show();
?>