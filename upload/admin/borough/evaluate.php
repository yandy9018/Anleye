<?php
require('path.inc.php');

$user->allow('boroughEvaluate');
$borough = new Borough($query);

if ($page->action=='save') {
	header('content-Type: text/html; charset=utf-8');
	$temp = explode('|',$_POST['id']);
	if(!$temp[1]){
		exit('wrong parm');
	}
	$_POST['borough_id'] = intval($temp[1]);
	$_POST['creater'] = $user->getAuthInfo('id');
	$_POST['borough_evaluate'] = intval($_POST['value']);
	$_POST['add_time'] = mktime(0,0,0,date('m'),date('d'),date('Y'));
	$boroughUpdate= new BoroughUpdate($query);
	try {
		$borough->saveEvaluteLog($_POST);
		$datafield = array(
			'borough_evaluate'=>$_POST['borough_evaluate'],
		);
		$borough->update($_POST['borough_id'],$datafield);
		echo $_POST['borough_evaluate'];
	}catch (Exception $e){
		echo $e->getMessage();
	}
	exit;
}else{
	$page->name = 'boroughEvaluate'; //页面名字,和文件名相同

	$page->addJs('jquery-1.2.6.min.js');
	$page->addJs('jquery.jeditable.js');
	
	$areaLists = Dd::getArray('cityarea');
	$page->tpl->assign('areaLists', $areaLists);
	
	$keyword = $_REQUEST['q']=='请输入小区名称,小区地址'?"":trim($_GET['q']); 
	$cityarea_id = intval($_GET['cityarea']);
	$where = " and isdel=0 ";
	if($cityarea_id){
		$where .= " and cityarea_id =".$cityarea_id;
	}
	if($keyword){
		$where .= " and (borough_name like '%".$keyword."%' or borough_letter like '%".$keyword."%' or borough_alias like '%".$keyword."%' or borough_address like '%".$keyword."%')";
	}
	
	if($_GET['time']){
		$time = intval($_GET['time']);
		switch ($time){
			case 1:
				//未评估
				$where .= " and ( borough_evaluate is null or borough_evaluate = 0 )";
				break;
			case 2:
				//一个月
				$lastMonth = mktime(date('h'),date('i'),date('s'),date('m')-1,date('d'),date('y'));
				$where .= " and evaluate_time < $lastMonth";
				break;
			case 3:
				//三个月
				$lastThreeMonth = mktime(date('h'),date('i'),date('s'),date('m')-3,date('d'),date('y'));
				$where .= " and evaluate_time < $lastThreeMonth";
				break;
			default:
				break;
		}
	}
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($borough->getCount(1,$where));
	$pageLimit = $pages->getLimit();
	$boroughList = $borough->getList($pageLimit,1,$where,' order by borough_name asc ');
	foreach ($boroughList as $key => $value){
		$boroughList[$key]['cityarea_id'] = $areaLists[$value['cityarea_id']];
		$last_evaluate_log = $borough->getLastEvaluateLog($value['id']);
		if($last_evaluate_log){
			$borough->update($value['id'],array('evaluate_time'=>$last_evaluate_log['add_time']));
		}
		$value['evaluate_time'] = $value['evaluate_time'] ? $value['evaluate_time'] : $last_evaluate_log['add_time'];
		if($value['evaluate_time'] ){
			$last_time = $cfg['time']-$value['evaluate_time'];
			if($last_time > 2592000){
				$boroughList[$key]['last_update'] = intval($last_time/2592000)."个月";
			}elseif($last_time > 604800){
				$boroughList[$key]['last_update'] = intval($last_time/604800)."个星期";
			}elseif($last_time > 86400){
				$boroughList[$key]['last_update'] = intval($last_time/86400)."天";
			}else{
				$boroughList[$key]['last_update'] = "今天";
			}
		}else{
			$boroughList[$key]['last_update'] = "未评估";
		}
 	}
	
	$page->tpl->assign('boroughList', $boroughList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>