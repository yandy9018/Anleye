<?php
/**
 * 小区详细信息
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');

$user->allow('boroughList');
$borough = new Borough($query);

if ($page->action=='edit') {
	$page->action = 'add';
}
if ($page->action=='add') {
	$page->name = 'boroughEdit'; //页面名字,和文件名相同
	$page->addJs('FormValid.js');
	//字典
	$cityarea_option = Dd::getArray('cityarea');
	$page->tpl->assign('cityarea_option', $cityarea_option);
		
	$borough_section_option = Dd::getArray('borough_section');
	$page->tpl->assign('borough_section_option', $borough_section_option);
	$borough_type_option = Dd::getArray('borough_type');
	$page->tpl->assign('borough_type_option', $borough_type_option);
	$borough_support_option = Dd::getArray('borough_support');
	$page->tpl->assign('borough_support_option', $borough_support_option);
	$borough_sight_option = Dd::getArray('borough_sight');
	$page->tpl->assign('borough_sight_option', $borough_sight_option);
	$picture_num = 0;
	$drawing_num = 0;
	
	if($_GET['id']){
		$id = intval($_GET['id']);
		$boroughInfo = $borough->getInfo($id,'*',1);
		$boroughInfo['boroughInfo']['borough_green'] = round($boroughInfo['boroughInfo']['borough_green'],2);
		$boroughInfo['boroughInfo']['borough_sight'] = explode(',',$boroughInfo['boroughInfo']['borough_sight']);
		$boroughInfo['boroughInfo']['borough_support'] = explode(',',$boroughInfo['boroughInfo']['borough_support']);
		$cityarea_option2 = Dd::getArray('cityarea2');
		$boroughInfo['borough']['cityarea2_name'] = $cityarea_option2[$boroughInfo['borough']['cityarea2_id']];
		$boroughInfo['borough']['botough_picture'] = $borough->getImgList($id,0);
		$picture_num = count($boroughInfo['borough']['botough_picture']);
		$boroughInfo['borough']['botough_drawing'] = $borough->getImgList($id,1);
		$drawing_num = count($boroughInfo['borough']['botough_drawing']);
		
		$page->tpl->assign('boroughInfo', $boroughInfo['boroughInfo']);
		$page->tpl->assign('borough', $boroughInfo['borough']);
	}

	$page->tpl->assign('to_url', $_SERVER['HTTP_REFERER']);
	$page->tpl->assign('picture_num', $picture_num);
	$page->tpl->assign('drawing_num', $drawing_num);
} elseif ($page->action=='save') {
	$to_url = $_POST['to_url'];
	$_POST['creater'] = $user->getAuthInfo('username');
	if($borough->save($_POST)){
		$page->urlto($to_url,'小区信息保存成功');	
	}else{
		$page->back('保存小区信息失败');
	}
	exit;
}elseif ($page->action=='search'){
	$page->name = 'boroughList'; //页面名字,和文件名相同
	if(isset($_GET['nofull'])){
		$where = " and isdel=0 and( borough_thumb is null or borough_address  is null )";
	}else{
		$keyword = $_REQUEST['q']=='请输入小区名称,小区地址'?"":trim($_REQUEST['q']); 
		$cityarea_id = intval($_REQUEST['cityarea']);
		$where = " and isdel=0 ";
		if($cityarea_id){
			$where .= " and cityarea_id =".$cityarea_id;
		}
		if($keyword){
			$where .= " and (borough_name like '%".$keyword."%' or borough_letter like '%".$keyword."%' or borough_alias like '%".$keyword."%' or borough_address like '%".$keyword."%')";
		}
	}
	$areaLists = Dd::getArray('cityarea');
	$page->tpl->assign('areaLists', $areaLists);
	$borough_section = Dd::getArray('borough_section');
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($borough->getCount(1,$where));
	$pageLimit = $pages->getLimit();
	$boroughList = $borough->getList($pageLimit,1,$where,' order by borough_name asc' );
	foreach ($boroughList as $key => $value){
		$boroughList[$key]['cityarea_id'] = $areaLists[$value['cityarea_id']];
		$boroughList[$key]['borough_section'] = $borough_section[$value['borough_section']];
	}
	$page->tpl->assign('cityarea', $cityarea_id);
	$page->tpl->assign('q', $keyword);
	$page->tpl->assign('boroughList', $boroughList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
	
}elseif ($page->action=='delete') {
	$back_to = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	if($borough->delete($ids)){
		$page->urlto($back_to,'删除小区成功');
	}else{
		$page->back('删除失败,可能所选择的小区中还有房源');
	}

	exit;
}elseif ($page->action=='combine') {
	//合并两个栏目

	//print_r($_POST);
	$action =$_POST['fromaction'];
	$q =$_POST['q'];
	$cityarea =$_POST['cityarea'];
	$pageno =$_POST['pageno'];
	
	if(empty($_POST['fromId'])){
		$page->back("没有选择需要合并的小区");
	}
	if(empty($_POST['toId'])){
		$page->back("没有选择合并到哪个小区");
	}
	//剔除目标ID
	$fromId = array_diff($_POST['fromId'], $_POST['toId']);
	
	$targetid = intval($_POST['toId'][0]);
	
	foreach ($fromId as $afromid){
		$afromid = intval($afromid);
		$add_field = array('old_id'=>$afromid,'new_id'=>$targetid);
		$borough->db->insert('fke_borough_log',$add_field);
		//删除
		$update_field = array('isdel'=>1);
		$borough->db->update('fke_borough',$update_field,'id='.$afromid);
		//房源移动到新的小区
		$housesell = new HouseSell($query);
		$housesell->db->execute("update ".$housesell->tName." set borough_id = $targetid where borough_id = $afromid" );
		$houserent = new HouseRent($query);
		$houserent->db->execute("update ".$houserent->tName." set borough_id = $targetid where borough_id = $afromid" );
		$member = new Member($query);
		$member->db->execute("update ".$member->tNameBrokerInfo." set borough_id = $targetid where borough_id = $afromid ");
		$member->db->execute("update ".$member->tNameOwnerInfo." set borough_id = $targetid where borough_id = $afromid ");
	}
	//echo 'index.php?action='.$action.'&q='.$q.'&cityarea='.$cityarea.'&pageno='.$pageno;
	//exit;
	$page->urlto('index.php?action='.$action.'&q='.$q.'&cityarea='.$cityarea.'&pageno='.$pageno,'合并小区成功');

	exit;
}else{
	$page->name = 'boroughList'; //页面名字,和文件名相同

	$areaLists = Dd::getArray('cityarea');
	$page->tpl->assign('areaLists', $areaLists);
	$borough_section = Dd::getArray('borough_section');
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($borough->getCount(1,' and isdel=0'));
	$pageLimit = $pages->getLimit();
	$boroughList = $borough->getList($pageLimit,1,' and isdel=0',' order by borough_name asc ');
	foreach ($boroughList as $key => $value){
		$boroughList[$key]['cityarea_id'] = $areaLists[$value['cityarea_id']];
		$boroughList[$key]['borough_section'] = $borough_section[$value['borough_section']];
	}
	
	$page->tpl->assign('boroughList', $boroughList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>