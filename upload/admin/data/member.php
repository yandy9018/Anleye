<?php
require('path.inc.php');

$sql ="select * from FK_User_Brokers ";
$query_mssql->open($sql);

while ($rs = $query_mssql->next()) {
	$rs = c_addslashes($rs,1);
	
	$rs['Brokers_Type'] = $rs['Brokers_Type'] ? 1:2;
	$rs['Brokers_Lock'] = $rs['Brokers_Lock'] ? 1:0;
	
	$created = $rs['Add_Time']?MyDate::transform('timestamp',$rs['Add_Time']):"";
	$last_login = $rs['Brokers_Time']?MyDate::transform('timestamp',$rs['Brokers_Time']):"";
	
	$fieldArray = array(
		'id'=>$rs['Brokers_ID'],
		'username'=>$rs['Brokers_LoginName'],
		'passwd'=>$rs['Brokers_LoginPwd'],
		'email'=>$rs['Brokers_Email'],
		'user_type'=>$rs['Brokers_Type'],
		'logins'=>$rs['Brokers_LoginCount'],
		'scores'=>$rs['Brokers_IntegralCount'],
		'last_login'=>$last_login,
		'add_time'=>$created,
		'status'=>$rs['Brokers_Lock'],
	);

	$query->insert('fke_member',$fieldArray);
	
	if($rs['Brokers_Type'] == 1){
		
		$Brokers_Images = '';
		$Brokers_IdImg = '';
		$Brokers_Eligible = '';
		$cityErea = array(111=>1,112=>3,113=>2,114=>5,115=>4,116=>4,117=>9,118=>7,119=>6,120=>8,121=>10,122=>11,123=>12,124=>13);
		
		if(trim($rs['Brokers_Images']) && $rs['Brokers_Images']!="exPic4.jpg"){
			$fileExtI = FileSystem::fileExt($rs['Brokers_Images'],true);
			$avatar = time().rand(0,100000).$fileExtI;
			$Brokers_Images = 'broker/avatar/'.$avatar;
		}
		if(trim($rs['Brokers_IdImg'])){
			$fileExtD = FileSystem::fileExt($rs['Brokers_IdImg'],true);
			$idimg = time().rand(100000,200000).$fileExtD;
			$Brokers_IdImg = 'broker/identity/'.$idimg;
		}
		if(trim($rs['Brokers_Eligible'])){
			$fileExtE = FileSystem::fileExt($rs['Brokers_Eligible'],true);
			$aptitude = time().rand(200000,300000).$fileExtE;
			$Brokers_Eligible = 'broker/aptitude/'.$aptitude;
		}
		$fieldArray = array(
			'id'=>$rs['Brokers_ID'],
			'realname'=>$rs['Brokers_RealName'],
			'cityarea_id'=>$cityErea[$rs['Borkers_CityEara']],
			'mobile'=>$rs['Brokers_TelPhone'],
			'avatar'=>$Brokers_Images,
			'idcard_pic'=>$Brokers_IdImg,
			'idcard'=>$rs['Brokers_Idcard'],
			'aptitude'=>$Brokers_Eligible,
			'signed'=>$rs['Brokers_SignIn'],
			'company'=>$rs['Brokers_Company'],
			'outlet'=>$rs['Brokers_NickCompany'].'-'.$rs['Brokers_Shop'],
			'outlet_addr'=>$rs['Brokers_Addr'],
			'post_code'=>$rs['Brokers_Zip'],
			'com_tell'=>$rs['Brokers_Phone'],
			'com_fax'=>$rs['Brokers_Fax'],
			'gender'=>$rs['Brokers_Sex'],
			'qq'=>$rs['Brokers_Qq'],
			'msn'=>$rs['Brokers_Msn'],
			'broker_type'=>$rs['Brokers_NickCompany']?1:2,
		);
	
		$query->insert('fke_broker_info',$fieldArray);
		
		//拷贝缩略图
		if($Brokers_Images){
			$rs['Brokers_Images'] = str_replace('%20',' ',$rs['Brokers_Images']);
			$sourcePath = "d:/系统/Brokers/".$rs['Brokers_Images'];
			$destinPath = "d:/AppServ/www/系统/upfile/broker/avatar/".$avatar;
			copy($sourcePath,$destinPath);
		}
		//拷贝缩略图
		if(trim($rs['Brokers_IdImg'])){
			$rs['Brokers_IdImg'] = str_replace('%20',' ',$rs['Brokers_IdImg']);
			$sourcePath = "d:/系统/Brokers/".$rs['Brokers_IdImg'];
			$destinPath = "d:/AppServ/www/系统/upfile/broker/identity/".$idimg;
			copy($sourcePath,$destinPath);
		}
		//拷贝缩略图
		if(trim($rs['Brokers_Eligible'])){
			$rs['Brokers_Eligible'] = str_replace('%20',' ',$rs['Brokers_Eligible']);
			$sourcePath = "d:/系统/Brokers/".$rs['Brokers_Eligible'];
			$destinPath = "d:/AppServ/www/系统/upfile/broker/aptitude/".$aptitude;
			copy($sourcePath,$destinPath);
		}
	}else{
		$Brokers_Images ='';
		$fileExt = FileSystem::fileExt($rs['Brokers_Images'],true);
		$avatar = time().rand(0,100000).$fileExt;
		$Brokers_Images = (trim($rs['Brokers_Images'])&& $rs['Brokers_Images']!="exPic4.jpg")?'owner/avatar/'.$avatar:'';
		$fieldArray = array(
			'id'=>$rs['Brokers_ID'],
			'name_publish'=>$rs['Name_Public'],
			'mobile'=>$rs['Brokers_TelPhone'],
			'mobile_publish'=>$rs['Tel_Public'],
			'tell'=>$rs['Brokers_Phone'],
			'tell_publish'=>$rs['Phone_Public'],
			'email_publish'=>0,
			'qq'=>$rs['Brokers_Qq'],
			'qq_publish'=>$rs['QQ_Public'],
			'msn'=>$rs['Brokers_Msn'],
			'msn_publish'=>$rs['Msn_Public'],
			'avatar'=>$Brokers_Images,
			'signed'=>$rs['Brokers_SignIn'],
			'gender'=>$rs['Brokers_Sex'],
			'gender_publish'=>$rs['Sex_Public'],
			'question'=>$rs['Brokers_Problems'],
			'answer'=>$rs['Brokers_Answer'],
		);
		$query->insert('fke_owner_info',$fieldArray);
		//拷贝缩略图
		if(trim($rs['Brokers_Images'])&& $rs['Brokers_Images']!="exPic4.jpg"){
			$rs['Brokers_Images'] = str_replace('%20',' ',$rs['Brokers_Images']);
			$sourcePath = "d:/系统/Brokers/".$rs['Brokers_Images'];
			$destinPath = "d:/AppServ/www/系统/upfile/owner/avatar/".$avatar;
			copy($sourcePath,$destinPath);
		}
	}
	
}
print_r("用户数据导入完成");
exit;
?>