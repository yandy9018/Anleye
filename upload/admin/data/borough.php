<?php
require('path.inc.php');

$sql ="select * from FK_H_Borough  where Borough_IsCheck = 1";
$query_mssql->open($sql);

while ($rs = $query_mssql->next()) {
	$rs = c_addslashes($rs,1);
	$school = explode(' ',$rs['Borough_School']);
	
	$borough_thumb = $query_mssql2->getValue('select * from FK_B_Images where borough_id ='.$rs['borough_id']);
	$Images_Url = $borough_thumb['Images_Url']?'borough/thumb/'.$borough_thumb['Images_Url']:'';
	
	$creater = $query_mssql2->getValue('select Brokers_LoginName from FK_User_Brokers where Brokers_LoginNum =\''.$rs['LoginNum'].'\'');

	$Borough_StartBuild = $rs['Borough_StartBuild']?@MyDate::transform('date',$rs['Borough_StartBuild']):"";
	$created = $rs['Add_Time']?MyDate::transform('timestamp',$rs['Add_Time']):"";
	
	$Borough_CityErea = array(111=>1,112=>3,113=>2,114=>5,115=>4,116=>4,117=>9,118=>7,119=>6,120=>8,121=>10,122=>11,123=>12,124=>13);
	$rs['Borough_CityErea'] = $Borough_CityErea[$rs['Borough_CityErea']];
	$fieldArray = array(
		'id'=>$rs['Borough_ID'],
		'borough_name'=>$rs['Borough_Name'],
		'borough_letter'=>$rs['Borough_Letter'],
		'cityarea_id'=>$rs['Borough_CityErea'],
		'borough_address'=>$rs['Borough_Address'],
		'borough_type'=>$rs['Borough_Type'],
		'elementary_school'=>$school[0],
		'middle_school'=>$school[1],
		'borough_thumb'=>$Images_Url,
		'sell_time'=>$Borough_StartBuild,
		'borough_avgprice'=>$rs['Borough_AvgPrice'],
		'is_checked'=>1,
		'creater'=>$creater,
		'created'=>$created,
		'updated'=>$created,
		);
	$query->insert('fke_borough',$fieldArray);
	//拷贝缩略图
	if($borough_thumb['Images_Url']){
		$sourcePath = "d:/系统/Borough/".$borough_thumb['Images_Url'];
		$destinPath = "d:/AppServ/www/系统/upfile/borough/thumb/".$borough_thumb['Images_Url'];
		copy($sourcePath,$destinPath);
	}
	$temp = explode(',',$rs['Borough_Support']);
	if(is_array($temp)){
		
		foreach ($temp as $key => $a_t){
			$temp[$key] = substr($a_t,3);
		}
		$rs['Borough_Support'] = implode(',',$temp);
	}
	
	$Borough_Completion = $rs['Borough_Completion']?@MyDate::transform('date',$rs['Borough_Completion']):"";
	
	$fieldArray = array(
		'id'=>$rs['Borough_ID'],
		'borough_developer'=>$rs['Borough_Developer'],
		'borough_company'=>$rs['Borough_Company'],
		'borough_costs'=>$rs['Borough_Costs'],
		'borough_totalarea'=>$rs['Borough_Totalarea'],
		'borough_area'=>$rs['Borough_Area'],
		'borough_green'=>$rs['Borough_Green'],
		'borough_volume'=>$rs['Borough_Volume'],
		'borough_parking'=>$rs['Borough_Parking'],
		'borough_number'=>$rs['Borough_Number'],
		'borough_completion'=>$Borough_Completion,
		'borough_support'=>$rs['Borough_Support'],
		'borough_bus'=>$rs['Borough_Bus'],
		'borough_shop'=>$rs['Borough_Shop'],
		'borough_hospital'=>$rs['Borough_Hospital'],
		'borough_bank'=>$rs['Borough_Bank'],
		'borough_dining'=>$rs['Borough_Dining'],
		'borough_content'=>str_replace("<br>","",$rs['Borough_Content']),
		'borough_map'=>$rs['Borough_Map'],
	);

	$query->insert('fke_borough_info',$fieldArray);
	
}
print_r("小区信息导入完成");
exit;
?>