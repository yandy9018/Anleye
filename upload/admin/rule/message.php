<?php
/**
 * 自动站内信 
 */

require('path.inc.php');

$user->allow('messageRule');
$messageRule = new MessageRule($query);

if ($page->action=='edit') {
	$page->action = 'add';
}
if ($page->action=='add') {
	$page->name = 'messageEdit'; //页面名字,和文件名相同
	$page->addJs('FormValid.js');
	
	$rule_class = Dd::getArray('message_ruleclass');
	$page->tpl->assign('rule_class', $rule_class);
	
	$id = intval($_GET['id']);
	if($id){
		$dataInfo = $messageRule->getInfo($id);
		$page->tpl->assign('dataInfo', $dataInfo);
	}

}elseif ($page->action=='save') {
	try{
		$messageRule->save($_POST);
		$page->urlto('message.php','保存成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	exit;
}elseif ($page->action=='status') {
	$ids = $_POST['ids'];
	$status=intval($_GET['status']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	try{
		//删除自己的条目
		$messageRule->changeStatus($ids,$status);
		
		$page->urlto('message.php','操作成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}elseif ($page->action=='delete') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	try{
		//删除自己的条目
		$messageRule->delete($ids);
		
		$page->urlto('message.php','删除成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else{
	$page->name = 'messageList'; //页面名字,和文件名相同
	
	//$where = ' rule_status <>2';
	$where = '';
	
	$rule_class = Dd::getArray('message_ruleclass');
	$page->tpl->assign('rule_class', $rule_class);
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($messageRule->getCount($where),50);
	$pageLimit = $pages->getLimit();
	$dataList = $messageRule->getList($pageLimit,'*',$where,' order by rule_class asc,id asc');
	foreach ($dataList as $key=>$item){
		$dataList[$key]['rule_class'] = $rule_class[$item['rule_class']];
	}
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>