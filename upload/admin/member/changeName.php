<?php
require('path.inc.php');

$page->title =  $page->title.' - 委托信息';

$member = new Member($query);

if($page->action == 'save'){
	//保存
	$id = intval($_POST['id']);
	$_POST = charsetIconv($_POST);
	if($_POST['user_type'] == 1){
		$fieldData = array('realname'=>trim($_POST['realname']));
		$member->updateInfo($id,$fieldData,true,1);
	}else{
		$fieldData = array('first_name'=>trim($_POST['first_name']),'last_name'=>trim($_POST['last_name']));
		$member->updateInfo($id,$fieldData,true,2);
	}
	echo 1;
}else{
	$page->name = 'changeName';
	if(!$_GET['id']){
		exit;
	}
	//编辑成交
	$id = intval($_GET['id']);
	$memberInfo = $member->getInfo($id,'*',true);
	
	$page->tpl->assign('dataInfo', $memberInfo);
}

$page->show();
?>