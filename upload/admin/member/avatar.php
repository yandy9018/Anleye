<?php
/**
 * 头像审核后台管理
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');

$user->allow('memberAvatar');
$avatar = new Avatar($query);

if ($page->action=='search'){
	$page->name = 'avatarList'; //页面名字,和文件名相同

	$keyword = $_REQUEST['q']=='请输入用户名称'?"":trim($_REQUEST['q']); 
	$where = " status= ".intval($_GET['status']);
	if($keyword){
		$where .= " and (username like '%".$keyword."%') ";
	}

	$user_type = Dd::getArray('user_type');
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($avatar->getCount($where));
	$pageLimit = $pages->getLimit();
	$avatarList = $avatar->getList($pageLimit,'*',$where,' order by add_time desc ');
	foreach ($avatarList as $key => $value){
		$avatarList[$key]['user_type'] = $user_type[$value['user_type']];
	}
	
	$page->tpl->assign('q', $keyword);
	$page->tpl->assign('dataList', $avatarList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
	
}elseif ($page->action=='delete') {

	$ids = $_POST['ids'];
	$back_url = $_SERVER['HTTP_REFERER'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	try{
		$avatar->delete($ids);
		$page->urlto($back_url,'删除成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}elseif ($page->action=='status') {
	$back_url = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	$dostatus = intval($_GET['dostatus']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择需要操作的条目');
	}
	$member = new Member($query);
	$messageRule = new MessageRule($query);
	$innernote = new Innernote($query);
	$integral = new Integral($query);
	try{
		if($dostatus==1){
			//通过，更该用户信息
			foreach($ids as $a_id){
				//修改单条信息
				$avatar->changeStatus($a_id,$dostatus);
				
				$dataInfo = $avatar->getInfo($a_id);
				$updateField = array (
					'avatar'=>$dataInfo['avatar_pic']
				);
				$member->updateInfo($dataInfo['broker_id'],$updateField,true,1);
				//发送站内信
		
				if($dataInfo['broker_id']){
					$message = $messageRule->getInfo(14,'rule_remark');
					$real_name = $member->getRealName($dataInfo['broker_id'],1);
					$username = $member->getInfo($dataInfo['broker_id'],'username');
					$message = sprintf($message,$real_name,$cfg['url_shop'],$dataInfo['broker_id']);
					$innernote->send('系统',$username,'系统消息',$message);
				}
			}
		}else{
			//其他更改标志即可，无需更改用户信息
			$avatar->changeStatus($ids,$dostatus);
			//发送站内信
			foreach($ids as $a_id){
				$dataInfo = $avatar->getInfo($a_id);
				if($dataInfo['broker_id']){
					$message = $messageRule->getInfo(13,'rule_remark');
					$real_name = $member->getRealName($dataInfo['broker_id'],1);
					$username = $member->getInfo($dataInfo['broker_id'],'username');
					$message = sprintf($message,$real_name);
					$innernote->send('系统',$username,'系统消息',$message);
				}
			}
		}
		$page->urlto($back_url,'操作成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else{
	$page->name = 'avatarList'; //页面名字,和文件名相同

	if(isset($_GET['status'])){
		$where = ' status ='.intval($_GET['status']);
	}
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($avatar->getCount($where));
	$pageLimit = $pages->getLimit();
	$avatarList = $avatar->getList($pageLimit,'*',$where,' order by add_time desc ');
	$member = new Member($query);
	foreach ($avatarList as $key => $value){
		$avatarList[$key]['user'] = $member->getInfo($value['broker_id'],'*',true);
	}

	$page->tpl->assign('dataList', $avatarList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>