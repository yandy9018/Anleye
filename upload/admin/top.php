<?php
require('path.inc.php');
$user = new User($query);
$user->auth();
$page->title .= ' 头部页面';
$page->name = 'top'; //页面名字,和文件名相同	
$page->addCss('adminframe.css');
$page->addJs('jquery-1.2.6.min.js');
$menus = require($cfg['path']['conf'] . 'adminMenu.cfg.php');

$rights =  $user->getRight($user->getAuthInfo('user_id'));
$rights['editpwd'] = 1; //给每个人修改自己密码的权限
//处理如果没有权限的导航,移除该导航
if($user->getAuthInfo('user_id')!=SYSTEM_ADMIN_ID){
	foreach($menus as $key=>$submenu){
		//$submenulist[$key] = $submenu;
		$hasitem = false;
		foreach($submenu['sub'] as $subkey=>$menuitem){
			if($rights[$subkey]){
				$hasitem = true;
				break;
			}
		}
		if(!$hasitem){
			unset($menus[$key]);
		}
	}
}
$page->tpl->assign('menus', $menus);
$page->tpl->assign('username',$user->getAuthInfo('username'));
$page->show();
?>