<?php
/**
 * 出售房源委托信息管理
 *
 * @package user
 * @author 阿一 ayi@yanwee.com
 * @version 1.0
 * 2012-08-26
 * 
 */

require('path.inc.php');

$user->allow('consignSale');
$consignSale = new ConsignSale($query);
$member = new Member($query);
$return_to = $_SERVER['HTTP_REFERER'];

if ($page->action=='search'){
	$page->name = 'consignList'; //页面名字,和文件名相同

	//成交使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	
	$keyword = $_REQUEST['q']=='请输入电话号码'?"":trim($_REQUEST['q']); 

	$where = "";

	if($keyword){
		$where .= " and (owner_phone like '%".$keyword."%')";
	}

	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($consignSale->getCount('0',$where));
	$pageLimit = $pages->getLimit();
	$consignList = $consignSale->getList($pageLimit,'*','0',$where ,' order by time desc ');
	;
	$page->tpl->assign('q', $keyword);
	$page->tpl->assign('dataList', $consignList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
	
}elseif ($page->action=='delete') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	try{
		$consignSale->delete($ids);
		$page->urlto($return_to,'删除委托房源成功');
	}catch (Exception $e){
		$page->back('删除失败，',$e->getMessage());
	}
	exit;
	}
	elseif ($page->action=='dostatus') {
	$ids = $_POST['ids'];
	$status = intval($_GET['status']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择更改房源');
	}
	
	try{
		$consignSale->check($ids,$status);
		$page->urlto($return_to,'状态修改成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	
	exit;
}else{
	$page->name = 'consignList'; //页面名字,和文件名相同

	//成交使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	
	$check = intval($_GET['check']);
	$where = " and 1 = 1";
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($consignSale->getCount($check,$where));
	$pageLimit = $pages->getLimit();
	$consignList = $consignSale->getList($pageLimit,'*',$check,$where,' order by time desc ');
	
	$page->tpl->assign('dataList', $consignList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>