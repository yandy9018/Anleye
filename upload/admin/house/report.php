<?php
/**
 * 身份证审核后台管理
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');

$user->allow('houseReport');
$report = new Report($query);

if ($page->action=='status') {
	$back_url = $_SERVER['HTTP_REFERER'];
	$ids = $_POST['ids'];
	$dostatus = intval($_GET['dostatus']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择需要操作的条目');
	}
	try{
		if($dostatus==1){
			//通过，更该用户信息
			foreach($ids as $a_id){
				//修改单条信息
				
				$dataInfo = $report->getInfo($a_id);
				if($dataInfo['status'] == 1){
					//已审核通过的不再记录分数
					continue;
				}
				$report->changeStatus($a_id,$dostatus);
				
				//被举报的人扣除积分
				$integral = new Integral($query);
				$integral->add($dataInfo['report_target'],27);
			}
		}else{
			//其他更改标志即可，无需更改用户信息
			$report->delete($ids);
		}
		$page->urlto($back_url,'操作成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else{
	$page->name = 'reportList'; //页面名字,和文件名相同

	$where = " 1 ";
	if($_GET['type']){
		$where .= " and house_type = '".$_GET['type']."'";
	}
	
	if(isset($_GET['status'])){
		$where .=" and status =  ".intval($_GET['status']);
	}
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($report->getCount($where),10,'pages.tpl');
	$pageLimit = $pages->getLimit();
	$dataList = $report->getList($pageLimit,'*',$where,' order by addtime desc ');
	
	$houseSell = new HouseSell($query);
	$houseRent = new HouseRent($query);
	$member = new Member($query);
	
	foreach ($dataList as $key => $item){
		if($item['house_type'] == 'sell'){
			$dataList[$key]['house'] = $houseSell->getInfo($item['house_id'],'*');
		}else{
			$dataList[$key]['house'] = $houseRent->getInfo($item['house_id'],'*');
		}
		$dataList[$key]['report_target'] = $member->getInfo($item['report_target'],'*',true);
	}
	$page->tpl->assign('dataList', $dataList);	
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel(5));//分页条
}
$page->show();
?>