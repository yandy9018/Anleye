<?php
require('path.inc.php');
//$member_id = $member->getAuthInfo('id');
if($page->action == 'getBoroughList'){
	$borough = new Borough($query);
	$_GET["q"] = charsetIconv($_GET["q"]);
	$q = strtolower($_GET["q"]);
	if (!$q) return;
	$datalist = $borough->getAll('*'," (borough_name like '%".$q."%' or borough_letter like '%".$q."%' or borough_alias like '%".$q."%') and isdel <>1");
	$str = "";
	foreach ($datalist as $key=>$value) {
		if($value['borough_alias']){
			//$str .= $value['borough_name'].'('.$value['borough_alias'].')'."|".$value['id']."|".$value['borough_address']."\n";
			$str .= $value['borough_name']."|".$value['id']."|".$value['borough_address']."\n";
		}else{
			$str .= $value['borough_name']."|".$value['id']."|".$value['borough_address']."\n";
		}
	}

	//$str .= "我要创建新小区|addBorough|addBorough\n";
	echo $str;
}elseif($page->action == 'checkPwd'){
	$pwd = $_GET['password'];
	if($member->checkPwd($pwd,$member_id)){
		echo 1;
	}else{
		echo 0;
	}
	exit;
}elseif($page->action == 'saveBorough'){
	$_POST = charsetIconv($_POST);
	$_POST['creater'] = $member->getAuthInfo('username');
	$borough = new Borough($query);
	if(!$exest_id = $borough->checkNameUnique($_POST['borough_name'])){
		if($borough_id = $borough->addBorough($_POST)){
			echo $borough_id."|1";
		}else{
			echo 0;
		}
	}else{
		echo $exest_id."|-1";
	}
	
}elseif($page->action == 'saveBargain'){
	$_POST['id'] = intval($_POST['id']);
	$_POST['house_id'] = intval($_POST['house_id']);
	$_POST['broker_id'] = $member_id;
	$saleBargain = new SaleBargain($query);
	if($bargain_id = $saleBargain->save($_POST)){
		//修改房源的状态
		$house = new HouseSell($query);
		$house->changeStatus($_POST['house_id'],4);
		echo $bargain_id;
	}else{
		echo 0;
	}
}elseif($page->action == 'saveBargainRemark'){
	$_POST['id'] = intval($_POST['id']);
	$saleBargain = new SaleBargain($query);
	if($bargain_id = $saleBargain->saveRemark($_POST)){
		echo $bargain_id;
	}else{
		echo 0;
	}
}elseif($page->action == 'saveRentBargain'){
	$_POST['id'] = intval($_POST['id']);
	$_POST['house_id'] = intval($_POST['house_id']);
	$_POST['broker_id'] = $member_id;
	$rentBargain = new RentBargain($query);
	if($bargain_id = $rentBargain->save($_POST)){
		//修改房源的状态
		$house = new HouseRent($query);
		$house->changeStatus($_POST['house_id'],4);
		echo $bargain_id;
	}else{
		echo 0;
	}
}elseif($page->action == 'saveRentBargainRemark'){
	$_POST['id'] = intval($_POST['id']);
	$rentBargain = new RentBargain($query);
	if($bargain_id = $rentBargain->saveRemark($_POST)){
		echo $bargain_id;
	}else{
		echo 0;
	}
}
?>