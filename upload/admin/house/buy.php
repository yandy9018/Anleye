<?php
/**
 * 求购后台管理
 *
 * @package house
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 */

require('path.inc.php');

$user->allow('houseBuy');
$houseWanted = new HouseWanted($query);
$return_to = $_SERVER['HTTP_REFERER'];

if ($page->action=='search'){
	$page->name = 'buyList'; //页面名字,和文件名相同

	$keyword = $_REQUEST['q']=='请输入联系人或联系电话'?"":trim($_REQUEST['q']); 
	
	$where =" wanted_type = 1 ";
	if(isset($_GET['status'])){
		$where .= ' and status ='.intval($_GET['status']);
	}
	if($keyword){
		$where .= " and (linkman like '%".$keyword."%' or link_tell like '%".$keyword."%') ";
	}
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($houseWanted->getCount($where));
	$pageLimit = $pages->getLimit();
	$houseWantedList = $houseWanted->getList($pageLimit,'*',$where,' order by add_time desc ');
	$member = new Member($query);
	foreach ($houseWantedList as $key => $value){
		$houseWantedList[$key]['expert'] = $member->getInfo($value['expert_id'],'*',true);
	}
	
	$page->tpl->assign('q', $keyword);
	$page->tpl->assign('dataList', $houseWantedList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
	
}elseif ($page->action=='delete') {

	$ids = $_POST['ids'];
	
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	try{
		$houseWanted->delete($ids);
		$page->urlto($return_to,'删除成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}elseif ($page->action=='status') {
	$ids = $_POST['ids'];
	$dostatus = intval($_GET['dostatus']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择需要操作的条目');
	}
	try{
		if($dostatus==1){
			//通过，更该用户信息
			foreach($ids as $a_id){
				//修改单条信息
				$houseWanted->changeStatus($a_id,$dostatus);
				//发送站内信
				/**
				$dataInfo = $houseWanted->getInfo($a_id);
				if($dataInfo['expert_id']){
					$message = new Message($query);
					$message->send('FangKE系统',$dataInfo['expert_id'],$title,$message);
				}
				*/	
			}
		}else{
			//其他更改标志即可，无需更改用户信息
			foreach ($ids as $id){
				if($houseWanted->getReplyList($id)){
					$houseWanted->delete($id);
					$houseWanted->deleterep($id);
					}else{
						$houseWanted->delete($id);
						}
			}
			
		}
		$page->urlto($return_to,'操作成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else{
	$page->name = 'buyList'; //页面名字,和文件名相同
	$where =" wanted_type = 1 ";
	
	if(isset($_GET['status'])){
		$where .= ' and status ='.intval($_GET['status']);
	}
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($houseWanted->getCount($where));
	$pageLimit = $pages->getLimit();
	$houseWantedList = $houseWanted->getList($pageLimit,'*',$where,' order by add_time desc ');
	$member = new Member($query);
	foreach ($houseWantedList as $key => $value){
		if($value['expert_id']){
			$tmp = explode(',',$value['expert_id']);
			array_remove_empty($tmp);
			foreach($tmp as $item){
				$houseWantedList[$key]['expert'][] = $member->getInfo($item,'*',true);
			}
		}
	}

	$page->tpl->assign('dataList', $houseWantedList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>