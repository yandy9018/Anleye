<?php
/**
 * 租房源信息管理
 *
 * @package user
 * @author 王岩 yandy@yanwee.com
 * @version 1.0
 * 
 * 功能需要添加 直接拖拉图片的功能
 * 
 */

require('path.inc.php');

$user->allow('houseRent');
$houserent = new HouseRent($query);
$member = new Member($query);
$return_to = $_SERVER['HTTP_REFERER'];

if ($page->action=='search'){
	$page->name = 'rentList'; //页面名字,和文件名相同

	//成交使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	
	$keyword = $_REQUEST['q']=='请输入房源编号'?"":trim($_REQUEST['q']); 
	$cityarea_id = intval($_REQUEST['cityarea']);
	$check = intval($_GET['check']);
	$where = "";
	if($cityarea_id){
		$where .= " and cityarea_id =".$cityarea_id;
	}
	if($keyword){
		$where .= " and (house_no like '%".$keyword."%')";
	}

	$areaLists = Dd::getArray('cityarea');
	$page->tpl->assign('areaLists', $areaLists);
	$houseTypeList = Dd::getArray('house_type');
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($houserent->getCount($check,$where));
	$pageLimit = $pages->getLimit();
	$houserentList = $houserent->getList($pageLimit,'*',$check,$where ,' order by created desc ');
	foreach ($houserentList as $key => $value){
		$houserentList[$key]['cityarea_id'] = $areaLists[$value['cityarea_id']];
		$houserentList[$key]['house_pic'] = $houserent->getImgList($value['id']);
		$houserentList[$key]['real_name'] = $member->getRealName($value['broker_id'],1);
        $houserentList[$key]['house_type'] = $houseTypeList[$value['house_type']];
		if($value['broker_id']==0){
		$houserentList[$key]['consigner_name'] = $member->getRealName($value['consigner_id'],0);
			}
	}
	$page->tpl->assign('cityarea', $cityarea_id);
	$page->tpl->assign('q', $keyword);
	$page->tpl->assign('dataList', $houserentList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
	
}elseif ($page->action=='delete') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	
	try{
		$messageRule = new MessageRule($query);
		$innernote = new Innernote($query);
		foreach ($ids as $id){
			$dataInfo = $houserent->getInfo($id);
			//没通过，发送自动站内信
			if($dataInfo['broker_id']){
				$message = $messageRule->getInfo(17,'rule_remark');
				$real_name = $member->getRealName($dataInfo['broker_id'],1);
				$username = $member->getInfo($dataInfo['broker_id'],'username');
				$message = sprintf($message,$real_name,$cfg['url_rent'],$dataInfo['id'],$dataInfo['house_no']);
				$innernote->send('系统',$username,'系统消息',$message);
			}
		}
		foreach ($ids as $id){
		$rentinfo = $houserent->getInfo($id);
		$houserent->delete($id);
		}
		$page->urlto($return_to,'删除房源成功');
	}catch (Exception $e){
		$page->back('删除失败，',$e->getMessage());
	}

	exit;
}elseif($page->action == 'refresh'){
	//刷新房源
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择刷新条目');
		} 
		
	try{
		$messageRule = new MessageRule($query);
		$innernote = new Innernote($query);
		foreach ($ids as $id){
			$dataInfo = $houserent->getInfo($id);
			//没通过，发送自动站内信
			if($dataInfo['broker_id']){
				$message = $messageRule->getInfo(17,'rule_remark');
				$real_name = $member->getRealName($dataInfo['broker_id'],1);
				$username = $member->getInfo($dataInfo['broker_id'],'username');
				$message = sprintf($message,$real_name,$cfg['url_rent'],$dataInfo['id'],$dataInfo['house_no']);
				$innernote->send('系统',$username,'系统消息',$message);
			}
		}
		foreach ($ids as $id){
		$rentinfo = $houserent->getInfo($id);
		$houserent->refresh($id);
		}
		$page->urlto($return_to,'刷新房源成功');
	}catch (Exception $e){
		$page->back('刷新失败，',$e->getMessage());
	}
	exit;}elseif($page->action=='isindex') {
   $ids = $_POST['ids'];
   $status = intval($_GET['status']);
   if(!is_array($ids) || empty($ids)){
		$page->back('没有选择推荐条目');
	}
	$isIndex = $houserent->getCount(5,'');
	if($isIndex>6 && $status ==1){
		$page->urlto($return_to,'推荐条数超过6条，请删除之后在推荐');
		}
	try{
		$houserent->update($ids,'is_index',$status);
		$page->urlto($return_to,'操作成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	
	exit;
}elseif ($page->action=='docheck') {
	$ids = $_POST['ids'];
	$status = intval($_GET['status']);
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择审核条目');
	}
	
	try{
		$houserent->check($ids,$status);
		$integral = new Integral($query);
		$member = new Member($query);
		$messageRule = new MessageRule($query);
		$innernote = new Innernote($query);
		
		foreach ($ids as $id){
			$dataInfo = $houserent->getInfo($id);
			if($status == 1){
				//通过，增加积分
				if($dataInfo['broker_id']){
					//发布一条增加5分
					$integral->add($dataInfo['broker_id'],10);
					$houseImg = $houserent->getImgNum($id);
					if($houseImg>=5){
						$integral->add($dataInfo['broker_id'],11);
					}
					if($dataInfo['drawing_id'] || $dataInfo['house_drawing'] ){
						$integral->add($dataInfo['broker_id'],12);
					}
				}
			}else{
				//没通过，发送自动站内信
				if($dataInfo['broker_id']){
					$message = $messageRule->getInfo(2,'rule_remark');
					$real_name = $member->getRealName($dataInfo['broker_id'],1);
					$username = $member->getInfo($dataInfo['broker_id'],'username');
					$message = sprintf($message,$real_name,$cfg['url_rent'],$dataInfo['id'],$dataInfo['house_no']);
					$innernote->send('系统',$username,'系统消息',$message);
				}
			}
		}
		$page->urlto($return_to,'审核房源成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	
	exit;
}elseif($page->action == 'dateDel'){
    if($houserent->dateDel($_POST['dateDel'])){
        echo '<script>alert("删除成功");location.href="rent.php?check=0"</script>';
    }else{
        echo '<script>alert("删除失败");location.href="rent.php?check=0"</script>';
    }
}else{
	$page->name = 'rentList'; //页面名字,和文件名相同

	//成交使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	$check = intval($_GET['check']);
	$where = " and (status =0 or status =1  or status = 2 or status =3 or status = 4 or status =7)";
	$areaLists = Dd::getArray('cityarea');
	$page->tpl->assign('areaLists', $areaLists);
	$houseTypeList = Dd::getArray('house_type');
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($houserent->getCount($check,$where));
	$pageLimit = $pages->getLimit();
	$houserentList = $houserent->getList($pageLimit,'*',$check,$where,' order by created desc ');
	
	
    $today = date("Y-m-d", $cfg['time']);
    $yestoday = date("Y-m-d", strtotime("-1 day"));
	
	foreach ($houserentList as $key => $value){
		$houserentList[$key]['yestoday_click'] = intval($houserent->getClick($value['id'], $yestoday));
        $houserentList[$key]['today_click'] = intval($houserent->getClick($value['id'], $today));
		$houserentList[$key]['cityarea_id'] = $areaLists[$value['cityarea_id']];
		$houserentList[$key]['house_type'] = $houseTypeList[$value['house_type']];
		$houserentList[$key]['house_pic'] = $houserent->getImgList($value['id']);
		$houserentList[$key]['real_name'] = $member->getRealName($value['broker_id'],1);
	}

	
	$page->tpl->assign('dataList', $houserentList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>