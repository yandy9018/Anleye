<?php
/**
 * 多城市
 *
 * @package user
 * @author 阿一 ayi@yanwee.com
 * @version 4.0
 * 2012-09-02
 */

require('path.inc.php');

$user->allow('city');
$city = new City($query);

if ($page->action=='edit') {
	$page->action = 'add';
}
if ($page->action=='add') {
	$page->name = 'cityItemEdit'; //页面名字,和文件名相同
	$page->addJs('FormValid.js');
	$id = intval($_GET['id']);
	if($id){
		$dataInfo=$city->getInfo($id);
		$page->tpl->assign('dataInfo', $dataInfo);
	}

}elseif ($page->action=='save') {
	try{
		$city->save($_POST);
		$page->urlto('index.php','保存成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	exit;
}elseif ($page->action=='lock') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择条目');
	}
	try{
		//锁定自己的条目
		$city->changeStatus($ids,1);
		$page->urlto('index.php','锁定成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	exit;
}elseif ($page->action=='unlock') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择条目');
	}
	try{
		//解锁自己的条目
		$city->changeStatus($ids,0);
		$page->urlto('index.php','解锁成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}
	exit;
}elseif ($page->action=='delete') {
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择删除条目');
	}
	try{
		//删除自己的条目
		$city->delete($ids);
		$page->urlto('index.php','删除成功');
	}catch (Exception $e){
		$page->back($e->getMessage());
	}

	exit;
}else{
	$page->name = 'cityList'; //页面名字,和文件名相同
	$where = '';
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($city->getCount($where));
	$pageLimit = $pages->getLimit();
	$cityList = $city->getList($pageLimit,'*',$where,' order by pid asc ');

	$page->tpl->assign('dataList', $cityList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel());//分页条
}
$page->show();
?>