<?php
 /**
  * 委托出售房源
  *
  * @copyright Copyright (c) 2007 - 2012 Yanwee.com (www.anleye.com)
  * @author 阿一 ayi@yanwee.com
  * @package package
  * 2012-08-26
  **/
 
require('path.inc.php');

$consignSale = new ConsignSale($query);
if($page->action == 'save'){
	try{
		
		$house_id = $consignSale->save($_POST);
	   	$page->urlto('consignSale.php','委托成功');
	
	}catch ( Exception $e){
		$page->back('保存信息失败,联系管理员');
		//$page->back($e->getMessage());
	}
	exit;
	
	}else{
	   	$page->name = 'consignSale';
     	$page->addJs('FormValid.js');
	    $page->addJs('FV_onBlur.js');
		}

	
$page->show();
?>