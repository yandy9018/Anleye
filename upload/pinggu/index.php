<?php
require('path.inc.php');
//页面标题
$page->title = '房产价格评估 - '.$page->titlec;
if($page->action == "save"){
	$pinggu = new PingGu($query);
	if(!$_POST['borough_id']){
		$page->urlto("index.php","请输入并选择小区");
	}
	if($_COOKIE['AUTH_MEMBER_STRING']){
		$member = new Member($queriy);
		$_POST['creater'] = $member->getAuthInfo('id');
	}else{
		$_POST['creater'] = '';
	}
	
	try{
		$pinggu_id = $pinggu->save($_POST);
		$page->urlto("more.php?id=".$pinggu_id);
	}catch ( Exception $e){
		$page->back('保存信息失败');
		//$page->back($e->getMessage());
	}
	exit;
}

$page->name = 'index'; //页面名字,和文件名相同	
$page->addJs('jquery-1.2.6.min.js');
$page->addJs('FormValid.js');
$page->addJs('FV_onBlur.js');
//增加小区的thickBox
$page->addcss("thickbox.css");
$page->addjs("thickbox.js");

$page->addjs($cfg['path']['js']."Autocompleter/lib/jquery.bgiframe.min.js");
$page->addjs($cfg['path']['js']."Autocompleter/lib/ajaxQueue.js");
$page->addcss($cfg['path']['js']."Autocompleter/jquery.autocomplete.css");
$page->addjs($cfg['path']['js']."Autocompleter/jquery.autocomplete.js");

//字典
$house_type_option = PingGuDd::getArray('house_type');
$page->tpl->assign('house_type_option', $house_type_option);

//特色
$house_toword_option = PingGuDd::getArray('house_toward');
$page->tpl->assign('house_toword_option', $house_toword_option);

if($_GET['id']){
	//修改
	$id = intval($_GET['id']);
	$pinggu = new PingGu($query);
	$dataInfo = $pinggu->getInfo($id);
	$page->tpl->assign('dataInfo', $dataInfo);	
}

//右边的优质房源列表
$house = new HouseSell($query);
$bestList = $house->getList(array('rowFrom'=>0,'rowTo'=>4),'*',1,' and status =1','order by order_weight desc');
$page->tpl->assign('bestList', $bestList);

$page->show();
?>