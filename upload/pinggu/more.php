<?php
require('path.inc.php');
$page->name = 'more'; //页面名字,和文件名相同	
//页面标题
$page->title = '详细的房产价格评估 - '.$page->titlec;

$house = new HouseSell($query);
$pinggu = new PingGu($query);
$borough = new Borough($query);

if($page->action == "save"){
	$pinggu_id = intval($_GET['id']);
	if($_POST['house_quality']){
		$_POST['house_quality'] = ",".implode(',',$_POST['house_quality']).",";
	}
	try{
		$pinggu->saveMore($_POST);
		$page->urlto("report.php?id=".$pinggu_id);
	}catch ( Exception $e){
		//$page->back('保存信息失败');
		$page->back($e->getMessage());
	}
	exit;
}

$id = intval($_GET['id']);
if(!$id){
	$page->back('index.php');
}

$dataInfo = $pinggu->getInfo($id);
if(!$dataInfo){
	$page->back('index.php');
}

$dataInfo['house_type'] = PingGuDd::getCaption('house_type',$dataInfo['house_type']);
$dataInfo['house_toward'] = PingGuDd::getCaption('house_toward',$dataInfo['house_toward']);

$dataInfo['borough_info'] = $borough->getInfo($dataInfo['borough_id'],'*',true,true);
$dataInfo['borough_info']['cityarea_name'] = Dd::getCaption('cityarea',$dataInfo['borough_info']['cityarea_id']);

$page->tpl->assign('dataInfo', $dataInfo);

//字典
$house_fitment_option = PingGuDd::getArray('house_fitment');
$page->tpl->assign('house_fitment_option', $house_fitment_option);
$pinggu_dd = new PingGuDd($query);
$fitment_quotiety_option = $pinggu_dd->getItemListByName('house_fitment');
$fitment_quotiety_option = array_to_hashmap($fitment_quotiety_option,'di_value','di_quotiety');
$page->tpl->assign('fitment_quotiety_option', $fitment_quotiety_option);

$house_place_option = PingGuDd::getArray('house_place');
$page->tpl->assign('house_place_option', $house_place_option);

$house_view_option = PingGuDd::getArray('house_view');
$page->tpl->assign('house_view_option', $house_view_option);

$house_light_option = PingGuDd::getArray('house_light');
$page->tpl->assign('house_light_option', $house_light_option);

$house_noise_option = PingGuDd::getArray('house_noise');
$page->tpl->assign('house_noise_option', $house_noise_option);

$house_quality_option = PingGuDd::getArray('house_quality');
$page->tpl->assign('house_quality_option', $house_quality_option);

//右边的优质房源列表
$house = new HouseSell($query);
$bestList = $house->getList(array('rowFrom'=>0,'rowTo'=>5),'*',1,' and status =1','order by order_weight desc');
$page->tpl->assign('bestList', $bestList);

$page->show();
?>