<?php
require('path.inc.php');


//页面标题
$page->title = '房产价格评估报告 - '.$page->titlec;

$page->name = 'report'; //页面名字,和文件名相同	

$house = new HouseSell($query);
$pinggu = new PingGu($query);
$borough = new Borough($query);

$id = intval($_GET['id']);
if(!$id){
	$page->back('index.php');
}
$dataInfo = $pinggu->getInfo($id);
if(!$dataInfo){
	$page->back('index.php');
}


$dataInfo['house_fitment'] = PingGuDd::getCaption('house_fitment',$dataInfo['house_fitment']);
$dataInfo['house_type'] = PingGuDd::getCaption('house_type',$dataInfo['house_type']);
$dataInfo['house_toward'] = PingGuDd::getCaption('house_toward',$dataInfo['house_toward']);

$dataInfo['house_light'] = PingGuDd::getCaption('house_light',$dataInfo['house_light']);
$dataInfo['house_place'] = PingGuDd::getCaption('house_place',$dataInfo['house_place']);
$dataInfo['house_view'] = PingGuDd::getCaption('house_view',$dataInfo['house_view']);
$dataInfo['house_noise'] = PingGuDd::getCaption('house_noise',$dataInfo['house_noise']);
if($dataInfo['house_quality']){
	$dataInfo['house_quality'] = PingGuDd::getCaption('house_quality',$dataInfo['house_quality']);
}else{
	$dataInfo['house_quality'] = "完好";
}
$dataInfo['borough_info'] = $borough->getInfo($dataInfo['borough_id'],'*',true,true);
if($dataInfo['borough_info']['borough_avgprice']){
	if($dataInfo['borough_info']['borough_avgprice'] > $dataInfo['house_avgprice'] ){
		$price_compare = (1 - round($dataInfo['house_avgprice']/$dataInfo['borough_info']['borough_avgprice'],4))*100;
		$dataInfo['price_compare'] = "低" .$price_compare."%";
	}else{
		$price_compare = (round($dataInfo['house_avgprice']/$dataInfo['borough_info']['borough_avgprice'],4)-1)*100;
		$dataInfo['price_compare'] = "高" .$price_compare."%";
	}
}

$dataInfo['borough_info']['cityarea_name'] = Dd::getCaption('cityarea',$dataInfo['borough_info']['cityarea_id']);


$page->tpl->assign('dataInfo', $dataInfo);

//该小区价格相近房源
$where = " and status =1 and (borough_id = ".$dataInfo['borough_id']." or borough_name = '".$dataInfo['borough_name']."')" ;
$sameBorougHouse = $house->getList(array('rowFrom'=>0,'rowTo'=>3),'*',3,$where,'order by order_weight desc');
$page->tpl->assign('sameBorougHouse', $sameBorougHouse);

//价格相近房源
$where = " and status =1 and house_price*10000/house_totalarea > ".($dataInfo['house_avgprice']-200)." and house_price*10000/house_totalarea <".($dataInfo['house_avgprice']+200);
$samePriceHouse = $house->getList(array('rowFrom'=>0,'rowTo'=>4),'*',3,$where,'order by order_weight desc');
$page->tpl->assign('samePriceHouse', $samePriceHouse);


$page->show();
?>