<?php
require('path.inc.php');


$page->name = 'detail'; //页面名字,和文件名相同

//成交使用的thickBox加载
$page->addcss("thickbox.css");
$page->addjs("thickbox.js");

$member = new Member($query);
if($_COOKIE['AUTH_MEMBER_NAME']){
	$member_id = $member->getAuthInfo('id');
	$user_type = $member->getAuthInfo('user_type');
	$page->tpl->assign('user_type', $user_type);
}
//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

$house_price_option = array(
	'0-40'=>'40万以下',
	'40-60'=>'40-60万',
	'60-80'=>'60-80万',
	'80-100'=>'80-100万',
	'100-120'=>'100-120万',
	'120-150'=>'100-120万',
	'150-200'=>'150-200万',
	'200-250'=>'200-250万',
	'250-300'=>'250-300万',
	'300-500'=>'300-500万',
	'500-0'=>'500万以上'
);
$page->tpl->assign('house_price_option', $house_price_option);

//房屋产权
$belongLists = Dd::getArray('belong');
$house = new HouseSell($query);
//房源特色
$house_feature_option = Dd::getArray('house_feature');
$page->tpl->assign('house_feature_option', $house_feature_option);
//id
$id = intval($_GET['id']);

if(!$id){
	$page->urlto('index.php');
}

//将浏览过的房源写入cookies
$house->cookies($id);

//详细信息
$dataInfo = $house->getInfo($id,'*');

if(!$dataInfo){
	$page->urlto('index.php','该房源不存在或已删除');
}
$dataInfo['updated'] = time2Units(time()-$dataInfo['updated']);
$dataInfo['belong'] = $belongLists[$dataInfo['belong']];
$dataInfo['cityarea_name'] = $cityarea_option[$dataInfo['cityarea_id']];
$dataInfo['house_toward'] = Dd::getCaption('house_toward',$dataInfo['house_toward']);
if($dataInfo['house_feature']){
	$dataInfo['house_feature'] =  Dd::getCaption('house_feature',$dataInfo['house_feature']);
}
$dataInfo['house_fitment'] =  Dd::getCaption('house_fitment',$dataInfo['house_fitment']);
if($dataInfo['house_price'] && $dataInfo['house_totalarea']){
	$dataInfo['avg_price'] = round($dataInfo['house_price']*10000/$dataInfo['house_totalarea']);
}else{
	$dataInfo['avg_price'] = "未知";
}
$page->tpl->assign('dataInfo', $dataInfo);
//图片列表
$houseImageList = $house->getImgList($id,'*');
$page->tpl->assign('houseImageList', $houseImageList);
$dataInfo['updated'] = time2Units(time()-$dataInfo['updated']);
//经纪人详细情况

	$brokerInfo = $member->getInfo($dataInfo['broker_id'],'*',true);
	if($brokerInfo['company_id']){
		$company = new Company($query);
		$brokerInfo['company_name'] = $company->getInfo($brokerInfo['company_id'],'company_name');
		$brokerInfo['company_phone'] = $company->getInfo($brokerInfo['company_id'],'company_phone');
		}
	//积分配置文件
	$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');
	$brokerInfo['brokerRank'] = getNumByScore($brokerInfo['scores'],$integral_array,'pic');
	$page->tpl->assign('brokerInfo', $brokerInfo);

//获取搬家公司信息
$company = new Company($query);
$moveCompanyList = $company->getList(array('rowFrom'=>0,'rowTo'=>9),'*',' and status=1 and type=1','');
$page->tpl->assign('moveCompanyList', $moveCompanyList);

//获取装修公司信息
$decorationCompanyList = $company->getList(array('rowFrom'=>0,'rowTo'=>9),'*',' and status=1 and type=2','');
$page->tpl->assign('decorationCompanyList', $decorationCompanyList);


//小区
$borough = new Borough($query);

if($dataInfo['borough_id']){
	//小区详细信息
	$boroughInfo = $borough->getInfo($dataInfo['borough_id'],'*',1,true);
	$boroughInfo['cityarea_name'] = $cityarea_option[$boroughInfo['cityarea_id']];
	$boroughInfo['borough_support'] = Dd::getCaption('borough_support',$boroughInfo['borough_support']);
	$page->tpl->assign('boroughInfo', $boroughInfo);
	$boroughImageList = $borough->getImgList($dataInfo['borough_id'],0,6);
	$page->tpl->assign('boroughImageList', $boroughImageList);
	
	if(!$dataInfo['house_thumb'] && $boroughImageList){
		//没有缩略图，把小区图片抽出一张
		$rand_key = array_rand($boroughImageList);
		
		$dataInfo['house_thumb'] = $boroughImageList[$rand_key]['pic_thumb']?$boroughImageList[$rand_key]['pic_thumb']:$boroughImageList[$rand_key]['pic_url'];

		$house->update($id,'house_thumb',$dataInfo['house_thumb']);
	}
	
	//该小区价格相近房源
	$where = " and status =1 and id <> ".$id ." and (borough_id = ".$dataInfo['borough_id']." or borough_name = '".$dataInfo['borough_name']."') and house_price > ".($dataInfo['house_price']-10)." and house_price <".($dataInfo['house_price']+10);
	$borougSamePrice = $house->getList(array('rowFrom'=>0,'rowTo'=>7),'*',3,$where,'order by order_weight desc');
	$page->tpl->assign('borougSamePrice', $borougSamePrice);
}

//同区域的价格相近房源
$where = " and status =1 and id <> ".$id ." and cityarea_id = ".$dataInfo['cityarea_id']." and house_price > ".($dataInfo['house_price']-10)." and house_price <".($dataInfo['house_price']+10);
$cityareaSamePrice = $house->getList(array('rowFrom'=>0,'rowTo'=>4),'*',3,$where,'order by order_weight desc');
$page->tpl->assign('cityareaSamePrice', $cityareaSamePrice);

//该经纪人的其他房源
$where = " and status =1 and id <> ".$id ." and broker_id = ".$dataInfo['broker_id'];
$brokerOthersList = $house->getList(array('rowFrom'=>0,'rowTo'=>4),'*',3,$where,'order by order_weight desc');
$page->tpl->assign('brokerOthersList', $brokerOthersList);

//页面标题
$page->title = $dataInfo['borough_name'].'二手房，'.$dataInfo['house_room'].'室'.$dataInfo['house_hall'].'厅'.$dataInfo['house_toilet'].'卫'.$dataInfo['house_veranda'].'阳，'.$dataInfo['house_title'].' - '.$page->city.$page->titlec;

//关键词
$page->keyword = $dataInfo['borough_name'].'二手房,'.$dataInfo['borough_name'].'房屋出售,'.$dataInfo['borough_name'];

//描述
$page->description='';

//点击增加统计
$house->addClick($id);

$back_to = $cfg['url_sale']."detail.php?id=".$id."&open=1";
$page->tpl->assign('back_to',$back_to);

$page->show();
?>