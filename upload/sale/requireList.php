<?php
require('path.inc.php');


$page->name = 'requireList'; //页面名字,和文件名相同	

//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);
$house_type_option = Dd::getArray('house_type');
$page->tpl->assign('house_type_option', $house_type_option);

$house_price_option = array(
	'0-40'=>'40万以下',
	'40-60'=>'40-60万',
	'60-80'=>'60-80万',
	'80-100'=>'80-100万',
	'100-120'=>'100-120万',
	'120-150'=>'100-120万',
	'150-200'=>'150-200万',
	'200-250'=>'200-250万',
	'250-300'=>'250-300万',
	'300-500'=>'300-500万',
	'500-0'=>'500万以上'
);
$page->tpl->assign('house_price_option', $house_price_option);

$where =' wanted_type=1 and status=1 and is_solve =0 ';

//q
$q = $_GET['q']=="输入求购联系人或联系电话" ? "":$_GET['q'];
if($q){
	// 小区名、路名或划片学校
	$where .=" and (linkman like '%".$q."%' or link_tell like '%".$q."%')" ;
}
//list_order 排序转换
$list_order = " order by add_time desc";

$list_num = 10;

$houseWanted = new HouseWanted($query);

require($cfg['path']['lib'] . 'classes/Pages.class.php');
$row_count = $houseWanted->getCount($where);
$pages = new Pages($row_count,$list_num);

$pageLimit = $pages->getLimit();
$dataList = $houseWanted->getList($pageLimit,'*',$where,$list_order);

foreach ($dataList as $key=> $item){
	$dataList[$key]['requirement_short'] = substrs($item['requirement'],58);
	$item['expert_id'] = trim($item['expert_id'],',');
	$dataList[$key]['expert_num'] = count(explode(',',$item['expert_id']));
}

$page->tpl->assign('dataList', $dataList);
$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条

$page->show();
?>