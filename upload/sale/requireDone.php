<?php
require('path.inc.php');


$page->name = 'requireDone'; //页面名字,和文件名相同	

$id = intval($_GET['id']);
if(!$id){
	$page->urlto('requireList.php');
}
//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

$house_price_option = array(
	'0-40'=>'40万以下',
	'40-60'=>'40-60万',
	'60-80'=>'60-80万',
	'80-100'=>'80-100万',
	'100-120'=>'100-120万',
	'120-150'=>'100-120万',
	'150-200'=>'150-200万',
	'200-250'=>'200-250万',
	'250-300'=>'250-300万',
	'300-500'=>'300-500万',
	'500-0'=>'500万以上'
);
$page->tpl->assign('house_price_option', $house_price_option);
//详细信息
$houseWanted = new HouseWanted($query);
$dataInfo = $houseWanted->getInfo($id,'*');

if(!$dataInfo){
	$page->urlto('requireList.php','信息不存在或已删除');
}
$page->tpl->assign('dataInfo', $dataInfo);

$page->show();
?>