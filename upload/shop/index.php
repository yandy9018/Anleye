<?php
require('path.inc.php');



$page->name = 'index'; //页面名字,和文件名相同	

$id = intval($_GET['id']);
if($id ==0){
	$page->back('参数错误');
}
//店长信息
$member = new Member($query);
$dataInfo = $member->getInfo($id,'*',true);
if($dataInfo['company_id']){
$company = new Company($query);
$dataInfo['company_name'] = $company->getInfo($dataInfo['company_id'],'company_name');
}
if(!$dataInfo){
	$page->back('经纪人不存在');
}
if($page->shop == '1'){
	if($dataInfo['mobile']=='' || $dataInfo['idcard']==''){
		$page->urlto('../broker','由于还没完善资料或通过实名认证,该经纪人网店还没开通。');
         }
	}else{
	if($dataInfo['mobile']==''){
	$page->urlto('../broker','由于还没完善资料,该经纪人网店还没开通。');
          } 
		
		}


$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');
$dataInfo['brokerRank'] = getNumByScore($dataInfo['scores'],$integral_array,'pic');
$dataInfo['cityarea_name'] = Dd::getCaption('cityarea',$dataInfo['cityarea_id']);
$dataInfo['active_str'] = explode('|',$dataInfo['active_str']);
$page->tpl->assign('dataInfo', $dataInfo);
$page->title = $dataInfo['realname']."的网店 - ".$page->city.$page->titlec;
//网店信息
$shop = new Shop($query);
$shopConf = $shop->getShopConf($id);
if(!$shopConf){
	$shopConf = array(
		'broker_id'=>$id,
		'shop_name'=>$dataInfo['realname']."的网店",
		'shop_notice'=>"欢迎来到".$dataInfo['realname']."的网店",
		'shop_style'=>'shopStyleDefault.css',
		'click_num'=>1
	);
	$shopConf['id']= $shop->saveConf($shopConf);
}
$page->tpl->assign('shopConf', $shopConf);
if($shopConf['shop_style']){
	$page->addCss($shopConf['shop_style']);
}else{
	$page->addCss('shopStyleDefault.css');
}

//实例化和字典
$houseSell = new HouseSell($query);
$houseRent = new HouseRent($query);
$house_fitment_option = Dd::getArray('house_fitment');

//店长推荐出售房源
$salePromote = $houseSell->getAll('*',' status =1 and is_promote =1 and broker_id ='.$id,'order by created desc');
foreach($salePromote as $key=>$item){
	if($item['house_totalarea']){
		$salePromote[$key]['avg_price']=round($item['house_price']*10000/$item['house_totalarea']);
	}else{
		$salePromote[$key]['avg_price'] ='0';
	}
}
$page->tpl->assign('salePromote', $salePromote);

//店长推荐出租房源
$rentPromote = $houseRent->getAll('*','  status =1 and is_promote =1 and broker_id ='.$id,'order by created desc');
foreach($rentPromote as $key=>$item){
	$rentPromote[$key]['house_fitment'] =$house_fitment_option[$item['house_fitment']];
}
$page->tpl->assign('rentPromote', $rentPromote);


//店长二手房列表

$brokerSaleList = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>7),'*',1,' and status=1  and broker_id ='.$id,' order by created desc');
foreach($brokerSaleList as $key=>$item){
	if($item['house_totalarea']){
		$brokerSaleList[$key]['avg_price']=round($item['house_price']*10000/$item['house_totalarea']);
	}else{
		$brokerSaleList[$key]['avg_price'] ='0';
	}
}
$page->tpl->assign('brokerSaleList', $brokerSaleList);
//店长租房列表
$brokerRentList = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>7),'*',1,' and status=1 and broker_id ='.$id,' order by created desc');
foreach($brokerRentList as $key=>$item){
	$brokerRentList[$key]['house_fitment'] =$house_fitment_option[$item['house_fitment']];
}
$page->tpl->assign('brokerRentList', $brokerRentList);


//二手房按小区统计出售
$saleCountBorough = $houseSell->getCountGroupBy('borough_id',' status =1 and borough_id <>0 and broker_id = '.$id);
$borough = new Borough($query);
foreach ($saleCountBorough as $key => $item){
	$saleCountBorough[$key]['borough_name'] = $borough->getInfo($item['borough_id'],'borough_name');
}
$page->tpl->assign('saleCountBorough', $saleCountBorough);

//二手房按价格统计
$sell_price_option = array(
	array('price'=>'0-40','house_price'=>'40万以下'),
	array('price'=>'40-60','house_price'=>'40-60万'),
	array('price'=>'60-80','house_price'=>'60-80万'),
	array('price'=>'80-100','house_price'=>'80-100万'),
	array('price'=>'100-120','house_price'=>'100-120万'),
	array('price'=>'120-150','house_price'=>'120-150万'),
	array('price'=>'150-200','house_price'=>'150-200万'),
	array('price'=>'200-250','house_price'=>'200-250万'),
	array('price'=>'250-300','house_price'=>'250-300万'),
	array('price'=>'300-500','house_price'=>'300-500万'),
	array('price'=>'500-0','house_price'=>'500万以上'),
);
foreach ($sell_price_option as $key=>$item){
	$where = " and status =1 and broker_id=".$id;
	$temp = explode('-',$item['price']);
	if($temp[0]){
		$where .= ' and house_price>='.$temp[0];
	}
	if($temp[1]){
		$where .= ' and house_price<='.$temp[1];
	}
	$house_num = $houseSell->getCount(1,$where);
	if($house_num){
		$item['house_num'] = $house_num;
		$saleCountPrice[$key] =$item;
	}
}
$page->tpl->assign('saleCountPrice', $saleCountPrice);

//二手房按房间数统计
$house_room_option = array(
	1=>"一室",
	2=>"二室",
	3=>"三室",
	4=>"四室",
	5=>"五室",
	6=>"其他"	
);

$saleCountRoom = $houseSell->getCountGroupBy('house_room',' status =1 and broker_id = '.$id,0);
foreach($saleCountRoom as $key=>$item){
	if($house_room_option[$item['house_room']]){
		$saleCountRoom[$key]['room'] = $item['house_room'];
		$saleCountRoom[$key]['house_room'] = $house_room_option[$item['house_room']];
	}else{
		unset($saleCountRoom[$key]);
	}
}
$page->tpl->assign('saleCountRoom', $saleCountRoom);

//租房按小区统计
$rentCountBorough = $houseRent->getCountGroupBy('borough_id',' status =1 and borough_id <>0 and broker_id = '.$id);
foreach ($rentCountBorough as $key => $item){
	$rentCountBorough[$key]['borough_name'] = $borough->getInfo($item['borough_id'],'borough_name');
}
$page->tpl->assign('rentCountBorough', $rentCountBorough);
//租房按价格统计

$rent_price_option = array(
	array('price'=>'0-600','house_price'=>'600元以下'),
	array('price'=>'600-800','house_price'=>'600-800元'),
	array('price'=>'800-1000','house_price'=>'800-1000元'),
	array('price'=>'1000-1200','house_price'=>'1000-1200元'),
	array('price'=>'1200-1500','house_price'=>'1200-1500元'),
	array('price'=>'1500-2000','house_price'=>'1500-2000元'),
	array('price'=>'2000-3000','house_price'=>'2000-3000元'),
	array('price'=>'3000-4000','house_price'=>'3000-4000元'),
	array('price'=>'4000-5000','house_price'=>'4000-5000元'),
	array('price'=>'5000-0','house_price'=>'5000元以上'),
);
foreach ($rent_price_option as $key=>$item){
	$where = " and status =1 and broker_id=".$id;
	$temp = explode('-',$item['price']);
	if($temp[0]){
		$where .= ' and house_price>='.$temp[0];
	}
	if($temp[1]){
		$where .= ' and house_price<='.$temp[1];
	}
	$house_num = $houseRent->getCount(1,$where);
	if($house_num){
		$item['house_num'] = $house_num;
		$rentCountPrice[$key] =$item;
	}
}
$page->tpl->assign('rentCountPrice', $rentCountPrice);

//租房按房间统计
$house_room_option = array(
	1=>"一室",
	2=>"二室",
	3=>"三室",
	4=>"四室",
	5=>"五室",
	6=>"其他"	
);

$rentCountRoom = $houseRent->getCountGroupBy('house_room',' status =1 and broker_id = '.$id,0);
foreach($rentCountRoom as $key=>$item){
	if($house_room_option[$item['house_room']]){
		$rentCountRoom[$key]['room'] = $item['house_room'];
		$rentCountRoom[$key]['house_room'] = $house_room_option[$item['house_room']];
	}else{
		unset($rentCountRoom[$key]);
	}
}
$page->tpl->assign('rentCountRoom', $rentCountRoom);

//店主人脉
$brokerFriend = new BrokerFriend($query);
//$brokerFriends = $brokerFriend->getList(array('rowFrom'=>0,'rowTo'=>8),'*',' broker_id ='.$id,'',true);
$brokerFriends = $brokerFriend->getList(array('rowFrom'=>0,'rowTo'=>8),'*',' broker_id ='.$id.' and status =1 ','',true);
$brokerFriends = array_sortby_multifields($brokerFriends,array('active_rate'=>SORT_DESC));

$page->tpl->assign('brokerFriends', $brokerFriends);
$brokerFriendCount = $brokerFriend->getCount(' broker_id ='.$id.' and status =1 ');
$page->tpl->assign('brokerFriendCount', $brokerFriendCount);

//来访用户
$shop_viewlog = new ShopViewLog($query);
$shopViewer = $shop_viewlog->getList(array('rowFrom'=>0,'rowTo'=>8),'*',' broker_id ='.$id,' order by add_time desc',true);
//$shopViewer = array_sortby_multifields($shopViewer,array('active_rate'=>SORT_DESC ));
$page->tpl->assign('shopViewer', $shopViewer);

//记录访问log
if($_COOKIE['AUTH_MEMBER_NAME']){
	$member_id = $member->getAuthInfo('id');
	if($member_id && $member_id <>$id){
		$shop_viewlog->addLog($id,$member_id);
	}
}
//增加人气
if($_COOKIE['VIEW_SHOP_LOG']){
	$view_log = unserialize($_COOKIE['VIEW_SHOP_LOG']);
	if($view_log && !in_array($id,(array)$view_log)){
		//今天没有访问过
		$shop->addClick($id);
		$view_log[] = $id;
		$view_log_serialize = serialize($view_log);
		setcookie('VIEW_SHOP_LOG',$view_log_serialize,time()+60*60*24*365,'/',$cfg['domain']);
	}
}else{
	$shop->addClick($id);
	$view_log = array();
	$view_log[] = $id;
	$view_log_serialize = serialize($view_log);
	setcookie('VIEW_SHOP_LOG',$view_log_serialize,time()+60*60*24*365,'/',$cfg['domain']);
}

$page->show();
?>