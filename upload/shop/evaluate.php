<?php
require('path.inc.php');
$member = new Member($query);

if($page->action == 'save') {
   $_POST['client_ip'] = $_SERVER["REMOTE_ADDR"];
   
  if( $_POST['accurate']==0 or $_POST['accurate']==0 or $_POST['accurate']==0){
	  $page->back('请将评价信息完善');
	  }
  
  //判断用户是否评价过
   $clientIp = $member->checkEvaluate($_POST['client_ip']);
   if($clientIp){
       $page->back('你已经评价过该经纪人');
	   }
	   
	  
   try{ 
	    $member->evaluateSave($_POST);
		$page->urlto('evaluate.php?id='.$_POST['broker_id'],'评价成功');

	}catch ( Exception $e){
	     $page->back('评价失败');
		//$page->back($e->getMessage());
	}
	exit;
    
}else{
	$page->name = 'evaluate'; //页面名字,和文件名相同
	
	$id = intval($_GET['id']);
	if(!$id){
		$page->back('参数错误');
	}

	//成交使用的thickBox加载
    $page->addcss("thickbox.css");
    $page->addjs("thickbox.js");
	
	$dataInfo = $member->getInfo($id,'*',true);
	if($dataInfo['company_id']){
$company = new Company($query);
$dataInfo['company_name'] = $company->getInfo($dataInfo['company_id'],'company_name');
}
	if(!$dataInfo){
		$page->back('经纪人不存在');
	}
	
	
	$list_num = intval($_GET['list_num']);
	if(!$list_num){
		$list_num = 10;
	}
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$row_count = $member->getCountEvaluate(' broker_id='.$id,'*');
	$pages = new Pages($row_count,$list_num);
	//page
	$pageno = $_GET['pageno']?intval($_GET['pageno']):1;
	$pre_page = $pageno>1?$pageno-1:1;
	$next_page = $pageno<$pages->pageCount?$pageno+1:$page_count;
	$page->tpl->assign('pageno', $pageno);
	$page->tpl->assign('row_count', $row_count);
	$page->tpl->assign('page_count', $pages->pageCount);
	$page->tpl->assign('pre_page', $pages->fileName.'pageno='.$pre_page);
	$page->tpl->assign('next_page', $pages->fileName.'pageno='.$next_page);
	$pageLimit = $pages->getLimit();
	
	//获取评价列表
	$dataList = $member->getEvaluateList($pageLimit,'*',' broker_id='.$id,'');
	
	foreach ($dataList as $key=> $item){
	  $reg1 = '/((?:\d+\.){3})\d+/';  
	  $dataList[$key]['ip'] =  preg_replace($reg1, "\\1*", $item['client_ip']);
	  }
	  
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条
	
	
	$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');
	$dataInfo['brokerRank'] = getNumByScore($dataInfo['scores'],$integral_array,'pic');
	$dataInfo['cityarea_name'] = Dd::getCaption('cityarea',$dataInfo['cityarea_id']);
	$dataInfo['active_str'] = explode('|',$dataInfo['active_str']);
	$dataInfo['broker_type'] = Dd::getCaption('broker_type',$dataInfo['broker_type']);
	$page->tpl->assign('dataInfo', $dataInfo);
	
	
	$evaluateCount = $member->getCountEvaluate(' broker_id='.$id,'*');
	
	//全站平均水平分值
	$ave = 3.0;
	//获取房屋信息平均值
if($evaluateCount!=0){
    $aveAccurate = round($dataInfo['e_accurate']/$evaluateCount,1);
	if($aveAccurate>$ave){
		$aveUp = $aveAccurate-$ave;
		$percentageAcc = round($aveUp/$ave,2)*100;
		}else{
			$aveUp = $ave-$aveAccurate;
			$percentageAcc = round($aveUp/$ave,2)*100;
			}

	
	
	//获取服务信息平均值
    $aveSatisfaction = round($dataInfo['e_satisfaction']/$evaluateCount,1);
	if($aveSatisfaction>$ave){
		$aveUp = $aveSatisfaction-$ave;
		$percentageSat = round($aveUp/$ave,2)*100;
		}else{
			$aveUp = $ave-$aveSatisfaction;
			$percentageSat = round($aveUp/$ave,2)*100;
			}
			

			
	//获取专业信息平均值
    $aveSpecialty = round($dataInfo['e_specialty']/$evaluateCount,1);
	if($aveSpecialty>$ave){
		$aveUp = $aveSpecialty-$ave;
		$percentageSpe = round($aveUp/$ave,2)*100;
		}else{
			$aveUp = $ave-$aveSpecialty;
			$percentageSpe = round($aveUp/$ave,2)*100;
			}
	}
	$start['spe1'] = $member->getCountEvaluate('specialty=1 and broker_id='.$id,'*');
	$start['spe2'] = $member->getCountEvaluate('specialty=2 and broker_id='.$id,'*');
	$start['spe3'] = $member->getCountEvaluate('specialty=3 and broker_id='.$id,'*');
	$start['spe4'] = $member->getCountEvaluate('specialty=4 and broker_id='.$id,'*');
	$start['spe5'] = $member->getCountEvaluate('specialty=5 and broker_id='.$id,'*');
	$start['sat1'] = $member->getCountEvaluate('satisfaction=1 and broker_id='.$id,'*');
	$start['sat2'] = $member->getCountEvaluate('satisfaction=2 and broker_id='.$id,'*');
	$start['sat3'] = $member->getCountEvaluate('satisfaction=3 and broker_id='.$id,'*');
	$start['sat4'] = $member->getCountEvaluate('satisfaction=4 and broker_id='.$id,'*');
	$start['sat5'] = $member->getCountEvaluate('satisfaction=5 and broker_id='.$id,'*');
	$start['acc1'] = $member->getCountEvaluate('accurate=1 and broker_id='.$id,'*');
	$start['acc2'] = $member->getCountEvaluate('accurate=2 and broker_id='.$id,'*');
	$start['acc3'] = $member->getCountEvaluate('accurate=3 and broker_id='.$id,'*');
	$start['acc4'] = $member->getCountEvaluate('accurate=4 and broker_id='.$id,'*');
	$start['acc5'] = $member->getCountEvaluate('accurate=5 and broker_id='.$id,'*');
			
			
	//获取好评率
	if($evaluateCount!=0){
	$good = round(($start['sat5']+$start['spe5']+$start['acc5'])/($row_count*3),2)*100;
	$page->tpl->assign('good', $good);
	}
	
	$page->tpl->assign('start', $start);
	$page->tpl->assign('aveSatisfaction', $aveSatisfaction);
	$page->tpl->assign('aveAccurate', $aveAccurate);
	$page->tpl->assign('aveSpecialty', $aveSpecialty);
	
	$page->tpl->assign('percentageAcc', $percentageAcc);
	$page->tpl->assign('percentageSpe', $percentageSpe);
	$page->tpl->assign('percentageSat', $percentageSat);
	

	//网店信息
	$shop = new Shop($query);
	$shopConf = $shop->getShopConf($id);
	$page->tpl->assign('shopConf', $shopConf);
	}
     
	//获取
$page->title = $dataInfo['realname']."的网店 - 经纪人评价 - ".$page->title;	
$page->show();
?>