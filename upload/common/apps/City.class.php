<?php
/**
 * 多城市管理类
 *
 * @author 阿一 ayi@yanwee.com
 * @package 4.0
 * 2012-09-02
 */

/**
 * 网站基本配置管理类
 * @package Apps
 */

class City {

	/**
	 * @var Object $db 数据库查询对象
	 * @access private
	 */
	var $db = NULL;
	
	/**
	 * 城市管理
	 *
	 * @var string
	 */
	var $tName = "fke_city";
	
	/**
	 * 加盟信息
	 *
	 * @var string
	 */
	var $tNameUnion = "fke_union";
	
	/**
	 * 构造函数
	 *
	 * @param source $db
	 */
	function __construct($db) {
		$this->db = $db;
	}
	
	 /**
	 * 取得信息列表
	 * @access public
	 * 
	 * @param array $pageLimit
	 * @return array
	 **/
	 function getList($pageLimit, $fileld='*' ,$where='', $order=' order by id asc ') {
	 	if($where){
			$where=' where '.$where;
		}
		//print_rr('select * from '.$this->tName.' '.$where.' '.$order , $pageLimit['rowFrom'],$pageLimit['rowTo']);
		$this->db->open('select * from '.$this->tName.' '.$where.' '.$order , $pageLimit['rowFrom'],$pageLimit['rowTo']);
		$result = array();
		while ($rs = $this->db->next()) {
			$result[] = $rs;
		}
		return $result;
	 }
	
	
	
	/**
	 * 取得详细信息
	 * @access public
	 * @param int $id
	 * @return array 
	 */
	function getInfo($id,$field = '*'){
		return $this->db->getValue('select '.$field.' from '.$this->tName.' where id =' .$id);
	}
	
	/**
	 * 验证城市信息
	 * @access public
	 * @param int $id
	 * @return array 
	 */
	function getCity($name,$field = '*',$where = '1 = 1'){
		
		$name=iconv("utf-8","gb2312",$name);
		return $this->db->getValue("select url from ".$this->tName." where name = '".$name."'"."and ".$where);
	}
	
	/**
	 * 取类别总数
	 * @access public
	 * @return int
	 */
	function getCount($where = '') {
		if($where){
			$where=' where '.$where;
		}
		return $this->db->getValue('select count(*) from '.$this->tName.' '.$where );
	}
	
	
	/**
	 * 取加盟信息类别总数
	 * @access public
	 * @return NULL
	 */
	function getCountUnion($flag = 0,$where_clouse = '') {
		$where =" where 1 = 1";
		if($flag == 1){
			$where .= " and status = 0 ";
		}
		if($flag == 2){
			$where .= " and status = 1 ";
		}
		if($flag == 3){
			$where .= " and status = 2 ";
		}
		
		if($where_clouse){
			$where.= $where_clouse;
		}
		
		return $this->db->getValue('select count(*) from '.$this->tNameUnion. $where );
	}
	
	 /**
	 * 取加盟信息列表
	 * @param array limit 
	 * @access public
	 * @return array
	 */
	function getListUnion($pageLimit,$fileld='*' , $flag = 0,$where_clouse = '',$order='') {
		$where =' where 1 = 1' ;
		if($where_clouse){
			$where .= $where_clouse;
		}
		if($flag == 1){
			$where .= " and status = 0 ";
		}
		if($flag == 2){
			$where .= " and status = 1 ";
		}
		if($flag == 3){
			$where .= " and status = 2 ";
		}
		$this->db->open('select '.$fileld.' from '.$this->tNameUnion.$where.' '.$order , $pageLimit['rowFrom'],$pageLimit['rowTo']);
		$result = array();
		while ($rs = $this->db->next()) {
			$result[] = $rs;
		}
		return $result;
	}
	
		/**
	 * 删除加盟信息
	 * @param mixed $ids 选择的ID
	 * @access public
	 * @return bool
	 */
	function deleteUnion($ids) {
		if (is_array($ids)) {
			$ids = implode(',',$ids);
			$where = ' where id in (' . $ids . ')';
		} else {
			$where = ' where id=' . intval($ids);
		}
		$this->db->execute('delete from '.$this->tNameUnion. $where);
		return true;
		
	}
	
	/**
	 * 修改加盟信息状态
	 * @param mixed $ids 选择的ID
	 * @access public
	 * @return bool
	 */
	function checkUnion($ids,$flag) {
		$where = ' where 1=1 ';
		if (is_array($ids)) {
			$ids = implode(',',$ids);
			$where .= ' and id in (' . $ids . ')';
		} else {
			$where .= ' and id=' . intval($ids);
		}
		
		return $this->db->execute('update '.$this->tNameUnion.' set status = '.$flag.$where);
	}
	
	/**
	 * 操作用户状态 不物理删除
	 * @param mixed $members 用户ID
	 * @access public
	 * @return bool
	 */
	function changeStatus($ids,$status) {
		if (is_array($ids)) {
			$ids = implode(',',$ids);
			$where = ' id in (' . $ids . ')';
		} else {
			$where = ' id=' . intval($ids);
		}
		return $this->db->execute('update '.$this->tName.' set status = '.$status.' where ' . $where);
	}
	
		/**
	 * 删除信息
	 * @param mixed $ids ID列表
	 * @access public
	 * @return bool
	 */
	function delete($ids) {
		if (is_array($ids)) {
			$ids = implode(',',$ids);
			$where = ' id in (' . $ids . ')';
		} else {
			$where = ' id=' . intval($ids);
		}
		return $this->db->execute('delete from '.$this->tName.' where ' . $where);
	}
	
	 /**
	 * 记录加盟信息
	 */
	function saveUnion($fileddata) {
		$field_array  = array(
			'cname'=>$fileddata['cname'],
			'cityid'=>$fileddata['cityid'],
			'address'=>$fileddata['address'],
			'con'=>$fileddata['con'],
			'mo'=>$fileddata['mo'],
			'tel'=>$fileddata['tel'],
			'company'=>$fileddata['company'],
			'good'=>$fileddata['good'],
			'time'=>time(),
			
		);
		return $this->db->insert($this->tNameUnion,$field_array);
		}
	
	
	
 /**
	 * 保存信息
	 * @access public
	 * 
	 * @param array $memberInfo
	 * @return bool
	 **/
	 function save($info){
	 	$info['id']=intval($info['id']);
	 	if(!$info['id']){
	 		$insertField = array(
		 		'name'=>$info['name'],
				'url'=>$info['url'],
				'logo'=>$info['logo'],
				'is_proxy'=>$info['is_proxy'],
				'pid'=>$info['pid'],
				'is_hot'=>$info['is_hot'],
				'add_time'=>time(),
				
		 	);
		 	$this->db->insert($this->tName,$insertField);
	
   	}else{
	 		$updateField  = array(
		 		'name'=>$info['name'],
				'url'=>$info['url'],
				'logo'=>$info['logo'],
				'pid'=>$info['pid'],
				'is_hot'=>$info['is_hot'],
				'is_proxy'=>$info['is_proxy'],
		 	);
	 		$this->db->update($this->tName,$updateField,'id=' . $info['id']);
	 		
	 	}
	 }
}
?>