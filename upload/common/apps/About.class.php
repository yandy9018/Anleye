<?php
/**
 * 关于我们配置
 *
 * @author 阿一 ayi@yanwee.com
 * @package 4.0
 * @version $Id$
 * 2012-08-28
 */

/**
 * 网站基本配置管理类
 * @package Apps
 */

class About {

	/**
	 * @var Object $db 数据库查询对象
	 * @access private
	 */
	var $db = NULL;
	
	/**
	 * 主用户表
	 *
	 * @var string
	 */
	var $tName = "fke_about";
	
	/**
	 * 构造函数
	 *
	 * @param source $db
	 */
	function __construct($db) {
		$this->db = $db;
	}
	
	/**
	 * 取得详细信息
	 * @access public
	 * @param int $id
	 * @return array 
	 */
	function getInfo($id,$field = '*'){
		return $this->db->getValue('select '.$field.' from '.$this->tName.' where id =' .$id);
	}
	
	/**
	 * 保存网站信息
	 * @access public
	 * @param int $id
	 * @return array 
	 */
	function save($id,$fileddata){
		 $this->db->execute('update '.$this->tName.' set content ="'.$fileddata.'" where id = '.$id);
	}
	
}
?>