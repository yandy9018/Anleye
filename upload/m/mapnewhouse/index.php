<?php
require('path.inc.php');
$page->name = 'index'; //页面名字,和文件名相同
$page->title = iconv('gb2312','utf-8',$page->title);
$page->titlec = iconv('gb2312','utf-8',$page->titlec);
$page->city = iconv('gb2312','utf-8',$page->city);
$page->mapkeyword = iconv('gb2312','utf-8',$page->mapkeyword);
$page->mapdescription = iconv('gb2312','utf-8',$page->mapdescription);
$page->googlekey = iconv('gb2312','utf-8',$page->googlekey);
$realname = iconv('gb2312','utf-8',$realname);
$page->tpl->assign('username',$realname);




//售价
$house_price_option = array(
	'0-5000'=>'5000元/㎡以下',
	'5000-6000'=>'5000-6000元/㎡',
	'6000-7000'=>'6000-7000元/㎡',
	'7000-8000'=>'7000-8000元/㎡',
	'8000-9000'=>'8000-9000元/㎡',
	'9000-10000'=>'9000-10000元/㎡',
	'10000-15000'=>'10000-15000元/㎡',
	'15000-0'=>'15000元/㎡以上'
);
$page->tpl->assign('house_price_option', $house_price_option);



//区域字典
$cmap = new CityareaMap($query);
$cmap_option = $cmap->getList('*','','');
foreach ($cmap_option as $key=> $item){
		$cmap_option[$key]['cityarea_name'] = iconv('gb2312','utf-8', $item['cityarea_name']);
	}
$page->tpl->assign('cmap_option', $cmap_option);



//面积
$borough_totalarea_option = array(
	'1-100000'=>'10万㎡以下',
	'100000-200000'=>'10万-20万㎡',
	'200000-400000'=>'20万-40万㎡',
	'400000-600000'=>'40万-60万㎡',
	'600000-800000'=>'60万-80万㎡',
	'800000-1000000'=>'80万-100万㎡',
	'1000000-1500000'=>'100万-150万㎡',
	'1500000-0'=>'150万㎡以上'
);
$page->tpl->assign('borough_totalarea_option', $borough_totalarea_option);

//开盘年份
$sell_time_option = array(
    '2005' => '2005年',
    '2006' => '2006年',
    '2007' => '2007年',
    '2008' => '2008年',
	'2009' => '2009年',
	'2010' => '2010年',
	'2011' => '2011年',
	'2012' => '2012年',
	'2013' => '2013年',
);
$page->tpl->assign('sell_time_option', $sell_time_option);

//房屋类型
$borough_type_option = array(
    1 => '住宅楼',
    2 => '别墅',
    3 => '写字楼',
    4 => '商业',
);
$page->tpl->assign('borough_type_option', $borough_type_option);
$page->show();
?>