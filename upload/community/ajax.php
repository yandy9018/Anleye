<?php
require('path.inc.php');
$dd_array = array(
	'borough_sight'=>'borough_sight',
	'borough_support'=>'borough_support',
	'borough_type'=>'borough_type',
	'borough_section'=>'borough_section',
	'cityarea_id'=>'cityarea',
);
$member = new Member($query);
$member_id = $member->getAuthInfo('id');
$borough = new Borough($query);

if($page->action == 'expert'){
	$borough_id = intval($_POST['id']);
	$exestExpert = $borough->checkExpertUnique($member_id);
	if(empty($exestExpert)){
		if($expert_id = $borough->addBoroughExpert($borough_id,$member_id)){
			$borough_name = $borough->getInfo($borough_id,'borough_name');
			echo $borough_name."|1";
		}else{
			echo 0;
		}
	}else{
		$borough_name = $borough->getInfo($exestExpert['borough_id'],'borough_name');
		if($status == 1){
			echo $borough_name."|-1";
		}else{
			echo $borough_name."|-2";
		}
	}
}elseif($page->action == 'saveBoroughText'){
	header('content-Type: text/html; charset=utf-8');
	$_POST['borough_id'] = intval($_REQUEST['borough_id']);
	$_POST['broker_id'] = $member_id;
	$postvalue = $_POST['value'];
	$_POST['value'] = iconv('utf-8','gbk',$_POST['value']);
	$boroughUpdate= new BoroughUpdate($query);
	try {
		$boroughInfo = $borough->getInfo($_POST['borough_id'],'*',true,true);
		if($boroughInfo[$_POST['id']] != $_POST['value']){
			$_POST['old_value'] = $boroughInfo[$_POST['id']];
			$boroughUpdate->save($_POST);
		}
		echo $postvalue;
	}catch (Exception $e){
		echo "";
	}
	
}elseif($page->action == 'saveBoroughSelect'){
	header('content-Type: text/html; charset=utf-8');
	$_POST['borough_id'] = intval($_REQUEST['borough_id']);
	$boroughUpdate= new BoroughUpdate($query);
	$_POST['broker_id'] = $member_id;
	$array = Dd::getArray($dd_array[$_POST['id']]);
	try {
		$boroughInfo = $borough->getInfo($_POST['borough_id'],'*',true,true);
		if($boroughInfo[$_POST['id']] != $_POST['value']){
			$_POST['old_value'] = $boroughInfo[$_POST['id']];
			$boroughUpdate->save($_POST);
		}
		//echo $array[$_POST['value']];
		echo iconv('gbk','utf-8',$array[$_POST['value']]);
	}catch (Exception $e){
		echo "";
	}
	
}elseif($page->action == 'saveBoroughCheckbox'){
	header('content-Type: text/html; charset=utf-8');
	$_POST['borough_id'] = intval($_REQUEST['borough_id']);
	$boroughUpdate= new BoroughUpdate($query);
	$_POST['broker_id'] = $member_id;
	$array = Dd::getArray($dd_array[$_POST['id']]);
	try {
		$boroughInfo = $borough->getInfo($_POST['borough_id'],'*',true,true);
		if($boroughInfo[$_POST['id']] != $_POST['value']){
			$_POST['old_value'] = $boroughInfo[$_POST['id']];
			$boroughUpdate->save($_POST);
		}
		//echo $array[$_POST['value']];
		//echo iconv('gbk','utf-8',Dd::getCaption($_POST['value']));
	}catch (Exception $e){
		echo "";
	}
	
}elseif($page->action == 'loadDic'){
	header('content-Type: text/html; charset=utf-8');
	$id =$_GET['id'];
	if(key_exists($id,$dd_array)){
		$array = Dd::getArray($dd_array[$id]);
	}
	foreach ($array as $key=> $item){
		$array[$key] = iconv('gb2312','utf-8', $item);
	}
	$json = new Services_JSON();
	echo $json->encode($array);
	//echo json_encode($array);
}elseif($page->action == 'uploadPicture'){
	$boroughUpdate= new BoroughUpdate($query);
	$_POST['broker_id'] = $member_id;
	$_POST['borough_id'] = intval($_REQUEST['borough_id']);
	
	if($_POST['id']=="borough_pic"){
		$pic_list = $borough->getImgList($_POST['borough_id'],false);
		if($pic_list){
			foreach ($pic_list as $item){
				$picList[] = $item['pic_url'];
			}
			$_POST['old_value'] = implode('|',$picList);
		}
	}else{
		$pic_list = $borough->getImgList($_POST['borough_id'],true);
		if($pic_list){
			foreach ($pic_list as $item){
				$picList[] = $item['pic_url'];
			}
			$_POST['old_value'] = implode('|',$picList);
		}
	}
	
	try {
		$boroughUpdate->save($_POST);
		echo $array[$_POST['value']];
	}catch (Exception $e){
		echo "";
	}
}
?>