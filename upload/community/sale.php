<?php
require('path.inc.php');

$page->name = 'sale'; //页面名字,和文件名相同

//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

$house_price_option = array(
	'0-40'=>'40万以下',
	'40-60'=>'40-60万',
	'60-80'=>'60-80万',
	'80-100'=>'80-100万',
	'100-120'=>'100-120万',
	'120-150'=>'120-150万',
	'150-200'=>'150-200万',
	'200-250'=>'200-250万',
	'250-300'=>'250-300万',
	'300-0'=>'300万以上'
);
$page->tpl->assign('house_price_option', $house_price_option);

$house_totalarea_option = array(
	'0-50'=>'50㎡以下',
	'50-70'=>'50-70㎡',
	'70-90'=>'70-90㎡',
	'90-110'=>'90-110㎡',
	'110-130'=>'110-130㎡',
	'130-150'=>'130-150㎡',
	'150-200'=>'150-200㎡',
	'200-250'=>'150-200㎡',
	'250-300'=>'250-300㎡',
	'300-0'=>'300㎡以上'
);
$page->tpl->assign('house_totalarea_option', $house_totalarea_option);


//id
$id = intval($_GET['id']);
if(!$id){
	$page->urlto('index.php');
}

//小区
$borough = new Borough($query);

//小区详细信息
//小区详细信息
$boroughInfo = $borough->getInfo($id,'*',1,true);

if(!$boroughInfo){
	$page->urlto('index.php');
}

$boroughInfo['cityarea_name'] = $cityarea_option[$boroughInfo['cityarea_id']];
$boroughInfo['borough_section'] = Dd::getCaption('borough_section',$boroughInfo['borough_section']);
$boroughInfo['borough_support'] = Dd::getCaption('borough_support',$boroughInfo['borough_support']);
$boroughInfo['borough_sight'] = Dd::getCaption('borough_sight',$boroughInfo['borough_sight']);
$boroughInfo['borough_type'] = Dd::getCaption('borough_type',$boroughInfo['borough_type']);
$boroughInfo['unsign_percent_change'] = abs($boroughInfo['percent_change']);
if(!$boroughInfo['borough_thumb']){
	if($boroughImageList){
		$boroughInfo['borough_thumb'] = $boroughImageList[0]['pic_url'];
		$borough->updateThumb($boroughInfo['id'],$boroughInfo['borough_thumb']);
	}
}
$page->tpl->assign('dataInfo', $boroughInfo);

//小区图片
$boroughImageList = $borough->getImgList($id,0,4);
$page->tpl->assign('boroughImageList', $boroughImageList);
$borough_img_num = count($boroughImageList);
if($borough_img_num%2){
	$borough_img_num = 2-$borough_img_num%2;
	$page->tpl->assign('borough_img_num', $borough_img_num);
}
//小区户型图
$boroughDrawList = $borough->getImgList($id,1,4);
$page->tpl->assign('boroughDrawList', $boroughDrawList);
$borough_draw_num = count($boroughDrawList);
if($borough_draw_num%2){
	$borough_draw_num = 2-$borough_draw_num%2;
	$page->tpl->assign('borough_draw_num', $borough_draw_num);
}
//小区专家
$boroughAdviserId = $borough->getAdviserList($id);
$integral_array = require($cfg['path']['conf'].'integral.cfg.php');
if($boroughAdviserId){
	foreach ($boroughAdviserId as $key => $item){
		$boroughAdviserList[$key] = $member->getInfo($item['member_id'],'*',true);
		$boroughAdviserList[$key]['brokerRank'] = getNumByScore($boroughAdviserList[$key]['scores'],$integral_array,'pic');
	}
}
$page->tpl->assign('boroughAdviserList', $boroughAdviserList);

//纪录列表

$where =' and status=1 and borough_id = '.$id;

//price
$price = $_GET['price'];
if($price){
	$tmp = explode('-',$price);
	if($tmp[0]){
		$where .= ' and house_price >= '.intval($tmp[0]);
	}
	if($tmp[1]){
		$where .= ' and house_price <= '.intval($tmp[1]);
	}
}

//room
$room = intval($_GET['room']);
if($room){
	$where .= ' and house_room = '.$room;
}

//totalarea
$totalarea = $_GET['totalarea'];
if($totalarea){
	$tmp = explode('-',$totalarea);
	if($tmp[0]){
		$where .= ' and house_totalarea >= '.intval($tmp[0]);
	}
	if($tmp[1]){
		$where .= ' and house_totalarea <= '.intval($tmp[1]);
	}
}
//list_order 排序转换
switch ($_GET['list_order']){
	case "avg_price desc":
		$list_order = " order by house_price/house_totalarea desc";
		break;
	case "avg_price asc":
		$list_order = " order by house_price/house_totalarea asc";
		break;
	case "created desc":
		$list_order = " order by updated desc";
		break;
	case "house_price asc":
		$list_order = " order by house_price asc";
		break;
	case "house_price desc":
		$list_order = " order by house_price desc";
		break;
	case "house_totalarea asc":
		$list_order = " order by house_totalarea asc";
		break;
	case "house_totalarea desc":
		$list_order = " order by house_totalarea desc";
		break;
	default:
		$list_order = " order by update_order desc";
		break;
}

/*print_rr($where);*/
//list_num
$list_num = intval($_GET['list_num']);
if(!$list_num){
	$list_num = 10;
}

$house = new HouseSell($query);

require($cfg['path']['lib'] . 'classes/Pages.class.php');
$row_count = $house->getCount(1,$where);
$pages = new Pages($row_count,$list_num);

//page
$pageno = $_GET['pageno']?intval($_GET['pageno']):1;
$pre_page = $pageno>1?$pageno-1:1;
$next_page = $pageno<$pages->pageCount?$pageno+1:$page_count;
$page->tpl->assign('pageno', $pageno);
$page->tpl->assign('row_count', $row_count);
$page->tpl->assign('page_count', $pages->pageCount);
$page->tpl->assign('pre_page', $pages->fileName.'pageno='.$pre_page);
$page->tpl->assign('next_page', $pages->fileName.'pageno='.$next_page);

$pageLimit = $pages->getLimit();
$dataList = $house->getList($pageLimit,'*',1,$where,$list_order);
$member = new Member($query);
//积分配置文件

foreach ($dataList as $key=> $item){
	if($item['house_price'] && $item['house_totalarea']){
		$dataList[$key]['avg_price'] = round($item['house_price']*10000/$item['house_totalarea']);
	}else{
		$dataList[$key]['avg_price'] = "未知";
	}
	//图片数量
	$dataList[$key]['pic_num'] = $house->getImgNum($item['id']);
	//经纪人信息
	$dataList[$key]['broker_info'] = $member->getInfo($item['broker_id'],'*',true);
	$dataList[$key]['broker_info']['outlet'] = substr($dataList[$key]['broker_info']['outlet'],0,strpos($dataList[$key]['broker_info']['outlet'],'-'));

	$dataList[$key]['broker_info']['brokerRank'] = getNumByScore($dataList[$key]['broker_info']['scores'],$integral_array,'pic');
	//是否是专家
	$dataList[$key]['broker_info']['is_expert'] =$member->is_expert($item['broker_id']);
}

$page->tpl->assign('dataList', $dataList);
$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条

//页面标题
$page->title = $boroughInfo['borough_name'].'二手房，'.$boroughInfo['borough_name'].'房屋出售，'.$boroughInfo['borough_name'].' - '.$page->city.$page->title;

//关键词
$page->keyword = $boroughInfo['borough_name'].','.$boroughInfo['borough_name'].'二手房,'.$boroughInfo['borough_name'].'房屋出售';
//描述
$page->description='';

$page->tpl->assign('community_menu','sale');
$page->show();
?>