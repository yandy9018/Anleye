<?php
require('path.inc.php');

$page->name = 'evaluate'; //页面名字,和文件名相同	

$id = intval($_GET['id']);
if(!$id){
	$page->back('参数错误');
}
//店长信息
$member = new Member($query);
$dataInfo = $member->getInfo($id,'*',true);
if(!$dataInfo){
	$page->back('经纪人不存在');
}
$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');
$dataInfo['brokerRank'] = getNumByScore($dataInfo['scores'],$integral_array,'pic');
$dataInfo['cityarea_name'] = Dd::getCaption('cityarea',$dataInfo['cityarea_id']);
$dataInfo['active_str'] = explode('|',$dataInfo['active_str']);
$dataInfo['broker_type'] = Dd::getCaption('broker_type',$dataInfo['broker_type']);
$page->tpl->assign('dataInfo', $dataInfo);
$page->title = $dataInfo['realname']."的网店 - 服务口碑 - ".$page->title;
//网店信息
$shop = new Shop($query);
$shopConf = $shop->getShopConf($id);
$page->tpl->assign('shopConf', $shopConf);
if($shopConf['shop_style']){
	$page->addCss($shopConf['shop_style']);
}else{
	$page->addCss('shopStyleDefault.css');
}
//成交数
//sell_num
$sellBargin = new SaleBargain($query);
$sell_num = $sellBargin->getCount(' broker_id = '.$id);
$page->tpl->assign('sell_num', $sell_num);
//rent_num
$rentBargin = new RentBargain($query);
$rent_num = $rentBargin->getCount(' broker_id = '.$id);
$page->tpl->assign('rent_num', $rent_num);

//积分
$integral = new Integral($query);
//scoreLog
$scoreLogAdd = $integral->getLogGroup($id," rule_score > 0 ");
$scoreLogDec = $integral->getLogGroup($id," rule_score < 0 ");
$integral_dd = Dd::getArray('integral_ruleclass');
foreach ($integral_dd as $key=>$item){
	if( !$scoreLogAdd[$key] && !$scoreLogDec[$key]){
		continue;
	}
	$scoreLog[$key]['caption'] = $item;
	$scoreLog[$key]['add_score'] = $scoreLogAdd[$key];
	$scoreLog[$key]['dec_score'] = $scoreLogDec[$key];
}
$page->tpl->assign('scoreLog', $scoreLog);

//成交记录
$evaluteSaleList = $sellBargin->getList($pageLimit,'*','broker_id = '.$id,'  order by id desc ');
$page->tpl->assign('evaluteSaleList',$evaluteSaleList);
$evaluteRentList = $rentBargin->getList($pageLimit,'*','broker_id = '.$id,'  order by id desc ');
$page->tpl->assign('evaluteRentList',$evaluteRentList);

$page->show();
?>