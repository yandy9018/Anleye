<?php
require('path.inc.php');


$page->name = 'rent'; //页面名字,和文件名相同	

$id = intval($_GET['id']);
if(!$id){
	$page->back('参数错误');
}
//店长信息
$company = new Company($query);
$dataInfo = $company->getInfo($id,'*');
if(!$dataInfo){
	$page->back('该店铺不存在');
}
$page->tpl->assign('dataInfo', $dataInfo);
$page->title = $dataInfo['company_name']."的门店 - 出租 - ".$page->title;

//公司网站样式判断
if($dataInfo['company_style']){
	$page->addCss($dataInfo['company_style']);
}else{
	$page->addCss('shopStyleDefault.css');
}


//实例化和字典
$houseRent = new HouseRent($query);
$house_fitment_option = Dd::getArray('house_fitment');

//租房按小区统计
$rentCountBorough = $houseRent->getCountGroupBy('borough_id',' status =1 and borough_id <>0 and company_id = '.$id);
$borough = new Borough($query);
foreach ($rentCountBorough as $key => $item){
	$rentCountBorough[$key]['borough_name'] = $borough->getInfo($item['borough_id'],'borough_name');
}
$page->tpl->assign('rentCountBorough', $rentCountBorough);
//租房按价格统计

$rent_price_option = array(
	array('price'=>'0-600','house_price'=>'600元以下'),
	array('price'=>'600-800','house_price'=>'600-800元'),
	array('price'=>'800-1000','house_price'=>'800-1000元'),
	array('price'=>'1000-1200','house_price'=>'1000-1200元'),
	array('price'=>'1200-1500','house_price'=>'1200-1500元'),
	array('price'=>'1500-2000','house_price'=>'1500-2000元'),
	array('price'=>'2000-3000','house_price'=>'2000-3000元'),
	array('price'=>'3000-4000','house_price'=>'3000-4000元'),
	array('price'=>'4000-5000','house_price'=>'4000-5000元'),
	array('price'=>'5000-0','house_price'=>'5000元以上'),
);
foreach ($rent_price_option as $key=>$item){
	$where = " and status =1 and company_id=".$id;
	$temp = explode('-',$item['price']);
	if($temp[0]){
		$where .= ' and house_price>='.$temp[0];
	}
	if($temp[1]){
		$where .= ' and house_price<='.$temp[1];
	}
	$house_num = $houseRent->getCount(1,$where);
	if($house_num){
		$item['house_num'] = $house_num;
		$rentCountPrice[$key] =$item;
	}
}
$page->tpl->assign('rentCountPrice', $rentCountPrice);

//租房按房间统计
$house_room_option = array(
	1=>"一室",
	2=>"二室",
	3=>"三室",
	4=>"四室",
	5=>"五室",
	6=>"其他"	
);

$rentCountRoom = $houseRent->getCountGroupBy('house_room',' status =1 and company_id = '.$id,0);
foreach($rentCountRoom as $key=>$item){
	if($house_room_option[$item['house_room']]){
		$rentCountRoom[$key]['room'] = $item['house_room'];
		$rentCountRoom[$key]['house_room'] = $house_room_option[$item['house_room']];
	}else{
		unset($rentCountRoom[$key]);
	}
}
$page->tpl->assign('rentCountRoom', $rentCountRoom);

//租房列表
$where =' and status=1 and company_id = '.$id;

//小区ID
$borough_id = intval($_GET['borough_id']);
if($borough_id){
	$where .= ' and borough_id = '.$borough_id;
}

//price
$price = $_GET['price'];
if($price){
	$tmp = explode('-',$price);
	if($tmp[0]){
		$where .= ' and house_price >= '.intval($tmp[0]);
	}
	if($tmp[1]){
		$where .= ' and house_price <= '.intval($tmp[1]);
	}
}
//room
$room = intval($_GET['room']);
if($room){
	$where .= ' and house_room = '.$room;
}
//q
$q = $_GET['q']=="可输入小区名、路名" ? "":$_GET['q'];
if($q){
	// 小区名、路名或划片学校
	$where_borough ="borough_name like '%".$q."%' or borough_address like '%".$q."%' or elementary_school like '%".$q."%' or middle_school like '%".$q."%'" ;
	$borough = new Borough($query);
	$borough_ids = $borough->getAll('id',$where_borough);
	if($borough_ids){
		$borough_ids = implode(',',$borough_ids);
		$where .=" and ( borough_id in (".$borough_ids.") or borough_name like '%".$q."%' )";
	}
}

switch ($_GET['list_order']){
	case "created desc":
		$list_order = " order by created desc";
		break;
	case "created asc":
		$list_order = " order by created asc";
		break;
	case "house_price asc":
		$list_order = " order by house_price asc";
		break;
	case "house_price desc":
		$list_order = " order by house_price desc";
		break;
	case "house_totalarea asc":
		$list_order = " order by house_totalarea asc";
		break;
	case "house_totalarea desc":
		$list_order = " order by house_totalarea desc";
		break;
	default:
		$list_order = " order by update_order desc";
		break;
}

$list_num = 10;

$house = new HouseRent($query);

require($cfg['path']['lib'] . 'classes/Pages.class.php');
$row_count = $house->getCount(1,$where);
$pages = new Pages($row_count,$list_num);

$pageLimit = $pages->getLimit();
$dataList = $house->getList($pageLimit,'*',1,$where,$list_order);
$member = new Member($query);


foreach ($dataList as $key=> $item){
	//图片数量
	$dataList[$key]['pic_num'] = $house->getImgNum($item['id']);
}

$page->tpl->assign('dataList', $dataList);
$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条

$page->show();
?>