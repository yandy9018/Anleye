<?php
require('path.inc.php');

$page->name = 'sale'; //页面名字,和文件名相同	

$id = intval($_GET['id']);
if(!$id){
	$page->back('参数错误');
}
//店长信息
$company = new Company($query);
$dataInfo = $company->getInfo($id,'*');
if(!$dataInfo){
	$page->back('该店铺不存在');
}
$page->tpl->assign('dataInfo', $dataInfo);
$page->title = $dataInfo['company_name']."的门店 - 二手房 - ".$page->title;

//公司网站样式判断
if($dataInfo['company_style']){
	$page->addCss($dataInfo['company_style']);
}else{
	$page->addCss('shopStyleDefault.css');
}



//实例化和字典
$houseSell = new HouseSell($query);

//二手房按小区统计出售
$saleCountBorough = $houseSell->getCountGroupBy('borough_id',' status =1 and borough_id <>0 and company_id = '.$id);
$borough = new Borough($query);
foreach ($saleCountBorough as $key => $item){
	$saleCountBorough[$key]['borough_name'] = $borough->getInfo($item['borough_id'],'borough_name');
}
$page->tpl->assign('saleCountBorough', $saleCountBorough);

//二手房按价格统计
$sell_price_option = array(
	array('price'=>'0-40','house_price'=>'40万以下'),
	array('price'=>'40-60','house_price'=>'40-60万'),
	array('price'=>'60-80','house_price'=>'60-80万'),
	array('price'=>'80-100','house_price'=>'80-100万'),
	array('price'=>'100-120','house_price'=>'100-120万'),
	array('price'=>'120-150','house_price'=>'120-150万'),
	array('price'=>'150-200','house_price'=>'150-200万'),
	array('price'=>'200-250','house_price'=>'200-250万'),
	array('price'=>'250-300','house_price'=>'250-300万'),
	array('price'=>'300-500','house_price'=>'300-500万'),
	array('price'=>'500-0','house_price'=>'500万以上'),
);
foreach ($sell_price_option as $key=>$item){
	$where = " and status =1 and company_id=".$id;
	$temp = explode('-',$item['price']);
	if($temp[0]){
		$where .= ' and house_price>='.$temp[0];
	}
	if($temp[1]){
		$where .= ' and house_price<='.$temp[1];
	}
	$house_num = $houseSell->getCount(1,$where);
	if($house_num){
		$item['house_num'] = $house_num;
		$saleCountPrice[$key] =$item;
	}
}
$page->tpl->assign('saleCountPrice', $saleCountPrice);

//二手房按房间数统计
$house_room_option = array(
	1=>"一室",
	2=>"二室",
	3=>"三室",
	4=>"四室",
	5=>"五室",
	6=>"其他"	
);

$saleCountRoom = $houseSell->getCountGroupBy('house_room',' status =1 and company_id = '.$id,0);
foreach($saleCountRoom as $key=>$item){
	if($house_room_option[$item['house_room']]){
		$saleCountRoom[$key]['room'] = $item['house_room'];
		$saleCountRoom[$key]['house_room'] = $house_room_option[$item['house_room']];
	}else{
		unset($saleCountRoom[$key]);
	}
}
$page->tpl->assign('saleCountRoom', $saleCountRoom);

//二手房列表
$where =' and status=1 and company_id = '.$id;

//price
$price = $_GET['price'];
if($price){
	$tmp = explode('-',$price);
	if($tmp[0]){
		$where .= ' and house_price >= '.intval($tmp[0]);
	}
	if($tmp[1]){
		$where .= ' and house_price <= '.intval($tmp[1]);
	}
}
//room
$room = intval($_GET['room']);
if($room){
	$where .= ' and house_room = '.$room;
}

//小区ID
$borough_id = intval($_GET['borough_id']);
if($borough_id){
	$where .= ' and borough_id = '.$borough_id;
}

//q
$q = $_GET['q']=="可输入小区名、路名或划片学校" ? "":$_GET['q'];
if($q){
	// 小区名、路名或划片学校
	$where_borough ="borough_name like '%".$q."%' or borough_address like '%".$q."%' or elementary_school like '%".$q."%' or middle_school like '%".$q."%'" ;
	$borough = new Borough($query);
	$borough_ids = $borough->getAll('id',$where_borough);
	if($borough_ids){
		$borough_ids = implode(',',$borough_ids);
		$where .=" and ( borough_id in (".$borough_ids.") or borough_name like '%".$q."%' )";
	}
}

switch ($_GET['list_order']){
	case "avg_price desc":
		$list_order = " order by house_price/house_totalarea desc";
		break;
	case "avg_price asc":
		$list_order = " order by house_price/house_totalarea asc";
		break;
	case "created desc":
		$list_order = " order by created desc";
		break;
	case "created asc":
		$list_order = " order by created asc";
		break;
	case "house_price asc":
		$list_order = " order by house_price asc";
		break;
	case "house_price desc":
		$list_order = " order by house_price desc";
		break;
	case "house_totalarea asc":
		$list_order = " order by house_totalarea asc";
		break;
	case "house_totalarea desc":
		$list_order = " order by house_totalarea desc";
		break;
	default:
		$list_order = " order by update_order desc";
		break;
}


/*print_rr($where);*/
//list_num
$list_num = 10;

$house = new HouseSell($query);

require($cfg['path']['lib'] . 'classes/Pages.class.php');
$row_count = $house->getCount(1,$where);
$pages = new Pages($row_count,$list_num);

$pageLimit = $pages->getLimit();
$dataList = $house->getList($pageLimit,'*',1,$where,$list_order);
$member = new Member($query);

foreach ($dataList as $key=> $item){
	if($item['house_price'] && $item['house_totalarea']){
		$dataList[$key]['avg_price'] = round($item['house_price']*10000/$item['house_totalarea']);
	}else{
		$dataList[$key]['avg_price'] = "未知";
	}
	//图片数量
	$dataList[$key]['pic_num'] = $house->getImgNum($item['id']);
}

$page->tpl->assign('dataList', $dataList);
$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条



$page->show();
?>