<?php
require('path.inc.php');



$page->name = 'profile'; //页面名字,和文件名相同	

$id = intval($_GET['id']);
if(!$id){
	$page->back('参数错误');
}
//店长信息
$company = new Company($query);
$dataInfo = $company->getInfo($id,'*',true);
if(!$dataInfo){
	$page->back('该店铺不存在');
}
$page->tpl->assign('dataInfo', $dataInfo);
$page->title = $dataInfo['company_name']."的网店 - 关于我们 - ".$page->title;

//公司网站样式判断
if($dataInfo['company_style']){
	$page->addCss($dataInfo['company_style']);
}else{
	$page->addCss('shopStyleDefault.css');
}


$page->show();
?>