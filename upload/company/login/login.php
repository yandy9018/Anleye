<?php
 /**
  * 中介公司登录管理
  *
  * @copyright Copyright (c) 2007 - 2012 Yanwee.com
  * @author 阿一 yandy@yanwee.com
  * @package package
  * @version $Id$
  * 2012-12-25
  */
 
require('path.inc.php');

$page->name = 'login';
$page->title =  $page->title.' - 公司登陆';
$page->addJs('jquery-1.2.6.min.js');
$page->addJs('FormValid.js');
$page->addJs('FV_onBlur.js');

$page->tpl->assign('back_to', $_GET['back_to']);

$company = new Company($query);
if ($_POST['action']=='login') {
	
	if (!$errorMsg1) {
		specConvert($_POST, array('username'));
		if ($_POST['username'] && $_POST['passwd']) {
			$result = $company->login($_POST['username'], $_POST['passwd']);
			if ($result===true) {
			
				$user_type = $company->getAuthInfo('type');
		
				//echo "登录成功";
				$page->urlto('../manage/index.php');
			
			}else{
				 $errorMsg1 = $result;
				}
			   
		} else {
			$errorMsg1 = '请输入用户名或密码！';
		}
	}
} elseif ($_GET['action']=='logout') {
	if($_COOKIE['AUTH_COMPANY_STRING']){
		$uid = $company->getAuthInfo('id');
	}
	$company->logout();

	$page->urlto($cfg['url'] . 'company/login/login.php');
}

$page->tpl->assign('errorMsg', $errorMsg);
$page->tpl->assign('errorMsg1', $errorMsg1);
$page->show();
?>