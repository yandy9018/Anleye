<?php
 /**
  * 房源管理页面
  *
  * @copyright Copyright (c) 2007 - 2008 Yanwee.net (www.anleye.com)
  * @author 阿一 yandy@yanwee.com
  * @package package
  * @version $Id$
  */
 
require('path.inc.php');

$houseSell = new HouseSell($query);
$member = new Member($query);
if($page->action == 'onsell'){
	//上架
	$to_url = $_SERVER['HTTP_REFERER'];
	$id = intval($_GET['id']);

	try{
		$houseSell->changeStatus($id,1);
		$page->urlto($to_url);
	}catch (Exception $e){
		$page->back('上架失败');
	}
	exit;
}elseif($page->action == 'delete'){
	//刷新房源
	$ids = $_POST['ids'];
	$to_url = $_POST['to_url'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择刷新条目');
	}else{
		array_walk($ids,'intval');
	}
	try{
		$houseSell->delete($ids);
		$page->urlto($to_url,'删除房源成功');
	}catch (Exception $e){
		$page->back('删除房源失败');
	}

	exit;
}else{
	//列表包括搜索
	$page->name = 'manageSaleRecycle';

	//成交使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	
	$where = ' and company_id = '.$company_id;
	$q = $_GET['q']=='输入房源编号或小区名称'?"":$_GET['q'];
	if($q){
		$borough = new Borough($query);
		$search_bid = $borough->getAll('id',' borough_name like \'%'.$q.'%\'');
		if($search_bid){
			$search_bid = implode(',',$search_bid);
			$where .= " and (borough_name like '%".$q."%' or house_no like '%".$q."%' or borough_id in (".$search_bid."))";
		}else{
			$where .= " and (borough_name like '%".$q."%' or house_no like '%".$q."%')";	
		}
	}
	$page->tpl->assign('q', $q);
	//这里显示状态为2,3（下架，无效）的房源
	$where .=" and (status = 2 or status = 3)";
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($houseSell->getCount(0,$where),10,'pages_g.tpl');
	$pageLimit = $pages->getLimit();
	$dataList = $houseSell->getList($pageLimit,'*',0,$where,' order by created desc ');
	 foreach ($dataList as $key => $value) {
        //echo date("Y-m-d" ,$value['created']);
		$dataList[$key]['brokerInfo'] = $member->getInfo($value['broker_id'],'*',true);  
    }

	$page->tpl->assign('to_url', $_SERVER['REQUEST_URI']);
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel_m(5));//分页条
	
}

$page->show();
?>