<?php
 /**
  * 会员页面
  */
 
require('path.inc.php');

$page->name = 'index';

//成交使用的thickBox加载
$page->addcss("thickbox.css");
$page->addjs("thickbox.js");

$nowHour = date('H');
if($nowHour > 17 || $nowHour < 6){
	$nowTime = "晚上";
}elseif($nowHour > 12){
	$nowTime = "下午";
}else{
	$nowTime = "早上";
}
$page->tpl->assign('nowTime',$nowTime);

$houseSell = new HouseSell($query);
$houseRent = new HouseRent($query);
$where = ' and company_id = '.$company_id;
$where .=" and status = 1 and is_top = 0";
$saleCount = $houseSell->getCount(0,$where);
$page->tpl->assign('saleCount', $saleCount);

$where .=" and is_top = 1 and status = 1";
$saleTopCount = $houseSell->getCount(0,$where);
$page->tpl->assign('saleTopCount', $saleTopCount);


$where .=" and is_top = 1 and status = 1";
$rentTopCount = $houseRent->getCount(0,$where);
$page->tpl->assign('rentTopCount', $rentTopCount);

$where .=" and status = 2";
$saleRecycleCount = $houseSell->getCount(0,$where);
$page->tpl->assign('saleRecycleCount', $saleRecycleCount);


$where .=" and status = 1 and is_top = 0";
$rentCount = $houseRent->getCount(0,$where);
$page->tpl->assign('rentCount', $rentCount);

$where .=" and status = 2";
$rentRecycleCount = $houseRent->getCount(0,$where);
$page->tpl->assign('rentRecycleCount', $rentRecycleCount);

$page->show();
?>