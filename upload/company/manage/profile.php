<?php
/**
 * 中介公司资料修改  阿一 ayi@yanwee.com
 * @package Apps
 * 2012-12-26
 */
 
require('path.inc.php');

if($page->action == 'save'){
	$_POST['id'] = $company_id;
	try{
		$company->save($_POST);
		$page->urlto('profile.php','修改成功');
	}catch (Exception $e){
		$page->back('保存出错了');
		//$page->back($e->getMessage());
	}
}else{
	$page->name = 'profile';
		$page->addJs('FormValid.js');
	$page->addJs('FV_onBlur.js');
	$companyInfo = $company->getInfo($company_id,'*');
	$page->tpl->assign('dataInfo',$companyInfo);

}

$page->show();
?>