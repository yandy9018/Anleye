<?php
/**
 * 公司管理公用文件
 */

require('../../common.inc.php');
$page->dir  = 'company/manage';//目录名
$page->addCss('member.css');
$page->addJs('jquery-1.2.6.min.js');
$page->addJs('map.js');  //加载地图选项

$company = new Company($query);
$company->auth();

$company_id = $company->getAuthInfo('id');
$page->tpl->assign('company_id',$company_id);
$companyInfo = $company->getInfo($company_id,'*');
$page->tpl->assign('companyInfo',$companyInfo);
$page->tpl->assign('msgCount',$newMsgCount);

?>