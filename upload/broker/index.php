<?php
require('path.inc.php');

$page->name = 'index'; //页面名字,和文件名相同	
$page->title = $page->city.'经纪人 - '.$page->titlec;   //网站名称

//区域字典
$cityarea_option = Dd::getArray('cityarea');
$page->tpl->assign('cityarea_option', $cityarea_option);

$specialty_option = Dd::getArray('specialty');
$page->tpl->assign('specialty_option', $specialty_option);

$where =' m.status=0 and m.user_type = 1 ';
//cityarea
$cityarea = intval($_GET['cityarea']);
if($cityarea){
	$where .= ' and b.cityarea_id = '.$cityarea;
}

//specialty
$specialty = intval($_GET['specialty']);
if($specialty){
	$where .= ' and b.specialty = '.$specialty;
}
 $dd = new Dd($query);
$borough_section=$dd->getSonList($cityarea);
$page->tpl->assign('borough_section',$borough_section);
//cityarea2
$cityarea2 = intval($_GET['cityarea2']);
if($cityarea2){
	$where .= ' and cityarea2_id = '.$cityarea2;
	$page->title = $cityarea_option[$cityarea]." - ".$page->title;
}

//company_id
$company = new Company($query);
$company_id = intval($_GET['company_id']);
if($company_id){
	$companyName = $company->getInfo($company_id,'company_name');
	$page->tpl->assign('companyName',$companyName);
	$where .= ' and company_id = '.$company_id;
	$page->title = $companyName."的团队精英 - ".$page->title;
}


//认证
$identity = intval($_GET['identity']);

if($identity==''){
	$where .= " and b.mobile <>'' ";
}
if($identity==1){
	$where .= " and b.idcard <>'' ";
}
if($identity==2){
	$where .= " and b.aptitude <>'' ";
}

//q
$q = $_GET['q']=="可输入经纪人名、门店名，或公司名称关键词" ? "":trim($_GET['q']);
if($q){
	// 可输入经纪人名、门店名，或公司名称关键词
	$where.=" and ( b.outlet like '%".$q."%' or b.realname like '%".$q."%' or b.company like '%".$q."%' )" ;
}
//list_order 排序转换
switch ($_GET['list_order']){
	case "active_rate desc":
		$list_order = " order by m.active_rate desc, m.active_total desc";
		break;
	case "sell_num desc":
		$list_order = " order by m.sell_num+m.rent_num desc";
		break;
	case "scores desc":
		$list_order = " order by m.scores desc";
		break;
	case "add_time desc":
		$list_order = " order by m.add_time desc";
		break;
	default:
		$list_order = " order by m.active_rate desc , m.active_total desc";
		break;
}

/*print_rr($where);*/
//list_num
$list_num = intval($_GET['list_num']);
if(!$list_num){
	$list_num = 10;
}

$member = new Member($query);

require($cfg['path']['lib'] . 'classes/Pages.class.php');
$row_count = $member->getCountBroker($where);
$pages = new Pages($row_count,$list_num);

//page
$pageno = $_GET['pageno']?intval($_GET['pageno']):1;
$pre_page = $pageno>1?$pageno-1:1;
$next_page = $pageno<$pages->pageCount?$pageno+1:$page_count;
$page->tpl->assign('pageno', $pageno);
$page->tpl->assign('row_count', $row_count);
$page->tpl->assign('page_count', $pages->pageCount);
$page->tpl->assign('pre_page', $pages->fileName.'pageno='.$pre_page);
$page->tpl->assign('next_page', $pages->fileName.'pageno='.$next_page);

$pageLimit = $pages->getLimit();
$dataList = $member->getListBroker($pageLimit,'*',$where,$list_order);
//积分配置文件
$integral_array = require_once($cfg['path']['conf'].'integral.cfg.php');
$houseSell = new HouseSell($query);
$houseRent = new HouseRent($query);

foreach ($dataList as $key=> $item){
	//区域
	$dataList[$key]['cityarea_name'] = Dd::getCaption('cityarea',$item['cityarea_id']);
	
	if($item['company_id']){
		$dataList[$key]['company_name'] = $company->getInfo($item['company_id'],'company_name');
		}
	//积分图片
	$where_house = " and status=1 and broker_id = ".$item['mid'];
	$dataList[$key]['brokerRank'] = getNumByScore($item['scores'],$integral_array,'pic');
	$dataList[$key]['active_str'] = explode('|',$item['active_str']);
	$dataList[$key]['last_login'] = time2Units(time()-$item['last_login']);
	if($item['rent_num'] >= 2 && $item['sell_num'] >= 2){
		//都是够数
		$sellList = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>1),'*',3,$where_house,' order by created desc');
		$rentList = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>1),'*',3,$where_house,' order by created desc');
	}elseif($item['rent_num'] < 2 && $item['sell_num'] > 2){
		//租房不够
		$getSellNum = 2-$item['rent_num']+1;
		$sellList = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>$getSellNum),'*',3,$where_house,' order by created desc');
		$rentList = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>1),'*',3,$where_house,' order by created desc');
		for ($i = 2 ;$i<=count($sellList); $i++){
			$sellList[$i]['is_sell']=1;
			$rentList[] = $sellList[$i];
			unset($sellList[$i]);
		}
	}elseif($item['rent_num'] > 2 && $item['sell_num'] < 2){
		//售房不够
		$getRentNum = 2-$item['sell_num']+1;
		$sellList = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>1),'*',3,$where_house,' order by created desc');
		$rentList = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>$getRentNum),'*',3,$where_house,' order by created desc');
		for ($i = 2 ;$i<=count($rentList); $i++){
			$rentList[$i]['is_rent']=1;
			$sellList[] = $rentList[$i];
			unset($rentList[$i]);
		}
	}else{
		//都不够
		$sellList = $houseSell->getList(array('rowFrom'=>0,'rowTo'=>1),'*',3,$where_house,' order by created desc');
		$rentList = $houseRent->getList(array('rowFrom'=>0,'rowTo'=>1),'*',3,$where_house,' order by created desc');
	}
	foreach ($sellList as $skey=>$sitem){
		$sellList[$skey]['house_title'] = substrs($sitem['house_title'],46 );
	}
	foreach ($rentList as $rkey=>$ritem){
		$rentList[$rkey]['house_title'] = substrs($ritem['house_title'],46 );
	}
	$dataList[$key]['sell_list'] = $sellList;
	$dataList[$key]['rent_list'] = $rentList;
}

$page->tpl->assign('dataList', $dataList);
$page->tpl->assign('pagePanel', $pages->showCtrlPanel_g('5'));//分页条
$arrRecommendBroker=$member->getListBroker(array('rowFrom'=>0,'rowTo'=>10),'*',' m.status=0 and m.user_type = 1',' order by m.sell_num+m.rent_num desc');
foreach ($arrRecommendBroker as $key=> $item){
	//积分图片
	$arrRecommendBroker[$key]['brokerRank'] = getNumByScore($item['scores'],$integral_array,'pic');
	$arrRecommendBroker[$key]['active_str'] = explode('|',$item['active_str']);
}
$page->tpl->assign('arrRecommendBroker', $arrRecommendBroker);
$page->tpl->assign('menu', 'broker');
$page->show();
?>