<?php

require('path.inc.php');

$member_id = $member->getAuthInfo('id');
$houseRent = new HouseRent($query);
$to_url = $_POST['to_url'];


if($page->action == 'bargain'){
	//ajax 提交 房源成交

}elseif($page->action == 'notSell'){
	//下架
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择下架条目');
	}else{
		array_walk($ids,'intval');
	}
	try{
		$houseRent->changeStatus($ids,2);
		//更新下架时间
		$houseRent->update($ids,'house_downtime',$cfg['time']);
		//取消推荐
		$houseRent->update($ids,'is_promote',0);
		$page->urlto($to_url,'下架房源成功');
	}catch (Exception $e){
		$page->back('下架失败');
		//$page->back($e->getMessage());
	}
	exit;
}elseif($page->action == 'refresh'){
	//刷新房源
	$ids = $_POST['ids'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择刷新条目');
	}else{
		array_walk($ids,'intval');
	}
	 foreach ($ids as $id){
		$houseInfo = $houseRent->getInfo($id);
	    if($houseInfo['refresh'] == 0){
			$page->back('请不要选择可刷新次数为0的房源');
			}
	    }
	try{
		$houseRent->updateRefresh($ids);
		$houseRent->refresh($ids);
		$page->urlto($to_url,'刷新房源成功');
	}catch (Exception $e){
		$page->back('刷新房源失败');
		//$page->back($e->getMessage());
	}

	exit;
}else{
	//列表包括搜索
	$page->name = 'manageRent';

	//计算剩余多少房源
	$where = ' and broker_id = '.$member_id;
	$where .=" and status = 1 ";
	$houseNum = $houseRent->getCount(0,$where);
	$page->tpl->assign('houseNum',$houseNum);//总共多少条
	$integral_array = require($cfg['path']['conf'].'integral.cfg.php');
	$scores = $member->getAuthInfo('scores');
	$allowNum = getNumByScore($scores,$integral_array,'sell_num')+$member->getAuthInfo('addrent');
	if($member->getAuthInfo('vip')==1){
		$allowNum = $page->vip1RentNum;
		}
	if($member->getAuthInfo('vip')==2){
		$allowNum = $page->vip2RentNum;
		}
	$memberVip = $member->getAuthInfo('vip');
	$page->tpl->assign('memberVip',$memberVip);

	$houseLeft= $allowNum -$houseNum;
	$page->tpl->assign('houseLeft',$houseLeft);

	//成交使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");

	$where = ' and broker_id = '.$member_id;
	$q = $_GET['q']=='输入房源编号或小区名称'?"":trim($_GET['q']);
	if($q){
		$borough = new Borough($query);
		$search_bid = $borough->getAll('id',' borough_name like \'%'.$q.'%\'');
		if($search_bid){
			$search_bid = implode(',',$search_bid);
			$where .= " and (borough_name like '%".$q."%' or house_no like '%".$q."%' or borough_id in (".$search_bid."))";
		}else{
			$where .= " and (borough_name like '%".$q."%' or house_no like '%".$q."%')";
		}
	}
	$page->tpl->assign('q', $q);
	//这里显示状态为1（正在）的房源
	$today = date("Y-m-d",$cfg['time']);
	$yestoday =  date("Y-m-d",strtotime("-1 day"));
	$where .=" and status = 1 and is_top = 0";
	require($cfg['path']['lib'] . 'classes/Pages.class.php');

	$pages = new Pages($houseRent->getCount(0,$where),10,'pages_g.tpl');
	$pageLimit = $pages->getLimit();
	$dataList = $houseRent->getList($pageLimit,'*',0,$where,' order by created desc ');
	foreach ($dataList as $key => $value){
		//echo date("Y-m-d" ,$value['created']);
		$dataList[$key]['yestoday_click'] = intval($houseRent->getClick($value['id'],$yestoday));
		$dataList[$key]['today_click'] = intval($houseRent->getClick($value['id'],$today));
                /**
                 * @author 房产
                 * @example 判断是否预约过 二零一二年二月九日 22:39:10
                 */
                $dataList[$key]['is_appo'] = $query->getValue("SELECT appo_list_id FROM fke_appolist WHERE house_id = '{$value['id']}' AND appo_site='rent'")?true:false;
	}

	$page->tpl->assign('to_url', $_SERVER['REQUEST_URI']);
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel_m(5));//分页条

}

$page->show();
?>