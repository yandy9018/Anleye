<?php
require('path.inc.php');


$member_id = $member->getAuthInfo('id');
$page->tpl->assign('member_id',$member_id);

$shop = new Shop($query);

if($page->action == 'save'){
	$_POST['broker_id'] = $member_id;
	try{
		$shop->saveShopStyle($_POST);
		$page->urlto('shopStyle.php','保存成功');
	}catch (Exception $e){
		$page->back('保存出错');
	}
	
}else{
	$page->name = 'shopStyle';
	$shopConf = $shop->getShopConf($member_id);
	$page->tpl->assign('dataInfo',$shopConf);
}

$page->show();
?>