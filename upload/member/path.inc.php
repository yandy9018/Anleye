<?php
/**
 * 用户中心
 */

require('../common.inc.php');
$page->dir  = 'member';//目录名
$page->addCss('member.css');
$page->addJs('jquery-1.2.6.min.js');
$page->addJs('map.js');  //加载地图选项

$member = new Member($query);
$member->auth();
$realname = $_COOKIE['AUTH_MEMBER_REALNAME'] ? $_COOKIE['AUTH_MEMBER_REALNAME'] :$_COOKIE['AUTH_MEMBER_NAME'];
$page->tpl->assign('username',$realname);
$member_id = $member->getAuthInfo('id');
$page->tpl->assign('member_id',$member_id);
$newMsgCount = 0;
if($_COOKIE['AUTH_MEMBER_NAME']){
	$innernote = new Innernote($query);
	$newMsgCount = $innernote->getCount(' to_del=0 and is_new = 1 and msg_to = \''.$_COOKIE['AUTH_MEMBER_NAME'].'\'');
}
//检测VIP用户过期执行函数

$page->tpl->assign('msgCount',$newMsgCount);
//2011.08.26
session_start();
$_SESSION["file_info"] = array();
$page->tpl->assign('session_id', session_id());	
//
?>