<?php
require('path.inc.php');

$member_id = $member->getAuthInfo('id');

$avatar = new Avatar($query);
if($page->action == 'save'){
	try{
		$_POST['broker_id'] = $member_id;
		$avatar->save($_POST);
		$page->urlto('brokerPhoto.php','申请已提交，请等待审核通过');
	}catch (Exception $e){
		$page->back("申请保存失败，请重试");
	}
	exit;
}else{
	$page->name = 'brokerPhoto';
	$page->addJs('FormValid.js');

	$memberInfo = $member->getInfo($member_id,'*',true);
	$page->tpl->assign('memberInfo',$memberInfo);
	//取最后一次提交的审核头像
	$lastAvatar =$avatar->getMyLastItem($member_id);
	$page->tpl->assign('lastAvatar',$lastAvatar);
}

$page->show();
?>