<?php
 /**
  * 房源管理页面
  *
  * @copyright Copyright (c) 2007 - 2008 Yanwee.net (www.anleye.com)
  * @author 阿一 yandy@yanwee.com
  * @package package
  * @version $Id$
  */
 
require('path.inc.php');

$member_id = $member->getAuthInfo('id');
$houseRent = new HouseRent($query);
if($page->action == 'onsell'){
	//上架
	$to_url = $_SERVER['HTTP_REFERER'];
	$id = intval($_GET['id']);
	try{
		$houseRent->changeStatus($id,1);
		$page->urlto($to_url);
	}catch (Exception $e){
		$page->back('上架失败');
	}
	exit;
}elseif($page->action == 'delete'){
	//刷新房源
	$ids = $_POST['ids'];
	$to_url = $_POST['to_url'];
	if(!is_array($ids) || empty($ids)){
		$page->back('没有选择刷新条目');
	}else{
		array_walk($ids,'intval');
	}
	try{
		$houseRent->delete($ids);
		$page->urlto($to_url,'删除房源成功');
	}catch (Exception $e){
		$page->back('删除房源失败');
	}

	exit;
}else{
	//列表包括搜索
	$page->name = 'manageRentRecycle';

	//成交使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	
	$where = ' and broker_id = '.$member_id;
	$q = $_GET['q']=='输入房源编号或小区名称'?"":$_GET['q'];
	if($q){
		$borough = new Borough($query);
		$search_bid = $borough->getAll('id',' borough_name like \'%'.$q.'%\'');
		if($search_bid){
			$search_bid = implode(',',$search_bid);
			$where .= " and (borough_name like '%".$q."%' or house_no like '%".$q."%' or borough_id in (".$search_bid."))";
		}else{
			$where .= " and (borough_name like '%".$q."%' or house_no like '%".$q."%')";	
		}
	}
	$page->tpl->assign('q', $q);
	//这里显示状态为2,3（下架，无效）的房源
	if($_GET['status']){
		$where .=" and status = ".intval($_GET['status']);
	}else{
		$where .=" and (status = 2 or status = 3)";
	}
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($houseRent->getCount(0,$where),10,'pages_g.tpl');
	$pageLimit = $pages->getLimit();
	$dataList = $houseRent->getList($pageLimit,'*',0,$where,' order by created desc ');
	
	$page->tpl->assign('to_url', $_SERVER['REQUEST_URI']);
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel_m(5));//分页条
	
}

$page->show();
?>