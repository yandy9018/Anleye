<?php
require('path.inc.php');

$member_id = $member->getAuthInfo('id');


if($page->action == 'save'){
	try{
		$_POST['broker_id'] = $member_id;
		if($member->checkPwd($_POST['password'],$member_id)){
			$updateInfo = array(
				'passwd'=>md5($_POST['newpass'])
			);
			if($page->uc==1){
			//同步修改uc用户密码
			require($cfg['path']['root'].'uc_client/client.php');
			$username = $member->getAuthInfo('username');
			uc_user_edit($username,'',$_POST['newpass'],'',1);
			}
			$member->updateInfo($member_id,$updateInfo);
		}else{
			$page->back('旧密码不正确');
		}
		$page->urlto('pwdEdit.php','修改密码成功');
	}catch (Exception $e){
		$page->back("修改密码失败，请重试");
	}
	exit;
}else{
	$page->name = 'pwdEdit';
	$page->addJs('FormValid.js');
	$page->addJs('FV_onBlur.js');
	$memberInfo = $member->getInfo($member_id,'*',true);
	$page->tpl->assign('memberInfo',$memberInfo);
}

$page->show();
?>