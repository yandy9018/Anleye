<?php
require('path.inc.php');


$housesell = new HouseSell($query);
$saleBargain = new SaleBargain($query);

if($page->action == 'save'){
	//保存在ajax页面
}elseif($page->action == 'remark'){
	$page->name = 'saleBargainRemark';
	if(!$_GET['id']){
		exit;
	}
	$id = intval($_GET['id']);
	$dataInfo = $saleBargain->getInfo($id);
	$page->tpl->assign('dataInfo', $dataInfo);
}else{
	$page->name = 'saleBargain';
	if(!$_GET['id']){
		//增加成交
		$house_id = intval($_GET['house_id']);
		if($house_id){
			$house = new HouseSell($query);
			$dataInfo = $house->getInfo($house_id,'borough_name,house_totalarea,house_price');
		}
	}else{
		//编辑成交
		$id = intval($_GET['id']);
		$dataInfo = $saleBargain->getInfo($id);
		$house_id = $dataInfo['house_id'];
	}
	$page->tpl->assign('house_id', $house_id);
	$page->tpl->assign('dataInfo', $dataInfo);
	//字典
	$bargain_from_option = Dd::getArray('bargain_from');
	$page->tpl->assign('bargain_from_option', $bargain_from_option);
}

$page->show();
?>