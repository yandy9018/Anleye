<?php
 /**
  * 房源管理页面
  *
  * @copyright Copyright (c) 2007 - 2008 Yanwee.net (www.anleye.com)
  * @author 阿一 yandy@yanwee.com
  * @package package
  * @version $Id$
  */
 
require('path.inc.php');

$member_id = $member->getAuthInfo('id');
$saleBargain = new SaleBargain($query);

$to_url = $POST['to_url'];

if($page->action == 'bargain'){
	//ajax 提交 房源成交
	
}else{
	//列表包括搜索
	$page->name = 'manageSaleDone';

	//成交使用的thickBox加载
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	
	//字典
	$bargain_from_option = Dd::getArray('bargain_from');
	$page->tpl->assign('bargain_from_option', $bargain_from_option);
	
	$where = ' broker_id = '.$member_id;
	
	//搜索
	if($_GET['from_date']){
		$from_date = MyDate::transform('timestamp',$_GET['from_date']);
		$where .= ' and bargain_time >= '.$from_date;
	}
	if($_GET['to_date']){
		$to_date = MyDate::transform('timestamp',$_GET['to_date']);
		$where .= ' and bargain_time <= '.$to_date;
	}
	if($_GET['bargain_from']){
		$where .= ' and bargain_from = '.intval($_GET['bargain_from']);
	}
	
	$q = $_GET['q'] =="输入小区名，或买/卖方信息，或备注信息"?"":$_GET['q'];
	if($q){
		$where .= " and (borough_name like '%".$q
			."%' or buyer like '%".$q
			."%' or buyer_tel like '%".$q
			."%' or saler like '%".$q
			."%' or saler_tel like '%".$q
			."%' or remark like '%".$q."%')";
	}
	$page->tpl->assign('q', $q);
	
	require($cfg['path']['lib'] . 'classes/Pages.class.php');
	$pages = new Pages($saleBargain->getCount($where),10,'pages_g.tpl');
	$pageLimit = $pages->getLimit();
	$dataList = $saleBargain->getList($pageLimit,'*',$where,' order by add_time desc ');
	foreach($dataList as $key => $item){
		$dataList[$key]['bargain_from']=$bargain_from_option[$item['bargain_from']];
	}
	
	$page->tpl->assign('to_url', $_SERVER['REQUEST_URI']);
	$page->tpl->assign('dataList', $dataList);
	$page->tpl->assign('pagePanel', $pages->showCtrlPanel_m(5));//分页条
	
}

$page->show();
?>