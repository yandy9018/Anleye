<?php
/*
	*功能：设置商品有关信息
	*版本：2.0
	*日期：2008-08-01
	*修改日期：2010-03-11
	'说明：
	'以下代码只是方便商户测试，提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
	'该代码仅供学习和研究支付宝接口使用，只是提供一个参考。

*/
require('path.inc.php');

require_once("alipay_service.php");
require_once("alipay_config.php");
$page->name = 'index1';

///////以下参数是需要通过下单时的订单数据传入进来获得//////////
$out_trade_no = date(Ymdhms);				//请与网站订单系统中的唯一订单号匹配
$username = $member->getAuthInfo('username');
$subject = '为用户（'.$username.'）充值';
$body = "网站充值";
$total_fee = $_POST["jiage"];
////////////////////////////////////////////////////////////

$parameter = array(
	"service"         => "create_direct_pay_by_user",  //交易类型
	"partner"         => $partner,          //合作商户号
	"return_url"      => $return_url,       //同步返回
	"notify_url"      => $notify_url,       //异步返回
	"_input_charset"  => $_input_charset,   //字符集，默认为GBK
	"subject"         => $subject,  	    //商品名称，必填
	"body"            => $body,     	    //商品描述，必填
	"out_trade_no"    => $out_trade_no,     //商品外部交易号，必填（保证唯一性）
	"total_fee"       => $total_fee,        //商品单价，必填（价格不能为0）
	"payment_type"    => "1",               //默认为1,不需要修改

	"show_url"        => $show_url,         //商品相关网站
	"seller_email"    => $seller_email      //卖家邮箱，必填
);
$alipay = new alipay_service($parameter,$security_code,$sign_type);

//POST方式传递，得到加密结果字符串
$sign = $alipay->Get_Sign();

//若改成GET方式传递，请取消下面的两行注释
$link=$alipay->create_url();
$page->tpl->assign('link',$link);
echo "<script>window.location =\"$link\";</script>"; 
$page->tpl->assign('parameter',$parameter);
$page->show();
?>
