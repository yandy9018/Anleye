<?php
require('path.inc.php');


$member_id = $member->getAuthInfo('id');

if($page->action == 'save'){
	$_POST['id'] = $member_id;
	try{
		$member->saveBroker($_POST);
		$page->urlto('brokerProfile.php','修改成功');
	}catch (Exception $e){
		$page->back('保存出错了');
	}
}else{
	$page->name = 'brokerProfile';
	//判断用户是不是通过认证等
	$page->addJs('FormValid.js');
	$page->addJs('FV_onBlur.js');
	
	//增加小区的thickBox
	$page->addcss("thickbox.css");
	$page->addjs("thickbox.js");
	//autocomplete
	$page->addjs($cfg['path']['js']."Autocompleter/lib/jquery.bgiframe.min.js");
	$page->addjs($cfg['path']['js']."Autocompleter/lib/ajaxQueue.js");
	$page->addcss($cfg['path']['js']."Autocompleter/jquery.autocomplete.css");
	$page->addjs($cfg['path']['js']."Autocompleter/jquery.autocomplete.js");
	//时间控件
	$page->addJs($cfg['path']['js'].'My97DatePicker/WdatePicker.js');
	
	//区域，增加小区使用
	$cityarea_option = Dd::getArray('cityarea');
	$page->tpl->assign('cityarea_option', $cityarea_option);
	
 	//经纪人特长
	$specialty_option = Dd::getArray('specialty');
	$page->tpl->assign('specialty_option', $specialty_option);

	$memberInfo = $member->getInfo($member_id,'*',true);
	
	if($memberInfo['company_id']){
		$company = new Company($query);
		$memberInfo['company_name'] = $company->getInfo($memberInfo['company_id'],'company_name');
	}
	$cityarea_option2 = Dd::getArray('cityarea2');
	$memberInfo['cityarea2_name']=$cityarea_option2[$memberInfo['cityarea2_id']];	
	if($memberInfo['borough_id']){
		$borough = new Borough($query);
		$memberInfo['borough_name'] = $borough->getInfo($memberInfo['borough_id'],'borough_name');
	}
	$page->tpl->assign('dataInfo',$memberInfo);

}

$page->show();
?>